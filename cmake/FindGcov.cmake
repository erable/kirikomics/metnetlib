#
# - Try to find gcov
#
# Once done this will define
#
#   GCOV_FOUND - system has gcov
#   GCOV_PROGRAM, the gcov executable.
#
find_program(GCOV_PROGRAM NAMES gcov
        PATH /usr/bin /usr/local/bin
        ${HOME}/.local/bin ${HOME}/Library/Python/2.7/bin)

include(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(gcov DEFAULT_MSG
        GCOV_PROGRAM)
