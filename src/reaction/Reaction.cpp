/*Copyright (C) <2016-2017>  David Parsons, Martin Wannagat

This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU Affero General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/
//
// Created by Martin Wannagat on 07.11.17.
//

#include "reaction/Reaction.h"

#include <string>

namespace metnetlib {

/// Constructor
Reaction::Reaction(
    ReactionIdType id,
    std::shared_ptr<MetNetElementContainer<Compound_t*>> substrates,
    std::shared_ptr<MetNetElementContainer<Compound_t*>> products,
    const SpecificAttributes& specificAttributes) :
    DirectedHyperedgeBase<Compound<Reaction>>(id, substrates, products),
    specificAttributes_(specificAttributes) {
}

/// Copy constructor
Reaction::Reaction(const Reaction& reaction) :
    DirectedHyperedgeBase<Compound<Reaction>>(reaction),
    specificAttributes_(reaction.specificAttributes_) {
}


/// Get the Sbml Id
const std::string& Reaction::
sbmlId() const {
  return specificAttributes_.sbmlId_;
}

/// Is the reaction reversible?
bool Reaction::
isReversible() const {
  return specificAttributes_.reversible_;
}

/// Is the reaction fast?
bool Reaction::
isFast() const {
  return specificAttributes_.fast_;
}

/// Get substrate stoichiometry of given substrate
double Reaction::
substrateStoichiometry(const Compound_t* compound) const {
  auto it = specificAttributes_.substrateStoichiometry_.cbegin();
  for (const auto& cmp : *(this->tails_)) {
    if (cmp == compound) {
      return (*it).first;
    }
    ++it;
  }
  std::string errorMsg = "Provided compound " + compound->sbmlId()
                         + " is not a substrate of the reaction " + sbmlId();
  throw std::invalid_argument(errorMsg);
}

/// Get product stoichiometry of given product
double Reaction::
productStoichiometry(const Compound_t* compound) const {
  auto it = specificAttributes_.productStoichiometry_.cbegin();
  for (const auto& cmp : *(this->heads_)) {
    if (cmp == compound) {
      return (*it).first;
    }
    ++it;
  }
  std::string errorMsg = "Provided compound " + compound->sbmlId()
                         + " is not a product of the reaction " + sbmlId();
  throw std::invalid_argument(errorMsg);
}


/**
 * Provides default arguments to create an irreversible reaction
 *
 * Returns the arguments to add to a metabolic network an irreversible
 * reaction with the given name, substrate and product names. The reaction is
 * not fast, and has constant stoichiometric values of 1.0 for each
 * substrate/product.
 *
 * @param reactionName Name of the reaction
 * @param substrateNames Vector of substrate names
 * @param productNames Vector of product names
 * @return ArgumentsToCreateReaction<Reaction> object that can be used to add
 * a reaction to a MetabolicNetwork.
 */
ArgumentsToCreateReaction<Reaction> Reaction::
argumentsToCreateDefaultIrreversibleReaction(
    const std::string& reactionName,
    const std::vector<std::string>& substrateNames,
    const std::vector<std::string>& productNames) {
    Stoichiometry substrateStoichiometry(substrateNames.size(),
                                         std::pair<double, bool>(1.0, true));
    Stoichiometry productStoichiometry(productNames.size(),
                                       std::pair<double, bool>(1.0, true));

    // build return object
    auto specificAttributes = SpecificAttributes(reactionName, false, false,
                                                 substrateStoichiometry,
                                                 productStoichiometry, 0,
                                                 defaultUpperBound);
    return ArgumentsToCreateReaction<Reaction>(
        std::make_pair(substrateNames, productNames), specificAttributes);
}

/// Get lower bound of the reaction
double Reaction::
lowerBound() const {
  return specificAttributes_.lowerBound_;
}

/// Get upper bound of the reaction
double Reaction::
upperBound() const {
  return specificAttributes_.upperBound_;
}

/// Get reverse reaction
const Reaction* Reaction::
reversibleReaction() const {
  return specificAttributes_.reversibleReaction;
}

/// Print the reaction
void Reaction::
print(std::ostream& out) const {
  // In case that the reaction is reversible we print only the forward reaction
  if (isBackwardReaction()) {
    return;
  }
  // Print Sbml Id
  out << sbmlId() << ": ";
  // print substrates
  const char *padding = "";
  for (const auto& tail : *tails_) {
    out << padding << substrateStoichiometry(tail) << ' ' << *tail;
    padding = " + ";
  }
  out << (specificAttributes_.reversible_ ? " <-> " : " -> ");
  // print products
  padding = "";
  for (const auto& head : *heads_) {
    out << padding << productStoichiometry(head) << ' ' << *head;
    padding = " + ";
  }
  // Add newline
  out << '\n';
}

/// Get arguments to create this Reaction
hglib::ArgumentsToCreateDirectedHyperedge<Reaction> Reaction::
argumentsToCreateHyperarc() const {
  std::vector<CompoundIdType> substrates, products;
  // Add substrate ids and their stoichiometry entry
  for (const auto substrate : *(tails_.get())) {
    substrates.push_back(substrate->id());
  }
  // Add product ids and their stoichiometry entry
  for (const auto product : *(heads_.get())) {
    products.push_back(product->id());
  }
  // build return object
  return hglib::ArgumentsToCreateDirectedHyperedge<Reaction>(
      std::make_pair(substrates, products), specificAttributes_);
}

/// Get arguments to create the reverse sense of this reaction
hglib::ArgumentsToCreateDirectedHyperedge<Reaction> Reaction::
argumentsToCreateReverseReaction() const {
  std::vector<CompoundIdType> substrates, products;
  Stoichiometry substrateStoichiometry, productStoichiometry;
  // Add substrate ids and their stoichiometry entry as products
  auto it = specificAttributes_.substrateStoichiometry_.begin();
  for (const auto substrate : *(tails_.get())) {
    products.push_back(substrate->id());
    productStoichiometry.push_back(*it);
    ++it;
  }
  // Add product ids and their stoichiometry entry as substrates
  it = specificAttributes_.productStoichiometry_.begin();
  for (const auto product : *(heads_.get())) {
    substrates.push_back(product->id());
    substrateStoichiometry.push_back(*it);
    ++it;
  }
  // build return object for reverse reaction
  std::string revSbmlId = specificAttributes_.sbmlId_ + suffixReverseReaction;
  auto reversibleReactionAttributes = SpecificAttributes(
      revSbmlId, specificAttributes_.reversible_, specificAttributes_.fast_,
      substrateStoichiometry, productStoichiometry,
      specificAttributes_.lowerBound_, specificAttributes_.upperBound_);
  return hglib::ArgumentsToCreateDirectedHyperedge<Reaction>(
      std::make_pair(substrates, products), reversibleReactionAttributes);
}

/// Set reversible reaction
void Reaction::
setReversibleReaction(Reaction* reversibleReaction) {
  specificAttributes_.reversibleReaction = reversibleReaction;
}

/// Set the reaction irreversible
void Reaction::
setIrreversible() {
  specificAttributes_.reversible_ = false;
  specificAttributes_.reversibleReaction = nullptr;
}

/// Check if this reaction (if reversible) is the backward reaction
bool Reaction::
isBackwardReaction() const {
  // It is a forward reaction if ireversible
  if (not isReversible()) {
    return false;
  }
  // Check if sblId ends with suffix of reverse reaction ("_REV")
  if (sbmlId().length() >= suffixReverseReactionLength) {
    return (0 == sbmlId().compare(
        sbmlId().length() - suffixReverseReactionLength,
        suffixReverseReactionLength, suffixReverseReaction));
  } else {
    return false;
  }
}
}  // namespace metnetlib
