/*Copyright (C) <2016-2017>  David Parsons, Martin Wannagat

This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU Affero General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/
//
// Created by Martin Wannagat on 07.11.17.
//

#include "compartment/Compartment.h"

namespace metnetlib {

/// Constructor
Compartment::
Compartment(const CompartmentIdType& id, const std::string& sbmlId,
            const double& size, bool constant) :
    id_(id), sbmlId_(sbmlId), size_(size), constant_(constant) {
}

/// Set size if the compartment is not constant
void Compartment::
setSize(const double& newSize) {
  if (not constant_) {
    size_ = newSize;
  }
}

/// Set Id of the compartment
void Compartment::
setId(const CompartmentIdType& newId) {
  id_ = newId;
}
}  // namespace metnetlib
