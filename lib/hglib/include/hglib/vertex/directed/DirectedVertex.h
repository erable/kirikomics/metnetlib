/*Copyright (C) <2016-2017>  David Parsons, Martin Wannagat

This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU Affero General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/
//
// Created by Martin Wannagat on 19.06.17.
//

#ifndef HGLIB_DIRECTEDVERTEX_H
#define HGLIB_DIRECTEDVERTEX_H

#include <memory>
#include <vector>

#include "hglib/vertex/VertexBase.h"
#include "hglib/utils/IsParentOf.h"
#include "hglib/utils/types.h"
#include "hglib/hyperedge/directed/DirectedHyperedgeBase.h"

namespace hglib {

template <typename DirectedVertex_t>
class DirectedHyperedgeBase;

/**
 * Class of a Directed vertex
 * @tparam Hyperarc_t
 */
template <typename Hyperarc_t>
class DirectedVertex : public VertexBase {
  template <template <typename> class Vertex_t, typename Hyperedge_t,
          typename VertexProperty, typename EdgeProperty,
          typename HypergraphProperty>
  friend class Hypergraph;
  template <template <typename> class Vertex_t, typename Hyperedge_t,
          typename VertexProperty, typename EdgeProperty,
          typename HypergraphProperty>
  friend class DirectedHypergraph;
  template <template <typename> class Vertex_t, typename Hyperedge_t,
          typename VertexProperty, typename EdgeProperty,
          typename HypergraphProperty>
  friend class DirectedSubHypergraph;

 public:
  // Assert that Hyperarc_t is derived from DirectedHyperedgeBase
  static_assert(is_parent_of<DirectedHyperedgeBase, Hyperarc_t>::value,
                "Template parameter of DirectedVertex must be a class derived "
                        "from DirectedHyperedgeBase");
  /// Alias to make the template parameter publicily available
  using HyperarcType = Hyperarc_t;

 public:
  /**
   * SpecificAttributes class
   *
   * A basic DirectedVertex has no specific arguments, but the class is needed to
   * call the addVertex function of a (Sub)Hypergraph.
   */
  class SpecificAttributes {};

 protected:
  /// DirectedVertex constructor
  DirectedVertex(VertexIdType id, std::string name,
                 const SpecificAttributes& specificAttributes);
  /// DirectedVertex constructor
  DirectedVertex(VertexIdType id,
                 const SpecificAttributes& specificAttributes);
  /// DirectedVertex copy constructor
  DirectedVertex(const DirectedVertex& vertex);
  /// Destructor
  ~DirectedVertex() {}
  /// Add an outgoing hyperarc
  void addOutHyperarc(Hyperarc_t* hyperarc);
  /// Add an incoming hyperarc
  void addInHyperarc(Hyperarc_t* hyperarc);
  /// Remove a hyperarc from the outgoing hyperarcs.
  void removeOutHyperarc(const Hyperarc_t& hyperarcToBeDeleted);
  /// Remove a hyperarc from the incoming hyperarcs.
  void removeInHyperarc(const Hyperarc_t& hyperarcToBeDeleted);
  /// Get arguments needed to create this vertex
  ArgumentsToCreateVertex<
          DirectedVertex<Hyperarc_t>> argumentsToCreateVertex() const;

 protected:
  /// Incoming hyperarcs
  std::shared_ptr<GraphElementContainer<Hyperarc_t*>> inHyperarcs_;
  /// Outgoing hyperarcs
  std::shared_ptr<GraphElementContainer<Hyperarc_t*>> outHyperarcs_;
  /// Specific attributes (none)
  SpecificAttributes specificAttributes_;
};
}  // namespace hglib

#include "DirectedVertex.hpp"
#endif  // HGLIB_DIRECTEDVERTEX_H
