/*Copyright (C) <2016-2017>  David Parsons, Martin Wannagat

This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU Affero General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/
//
// Created by Martin Wannagat on 21.06.17.
//

#ifndef HGLIB_UNDIRECTEDVERTEX_H
#define HGLIB_UNDIRECTEDVERTEX_H

#include <memory>
#include <vector>

#include "hglib/vertex/VertexBase.h"
#include "hglib/vertex/ArgumentsToCreateVertex.h"
#include "hglib/hyperedge/undirected/UndirectedHyperedgeBase.h"
#include "hglib/utils/IsParentOf.h"
#include "hglib/utils/types.h"

namespace hglib {

template <typename UndirectedVertex_t>
class UndirectedHyperedgeBase;

/**
 * Class UndirectedVertex
 *
 * @tparam Hyperedge_t
 */
template <typename Hyperedge_t>
class UndirectedVertex : public VertexBase {
  template <template <typename> class Vertex_t, typename Edge_t,
          typename VertexProperty, typename EdgeProperty,
          typename HypergraphProperty>
  friend class Hypergraph;
  template <template <typename> class Vertex_t, typename Edge_t,
          typename VertexProperty, typename EdgeProperty,
          typename HypergraphProperty>
  friend class UndirectedHypergraph;
  template <template <typename> class Vertex_t, typename Edge_t,
          typename VertexProperty, typename EdgeProperty,
          typename HypergraphProperty>
  friend class UndirectedSubHypergraph;

 public:
  // Assert that Hyperedge_t is derived from UndirectedHyperedgeBase
  static_assert(is_parent_of<UndirectedHyperedgeBase, Hyperedge_t>::value,
                "Template parameter of UndirectedVertex must be a class "
                        "derived from UndirectedHyperedgeBase");
  /// Alias to make the template parameter publicily available
  using HyperedgeType = Hyperedge_t;

 public:
  /**
   * SpecificAttributes class
   *
   * A basic UndirectedVertex has no specific arguments, but the class is needed to
   * call the addVertex function of a (Sub)Hypergraph.
   */
  class SpecificAttributes {};

 protected:
  /// UndirectedVertex constructor
  UndirectedVertex(VertexIdType id, std::string name,
                   const SpecificAttributes& specificAttributes);
  /// UndirectedVertex constructor
  UndirectedVertex(VertexIdType id,
                   const SpecificAttributes& specificAttributes);
  /// UndirectedVertex copy constructor
  UndirectedVertex(const UndirectedVertex& vertex);
  /// Destructor
  ~UndirectedVertex() {}
  /// Add a hyperedge
  void addHyperedge(Hyperedge_t* hyperedge);
  /// Remove a hyperedge
  void removeHyperedge(const Hyperedge_t& hyperedgeToBeDeleted);
  /// Get arguments needed to create this vertex
  ArgumentsToCreateVertex<
          UndirectedVertex<Hyperedge_t>> argumentsToCreateVertex() const;

 protected:
  /// Hyperedges that contain the undirected vertex
  std::shared_ptr<GraphElementContainer<Hyperedge_t*>> hyperedges_;
  /// Specific attributes (none)
  SpecificAttributes specificAttributes_;
};
}  // namespace hglib

#include "UndirectedVertex.hpp"
#endif  // HGLIB_UNDIRECTEDVERTEX_H
