/*Copyright (C) <2016-2017>  David Parsons, Martin Wannagat

This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU Affero General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/
//
// Created by Martin Wannagat on 04.10.17.
//

#ifndef HGLIB_UNDIRECTEDSUBHYPERGRAPH_H
#define HGLIB_UNDIRECTEDSUBHYPERGRAPH_H

#include "UndirectedHypergraphInterface.h"
#include "hglib/utils/observation/Observer.h"
#include "hglib/hyperedge/undirected/UndirectedHyperedge.h"
#include "hglib/hyperedge/undirected/ArgumentsToCreateUndirectedHyperedge.h"
#include "hglib/utils/types.h"
#include "hglib/vertex/undirected/UndirectedVertex.h"

namespace hglib {
template<template<typename> class VertexTemplate_t = UndirectedVertex,
        typename Hyperedge_t = UndirectedHyperedge,
        typename VertexProperty = emptyProperty,
        typename HyperedgeProperty = emptyProperty,
        typename HypergraphProperty = emptyProperty>
class UndirectedSubHypergraph : public UndirectedHypergraphInterface<
        VertexTemplate_t, Hyperedge_t, VertexProperty, HyperedgeProperty,
        HypergraphProperty>,
                                public Observer {
  template<template<typename> class DirectedVertex_t, typename Arc_t,
          typename VertexProp, typename EdgeProp,
          typename HypergraphProp>
  friend class UndirectedHypergraph;

 public:
  /// Alias for the directed vertex type
  using Vertex_t = typename UndirectedHypergraphInterface<VertexTemplate_t,
          Hyperedge_t, VertexProperty, HyperedgeProperty,
          HypergraphProperty>::Vertex_t;
  /// Alias for Hyperedge type
  using Hyperedge_type = typename UndirectedHypergraphInterface<
          VertexTemplate_t, Hyperedge_t, VertexProperty, HyperedgeProperty,
          HypergraphProperty>::Hyperedge_type;
  /// Alias for Hyperedge property type
  using HyperedgeProperty_t = typename UndirectedHypergraphInterface<
          VertexTemplate_t, Hyperedge_t, VertexProperty, HyperedgeProperty,
          HypergraphProperty>::HyperedgeProperty_t;
  /// Alias for conditional range of a container of Vertex_t ptr
  using VertexContainer = typename UndirectedHypergraphInterface<
          VertexTemplate_t, Hyperedge_t, VertexProperty, HyperedgeProperty,
          HypergraphProperty>::VertexContainer;
  /// Alias for conditional range of a container of Hyperedge_t ptr
  using HyperedgeContainer = typename UndirectedHypergraphInterface<
          VertexTemplate_t, Hyperedge_t, VertexProperty, HyperedgeProperty,
          HypergraphProperty>::HyperedgeContainer;
  /// Alias for iterator over a container of vertices
  using vertex_iterator = typename UndirectedHypergraphInterface<
          VertexTemplate_t, Hyperedge_t, VertexProperty, HyperedgeProperty,
          HypergraphProperty>::vertex_iterator;
  /// Alias for iterator over a container of hyperedges
  using hyperedge_iterator = typename UndirectedHypergraphInterface<
          VertexTemplate_t, Hyperedge_t, VertexProperty, HyperedgeProperty,
          HypergraphProperty>::hyperedge_iterator;
  /// \brief Alias for the data container that stores the names of vertices
  /// of a hyperedge
  using VertexNames = typename UndirectedHypergraphInterface<VertexTemplate_t,
          Hyperedge_t, VertexProperty, HyperedgeProperty,
          HypergraphProperty>::VertexNames;
  /// \brief Alias for the data container that stores the ids of vertices
  /// of a hyperedge
  using VertexIds = typename UndirectedHypergraphInterface<VertexTemplate_t,
          Hyperedge_t, VertexProperty, HyperedgeProperty,
          HypergraphProperty>::VertexIds;
  /// Alias for unique_ptr of GraphElementContainer<Vertex_t*>
  using VertexContainerPtr = typename UndirectedHypergraphInterface<
          VertexTemplate_t, Hyperedge_t, VertexProperty, HyperedgeProperty,
          HypergraphProperty>::VertexContainerPtr;
  /// Alias for unique_ptr of GraphElementContainer<VertexProperty*>
  using VertexPropertyContainerPtr = typename UndirectedHypergraphInterface<
          VertexTemplate_t, Hyperedge_t, VertexProperty, HyperedgeProperty,
          HypergraphProperty>::VertexPropertyContainerPtr;
  /// Alias for unique_ptr of GraphElementContainer<Hyperedge_t*>
  using HyperedgeContainerPtr = typename UndirectedHypergraphInterface<
          VertexTemplate_t, Hyperedge_t, VertexProperty, HyperedgeProperty,
          HypergraphProperty>::HyperedgeContainerPtr;
  /// Alias for unique_ptr of GraphElementContainer<HyperedgeProperty*>
  using HyperedgePropertyContainerPtr = typename UndirectedHypergraphInterface<
          VertexTemplate_t, Hyperedge_t, VertexProperty, HyperedgeProperty,
          HypergraphProperty>::HyperedgePropertyContainerPtr;


  /* **************************************************************************
   * **************************************************************************
   *                    Constructor/Destructor
   * **************************************************************************
   * *************************************************************************/
 public:
  /// Constructor
  UndirectedSubHypergraph(UndirectedHypergraphInterface<VertexTemplate_t,
          Hyperedge_t, VertexProperty, HyperedgeProperty,
          HypergraphProperty>* parent,
                          const std::unordered_set<VertexIdType>&
                          whitelistedVertexIds,
                          const std::unordered_set<HyperedgeIdType>&
                          whitelistedHyperedgeIds);

  /// Copy constructor
  UndirectedSubHypergraph(const UndirectedSubHypergraph& other);
  /// Destructor
  virtual ~UndirectedSubHypergraph();
  /// Assignment operator
  UndirectedSubHypergraph& operator=(const UndirectedSubHypergraph& source);

  /* **************************************************************************
   * **************************************************************************
   *                    General
   * **************************************************************************
   * *************************************************************************/
  /// Check if multi-hyperedges are allowed in this hypergraph
  bool allowMultiHyperedges() const override;

  /* **************************************************************************
   * **************************************************************************
   *                        Hyperedge access
   * **************************************************************************
   * *************************************************************************/
 public:
  /// Number of hyperedges in the hypergraph
  size_t nbHyperedges() const override;
  /// Highest assigned hyperedge id of the hypergraph
  size_t hyperedgeContainerSize() const override;

  /**
    * \brief Add a hyperedge with the provided vertices, and additional
    * parameters (if needed to create a hyperedge of type Hyperedge_t) to the
    * hypergraph
    */
  const Hyperedge_t* addHyperedge(
          decltype(hglib::NAME),
          const VertexNames& vertices,
          const typename Hyperedge_t::SpecificAttributes& attributes = {})
    override;

  /**
  * \brief Add a hyperedge with the provided vertex ids, and additional
  * parameters (if needed to create a hyperedge of type Hyperedge_t) to the
  * hypergraph
  */
  const Hyperedge_t* addHyperedge(
          const VertexIds& vertices,
          const typename Hyperedge_t::SpecificAttributes& attributes = {})
    override;

  /// Get arguments to create the hyperedge
  ArgumentsToCreateUndirectedHyperedge<Hyperedge_t> argumentsToCreateHyperedge(
          const hglib::HyperedgeIdType& hyperedgeId) const override;

  /// Remove a hyperedge with the given id from the hypergraph.
  void removeHyperedge(const HyperedgeIdType& hyperedgeId) override;

  /// Search hyperedge list for an hyperedge with the provided id
  const Hyperedge_t* hyperedgeById(
          const HyperedgeIdType & hyperedgeId) const override;

  /// Const hyperedge iterator pointing to begin of the hyperedges
  hyperedge_iterator hyperedgesBegin() const override;
  /// Const hyperedge iterator pointing to end of the hyperedges
  hyperedge_iterator hyperedgesEnd() const override;
  /// \brief Pair of const hyperedge iterator pointing to begin and end
  /// of the hyperedges
  std::pair<hyperedge_iterator,
          hyperedge_iterator> hyperedgesBeginEnd() const override;
  /// Return a reference to the container of hyperedges.
  const HyperedgeContainer& hyperedges() const override;

  // Vertices of a given hyperedge
  /// Return number of vertices in the hyperedge with given id.
  size_t nbImpliedVertices(const HyperedgeIdType& hyperedgeId) const override;
  /// Check if hyperedge with given id has vertices.
  bool hasVertices(const HyperedgeIdType& hyperedgeId) const override;
  /// \brief Const vertex iterator pointing to the begin of the vertices
  /// of the hyperedge with the given id
  vertex_iterator impliedVerticesBegin(
          const HyperedgeIdType& hyperedgeId) const override;
  /// \brief Const vertex iterator pointing to the end of the vertices of
  /// the hyperedge with the given id
  vertex_iterator impliedVerticesEnd(
          const HyperedgeIdType& hyperedgeId) const override;
  /// Const vertex iterator pointing to begin and end of the vertices of
  /// the hyperedge with the given id
  std::pair<vertex_iterator, vertex_iterator> impliedVerticesBeginEnd(
          const HyperedgeIdType& hyperedgeId) const override;
  /// Get container of vertices of given hyperedge id
  std::shared_ptr<const GraphElementContainer<Vertex_t*>> impliedVertices(
          const HyperedgeIdType& hyperedgeId) const override;

  /* **************************************************************************
   * **************************************************************************
   *                       Vertex access
   * **************************************************************************
   * *************************************************************************/
 public:
  /// Highest assigned vertex id of the hypergraph
  size_t vertexContainerSize() const override;
  /// Number of vertices in the hypergraph
  size_t nbVertices() const override;
  /// Search vertex list for a vertex with the provided name
  const Vertex_t* vertexByName(const std::string& vertexName) const override;
  /// Search vertex list for a vertex with the provided id
  const Vertex_t* vertexById(const VertexIdType & vertexId) const override;
  /// Const vertex iterator pointing to begin of vertices
  vertex_iterator verticesBegin() const override;
  /// Const vertex iterator pointing to end of vertices
  vertex_iterator verticesEnd() const override;
  /// Const vertex iterator pointing to begin and end of vertices
  std::pair<vertex_iterator, vertex_iterator> verticesBeginEnd() const override;

  /// Return a reference to the container of vertices.
  const VertexContainer& vertices() const override;

  /// Add a vertex
  const Vertex_t* addVertex(
          const typename Vertex_t::SpecificAttributes& attributes = {})
    override;
  /// Add a vertex with a given name
  const Vertex_t* addVertex(
          const std::string& name,
          const typename Vertex_t::SpecificAttributes& attributes = {})
    override;

  /// Get arguments to create the vertex
  ArgumentsToCreateVertex<Vertex_t> argumentsToCreateVertex(
          const hglib::VertexIdType& vertexId) const override;
  // Hyperedges that contain a given vertex
  /// Number of hyperedges that contain the vertex with given id
  size_t nbHyperedges(const VertexIdType& vertexId) const override;
  /// Check if the vertex is part of at least one hyperedge
  bool isContainedInAnyHyperedge(const VertexIdType& vertexId) const override;
  /// \brief Const hyperedge iterator pointing to the begin of the
  /// hyperedges that contain the vertex with the given id
  hyperedge_iterator hyperedgesBegin(
          const VertexIdType& vertexId) const override;
  /// \brief Const hyperedge iterator pointing to the end of the
  /// hyperedges that contain the vertex with the given id
  hyperedge_iterator hyperedgesEnd(
          const VertexIdType& vertexId) const override;
  /// \brief Const hyperedge iterator pointing to begin and end of the
  /// hyperedges that contain the vertex with the given id
  std::pair<hyperedge_iterator, hyperedge_iterator> hyperedgesBeginEnd(
          const VertexIdType& vertexId) const override;
  /// Get container of hyperedges that contain the vertex with the given id
  std::shared_ptr<const GraphElementContainer<Hyperedge_t*>> hyperedges(
          const VertexIdType& vertexId) const override;

  /// Remove a vertex with given name from the hypergraph.
  void removeVertex(const std::string& vertexName,
                    bool removeImpliedHyperedges = true) override;
  /// Remove a vertex with given id from the hypergraph.
  void removeVertex(const VertexIdType& vertexId,
                    bool removeImpliedHyperedges = true) override;

  /* **************************************************************************
   * **************************************************************************
   *                       Vertex/Hyperedge dependent access
   * **************************************************************************
   * *************************************************************************/
 public:
  /// Check if given vertex is a vertex of given hyperedge.
  bool isVertexOfHyperedge(
          const VertexIdType& vertexId,
          const HyperedgeIdType& hyperedgeId) const override;

  /* **************************************************************************
   * **************************************************************************
   *                        HyperedgeProperty access
   * **************************************************************************
   * *************************************************************************/
 public:
  /// Get properties of the hyperedge with the provided id.
  const HyperedgeProperty* getHyperedgeProperties(
          const HyperedgeIdType& hyperedgeId) const override;
  /// Get properties of the hyperedge with the provided id.
  HyperedgeProperty* getHyperedgeProperties_(
          const HyperedgeIdType& hyperedgeId) const override;

  /* **************************************************************************
   * **************************************************************************
   *                    Vertex property access methods
   * **************************************************************************
   * *************************************************************************/
 public:
  /// Get properties of the vertex with the provided id.
  const VertexProperty* getVertexProperties(
          const VertexIdType& vertexId) const override;
  /// Get properties of the vertex with the provided id.
  VertexProperty* getVertexProperties_(
          const VertexIdType& vertexId) const override;

  /* **************************************************************************
   * **************************************************************************
   *               Hypergraph property access methods
   * **************************************************************************
   * *************************************************************************/
  /// Get properties of the hypergraph.
  const HypergraphProperty* getHypergraphProperties() const override;
  /// Get properties of the hypergraph.
  HypergraphProperty* getHypergraphProperties_() const override;

  /* **************************************************************************
   * **************************************************************************
   *                  Hypergraph hierarchy/Observer pattern
   * **************************************************************************
   * *************************************************************************/
  /// Get root hypergraph
  const UndirectedHypergraphInterface<VertexTemplate_t, Hyperedge_t,
          VertexProperty, HyperedgeProperty,
          HypergraphProperty>* getRootHypergraph() const override;

 protected:
  /// Observer update method
  void Update(const Observable& o, ObservableEvent e, void* arg) override;

  /* **************************************************************************
   * **************************************************************************
   *                        Protected methods
   * **************************************************************************
   * *************************************************************************/
 protected:
  /** \brief If the dtor of the root directed hypergraph was called we
   * invalidate all parents of its child hierarchy
   */
  void setParentOfChildsToNull();
  /**\brief Throw a exception if one tries to use a member function on a
   * subgraph whose parent was set to nullptr previously
   */
  void throwExceptionOnNullptrParent() const;
  /**\brief Adapt the content of container whitelistedVerticesList_ in
   * response to the call resetVertexIds in the root hypergraph
   */
  void updateVertexContainerUponResetVertexIds();
  /**\brief Adapt the content of container whitelistedHyperedgesList_ in
   * response to the call resetHyperedgeIds in the root hypergraph
   */
  void updateHyperedgeContainerUponResetHyperedgeIds();
  /// Return pointer to vertices container
  VertexContainerPtr getRootVerticesContainerPtr() const override;
  /// Return pointer to hyperarc container
  HyperedgeContainerPtr getRootHyperedgeContainerPtr() const override;

 protected:
  /// Parent hypergraph
  UndirectedHypergraphInterface<VertexTemplate_t, Hyperedge_t, VertexProperty,
  HyperedgeProperty, HypergraphProperty>* parent_;
  /// Container of whitelisted vertices
  VertexContainerPtr whitelistedVerticesList_;
  /// Container of whitelisted hyperarcs
  HyperedgeContainerPtr whitelistedHyperedgesList_;
  /// SubHypergraph property
  HypergraphProperty* subHypergraphProperty_;
};

/**
 * Factory method to create an undirected sub-hypergraph.
 *
 * @tparam UndirectedHypergraph_t
 * @param g
 * @param whitelistedVertexIds
 * @param whitelistedHyperarcIds
 * @return unique_ptr to directed sub-hypergraph of type
 * DirectedSubHypergraph<DirectedHypergraph_t>
 */
template <template <typename> class VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename HyperedgeProperty,
        typename HypergraphProperty>
std::unique_ptr<UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t,
        VertexProperty, HyperedgeProperty, HypergraphProperty>>
createUndirectedSubHypergraph(UndirectedHypergraphInterface<VertexTemplate_t,
        Hyperedge_t, VertexProperty, HyperedgeProperty,
        HypergraphProperty>* parent,
                            const std::unordered_set<VertexIdType>&
                            whitelistedVertexIds,
                            const std::unordered_set<HyperedgeIdType>&
                            whitelistedHyperedgeIds) {
  return std::make_unique<UndirectedSubHypergraph<VertexTemplate_t,
          Hyperedge_t, VertexProperty, HyperedgeProperty,
          HypergraphProperty>>(parent, whitelistedVertexIds,
                               whitelistedHyperedgeIds);
}


/**
 * Factory method to copy a given undirected sub-hypergraph
 *
 * @tparam UndirectedHypergraph_t
 * @param g
 * @return unique_ptr to directed sub-hypergraph of type
 * DirectedSubHypergraph<DirectedHypergraph_t>
 */
template <template <typename> class VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename HyperedgeProperty,
        typename HypergraphProperty>
std::unique_ptr<UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t,
        VertexProperty, HyperedgeProperty, HypergraphProperty>>
copyUndirectedSubHypergraph(
        const UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t,
                VertexProperty, HyperedgeProperty, HypergraphProperty>& g) {
  return std::make_unique<UndirectedSubHypergraph<VertexTemplate_t,
          Hyperedge_t, VertexProperty, HyperedgeProperty,
          HypergraphProperty>>(g);
}
}  // namespace hglib
#include "UndirectedSubHypergraph.hpp"
#endif  // HGLIB_UNDIRECTEDSUBHYPERGRAPH_H
