/*Copyright (C) <2016-2017>  David Parsons, Martin Wannagat

This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU Affero General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/
//
// Created by Martin Wannagat on 03.10.17.
//

#ifndef HGLIB_UNDIRECTEDHYPERGRAPHINTERFACE_H
#define HGLIB_UNDIRECTEDHYPERGRAPHINTERFACE_H

#include <string>
#include <vector>
#include <cstddef>

#include "hglib/hypergraph/HypergraphInterface.h"
#include "hglib/hyperedge/undirected/ArgumentsToCreateUndirectedHyperedge.h"
#include "hglib/utils/types.h"

namespace hglib {

/**
 * Interface of an undirected hypergraph.
 *
 * @tparam VertexTemplate_t Vertex type
 * @tparam Hyperedge_type Hyperedge type
 * @tparam VertexProperty Vertex property type
 * @tparam HyperedgeProperty Hyperedge property type
 * @tparam HypergraphProperty Hypergraph property type
 */
template <template <typename> class VertexTemplate_t,
        typename Hyperedge_t,
        typename VertexProperty,
        typename HyperedgeProperty,
        typename HypergraphProperty>
class UndirectedHypergraphInterface : virtual public HypergraphInterface<
        VertexTemplate_t, Hyperedge_t, VertexProperty, HyperedgeProperty,
        HypergraphProperty> {
 public:
  /// Alias for the directed vertex type
  using Vertex_t = typename HypergraphInterface<VertexTemplate_t, Hyperedge_t,
          VertexProperty, HyperedgeProperty, HypergraphProperty>::Vertex_t;
  /// Alias for Hyperedge type
  using Hyperedge_type = Hyperedge_t;
  /// Alias for Hyperedge property type
  using HyperedgeProperty_t = HyperedgeProperty;
  /// Alias for conditional range of a container of Vertex_t ptr
  using VertexContainer = typename HypergraphInterface<
          VertexTemplate_t, Hyperedge_t, VertexProperty, HyperedgeProperty,
          HypergraphProperty>::VertexContainer;
  /// Alias for conditional range of a container of Hyperedge_t ptr
  using HyperedgeContainer = GraphElementContainer<Hyperedge_t*>;
  /// Alias for iterator over a container of vertices
  using vertex_iterator = typename HypergraphInterface<VertexTemplate_t,
          Hyperedge_t, VertexProperty, HyperedgeProperty,
          HypergraphProperty>::vertex_iterator;
  /// Alias for iterator over a container of hyperedges
  using hyperedge_iterator = typename GraphElementContainer<
          Hyperedge_t*>::const_iterator;
  /// \brief Alias for the data container that stores the names of vertices
  /// of a hyperedge
  typedef std::vector<std::string> VertexNames;
  /// \brief Alias for the data container that stores the ids of vertices
  /// of a hyperedge
  typedef std::vector<VertexIdType> VertexIds;
  /// Alias for unique_ptr of GraphElementContainer<Vertex_t*>
  using VertexContainerPtr = typename HypergraphInterface<VertexTemplate_t,
          Hyperedge_t, VertexProperty, HyperedgeProperty,
          HypergraphProperty>::VertexContainerPtr;
  /// Alias for unique_ptr of GraphElementContainer<VertexProperty*>
  using VertexPropertyContainerPtr = typename HypergraphInterface<
          VertexTemplate_t, Hyperedge_t, VertexProperty, HyperedgeProperty,
          HypergraphProperty>::VertexPropertyContainerPtr;
  /// Alias for unique_ptr of GraphElementContainer<Hyperedge_t*>
  using HyperedgeContainerPtr =
      std::shared_ptr<GraphElementContainer<Hyperedge_t*>>;
  /// Alias for unique_ptr of GraphElementContainer<HyperedgeProperty*>
  using HyperedgePropertyContainerPtr =
      std::shared_ptr<GraphElementContainer<HyperedgeProperty*>>;

  /* **************************************************************************
   * **************************************************************************
   *                    Destructor
   * **************************************************************************
   * *************************************************************************/
 public:
  /// Destructor
  virtual ~UndirectedHypergraphInterface() = default;

  /* **************************************************************************
   * **************************************************************************
   *                    General
   * **************************************************************************
   * *************************************************************************/
  /// Check if multi-hyperedges are allowed in this hypergraph
  virtual bool allowMultiHyperedges() const = 0;

  /* **************************************************************************
  * **************************************************************************
  *                        Hyperedge access
  * **************************************************************************
  * *************************************************************************/
 public:
  /// Number of hyperedges in the hypergraph
  virtual size_t nbHyperedges() const = 0;
  /// Highest assigned hyperedge id of the hypergraph
  virtual size_t hyperedgeContainerSize() const = 0;

  /**
    * \brief Add a hyperedge with the provided vertices, and additional
    * parameters (if needed to create a hyperedge of type Hyperedge_t) to the
    * hypergraph
    */
  virtual const Hyperedge_t* addHyperedge(
          decltype(hglib::NAME),
          const VertexNames& vertices,
          const typename Hyperedge_t::SpecificAttributes& attributes = {}) = 0;

  /**
  * \brief Add a hyperedge with the provided vertex ids, and additional
  * parameters (if needed to create a hyperedge of type Hyperedge_t) to the
  * hypergraph
  */
  virtual const Hyperedge_t* addHyperedge(
          const VertexIds& vertices,
          const typename Hyperedge_t::SpecificAttributes& attributes = {}) = 0;

  /// Get arguments to create the hyperedge
  virtual ArgumentsToCreateUndirectedHyperedge<Hyperedge_t>
  argumentsToCreateHyperedge(
          const hglib::HyperedgeIdType& hyperedgeId) const = 0;

  /// Remove a hyperedge with the given id from the hypergraph.
  virtual void removeHyperedge(const HyperedgeIdType& hyperedgeId) = 0;

  /// Search hyperedge list for an hyperedge with the provided id
  virtual const Hyperedge_t* hyperedgeById(
          const HyperedgeIdType & hyperedgeId) const = 0;

  /// Const hyperedge iterator pointing to begin of the hyperedges
  virtual hyperedge_iterator hyperedgesBegin() const = 0;
  /// Const hyperedge iterator pointing to end of the hyperedges
  virtual hyperedge_iterator hyperedgesEnd() const = 0;
  /// \brief Pair of const hyperedge iterator pointing to begin and end
  /// of the hyperedges
  virtual std::pair<hyperedge_iterator,
  hyperedge_iterator> hyperedgesBeginEnd() const = 0;
  /// Return a reference to the container of hyperedges.
  virtual const HyperedgeContainer& hyperedges() const = 0;

  // Vertices of a given hyperedge
  /// Return number of vertices in the hyperedge with given id.
  virtual size_t nbImpliedVertices(
          const HyperedgeIdType& hyperedgeId) const = 0;
  /// Check if hyperedge with given id has vertices.
  virtual bool hasVertices(const HyperedgeIdType& hyperedgeId) const = 0;
  /// \brief Const vertex iterator pointing to the begin of the vertices
  /// of the hyperedge with the given id
  virtual vertex_iterator impliedVerticesBegin(
          const HyperedgeIdType& hyperedgeId) const = 0;
  /// \brief Const vertex iterator pointing to the end of the vertices of
  /// the hyperedge with the given id
  virtual vertex_iterator impliedVerticesEnd(
          const HyperedgeIdType& hyperedgeId) const = 0;
  /// Const vertex iterator pointing to begin and end of the vertices of
  /// the hyperedge with the given id
  virtual std::pair<vertex_iterator, vertex_iterator> impliedVerticesBeginEnd(
          const HyperedgeIdType& hyperedgeId) const = 0;
  /// Get container of vertices of given hyperedge id
  virtual std::shared_ptr<
    const GraphElementContainer<Vertex_t*>> impliedVertices(
      const HyperedgeIdType& hyperedgeId) const = 0;

  /* **************************************************************************
   * **************************************************************************
   *                       Vertex access
   * **************************************************************************
   * *************************************************************************/
  // Hyperedges that contain a given vertex
  /// Number of hyperedges that contain the vertex with given id
  virtual size_t nbHyperedges(const VertexIdType& vertexId) const = 0;
  /// Check if the vertex is part of at least one hyperedge
  virtual bool isContainedInAnyHyperedge(
          const VertexIdType& vertexId) const = 0;
  /// \brief Const hyperedge iterator pointing to the begin of the
  /// hyperedges that contain the vertex with the given id
  virtual hyperedge_iterator hyperedgesBegin(
          const VertexIdType& vertexId) const = 0;
  /// \brief Const hyperedge iterator pointing to the end of the
  /// hyperedges that contain the vertex with the given id
  virtual hyperedge_iterator hyperedgesEnd(
          const VertexIdType& vertexId) const = 0;
  /// \brief Const hyperedge iterator pointing to begin and end of the
  /// hyperedges that contain the vertex with the given id
  virtual std::pair<hyperedge_iterator, hyperedge_iterator> hyperedgesBeginEnd(
          const VertexIdType& vertexId) const = 0;
  /// Get container of hyperedges that contain the vertex with the given id
  virtual std::shared_ptr<const GraphElementContainer<Hyperedge_t*>>
    hyperedges(const VertexIdType& vertexId) const = 0;

  /// Remove a vertex with given name from the hypergraph.
  virtual void removeVertex(const std::string& vertexName,
                            bool removeImpliedHyperedges = true) = 0;
  /// Remove a vertex with given id from the hypergraph.
  virtual void removeVertex(const VertexIdType& vertexId,
                            bool removeImpliedHyperedges = true) = 0;

  /* **************************************************************************
   * **************************************************************************
   *                       Vertex/Hyperedge dependent access
   * **************************************************************************
   * *************************************************************************/
  /// Check if given vertex is a vertex of given hyperedge.
  virtual bool isVertexOfHyperedge(
          const VertexIdType& vertexId,
          const HyperedgeIdType& hyperedgeId) const = 0;

  /* **************************************************************************
  * **************************************************************************
  *                        HyperedgeProperty access
  * **************************************************************************
  * *************************************************************************/
  /// Get properties of the hyperedge with the provided id.
  virtual const HyperedgeProperty* getHyperedgeProperties(
          const HyperedgeIdType& hyperedgeId) const = 0;
  /// Get properties of the hyperedge with the provided id.
  virtual HyperedgeProperty* getHyperedgeProperties_(
          const HyperedgeIdType& hyperedgeId) const = 0;

  /* **************************************************************************
   * **************************************************************************
   *                  Hypergraph hierarchy/Observer pattern
   * **************************************************************************
   * *************************************************************************/
  /// Get root hypergraph
  virtual const UndirectedHypergraphInterface* getRootHypergraph() const = 0;
  /// Return pointer to hyperedge container
  virtual HyperedgeContainerPtr getRootHyperedgeContainerPtr() const = 0;
};




/* **************************************************************************
 * **************************************************************************
 *               Non-member functions
 * **************************************************************************
 * *************************************************************************/

/**
 * Add pointers to const hyperedge objects to container.
 *
 * An insert iterator of a container is provided as a parameter
 * to this function. After checking if the value_type of the
 * container is const Hyperedge_type*, and that the container has an
 * insert method, the hyperedges of the hypergraph are added to the
 * container via the insert iterator.
 *
 * Example calls:
 \verbatim
 hglib::UnDirectedHypergraph<> g;
 for (auto vertexName : vertices) {
   g.addVertex(vertexName);
 }
 for (auto edge : edges) {
    g.addHyperedge(edge);
 }

 std::vector<const Hyperedge*> hyperedgeContainer;
 hglib::hyperedges(g, std::back_inserter(hyperedgeContainer));

 std::set<const Hyperedge*> s;
 hglib::hyperedges(g, std::inserter(s, s.begin()));
 \endverbatim
 *
 * The following is not supported because a std::array does
 * not have an insert method:
 \verbatim
 std::array<const Hyperedge*, 5> a;
 hglib::hyperedges(g, std::inserter(a, a.end()));
 \endverbatim
 *
 * Complexity: Linear
 *
 * @param inserter insert iterator of a container
 */
template <typename HGraph, typename InsertIter>
void hyperedges(const HGraph& hypergraph, InsertIter inserter) {
  // desired value_type of the container
  typedef typename HGraph::Hyperedge_type T;
  // assert that the value_type of the container of the insert iterator
  // is const Hyperedge_t*
  static_assert(std::is_same<typename InsertIter::container_type::value_type,
                        const T*>::value,
                "Wrong Type: The value_type of the container must be "
                        "e.g. set<const Hyperedge*>");
  // assert that the container C has a insert function that returns a
  // C::iterator
  static_assert(has_insert_member_function<typename InsertIter::container_type,
                        const T*>::value,
                "Provide a container with the 'insert' member function as in "
                        "std::vector. For example std::array is not"
                        " supported.");
  // insert pointers to vertices that are not nullptr
  for (const auto& hyperedge : hypergraph.hyperedges()) {
    inserter = hyperedge;
  }
}


/**
 * Add to the container the pointers to those const hyperedge objects
 * for which the lambda function evaluates to true
 *
 * An insert iterator of a container is provided as a second parameter
 * to this function. After checking if the value_type of the
 * container is const Hyperedge_type*, and that the container has an
 * insert method, the hyperedges, for which the lambda function (third
 * parameter) evaluates to true, are added to the container via the insert
 * iterator. The lambda function takes the index of the hyperedge (type
 * HyperedgeIdType) as parameter.
 *
 * The method can be used to recover e.g. all hyperedges with a specific
 * property:
 *
 * Example call:
 \verbatim
 enum Colour {
     red, blue, green, black
 };
 struct VertexProperty {
     string name = "Default";
     Colour colour = Colour::green;
 };
 struct EdgeProperty {
     string name = "Default";
     Colour colour = Colour::green;
 };

 hglib::UnDirectedHypergraph<DirectedVertex, Hyperedge, VertexProperty,
          EdgeProperty> g;
 for (auto vertexName : vertices) {
   g.addVertex(vertexName);
 }
  for (auto edge : edges) {
    g.addHyperedge(edge);
 }

 // Get all (green) hyperedges
 std::vector<const Hyperedge*> hyperedgeContainer;
 hglib::hyperedges(g, std::back_inserter(hyperedgeContainer),
                [&] (hglib::HyperedgeIdType hyperedgeId) -> bool {
                    return g.getHyperedgeProperties(hyperedgeId)->colour ==
                           Colour::green;});
 // ... change colour of some hyperedges
 // Get hyperedges by colour
 for ( int idx = red; idx != black; idx++ ) {
   hyperedgeContainer.clear();  // empty container
   // Get hyperedge of specified colour
   Colour colour = static_cast<Colour>(idx);
   hglib::hyperedges(g, std::back_inserter(hyperedgeContainer),
                  [&] (hglib::HyperedgeIdType hyperedgeId) -> bool {
                       return g.getHyperedgeProperties(hyperedgeId)->colour ==
                              colour;});
 }
 \endverbatim
 *
 * Complexity: O(n * O(func)). Complexity depends on the complexity of the
 * lambda function which is evaluated for each hyperedge.
 *
 * @param inserter insert iterator of a container
 * @param func lambda function that takes the index of a hyperedge (type
 * HyperedgeIdType) as parameter.
 */
template <typename HGraph, typename InsertIter, typename Lambda>
void hyperedges(const HGraph& hypergraph, InsertIter inserter, Lambda&& func) {
  // desired value_type of the container
  typedef typename HGraph::Hyperedge_type T;
  // assert that the value_type of the container of the insert iterator
  // is const Hyperedge_t*
  static_assert(std::is_same<typename InsertIter::container_type::value_type,
                        const T*>::value,
                "Wrong Type: The value_type of the container must be "
                        "e.g. set<const Hyperedge*>");
  // assert that the container C has a insert function that returns a
  // C::iterator
  static_assert(has_insert_member_function<typename InsertIter::container_type,
                        const T*>::value,
                "Provide a container with the 'insert' member function as "
                        "in std::vector. For example std::array is not "
                        "supported.");
  // insert pointers to hyperedges that fulfill the lambda function
  for (const auto& hyperedge : hypergraph.hyperedges()) {
    if (func(hyperedge->id())) {
      inserter = hyperedge;
    }
  }
}
}  // namespace hglib
#endif  // HGLIB_UNDIRECTEDHYPERGRAPHINTERFACE_H
