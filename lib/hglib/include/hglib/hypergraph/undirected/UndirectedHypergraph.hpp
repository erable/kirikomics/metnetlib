/*Copyright (C) <2016-2017>  David Parsons, Martin Wannagat

This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU Affero General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/
//
// Created by Martin Wannagat on 03.10.17.
//

#include "UndirectedHypergraph.h"
#include "UndirectedSubHypergraph.h"

#include "filtered_vector.h"

namespace hglib {

/**
 * Constructor
 *
 * @param allowMultiHyperedges flag to signal if multi-hyperedges are allowed
 */
template <template <typename> class VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename HyperedgeProperty,
        typename HypergraphProperty>
UndirectedHypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
HyperedgeProperty, HypergraphProperty>::
UndirectedHypergraph(bool allowMultiHyperedges) :
        UndirectedHypergraphInterface<VertexTemplate_t, Hyperedge_t,
                VertexProperty, HyperedgeProperty, HypergraphProperty>(),
        Hypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
                HyperedgeProperty, HypergraphProperty>(allowMultiHyperedges),
        hyperedgeMaxId_(-1) {
  // create hyperedges_
  hyperedges_ = create_filtered_vector<Hyperedge_t*>(
          create_filtered_vector<Hyperedge_t*>(),
          std::make_unique<NullptrFilter<Hyperedge_t*>>());
  // create hyperedgeProperties_
  hyperedgeProperties_ = create_filtered_vector<HyperedgeProperty*>(
          create_filtered_vector<HyperedgeProperty*>(),
          std::make_unique<NullptrFilter<HyperedgeProperty*>>());
}


/**
 * Copy constructor
 *
 * @param other To be copied hypergraph
 */
template <template <typename> class VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename HyperedgeProperty,
        typename HypergraphProperty>
UndirectedHypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        HyperedgeProperty, HypergraphProperty>::
UndirectedHypergraph(
        const UndirectedHypergraph& other) :
        UndirectedHypergraphInterface<VertexTemplate_t, Hyperedge_t,
                VertexProperty, HyperedgeProperty, HypergraphProperty>(),
        Hypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
                HyperedgeProperty, HypergraphProperty>(
                other),
        hyperedgeMaxId_(-1) {
  try {
    // create hyperedges_
    hyperedges_ = create_filtered_vector<Hyperedge_t*>(
            create_filtered_vector<Hyperedge_t*>(),
            std::make_unique<NullptrFilter<Hyperedge_t*>>());
    // create hyperedgeProperties_
    hyperedgeProperties_ = create_filtered_vector<HyperedgeProperty*>(
            create_filtered_vector<HyperedgeProperty*>(),
            std::make_unique<NullptrFilter<HyperedgeProperty*>>());
    // copy edges and associated edge properties
    for (std::size_t i = 0; i < other.hyperedges_->size(); ++i) {
      Hyperedge_t* hyperedge = other.hyperedges_->operator[](i);
      ++hyperedgeMaxId_;
      // hyperedge was deleted in other
      if (hyperedge == nullptr) {
        hyperedges_->push_back(nullptr);
        hyperedgeProperties_->push_back(nullptr);
      } else {
        Hyperedge_t* copy = new Hyperedge_t(*hyperedge);
        // add vertices to hyperedge
        for (const auto& vertex : *(hyperedge->vertices_.get())) {
          auto& v = this->vertices_->operator[](vertex->id());
          copy->addVertex(v);
          v->addHyperedge(copy);
        }
        // add hyperedge to hypergraph
        hyperedges_->push_back(copy);
        // copy hyperedge property
        HyperedgeProperty* hyperedgePropertyCopy = new HyperedgeProperty(
                *other.hyperedgeProperties_->operator[](i));
        hyperedgeProperties_->push_back(hyperedgePropertyCopy);
      }
    }
  } catch (std::bad_alloc& e) {
    for (auto& hyperedge : *(hyperedges_.get())) {
      delete hyperedge;
    }
    for (auto& hyperedgeProperty : *(hyperedgeProperties_.get())) {
      delete hyperedgeProperty;
    }
    throw;
  }
}


/// Assignment operator
template <template <typename> class VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename HyperedgeProperty,
        typename HypergraphProperty>
UndirectedHypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        HyperedgeProperty, HypergraphProperty>& UndirectedHypergraph<
        VertexTemplate_t, Hyperedge_t, VertexProperty, HyperedgeProperty,
        HypergraphProperty>::
operator=(const UndirectedHypergraph<VertexTemplate_t, Hyperedge_t,
        VertexProperty, HyperedgeProperty, HypergraphProperty>& source) {
  // check for self assignment
  if (this != &source) {
    // Call assignment oeprator of Hypergraph
    Hypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
            HyperedgeProperty, HypergraphProperty>::operator=(source);

    // Set the parent of all childs in the subgraph-hierarchy to null
    std::unordered_set<UndirectedSubHypergraph<VertexTemplate_t,
            Hyperedge_t, VertexProperty, HyperedgeProperty,
            HypergraphProperty>*> observers;
    for (auto itEvent = this->observers_.begin();
         itEvent != this->observers_.end(); ++itEvent) {
      for (auto itObserver : itEvent->second) {
        auto subgraph = dynamic_cast<UndirectedSubHypergraph<VertexTemplate_t,
        Hyperedge_t, VertexProperty, HyperedgeProperty,
                HypergraphProperty>*>(itObserver);
        observers.insert(subgraph);
      }
    }
    for (auto& subgraph : observers) {
      subgraph->setParentOfChildsToNull();
    }

    // deallocate hyperedges_
    for (auto& hyperedge : *(hyperedges_.get())) {
      delete hyperedge;
    }
    hyperedges_->clear();

    // deallocate hyperedgeProperties_
    for (auto& eProp : *(hyperedgeProperties_.get())) {
      delete eProp;
    }
    hyperedgeProperties_->clear();

    // re-init the edge max Id
    hyperedgeMaxId_ = -1;

    // copy edges and associated edge properties
    try {
      for (std::size_t i = 0; i < source.hyperedges_->size(); ++i) {
        Hyperedge_t* hyperedge = source.hyperedges_->operator[](i);
        ++hyperedgeMaxId_;
        // hyperedge was deleted in the source undirectedHypergraph
        if (hyperedge == nullptr) {
          hyperedges_->push_back(nullptr);
          hyperedgeProperties_->push_back(nullptr);
        } else {
          Hyperedge_t* copy = new Hyperedge_t(*hyperedge);
          // add vertices to hyperedge
          for (const auto& vertex : *(hyperedge->vertices_.get())) {
            auto& v = this->vertices_->operator[](vertex->id());
            copy->addVertex(v);
            v->addHyperedge(copy);
          }
          // add hyperedge to hypergraph
          hyperedges_->push_back(copy);
          // copy hyperedge property
          HyperedgeProperty* hyperedgePropertyCopy = new HyperedgeProperty(
                  *source.hyperedgeProperties_->operator[](i));
          hyperedgeProperties_->push_back(hyperedgePropertyCopy);
        }
      }
    } catch (std::bad_alloc& e) {
      for (auto& hyperedge : *(hyperedges_.get())) {
        delete hyperedge;
      }
      for (auto& hyperedgeProperty : *(hyperedgeProperties_.get())) {
        delete hyperedgeProperty;
      }
      throw;
    }
  }
  return *this;
}


/// Destructor
template <template <typename> class VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename HyperedgeProperty,
        typename HypergraphProperty>
UndirectedHypergraph<VertexTemplate_t, Hyperedge_t,
        VertexProperty, HyperedgeProperty, HypergraphProperty>::
~UndirectedHypergraph() {
  // Set the parent of all childs in the subgraph-hierarchy to null
  std::unordered_set<UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t,
      VertexProperty, HyperedgeProperty, HypergraphProperty>*> observers;
  for (auto itEvent = this->observers_.begin();
       itEvent != this->observers_.end(); ++itEvent) {
    for (auto itObserver : itEvent->second) {
      auto subgraph = dynamic_cast<UndirectedSubHypergraph<VertexTemplate_t,
          Hyperedge_t, VertexProperty, HyperedgeProperty,
          HypergraphProperty>*>(itObserver);
      observers.insert(subgraph);
    }
  }
  for (auto& subgraph : observers) {
    subgraph->setParentOfChildsToNull();
  }

  // Delete pointers to hyperedges
  for (auto& hyperedge : *(hyperedges_.get())) {
    delete hyperedge;
  }
  // Delete pointers to hyperedge properties
  for (auto& hyperedgeProperty : *(hyperedgeProperties_.get())) {
    delete hyperedgeProperty;
  }
}


/// Check if multi-hyperedges are allowed in this hypergraph
template <template <typename> class VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename HyperedgeProperty,
        typename HypergraphProperty>
bool UndirectedHypergraph<VertexTemplate_t, Hyperedge_t,
        VertexProperty, HyperedgeProperty, HypergraphProperty>::
allowMultiHyperedges() const {
  return this->allowMultiHyperedges_;
}


/// Number of hyperedges in the hypergraph
template <template <typename> class VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename HyperedgeProperty,
        typename HypergraphProperty>
size_t UndirectedHypergraph<VertexTemplate_t, Hyperedge_t,
        VertexProperty, HyperedgeProperty, HypergraphProperty>::
nbHyperedges() const {
  size_t nbHyperedges = 0;
  auto it = hyperedges_->cbegin();
  auto end = hyperedges_->cend();
  for (; it != end; ++it) {
    ++nbHyperedges;
  }
  return nbHyperedges;
}


/// Highest assigned hyperedge id of the hypergraph
template <template <typename> class VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename HyperedgeProperty,
        typename HypergraphProperty>
size_t UndirectedHypergraph<VertexTemplate_t, Hyperedge_t,
        VertexProperty, HyperedgeProperty, HypergraphProperty>::
hyperedgeContainerSize() const {
  return hyperedges_->size();
}


/**
 * \brief Add a hyperedge with the provided vertices, and additional
 * parameters (if needed to create a hyperedge of type Hyperedge_t) to the
 * hypergraph
 */
template <template <typename> class VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename HyperedgeProperty,
        typename HypergraphProperty>
const Hyperedge_t* UndirectedHypergraph<VertexTemplate_t,
        Hyperedge_t, VertexProperty, HyperedgeProperty,
        HypergraphProperty>::
addHyperedge(decltype(hglib::NAME),
             const VertexNames& vertices,
             const typename Hyperedge_t::SpecificAttributes& attributes) {
  /*
   * Get first the ids of the vertices with given names. An invalid_argurment
   * exception is thrown if a vertex name does not exists in the hypergraph.
   * Then, call addHyperedge(VertexIds, args) to add the hyperedge.
   */
  std::vector<VertexIdType> vertexIds = this->getVertexIdsFromNames(vertices);
  return addHyperedge(vertexIds, attributes);
}


/**
* \brief Add a hyperedge with the provided vertex ids, and additional
* parameters (if needed to create a hyperedge of type Hyperedge_t) to the
* hypergraph
*/
template <template <typename> class VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename HyperedgeProperty,
        typename HypergraphProperty>
const Hyperedge_t* UndirectedHypergraph<VertexTemplate_t,
        Hyperedge_t, VertexProperty, HyperedgeProperty,
        HypergraphProperty>::
addHyperedge(const VertexIds& vertices,
             const typename Hyperedge_t::SpecificAttributes& attributes) {
  // Check if all vertices are declared in the hypergraph.
  // If not, an exception is thrown.
  // In the case that multi-hyperedges are not allowed we check if there exists
  // already a hyperedge with the same vertices. If this is the case we
  // throw an exception. Otherwise and in case multi-hyperedges are allowed we
  // add the hyperedge to the hypergraph.

  // The following vector will hold pointers to the Vertex_t corresponding
  // to the provided vertex ids
  std::vector<const Vertex_t*> vertices_ptr_const;
  for (const auto& vertexId : vertices) {
    vertices_ptr_const.push_back(this->vertexById(vertexId));
  }

  if (not this->allowMultiHyperedges_) {
    // check if there exists a hyperedge with the same vertices
    for (const auto& hyperedge : *(hyperedges_.get())) {
      if (hyperedge != nullptr and
              hyperedge->UndirectedHyperedgeBase<Vertex_t>::
                  compare(vertices_ptr_const)) {
        // build error message
        std::string verticesString("");
        for (auto& vertexId : vertices) {
          verticesString += std::to_string(vertexId) + ", ";
        }
        std::string errorMessage("Multiple hyperedges are not allowed in this "
                                         "hypergraph, but the hyperedge with "
                                         "vertices ids: " + verticesString +
                                 " already exists.\n");
        throw std::invalid_argument(errorMessage);
      }
    }
  }

  // Non-const pointers are needed for the Hyperedge_t ctor
  std::unique_ptr<GraphElementContainer<Vertex_t*>> vertices_ptr =
          create_filtered_vector<Vertex_t*>();
  for (const Vertex_t* vPtr : vertices_ptr_const) {
    vertices_ptr->push_back(const_cast<Vertex_t*>(vPtr));
  }
  // add hyperedge
  const auto hyperedge = buildHyperedgeAndItsDependencies(
          std::move(vertices_ptr), attributes);
  return hyperedge;
}


/// Get arguments to create the hyperedge
template <template <typename> class VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename HyperedgeProperty,
        typename HypergraphProperty>
ArgumentsToCreateUndirectedHyperedge<Hyperedge_t> UndirectedHypergraph<
        VertexTemplate_t, Hyperedge_t, VertexProperty, HyperedgeProperty,
        HypergraphProperty>::
argumentsToCreateHyperedge(const hglib::HyperedgeIdType& hyperedgeId) const {
  try {
    const auto &hyperedge = hyperedgeById(hyperedgeId);
    if (hyperedge == nullptr) {
      std::string errorMessage("Called UndirectedHypergraph::"
                                       "argumentsToCreateHyperedge with "
                                       "the hyperedgeId id ");
      errorMessage += std::to_string(hyperedgeId);
      errorMessage += " which does not exists in the undirected hypergraph.\n";
      throw std::invalid_argument(errorMessage);
    }
    return hyperedge->argumentsToCreateHyperedge();
  } catch (const std::invalid_argument& invalid_argument) {
    throw;
  }
}


/// Remove a hyperedge with the given id from the hypergraph.
template <template <typename> class VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename HyperedgeProperty,
        typename HypergraphProperty>
void UndirectedHypergraph<VertexTemplate_t, Hyperedge_t,
        VertexProperty, HyperedgeProperty, HypergraphProperty>::
removeHyperedge(const HyperedgeIdType& hyperedgeId) {
  /*
   * Get first the pointer to the hyperedge with the given id.
   * Throw invalid_argument exception if a nullptr is returned,
   * which corresponds to the case that the hyperedge with the given id
   * was removed from the hypergraph before.
   * Catch IndexOutOfRangeException if given id is out of bound.
   *
   * Remove hyperedge h with given id from the hyperedge list
   * of the vertices of h.
   * Delete the pointer at index hyperedgeId in hyperedges_ and
   * hyperedgeProperties_ and assign nullptr.
   */
  try {
    const auto& hyperedge = hyperedgeById(hyperedgeId);
    if (hyperedge == nullptr) {
      std::string errorMessage("Called UndirectedHypergraph::"
                                       "removeHyperedge with "
                                       "the hyperedge id ");
      errorMessage += std::to_string(hyperedgeId);
      errorMessage += " which does not exists in the undirected hypergraph.\n";
      throw std::invalid_argument(errorMessage);
    }

    HyperedgeIdType idx(hyperedgeId);
    removeHyperedgeDependencies(*hyperedge);
    // Notify observers of change
    this->NotifyObservers(ObservableEvent::HYPEREDGE_REMOVED, &idx);

    // delete hyperedge
    delete hyperedges_->operator[](idx);
    hyperedges_->operator[](idx) = nullptr;
    // assign nullptr in hyperedgeProperties_ at the index that corresponds
    // to the id of the hyperedge
    delete hyperedgeProperties_->operator[](idx);
    hyperedgeProperties_->operator[](idx) = nullptr;
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in UndirectedHypergraph::"
            "removeHyperedge: index = " << hyperedgeId << "; min = 0; max = "
              << hyperedges_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& invalid_argument) {
    throw;
  }
}


/// Search hyperedge list for an hyperedge with the provided id
template <template <typename> class VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename HyperedgeProperty,
        typename HypergraphProperty>
const Hyperedge_t* UndirectedHypergraph<VertexTemplate_t, Hyperedge_t,
        VertexProperty, HyperedgeProperty, HypergraphProperty>::
hyperedgeById(const HyperedgeIdType & hyperedgeId) const {
  try {
    return hyperedges_->at(hyperedgeId);
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in UndirectedHypergraph::"
            "hyperedgeById: index = " << hyperedgeId << "; min = 0; max = "
              << hyperedges_->size() << '\n';
    throw;
  }
}


/// Const hyperedge iterator pointing to begin of the hyperedges
template <template <typename> class VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename HyperedgeProperty,
        typename HypergraphProperty>
typename UndirectedHypergraph<VertexTemplate_t, Hyperedge_t,
        VertexProperty, HyperedgeProperty,
        HypergraphProperty>::hyperedge_iterator UndirectedHypergraph<
        VertexTemplate_t, Hyperedge_t, VertexProperty, HyperedgeProperty,
        HypergraphProperty>::
hyperedgesBegin() const {
  return hyperedges_->cbegin();
}


/// Const hyperedge iterator pointing to end of the hyperedges
template <template <typename> class VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename HyperedgeProperty,
        typename HypergraphProperty>
typename UndirectedHypergraph<VertexTemplate_t, Hyperedge_t,
        VertexProperty, HyperedgeProperty,
        HypergraphProperty>::hyperedge_iterator UndirectedHypergraph<
        VertexTemplate_t, Hyperedge_t, VertexProperty, HyperedgeProperty,
        HypergraphProperty>::
hyperedgesEnd() const {
  return hyperedges_->cend();
}


/// \brief Pair of const hyperedge iterator pointing to begin and end
/// of the hyperedges
template <template <typename> class VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename HyperedgeProperty,
        typename HypergraphProperty>
std::pair<typename UndirectedHypergraph<VertexTemplate_t, Hyperedge_t,
        VertexProperty, HyperedgeProperty,
        HypergraphProperty>::hyperedge_iterator,
        typename UndirectedHypergraph<VertexTemplate_t, Hyperedge_t,
                VertexProperty, HyperedgeProperty,
                HypergraphProperty>::hyperedge_iterator>
UndirectedHypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        HyperedgeProperty, HypergraphProperty>::
hyperedgesBeginEnd() const {
  return std::make_pair(hyperedgesBegin(), hyperedgesEnd());
}


/// Return a reference to the container of hyperedges.
template <template <typename> class VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename HyperedgeProperty,
        typename HypergraphProperty>
const typename UndirectedHypergraph<VertexTemplate_t, Hyperedge_t,
        VertexProperty, HyperedgeProperty,
        HypergraphProperty>::HyperedgeContainer&
UndirectedHypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        HyperedgeProperty, HypergraphProperty>::hyperedges() const {
  return *(hyperedges_.get());
}


/// Return number of vertices in the hyperedge with given id.
template <template <typename> class VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename HyperedgeProperty,
        typename HypergraphProperty>
size_t UndirectedHypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        HyperedgeProperty, HypergraphProperty>::
nbImpliedVertices(const HyperedgeIdType& hyperedgeId) const {
  /*
   * Get first the pointer to the hyperedge with the given id.
   * Throw invalid_argument exception if a nullptr is returned,
   * which corresponds to the case that the hyperedge with the given id
   * was removed from the hypergraph before.
   * Catch IndexOutOfRangeException if given id is out of bound.
   */
  try {
    const auto& hyperedge = hyperedgeById(hyperedgeId);
    if (hyperedge == nullptr) {
      std::string errorMessage("Called UndirectedHypergraph::nbVertices "
                                       "with the hyperedge id ");
      errorMessage += std::to_string(hyperedgeId);
      errorMessage += " which does not exists in the undirected hypergraph.\n";
      throw std::invalid_argument(errorMessage);
    }
    return hyperedge->vertices_->size();
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in UndirectedHypergraph::"
            "nbVertices: index = " << hyperedgeId << "; min = 0; max = "
              << hyperedges_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& invalid_argument) {
    throw;
  }
}


/// Check if hyperedge with given id has vertices.
template <template <typename> class VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename HyperedgeProperty,
        typename HypergraphProperty>
bool UndirectedHypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        HyperedgeProperty, HypergraphProperty>::
hasVertices(const HyperedgeIdType& hyperedgeId) const {
  /*
   * Get first the pointer to the hyperedge with the given id.
   * Throw invalid_argument exception if a nullptr is returned,
   * which corresponds to the case that the hyperedge with the given id
   * was removed from the hypergraph before.
   * Catch IndexOutOfRangeException if given id is out of bound.
   */
  try {
    const auto& hyperedge = hyperedgeById(hyperedgeId);
    if (hyperedge == nullptr) {
      std::string errorMessage("Called UndirectedHypergraph::hasVertices "
                                       "with the hyperedge id ");
      errorMessage += std::to_string(hyperedgeId);
      errorMessage += " which does not exists in the undirected hypergraph.\n";
      throw std::invalid_argument(errorMessage);
    }
    return not hyperedge->vertices_->empty();
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in UndirectedHypergraph::"
            "hasVertices: index = " << hyperedgeId << "; min = 0; max = "
              << hyperedges_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& invalid_argument) {
    throw;
  }
}


/// \brief Const vertex iterator pointing to the begin of the vertices
/// of the hyperedge with the given id
template <template <typename> class VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename HyperedgeProperty,
        typename HypergraphProperty>
typename UndirectedHypergraph<VertexTemplate_t, Hyperedge_t,
        VertexProperty, HyperedgeProperty, HypergraphProperty>::vertex_iterator
UndirectedHypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        HyperedgeProperty, HypergraphProperty>::
impliedVerticesBegin(const HyperedgeIdType& hyperedgeId) const {
  /*
   * Get first the pointer to the hyperedge with the given id.
   * Throw invalid_argument exception if a nullptr is returned,
   * which corresponds to the case that the hyperedge with the given id
   * was removed from the hypergraph before.
   * Catch IndexOutOfRangeException if given id is out of bound.
   */
  try {
    const auto& hyperedge = hyperedgeById(hyperedgeId);
    if (hyperedge == nullptr) {
      std::string errorMessage("Called UndirectedHypergraph::"
                                       "verticesBegin with the hyperedge id ");
      errorMessage += std::to_string(hyperedgeId);
      errorMessage += " which does not exists in the undirected hypergraph.\n";
      throw std::invalid_argument(errorMessage);
    }
    return hyperedge->vertices_->cbegin();
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in UndirectedHypergraph::"
            "verticesBegin: index = " << hyperedgeId << "; min = 0; max = "
              << hyperedges_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& invalid_argument) {
    throw;
  }
}

/// \brief Const vertex iterator pointing to the end of the vertices of
/// the hyperedge with the given id
template <template <typename> class VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename HyperedgeProperty,
        typename HypergraphProperty>
typename UndirectedHypergraph<VertexTemplate_t, Hyperedge_t,
        VertexProperty, HyperedgeProperty, HypergraphProperty>::vertex_iterator
UndirectedHypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        HyperedgeProperty, HypergraphProperty>::
impliedVerticesEnd(const HyperedgeIdType& hyperedgeId) const {
  /*
   * Get first the pointer to the hyperedge with the given id.
   * Throw invalid_argument exception if a nullptr is returned,
   * which corresponds to the case that the hyperedge with the given id
   * was removed from the hypergraph before.
   * Catch IndexOutOfRangeException if given id is out of bound.
   */
  try {
    const auto& hyperedge = hyperedgeById(hyperedgeId);
    if (hyperedge == nullptr) {
      std::string errorMessage("Called UndirectedHypergraph::"
                                       "verticesEnd with the hyperedge id ");
      errorMessage += std::to_string(hyperedgeId);
      errorMessage += " which does not exists in the undirected hypergraph.\n";
      throw std::invalid_argument(errorMessage);
    }
    return hyperedge->vertices_->cend();
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in UndirectedHypergraph::"
            "verticesEnd: index = " << hyperedgeId << "; min = 0; max = "
              << hyperedges_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& invalid_argument) {
    throw;
  }
}


/// Const vertex iterator pointing to begin and end of the vertices of
/// the hyperedge with the given id
template <template <typename> class VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename HyperedgeProperty,
        typename HypergraphProperty>
std::pair<typename UndirectedHypergraph<VertexTemplate_t, Hyperedge_t,
        VertexProperty, HyperedgeProperty,
        HypergraphProperty>::vertex_iterator,
        typename UndirectedHypergraph<VertexTemplate_t, Hyperedge_t,
                VertexProperty, HyperedgeProperty,
                HypergraphProperty>::vertex_iterator>
UndirectedHypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        HyperedgeProperty, HypergraphProperty>::
impliedVerticesBeginEnd(const HyperedgeIdType& hyperedgeId) const {
  /*
   * Get first the pointer to the hyperedge with the given id.
   * Throw invalid_argument exception if a nullptr is returned,
   * which corresponds to the case that the hyperedge with the given id
   * was removed from the hypergraph before.
   * Catch IndexOutOfRangeException if given id is out of bound.
   */
  try {
    const auto& hyperedge = hyperedgeById(hyperedgeId);
    if (hyperedge == nullptr) {
      std::string errorMessage("Called UndirectedHypergraph::"
                                       "verticesBeginEnd with the hyperedge"
                                       " id ");
      errorMessage += std::to_string(hyperedgeId);
      errorMessage += " which does not exists in the undirected hypergraph.\n";
      throw std::invalid_argument(errorMessage);
    }
    return std::make_pair(hyperedge->vertices_->cbegin(),
                          hyperedge->vertices_->cend());
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in UndirectedHypergraph::"
            "verticesBeginEnd: index = " << hyperedgeId << "; min = 0; max = "
              << hyperedges_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& invalid_argument) {
    throw;
  }
}


/// Get container of vertices of given hyperedge id
template <template <typename> class VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename HyperedgeProperty,
        typename HypergraphProperty>
std::shared_ptr<const GraphElementContainer<typename UndirectedHypergraph<
    VertexTemplate_t, Hyperedge_t, VertexProperty, HyperedgeProperty,
    HypergraphProperty>::Vertex_t*>> UndirectedHypergraph<VertexTemplate_t,
    Hyperedge_t, VertexProperty, HyperedgeProperty, HypergraphProperty>::
impliedVertices(const HyperedgeIdType& hyperedgeId) const {
  /*
   * Get first the pointer to the hyperedge with the given id.
   * Throw invalid_argument exception if a nullptr is returned,
   * which corresponds to the case that the hyperedge with the given id
   * was removed from the hypergraph before.
   * Catch IndexOutOfRangeException if given id is out of bound.
   */
  try {
    const auto& hyperedge = hyperedgeById(hyperedgeId);
    if (hyperedge == nullptr) {
      std::string errorMessage("Called UndirectedHypergraph::"
                                       "vertices with the hyperedge"
                                       " id ");
      errorMessage += std::to_string(hyperedgeId);
      errorMessage += " which does not exists in the undirected hypergraph.\n";
      throw std::invalid_argument(errorMessage);
    }
    return hyperedge->vertices_;
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in UndirectedHypergraph::"
            "vertices: index = " << hyperedgeId << "; min = 0; max = "
              << hyperedges_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& invalid_argument) {
    throw;
  }
}


/// Establish consecutive hyperarc ids
template <template <typename> class VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename HyperedgeProperty,
        typename HypergraphProperty>
void UndirectedHypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        HyperedgeProperty, HypergraphProperty>::
resetHyperedgeIds() {
  // Notify observers
  this->NotifyObservers(ObservableEvent::RESET_EDGE_IDS);
  // erase all nullptrs
  // remove nullptrs from underlying vector: std::remove puts all nullptrs at
  // the end of the vector
  auto posFirstNullptr = std::remove(hyperedges_->data(),
                                     hyperedges_->data() +
                                     hyperedges_->size(),
                                     static_cast<Hyperedge_t*>(NULL));
  // Compute nb of nullptr
  size_t nbNullptr = (hyperedges_->data() + hyperedges_->size()) -
                     posFirstNullptr;
  // Resize
  hyperedges_->resize(hyperedges_->size() - nbNullptr);

  // assign new Ids
  HyperedgeIdType id(-1);
  for (auto it = hyperedges_->begin(); it != hyperedges_->end();
       ++it) {
    (*it)->setId(++id);  // assign new Id
  }

  // set vertexMaxId_ to the highest assigned id
  hyperedgeMaxId_ = id;

  // remove nullptr entries from vector of hyperarc properties
  removeNullEntriesFromHyperedgeProperties();
}


/// Establish consecutive vertex and hyperarc ids
template <template <typename> class VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename HyperedgeProperty,
        typename HypergraphProperty>
void UndirectedHypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        HyperedgeProperty, HypergraphProperty>::
resetIds() {
  this->resetVertexIds();
  resetHyperedgeIds();
}



/// Number of hyperedges that contain the vertex with given id
template <template <typename> class VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename HyperedgeProperty,
        typename HypergraphProperty>
size_t UndirectedHypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        HyperedgeProperty, HypergraphProperty>::
nbHyperedges(const VertexIdType& vertexId) const {
  /*
   * Get first the pointer to the vertex with the given id.
   * Throw invalid_argument exception if a nullptr is returned,
   * which corresponds to the case that the vertex with the given id
   * was removed from the hypergraph before.
   * Catch IndexOutOfRangeException if given id is out of bound.
   */
  try {
    const auto& vertex = this->vertexById(vertexId);
    if (vertex == nullptr) {
      std::string errorMessage("Called UndirectedHypergraph::"
                                       "nbHyperedges "
                                       "with the vertex id ");
      errorMessage += std::to_string(vertexId);
      errorMessage += " which does not exists in the undirected hypergraph.\n";
      throw std::invalid_argument(errorMessage);
    }
    return vertex->hyperedges_->size();
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in UndirectedHypergraph::"
            "nbHyperedges: index = " << vertexId << "; min = 0; max = "
              << this->vertices_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& invalid_argument) {
    throw;
  }
}


/// Check if the vertex is part of at least one hyperedge
template <template <typename> class VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename HyperedgeProperty,
        typename HypergraphProperty>
bool UndirectedHypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        HyperedgeProperty, HypergraphProperty>::
isContainedInAnyHyperedge(const VertexIdType& vertexId) const {
  /*
   * Get first the pointer to the vertex with the given id.
   * Throw invalid_argument exception if a nullptr is returned,
   * which corresponds to the case that the vertex with the given id
   * was removed from the hypergraph before.
   * Catch IndexOutOfRangeException if given id is out of bound.
   */
  try {
    const auto& vertex = this->vertexById(vertexId);
    if (vertex == nullptr) {
      std::string errorMessage("Called UndirectedHypergraph::"
                                       "isContainedInAnyHyperedge "
                                       "with the vertex id ");
      errorMessage += std::to_string(vertexId);
      errorMessage += " which does not exists in the undirected hypergraph.\n";
      throw std::invalid_argument(errorMessage);
    }
    return not vertex->hyperedges_->empty();
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in UndirectedHypergraph::"
            "isContainedInAnyHyperedge: index = " << vertexId << "; min = 0; "
            "max = " << this->vertices_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& invalid_argument) {
    throw;
  }
}


/// \brief Const hyperedge iterator pointing to the begin of the
/// hyperedges that contain the vertex with the given id
template <template <typename> class VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename HyperedgeProperty,
        typename HypergraphProperty>
typename UndirectedHypergraph<VertexTemplate_t, Hyperedge_t,
        VertexProperty, HyperedgeProperty,
        HypergraphProperty>::hyperedge_iterator UndirectedHypergraph<
        VertexTemplate_t, Hyperedge_t, VertexProperty, HyperedgeProperty,
        HypergraphProperty>::
hyperedgesBegin(const VertexIdType& vertexId) const {
  /*
   * Get first the pointer to the vertex with the given id.
   * Throw invalid_argument exception if a nullptr is returned,
   * which corresponds to the case that the vertex with the given id
   * was removed from the hypergraph before.
   * Catch IndexOutOfRangeException if given id is out of bound.
   */
  try {
    const auto& vertex = this->vertexById(vertexId);
    if (vertex == nullptr) {
      std::string errorMessage("Called UndirectedHypergraph::"
                                       "hyperedgesBegin "
                                       "with the vertex id ");
      errorMessage += std::to_string(vertexId);
      errorMessage += " which does not exists in the undirected hypergraph.\n";
      throw std::invalid_argument(errorMessage);
    }
    return vertex->hyperedges_->cbegin();
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in UndirectedHypergraph::"
            "hyperedgesBegin: index = " << vertexId << "; min = 0; "
                      "max = " << this->vertices_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& invalid_argument) {
    throw;
  }
}


/// \brief Const hyperedge iterator pointing to the end of the
/// hyperedges that contain the vertex with the given id
template <template <typename> class VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename HyperedgeProperty,
        typename HypergraphProperty>
typename UndirectedHypergraph<VertexTemplate_t, Hyperedge_t,
        VertexProperty, HyperedgeProperty,
        HypergraphProperty>::hyperedge_iterator UndirectedHypergraph<
        VertexTemplate_t, Hyperedge_t, VertexProperty, HyperedgeProperty,
        HypergraphProperty>::
hyperedgesEnd(const VertexIdType& vertexId) const {
  /*
   * Get first the pointer to the vertex with the given id.
   * Throw invalid_argument exception if a nullptr is returned,
   * which corresponds to the case that the vertex with the given id
   * was removed from the hypergraph before.
   * Catch IndexOutOfRangeException if given id is out of bound.
   */
  try {
    const auto& vertex = this->vertexById(vertexId);
    if (vertex == nullptr) {
      std::string errorMessage("Called UndirectedHypergraph::"
                                       "hyperedgesEnd "
                                       "with the vertex id ");
      errorMessage += std::to_string(vertexId);
      errorMessage += " which does not exists in the undirected hypergraph.\n";
      throw std::invalid_argument(errorMessage);
    }
    return vertex->hyperedges_->cend();
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in UndirectedHypergraph::"
            "hyperedgesEnd: index = " << vertexId << "; min = 0; "
                      "max = " << this->vertices_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& invalid_argument) {
    throw;
  }
}


/// \brief Const hyperedge iterator pointing to begin and end of the
/// hyperedges that contain the vertex with the given id
template <template <typename> class VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename HyperedgeProperty,
        typename HypergraphProperty>
std::pair<typename UndirectedHypergraph<VertexTemplate_t, Hyperedge_t,
        VertexProperty, HyperedgeProperty,
        HypergraphProperty>::hyperedge_iterator,
        typename UndirectedHypergraph<VertexTemplate_t, Hyperedge_t,
                VertexProperty, HyperedgeProperty,
                HypergraphProperty>::hyperedge_iterator>
UndirectedHypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        HyperedgeProperty, HypergraphProperty>::
hyperedgesBeginEnd(const VertexIdType& vertexId) const {
  /*
   * Get first the pointer to the vertex with the given id.
   * Throw invalid_argument exception if a nullptr is returned,
   * which corresponds to the case that the vertex with the given id
   * was removed from the hypergraph before.
   * Catch IndexOutOfRangeException if given id is out of bound.
   */
  try {
    const auto& vertex = this->vertexById(vertexId);
    if (vertex == nullptr) {
      std::string errorMessage("Called UndirectedHypergraph::"
                                       "hyperedgesBeginEnd "
                                       "with the vertex id ");
      errorMessage += std::to_string(vertexId);
      errorMessage += " which does not exists in the undirected hypergraph.\n";
      throw std::invalid_argument(errorMessage);
    }
    return std::make_pair(vertex->hyperedges_->cbegin(),
                          vertex->hyperedges_->cend());
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in UndirectedHypergraph::"
            "hyperedgesBeginEnd: index = " << vertexId << "; min = 0; "
                      "max = " << this->vertices_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& invalid_argument) {
    throw;
  }
}


/// Get container of hyperedges that contain the vertex with the given id
template <template <typename> class VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename HyperedgeProperty,
        typename HypergraphProperty>
std::shared_ptr<const GraphElementContainer<
    Hyperedge_t*>> UndirectedHypergraph<VertexTemplate_t, Hyperedge_t,
    VertexProperty, HyperedgeProperty, HypergraphProperty>::
hyperedges(const VertexIdType& vertexId) const {
  /*
   * Get first the pointer to the vertex with the given id.
   * Throw invalid_argument exception if a nullptr is returned,
   * which corresponds to the case that the vertex with the given id
   * was removed from the hypergraph before.
   * Catch IndexOutOfRangeException if given id is out of bound.
   */
  try {
    const auto& vertex = this->vertexById(vertexId);
    if (vertex == nullptr) {
      std::string errorMessage("Called UndirectedHypergraph::"
                                       "hyperedges "
                                       "with the vertex id ");
      errorMessage += std::to_string(vertexId);
      errorMessage += " which does not exists in the undirected hypergraph.\n";
      throw std::invalid_argument(errorMessage);
    }
    return vertex->hyperedges_;
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in UndirectedHypergraph::"
            "hyperedges: index = " << vertexId << "; min = 0; "
                      "max = " << this->vertices_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& invalid_argument) {
    throw;
  }
}


/// Remove a vertex with given name from the hypergraph.
template <template <typename> class VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename HyperedgeProperty,
        typename HypergraphProperty>
void UndirectedHypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        HyperedgeProperty, HypergraphProperty>::
removeVertex(const std::string& vertexName,
             bool removeImpliedHyperedges) {
  // Find vertex by name in the hypergraph and call the method removeVertex
  // that takes the id of the vertex
  const auto& vertex = this->vertexByName(vertexName);
  if (vertex == nullptr) {
    std::string errorMessage("Called UndirectedHypergraph::removeVertex"
                                     " with the vertex name " + vertexName +
                             " which does not exists in the undirected "
                                     "hypergraph.\n");
    throw std::invalid_argument(errorMessage);
  }

  removeVertex(vertex->id(), removeImpliedHyperedges);
}


/// Remove a vertex with given id from the hypergraph.
template <template <typename> class VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename HyperedgeProperty,
        typename HypergraphProperty>
void UndirectedHypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        HyperedgeProperty, HypergraphProperty>::
removeVertex(const VertexIdType& vertexId,
             bool removeImpliedHyperedges) {
  /*
   * Get first the pointer to the vertex with the given id.
   * Throw invalid_argument exception if a nullptr is returned,
   * which corresponds to the case that the vertex with the given id
   * was removed from the hypergraph before.
   * Catch IndexOutOfRangeException if given id is out of bound.
   *
   * Remove all implied hyperedges if flag is set to true (default).
   * Otherwise, remove only the vertex v with given id 'vertexId' from the
   * vetices list of the hyperedges of v. Remove hyperedges that have no
   * vertices.
   *
   * Delete the pointer at index vertexId in vertices_ and vertexProperties_
   * and assign nullptr.
   */
  try {
    const auto& vertex = this->vertexById(vertexId);
    if (vertex == nullptr) {
      std::string errorMessage("Called UndirectedHypergraph::removeVertex"
                                       " with the vertex id ");
      errorMessage += std::to_string(vertexId);
      errorMessage += " which does not exists in the undirected hypergraph.\n";
      throw std::invalid_argument(errorMessage);
    }

    if (removeImpliedHyperedges) {
      // hyperedges that will be removed from the hypergraph
      std::vector<Hyperedge_t*> hyperedgesToBeRemoved;
      for (auto& hyperedge : *(hyperedges_.get())) {
        // all hyperedges that contain the vertex will be removed from the
        // hypergraph
        if (hyperedge != nullptr and isVertexOfHyperedge(vertexId,
                                                         hyperedge->id())) {
          hyperedgesToBeRemoved.emplace_back(hyperedge);
        }
      }
      // remove hyperedges from hypergraph
      for (auto& hyperedge : hyperedgesToBeRemoved) {
        removeHyperedge(hyperedge->id());
      }
    } else {
      // remove vertex from vertices_ of its hyperedges
      removeVertexDependencies(*vertex);
      // remove hyperedges without vertices
      for (auto& hyperedge : *(hyperedges_.get())) {
        if (hyperedge != nullptr and not hasVertices(hyperedge->id())) {
          removeHyperedge(hyperedge->id());
        }
      }
    }

    VertexIdType idx(vertexId);
    // Notify observers of change
    Remove_vertex_args args;
    args.vertexId_ = idx;
    args.removeImpliedHyperarcs_ = removeImpliedHyperedges;
    this->NotifyObservers(ObservableEvent::VERTEX_REMOVED, &args);

    // assign NULL in vertices_ and vertexProperties_ at the index that
    // corresponds to the id of the vertex
    this->setVertexAndVertexPropertyEntryToNullptr(idx);
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in UndirectedHypergraph::"
            "removeVertex:"
            " index = " << vertexId << "; min = 0; max = "
              << this->vertices_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& invalid_argument) {
    throw;
  }
}


/// Check if given vertex is a vertex of given hyperedge.
template <template <typename> class VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename HyperedgeProperty,
        typename HypergraphProperty>
bool UndirectedHypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        HyperedgeProperty, HypergraphProperty>::
isVertexOfHyperedge(const VertexIdType& vertexId,
                    const HyperedgeIdType& hyperedgeId) const {
  /*
   * Get first the pointer to the vertex/hyperedge with the given ids.
   * Throw invalid_argument exception if a nullptr is returned,
   * which corresponds to the case that the vertex/hyperedge with the given id
   * was removed from the hypergraph before.
   * Catch IndexOutOfRangeException if given id is out of bound.
   */
  try {
    const auto& vertex = this->vertexById(vertexId);
    if (vertex == nullptr) {
      std::string errorMessage("Called UndirectedHypergraph::"
                                       "isVertexOfHyperedge "
                                       "with the vertex id ");
      errorMessage += std::to_string(vertexId);
      errorMessage += " which does not exists in the undirected hypergraph.\n";
      throw std::invalid_argument(errorMessage);
    }
    const auto& hyperedge = hyperedgeById(hyperedgeId);
    if (hyperedge == nullptr) {
      std::string errorMessage("Called UndirectedHypergraph::"
                                       "isVertexOfHyperedge with the hyperedge"
                                       " id ");
      errorMessage += std::to_string(hyperedgeId);
      errorMessage += " which does not exists in the undirected hypergraph.\n";
      throw std::invalid_argument(errorMessage);
    }

    for (const auto& v : *(hyperedge->vertices_.get())) {
      if (v->id() == vertex->id()) {
        return true;
      }
    }
    return false;
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in UndirectedHypergraph::"
            "isVertexOfHyperedge: " << '\n' <<
              "index vertex = " << vertexId << "; min = 0; max = " <<
              this->vertices_->size() << '\n' <<
              "index hyperedge = " << hyperedgeId << "; min = 0; max = " <<
              hyperedges_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& invalid_argument) {
    throw;
  }
}


/// Get properties of the hyperedge with the provided id.
template <template <typename> class VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename HyperedgeProperty,
        typename HypergraphProperty>
const HyperedgeProperty* UndirectedHypergraph<VertexTemplate_t,
        Hyperedge_t, VertexProperty, HyperedgeProperty, HypergraphProperty>::
getHyperedgeProperties(const HyperedgeIdType& hyperedgeId) const {
  try {
    return hyperedgeProperties_->at(hyperedgeId);
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in UndirectedHypergraph::"
            "getHyperedgeProperties: index = " << hyperedgeId <<
              "; min = 0; " <<
              "max = " << hyperedgeProperties_->size() << '\n';
    throw;
  }
}


/// Get properties of the hyperedge with the provided id.
template <template <typename> class VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename HyperedgeProperty,
        typename HypergraphProperty>
HyperedgeProperty* UndirectedHypergraph<VertexTemplate_t,
        Hyperedge_t, VertexProperty, HyperedgeProperty, HypergraphProperty>::
getHyperedgeProperties_(const HyperedgeIdType& hyperedgeId) const {
  try {
    return const_cast<HyperedgeProperty*>(this->getHyperedgeProperties(
            hyperedgeId));
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in UndirectedHypergraph::"
            "getHyperedgeProperties_: index = " << hyperedgeId <<
              "; min = 0; " <<
              "max = " << hyperedgeProperties_->size() << '\n';
    throw;
  }
}


/// Get root hypergraph
template <template <typename> class VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename HyperedgeProperty,
        typename HypergraphProperty>
const UndirectedHypergraphInterface<VertexTemplate_t, Hyperedge_t,
        VertexProperty, HyperedgeProperty,
        HypergraphProperty>* UndirectedHypergraph<VertexTemplate_t,
        Hyperedge_t, VertexProperty, HyperedgeProperty, HypergraphProperty>::
getRootHypergraph() const {
  return this;
}


/* **************************************************************************
 * **************************************************************************
 *                        Protected methods
 * **************************************************************************
 * *************************************************************************/

/// Construct a hyperedge and a HyperedgeProperty object
template <template <typename> class VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename HyperedgeProperty,
        typename HypergraphProperty>
const Hyperedge_t* UndirectedHypergraph<VertexTemplate_t, Hyperedge_t,
        VertexProperty, HyperedgeProperty, HypergraphProperty>::
buildHyperedgeAndItsDependencies(
        std::unique_ptr<GraphElementContainer<Vertex_t*>> vertices,
        const typename Hyperedge_t::SpecificAttributes& attributes) {
  // A hyperedge x is created and a
  // pointer to this hyperedge is added to the list of hyperedges of the
  // hypergraph. Hyperedge dependencies are solved:
  // 1. Pointer to hyperedge x is added to the hyperedge list of the
  // vertices of x.
  // A hyperedge property object is created using the default ctor.

  // create the hyperedge and add it to the list of hyperedges of the
  // hypergraph
  auto hyperedge = new Hyperedge_t(++hyperedgeMaxId_, std::move(vertices),
                                   attributes);
  hyperedges_->push_back(hyperedge);

  // Solve hyperedge dependencies
  // add hyperedge to hyperedges of the vertices of the hyperedge
  for (auto&& vertex : *(hyperedge->vertices_.get())) {
    vertex->addHyperedge(hyperedge);
  }

  // build hyperedge property and add it to the list of hyperedge properties
  auto hyperedgeProperty = new HyperedgeProperty();
  hyperedgeProperties_->push_back(hyperedgeProperty);
  return hyperedge;
}


/// Remove vertex dependencies.
template <template <typename> class VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename HyperedgeProperty,
        typename HypergraphProperty>
void UndirectedHypergraph<VertexTemplate_t, Hyperedge_t,
        VertexProperty, HyperedgeProperty, HypergraphProperty>::
removeVertexDependencies(const Vertex_t& vertex) {
  // remove the vertex from the list of vertices of the vertex's hyperedge list
  for (auto&& edge : *(vertex.hyperedges_.get())) {
    edge->removeVertex(vertex);
  }
}


/// Remove hyperedge dependencies.
template <template <typename> class VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename HyperedgeProperty,
        typename HypergraphProperty>
void UndirectedHypergraph<VertexTemplate_t, Hyperedge_t,
        VertexProperty, HyperedgeProperty, HypergraphProperty>::
removeHyperedgeDependencies(const Hyperedge_t& hyperedge) {
  // remove hyperedge e from the hyperedge list of e's vertices
  for (auto&& vertex : *(hyperedge.vertices_.get())) {
    vertex->removeHyperedge(hyperedge);
  }
}


/// Remove nullptr entries from vector of hyperedge properties.
template <template <typename> class VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename HyperedgeProperty,
        typename HypergraphProperty>
void UndirectedHypergraph<VertexTemplate_t, Hyperedge_t,
        VertexProperty, HyperedgeProperty, HypergraphProperty>::
removeNullEntriesFromHyperedgeProperties() {
  // remove nullptrs from underlying vector: std::remove puts all nullptrs at
  // the end of the vector
  auto posFirstNullptr = std::remove(hyperedgeProperties_->data(),
                                     hyperedgeProperties_->data() +
                                     hyperedgeProperties_->size(),
                                     static_cast<HyperedgeProperty*>(NULL));
  // Compute nb of nullptr
  size_t nbNullptr = (hyperedgeProperties_->data() +
                      hyperedgeProperties_->size()) - posFirstNullptr;
  // Resize
  hyperedgeProperties_->resize(hyperedgeProperties_->size() - nbNullptr);
}


/// Return pointer to hyperedge container
template <template <typename> class VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename HyperedgeProperty,
        typename HypergraphProperty>
typename UndirectedHypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
    HyperedgeProperty, HypergraphProperty>::HyperedgeContainerPtr
UndirectedHypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
    HyperedgeProperty, HypergraphProperty>::
getRootHyperedgeContainerPtr() const {
  return hyperedges_;
}
};  // namespace hglib
