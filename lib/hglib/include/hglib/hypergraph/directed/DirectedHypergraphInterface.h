/*Copyright (C) <2016-2017>  David Parsons, Martin Wannagat

This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU Affero General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/
//
// Created by Martin Wannagat on 18.09.17.
//

#ifndef HGLIB_DIRECTEDHYPERGRAPHINTERFACE_H
#define HGLIB_DIRECTEDHYPERGRAPHINTERFACE_H

#include "filtered_vector.h"

#include "hglib/hypergraph/HypergraphInterface.h"
#include "hglib/utils/observation/Observable.h"
#include "hglib/hyperedge/directed/ArgumentsToCreateDirectedHyperedge.h"
#include "hglib/utils/types.h"

namespace hglib {

template <template <typename> class VertexTemplate_t,
        typename Hyperarc_type,
        typename VertexProperty,
        typename HyperarcProperty,
        typename HypergraphProperty>
class DirectedHypergraphInterface : virtual public HypergraphInterface<
        VertexTemplate_t, Hyperarc_type, VertexProperty, HyperarcProperty,
        HypergraphProperty> {
 public:
  /// Alias for the directed vertex type
  using Vertex_t = typename HypergraphInterface<VertexTemplate_t,
          Hyperarc_type, VertexProperty, HyperarcProperty,
          HypergraphProperty>::Vertex_t;
  /// Alias for Hyperarc type
  using Hyperarc_t = Hyperarc_type;
  /// Alias for Hyperarc property type
  using HyperarcProperty_t = HyperarcProperty;
  /// Alias for conditional range of a container of Vertex_t ptr
  using VertexContainer = typename HypergraphInterface<VertexTemplate_t,
          Hyperarc_type, VertexProperty, HyperarcProperty,
          HypergraphProperty>::VertexContainer;
  /// Alias for conditional range of a container of Hyperarc_t ptr
  using HyperarcContainer = GraphElementContainer<Hyperarc_t*>;
  /// Alias for iterator over a container of vertices
  using vertex_iterator = typename HypergraphInterface<VertexTemplate_t,
          Hyperarc_type, VertexProperty, HyperarcProperty,
          HypergraphProperty>::vertex_iterator;
  /// Alias for iterator over a container of hyperarcs
  using hyperarc_iterator = typename GraphElementContainer<
          Hyperarc_t*>::const_iterator;
  /// \brief Alias for the data container that stores the names of tails and
  /// heads of a hyperarc
  typedef std::pair<std::vector<std::string>, std::vector<std::string>>
          TailAndHeadNames;
  /// \brief Alias for the data container that stores the ids of tails and
  /// heads of a hyperarc
  typedef std::pair<std::vector<VertexIdType>, std::vector<VertexIdType>>
          TailAndHeadIds;
  /// Alias for unique_ptr of GraphElementContainer<Vertex_t*>
  using VertexContainerPtr = typename HypergraphInterface<VertexTemplate_t,
          Hyperarc_type, VertexProperty, HyperarcProperty,
          HypergraphProperty>::VertexContainerPtr;
  /// Alias for unique_ptr of GraphElementContainer<VertexProperty*>
  using VertexPropertyContainerPtr = typename HypergraphInterface<
          VertexTemplate_t, Hyperarc_type, VertexProperty, HyperarcProperty,
          HypergraphProperty>::VertexPropertyContainerPtr;
  /// Alias for unique_ptr of GraphElementContainer<Hyperarc_t*>
  using HyperarcContainerPtr =
      std::shared_ptr<GraphElementContainer<Hyperarc_t*>>;
  /// Alias for unique_ptr of GraphElementContainer<HyperarcProperty*>
  using HyperarcPropertyContainerPtr =
      std::shared_ptr<GraphElementContainer<HyperarcProperty*>>;

  /* **************************************************************************
   * **************************************************************************
   *                    Destructor
   * **************************************************************************
   * *************************************************************************/
 public:
  /// Destructor
  virtual ~DirectedHypergraphInterface() = default;

  /* **************************************************************************
   * **************************************************************************
   *                    General
   * **************************************************************************
   * *************************************************************************/
 public:
  /// Check if multi-hyperarcs are allowed in this hypergraph
  virtual bool allowMultiHyperarcs() const = 0;

  /* **************************************************************************
  * **************************************************************************
  *                        Hyperarc access
  * **************************************************************************
  * *************************************************************************/
 public:
  /// Number of hyperarcs in the hypergraph
  virtual size_t nbHyperarcs() const = 0;
  /// Highest assigned hyperarc id of the hypergraph
  virtual size_t hyperarcContainerSize() const = 0;

  /**
    * \brief Add a hyperarc with the provided tails, heads, and additional
    * parameters (if needed to create a hyperarc of type Hyperarc_t) to the
    * hypergraph
    */
  virtual const Hyperarc_t* addHyperarc(
          decltype(hglib::NAME),
          const TailAndHeadNames& tails_heads,
          const typename Hyperarc_t::SpecificAttributes& attributes = {}) = 0;

  /**
  * \brief Add a hyperarc with the provided tail ids, head ids, and additional
  * parameters (if needed to create a hyperarc of type Hyperarc_t) to the
  * hypergraph
  */
  virtual const Hyperarc_t* addHyperarc(
          const TailAndHeadIds& tails_heads,
          const typename Hyperarc_t::SpecificAttributes& attributes = {}) = 0;

  /// Get arguments to create the hyperarc
  virtual ArgumentsToCreateDirectedHyperedge<Hyperarc_t>
  argumentsToCreateHyperarc(
          const hglib::HyperedgeIdType& hyperarcId) const = 0;

  /// Remove a hyperarc with the given id from the hypergraph.
  virtual void removeHyperarc(const HyperedgeIdType& hyperarcId) = 0;

  /// Search hyperarc list for an hyperarc with the provided id
  virtual const Hyperarc_t* hyperarcById(
          const HyperedgeIdType & hyperarcId) const = 0;

  /// Const hyperarc iterator pointing to begin of the hyperarcs
  virtual hyperarc_iterator hyperarcsBegin() const = 0;
  /// Const hyperarc iterator pointing to end of the hyperarcs
  virtual hyperarc_iterator hyperarcsEnd() const = 0;
  /// Const hyperarc iterator pointing to begin and end of the hyperarcs
  virtual std::pair<hyperarc_iterator,
          hyperarc_iterator> hyperarcsBeginEnd() const = 0;
  /// Return a reference to the container of hyperarcs.
  virtual const HyperarcContainer& hyperarcs() const = 0;

  // Tails
  /// Return number of tail vertices in the hyperarc with given id.
  virtual size_t nbTailVertices(const HyperedgeIdType & hyperarcId) const = 0;
  /// Check if hyperarc with given id has tail vertices.
  virtual bool hasTailVertices(const HyperedgeIdType & hyperarcId) const = 0;
  /// \brief Const vertex iterator pointing to the begin of the tail vertices
  /// of the hyperarc with the given id
  virtual vertex_iterator tailsBegin(
          const HyperedgeIdType & hyperarcId) const = 0;
  /// \brief Const vertex iterator pointing to the end of the tail vertices of
  /// the hyperarc with the given id
  virtual vertex_iterator tailsEnd(
          const HyperedgeIdType & hyperarcId) const = 0;
  /// Const vertex iterator pointing to begin and end of the tails
  virtual std::pair<vertex_iterator, vertex_iterator> tailsBeginEnd(
          const HyperedgeIdType & hyperarcId) const = 0;
  /// Get container of tail vertices of given hyperarc id
  virtual std::shared_ptr<const GraphElementContainer<Vertex_t*>> tails(
          const HyperedgeIdType& hyperarcId) const = 0;

  // Heads
  /// Return number of head vertices in the hyperarc with given id.
  virtual size_t nbHeadVertices(const HyperedgeIdType & hyperarcId) const = 0;
  /// Check if hyperarc with given id has head vertices.
  virtual bool hasHeadVertices(const HyperedgeIdType & hyperarcId) const = 0;
  /// \brief Const vertex iterator pointing to the begin of the head vertices
  /// of the hyperarc with the given id
  virtual vertex_iterator headsBegin(
          const HyperedgeIdType & hyperarcId) const = 0;
  //// \brief Const vertex iterator pointing to the end of the head vertices
  /// of the hyperarc with the given id
  virtual vertex_iterator headsEnd(
          const HyperedgeIdType & hyperarcId) const = 0;
  /// Const vertex iterator pointing to begin and end of the heads
  virtual std::pair<vertex_iterator, vertex_iterator> headsBeginEnd(
          const HyperedgeIdType & hyperarcId) const = 0;
  /// Get container of head vertices of given hyperarc id
  virtual std::shared_ptr<const GraphElementContainer<Vertex_t*>> heads(
          const HyperedgeIdType& hyperarcId) const = 0;

  /* **************************************************************************
   * **************************************************************************
   *                       DirectedVertex access
   * **************************************************************************
   * *************************************************************************/
 public:
  // incoming hyperarcs
  /// Number of incoming hyperarcs of the vertex with given id
  virtual size_t inDegree(const VertexIdType& vertexId) const = 0;
  /// Check if vertex has incoming hyperarcs
  virtual bool hasInHyperarcs(const VertexIdType& vertexId) const = 0;
  /// \brief Const hyperarc iterator pointing to the begin of the incoming
  /// hyperarcs of the vertex with the given id
  virtual hyperarc_iterator inHyperarcsBegin(
          const VertexIdType& vertexId) const = 0;
  /// \brief Const hyperarc iterator pointing to the end of the incoming
  /// hyperarcs of the vertex with the given id
  virtual hyperarc_iterator inHyperarcsEnd(
          const VertexIdType& vertexId) const = 0;
  /// Const hyperarc iterator pointing to begin and end of the in-hyperarcs
  virtual std::pair<hyperarc_iterator, hyperarc_iterator> inHyperarcsBeginEnd(
          const VertexIdType& vertexId) const = 0;
  /// Get container of incoming hyperarcs of given vertex id
  virtual std::shared_ptr<const GraphElementContainer<Hyperarc_t*>> inHyperarcs(
          const VertexIdType& vertexId) const = 0;

  // out-going hyperarcs
  /// Number of outgoing hyperarcs of the vertex with given id
  virtual size_t outDegree(const VertexIdType& vertexId) const = 0;
  /// Check if vertex has outgoing hyperarcs
  virtual bool hasOutHyperarcs(const VertexIdType& vertexId) const = 0;
  /// \brief Const hyperarc iterator pointing to the begin of the outgoing
  /// hyperarcs of the vertex with the given id
  virtual hyperarc_iterator outHyperarcsBegin(
          const VertexIdType& vertexId) const = 0;
  /// \brief Const hyperarc iterator pointing to the end of the outgoing
  /// hyperarcs of the vertex with the given id
  virtual hyperarc_iterator outHyperarcsEnd(
          const VertexIdType& vertexId) const = 0;
  /// Const hyperarc iterator pointing to begin and end of the in-hyperarcs
  virtual std::pair<hyperarc_iterator, hyperarc_iterator> outHyperarcsBeginEnd(
          const VertexIdType& vertexId) const = 0;
  /// Get container of outgoing hyperarcs of given vertex id
  virtual std::shared_ptr<const GraphElementContainer<
      Hyperarc_t*>> outHyperarcs(const VertexIdType& vertexId) const = 0;

  /// Remove a vertex with given name from the directed hypergraph.
  virtual void removeVertex(const std::string& vertexName,
                            bool removeImpliedHyperarcs = true) = 0;
  /// Remove a vertex with given id from the directed hypergraph.
  virtual void removeVertex(const VertexIdType& vertexId,
                            bool removeImpliedHyperarcs = true) = 0;

  /* **************************************************************************
   * **************************************************************************
   *                       Vertex/Hyperarc dependent access
   * **************************************************************************
   * *************************************************************************/
 public:
  /// Check if given vertex is a tail vertex of given hyperarc.
  virtual bool isTailVertexOfHyperarc(
          const VertexIdType& vertexId,
          const HyperedgeIdType& hyperarcId) const = 0;
  /// Check if given vertex is a head vertex of given hyperarc.
  virtual bool isHeadVertexOfHyperarc(
          const VertexIdType& vertexId,
          const HyperedgeIdType& hyperarcId) const = 0;
  /// Check if given vertex is a tail or head vertex of given hyperarc.
  virtual bool isVertexOfHyperarc(
          const VertexIdType& vertexId,
          const HyperedgeIdType& hyperarcId) const = 0;

  /* **************************************************************************
  * **************************************************************************
  *                        HyperarcProperty access
  * **************************************************************************
  * *************************************************************************/
 public:
  /// Get properties of the hyperarc with the provided id.
  virtual const HyperarcProperty* getHyperarcProperties(
          const HyperedgeIdType& hyperarcId) const = 0;
  /// Get properties of the hyperarc with the provided id.
  virtual HyperarcProperty* getHyperarcProperties_(
          const HyperedgeIdType& hyperarcId) const = 0;

  /* **************************************************************************
   * **************************************************************************
   *                  Hypergraph hierarchy/Observer pattern
   * **************************************************************************
   * *************************************************************************/
 public:
  /// Get root hypergraph
  virtual const DirectedHypergraphInterface* getRootHypergraph() const = 0;
  /// Return pointer to hyperarc container
  virtual HyperarcContainerPtr getRootHyperarcContainerPtr() const = 0;
};



/* **************************************************************************
 * **************************************************************************
 *               Non-member functions
 * **************************************************************************
 * *************************************************************************/

/**
 * \brief Retrieve graph's arcs into a container
 *
 * \details
 * Retrieve hypergraph's arcs and insert them into a container through the
 * provided output iterator.
 *
 * \param hypergraph graph from which to retrieve the hyperarcs
 * \param output_iter
 * \parblock
 * OutputIterator to the container to fill \n
 * The underlying container's value_type should be `const Hyperarc_t*`
 * \endparblock
 * \param fn (optional) if provided, only those hyperarcs for which
 * `fn(hyperarc.id())` evaluates to true will be inserted
 *
 * \par Examples
 * \parblock
 * Simple example
 * \code
 hglib::DirectedHypergraph<> g;
 for (auto vertexName : vertices) {
   g.addVertex(vertexName);
 }
 for (auto arc : arcs) {
   g.addHyperarc(arc);
 }

 std::vector<const Hyperarc*> hyperarcContainer;
 hglib::hyperarcs(g, std::back_inserter(hyperarcContainer));

 std::set<const Hyperarc*> s;
 hglib::hyperarcs(g, std::inserter(s, s.begin()));
 * \endcode
 *
 * Retrieve hyperarcs with a simple constraint
 * \code
 enum class Colour {
    red, blue, green, black
  };
  struct EdgeProperty {
    string name = "Default";
    Colour colour = Colour::green;
  };

  hglib::DirectedHypergraph<hglib::DirectedVertex,
                            hglib::DirectedHyperedge,
                            hglib::emptyProperty,
                            EdgeProperty> g;
  for (auto vertexName : vertices) {
    g.addVertex(vertexName);
  }
  for (auto hyperarc : hyperarcs) {
    g.addHyperarc(hglib::NAME, hyperarc.first);
  }

  // Get green hyperarcs
  std::vector<const hglib::DirectedHyperedge*> greenHyperarcs;
  hglib::hyperarcs(g, std::back_inserter(greenHyperarcs),
                   [&](hglib::HyperedgeIdType hyperarcId) -> bool {
                     return g.getHyperarcProperties(hyperarcId)->colour ==
                            Colour::green;
                   });
 * \endcode
 * \endparblock
 *
 * \par Complexity
 * Linear most of the time\n
 * O(n * O(\a fn)) if \a fn is provided
 *
 * \relates DirectedHypergraphInterface
 */
template <typename HGraph, typename OutputIterator>
void hyperarcs(const HGraph& hypergraph,
               OutputIterator output_iter,
               std::function<bool(hglib::HyperedgeIdType)>&& fn = nullptr) {
  // Check that output_iter satisfies the OutputIterator requirements
  static_assert(
      std::is_same<
          typename std::iterator_traits<OutputIterator>::iterator_category,
          typename std::output_iterator_tag>::value,
      "provided iterator not an OutputIterator");

  // Insert hyperarcs through output_iter
  for (const auto& hyperarc : hypergraph.hyperarcs()) {
    // Insert if no fn provided or fn returns true
    if (not fn or fn(hyperarc->id())) {
      output_iter = hyperarc;
    }
  }
}

}  // namespace hglib
#endif  // HGLIB_DIRECTEDHYPERGRAPHINTERFACE_H
