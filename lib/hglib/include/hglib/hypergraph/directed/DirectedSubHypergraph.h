/*Copyright (C) <2016-2017>  David Parsons, Martin Wannagat

This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU Affero General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/
//
// Created by Martin Wannagat on 01.08.17.
//

#ifndef HGLIB_SUBDIRECTEDHYPERGRAPH_H
#define HGLIB_SUBDIRECTEDHYPERGRAPH_H

#include <unordered_set>
#include <memory>

#include "DirectedHypergraphInterface.h"
#include "hglib/utils/observation/Observer.h"
#include "hglib/hyperedge/directed/DirectedHyperedge.h"
#include "hglib/utils/types.h"
#include "hglib/vertex/directed/DirectedVertex.h"

namespace hglib {

template <template <typename> class VertexTemplate_t = DirectedVertex,
        typename Hyperarc_t = DirectedHyperedge,
        typename VertexProperty = emptyProperty,
        typename HyperarcProperty = emptyProperty,
        typename HypergraphProperty = emptyProperty>
class DirectedSubHypergraph : public DirectedHypergraphInterface<
        VertexTemplate_t, Hyperarc_t, VertexProperty, HyperarcProperty,
        HypergraphProperty>,
                              public Observer {
  template <template <typename> class DirectedVertex_t, typename Arc_t,
          typename VertexProp, typename EdgeProp,
          typename HypergraphProp>
  friend class DirectedHypergraph;

 public:
  /// Alias for the directed vertex type
  using Vertex_t = typename DirectedHypergraphInterface<VertexTemplate_t,
          Hyperarc_t, VertexProperty, HyperarcProperty,
          HypergraphProperty>::Vertex_t;
  /// Alias for Hyperarc type
  using Hyperarc_type = Hyperarc_t;
  /// Alias for Hyperarc property type
  using HyperarcProperty_type = HyperarcProperty;
  /// Alias for conditional range of a container of Vertex_t ptr
  using VertexContainer = typename DirectedHypergraphInterface<
          VertexTemplate_t, Hyperarc_t, VertexProperty, HyperarcProperty,
          HypergraphProperty>::VertexContainer;
  /// Alias for conditional range of a container of Hyperarc_t ptr
  using HyperarcContainer = typename DirectedHypergraphInterface<
          VertexTemplate_t, Hyperarc_t, VertexProperty, HyperarcProperty,
          HypergraphProperty>::HyperarcContainer;
  ///
  using vertex_iterator = typename DirectedHypergraphInterface<
          VertexTemplate_t, Hyperarc_t, VertexProperty, HyperarcProperty,
          HypergraphProperty>::vertex_iterator;
  ///
  using hyperarc_iterator = typename DirectedHypergraphInterface<
          VertexTemplate_t, Hyperarc_t, VertexProperty, HyperarcProperty,
          HypergraphProperty>::hyperarc_iterator;
  /// \brief Alias for the data container that stores the names of tails and
  /// heads of a hyperarc
  typedef std::pair<std::vector<std::string>, std::vector<std::string>>
          TailAndHeadNames;
  /// \brief Alias for the data container that stores the ids of tails and
  /// heads of a hyperarc
  typedef std::pair<std::vector<VertexIdType>, std::vector<VertexIdType>>
          TailAndHeadIds;
  /// Alias for unique_ptr of GraphElementContainer<Hyperarc_t*>
  using HyperarcContainerPtr = typename DirectedHypergraphInterface<
          VertexTemplate_t, Hyperarc_t, VertexProperty, HyperarcProperty,
          HypergraphProperty>::HyperarcContainerPtr;
  /// Alias for unique_ptr of GraphElementContainer<HyperarcProperty*>
  using HyperarcPropertyContainerPtr = typename DirectedHypergraphInterface<
          VertexTemplate_t, Hyperarc_t, VertexProperty, HyperarcProperty,
          HypergraphProperty>::HyperarcPropertyContainerPtr;
  /// Alias for unique_ptr of GraphElementContainer<Vertex_t*>
  using VertexContainerPtr = typename DirectedHypergraphInterface<
          VertexTemplate_t, Hyperarc_t, VertexProperty, HyperarcProperty,
          HypergraphProperty>::VertexContainerPtr;
  /// Alias for unique_ptr of GraphElementContainer<VertexProperty*>
  using VertexPropertyContainerPtr = typename DirectedHypergraphInterface<
          VertexTemplate_t, Hyperarc_t, VertexProperty, HyperarcProperty,
          HypergraphProperty>::VertexPropertyContainerPtr;

  /* **************************************************************************
   * **************************************************************************
   *                        Constructor/Destructor
   * **************************************************************************
   * *************************************************************************/
  /// Constructor
  DirectedSubHypergraph(DirectedHypergraphInterface<VertexTemplate_t,
          Hyperarc_t, VertexProperty, HyperarcProperty,
          HypergraphProperty>* parent,
                        const std::unordered_set<VertexIdType>&
                        whitelistedVertexIds,
                        const std::unordered_set<HyperedgeIdType>&
                        whitelistedHyperarcIds);

  /// Copy constructor
  DirectedSubHypergraph(const DirectedSubHypergraph& directedSubHypergraph);
  /// Destructor
  virtual ~DirectedSubHypergraph();
  /// Assignment operator
  DirectedSubHypergraph& operator=(const DirectedSubHypergraph& source);

  /* **************************************************************************
   * **************************************************************************
   *                    General
   * **************************************************************************
   * *************************************************************************/
 public:
  /// Check if multi-hyperarcs are allowed in this hypergraph
  bool allowMultiHyperarcs() const override;

  /* **************************************************************************
   * **************************************************************************
   *                        DirectedHyperedge access
   * **************************************************************************
   * *************************************************************************/
  /// Number of hyperarcs in the hypergraph
  size_t nbHyperarcs() const override;
  /// Highest assigned hyperarc id of the hypergraph
  size_t hyperarcContainerSize() const override;

  /**
    * \brief Add a hyperarc with the provided tails, heads, and additional
    * parameters (if needed to create a hyperarc of type Hyperarc_t) to the
    * hypergraph
    */
  const Hyperarc_t* addHyperarc(
          decltype(hglib::NAME),
          const TailAndHeadNames& tails_heads,
          const typename Hyperarc_t::SpecificAttributes& attributes = {})
    override;

  /**
  * \brief Add a hyperarc with the provided tail ids, head ids, and additional
  * parameters (if needed to create a hyperarc of type Hyperarc_t) to the
  * hypergraph
  */
  const Hyperarc_t* addHyperarc(
          const TailAndHeadIds& tails_heads,
          const typename Hyperarc_t::SpecificAttributes& attributes = {})
    override;

  /// Get arguments to create the hyperarc
  ArgumentsToCreateDirectedHyperedge<Hyperarc_t> argumentsToCreateHyperarc(
          const hglib::HyperedgeIdType& hyperarcId) const override;

  /// Remove a hyperarc with the given id from the hypergraph.
  void removeHyperarc(const HyperedgeIdType& hyperarcId) override;

  /// Search hyperarc list for an hyperarc with the provided id
  const Hyperarc_t* hyperarcById(
          const HyperedgeIdType & hyperarcId) const override;

  /// Const hyperarc iterator pointing to begin of the hyperarcs
  hyperarc_iterator hyperarcsBegin() const override;
  /// Const hyperarc iterator pointing to end of the hyperarcs
  hyperarc_iterator hyperarcsEnd() const override;
  ///
  std::pair<hyperarc_iterator, hyperarc_iterator> hyperarcsBeginEnd() const
    override;
  /// Return a reference to the container of hyperarcs.
  const HyperarcContainer& hyperarcs() const override;

  // Tails
  /// Return number of tail vertices in the hyperarc with given id.
  size_t nbTailVertices(const HyperedgeIdType & hyperarcId) const override;
  /// Check if hyperarc with given id has tail vertices.
  bool hasTailVertices(const HyperedgeIdType & hyperarcId) const override;
  /// \brief Const vertex iterator pointing to the begin of the tail vertices
  /// of the hyperarc with the given id
  vertex_iterator tailsBegin(const HyperedgeIdType & hyperarcId) const
    override;
  /// \brief Const vertex iterator pointing to the end of the tail vertices of
  /// the hyperarc with the given id
  vertex_iterator tailsEnd(const HyperedgeIdType & hyperarcId) const override;
  ///
  std::pair<vertex_iterator, vertex_iterator> tailsBeginEnd(
          const HyperedgeIdType & hyperarcId) const override;
  /// Get container of tail vertices of given hyperarc id
  std::shared_ptr<const GraphElementContainer<Vertex_t*>> tails(
          const HyperedgeIdType& hyperarcId) const override;

  // Heads
  /// Return number of head vertices in the hyperarc with given id.
  size_t nbHeadVertices(const HyperedgeIdType & hyperarcId) const override;
  /// Check if hyperarc with given id has head vertices.
  bool hasHeadVertices(const HyperedgeIdType & hyperarcId) const override;
  /// \brief Const vertex iterator pointing to the begin of the head vertices
  /// of the hyperarc with the given id
  vertex_iterator headsBegin(const HyperedgeIdType & hyperarcId) const
    override;
  //// \brief Const vertex iterator pointing to the end of the head vertices
  /// of the hyperarc with the given id
  vertex_iterator headsEnd(const HyperedgeIdType & hyperarcId) const override;
  ///
  std::pair<vertex_iterator, vertex_iterator> headsBeginEnd(
          const HyperedgeIdType & hyperarcId) const override;
  /// Get container of head vertices of given hyperarc id
  std::shared_ptr<const GraphElementContainer<Vertex_t*>> heads(
          const HyperedgeIdType& hyperarcId) const override;

  /* **************************************************************************
   * **************************************************************************
   *                       DirectedVertex access
   * **************************************************************************
   * *************************************************************************/
  /// Highest assigned vertex id of the hypergraph
  size_t vertexContainerSize() const override;
  /// Number of vertices in the hypergraph
  size_t nbVertices() const override;
  /// Search vertex list for a vertex with the provided name
  const Vertex_t* vertexByName(const std::string& vertexName) const override;
  /// Search vertex list for a vertex with the provided id
  const Vertex_t* vertexById(const VertexIdType & vertexId) const override;
  /// Const vertex iterator pointing to begin of vertices
  vertex_iterator verticesBegin() const override;
  /// Const vertex iterator pointing to end of vertices
  vertex_iterator verticesEnd() const override;
  /// Const vertex iterator pointing to begin and end of vertices
  std::pair<vertex_iterator, vertex_iterator> verticesBeginEnd() const
    override;

  /// Return a reference to the container of vertices.
  const VertexContainer& vertices() const override;

  /// Add a vertex
  const Vertex_t* addVertex(
          const typename Vertex_t::SpecificAttributes& attributes = {})
    override;
  /// Add a vertex with a given name
  const Vertex_t* addVertex(
          const std::string& name,
          const typename Vertex_t::SpecificAttributes& attributes = {})
    override;

  /// Get arguments to create the vertex
  ArgumentsToCreateVertex<Vertex_t> argumentsToCreateVertex(
          const hglib::VertexIdType& vertexId) const override;
  // incoming hyperarcs
  /// Number of incoming hyperarcs of the vertex with given id
  size_t inDegree(const VertexIdType& vertexId) const override;
  /// Check if vertex has incoming hyperarcs
  bool hasInHyperarcs(const VertexIdType& vertexId) const override;
  /// \brief Const hyperarc iterator pointing to the begin of the incoming
  /// hyperarcs of the vertex with the given id
  hyperarc_iterator inHyperarcsBegin(
          const VertexIdType& vertexId) const override;
  /// \brief Const hyperarc iterator pointing to the end of the incoming
  /// hyperarcs of the vertex with the given id
  hyperarc_iterator inHyperarcsEnd(
          const VertexIdType& vertexId) const override;
  ///
  std::pair<hyperarc_iterator, hyperarc_iterator> inHyperarcsBeginEnd(
          const VertexIdType& vertexId) const override;
  /// Get container of incoming hyperarcs of given vertex id
  std::shared_ptr<const GraphElementContainer<Hyperarc_t*>> inHyperarcs(
      const VertexIdType& vertexId) const override;

  // out-going hyperarcs
  /// Number of outgoing hyperarcs of the vertex with given id
  size_t outDegree(const VertexIdType& vertexId) const override;
  /// Check if vertex has outgoing hyperarcs
  bool hasOutHyperarcs(const VertexIdType& vertexId) const override;
  /// \brief Const hyperarc iterator pointing to the begin of the outgoing
  /// hyperarcs of the vertex with the given id
  hyperarc_iterator outHyperarcsBegin(
          const VertexIdType& vertexId) const override;
  /// \brief Const hyperarc iterator pointing to the end of the outgoing
  /// hyperarcs of the vertex with the given id
  hyperarc_iterator outHyperarcsEnd(
          const VertexIdType& vertexId) const override;
  ///
  std::pair<hyperarc_iterator, hyperarc_iterator> outHyperarcsBeginEnd(
          const VertexIdType& vertexId) const override;
  /// Get container of outgoing hyperarcs of given vertex id
  std::shared_ptr<const GraphElementContainer<Hyperarc_t*>> outHyperarcs(
      const VertexIdType& vertexId) const override;

  /// Remove a vertex with given name from the directed hypergraph.
  void removeVertex(const std::string& vertexName,
                            bool removeImpliedHyperarcs = true) override;
  /// Remove a vertex with given id from the directed hypergraph.
  void removeVertex(const VertexIdType& vertexId,
                            bool removeImpliedHyperarcs = true) override;

  /* **************************************************************************
   * **************************************************************************
   *                       Vertex/DirectedHyperedge dependent access
   * **************************************************************************
   * *************************************************************************/
  /// Check if given vertex is a tail vertex of given hyperarc.
  bool isTailVertexOfHyperarc(
          const VertexIdType& vertexId,
          const HyperedgeIdType& hyperarcId) const override;
  /// Check if given vertex is a head vertex of given hyperarc.
  bool isHeadVertexOfHyperarc(
          const VertexIdType& vertexId,
          const HyperedgeIdType& hyperarcId) const override;
  /// Check if given vertex is a tail or head vertex of given hyperarc.
  bool isVertexOfHyperarc(
          const VertexIdType& vertexId,
          const HyperedgeIdType& hyperarcId) const override;

  /* **************************************************************************
   * **************************************************************************
   *                        HyperarcProperty access
   * **************************************************************************
   * *************************************************************************/
  /// Get properties of the hyperarc with the provided id.
  const HyperarcProperty* getHyperarcProperties(
          const HyperedgeIdType& hyperarcId) const override;
  /// Get properties of the hyperarc with the provided id.
  HyperarcProperty* getHyperarcProperties_(
          const HyperedgeIdType& hyperarcId) const override;

  /* **************************************************************************
   * **************************************************************************
   *                    Vertex property access methods
   * **************************************************************************
   * *************************************************************************/
  /// Get properties of the vertex with the provided id.
  const VertexProperty* getVertexProperties(
          const VertexIdType& vertexId) const override;
  /// Get properties of the vertex with the provided id.
  VertexProperty* getVertexProperties_(
          const VertexIdType& vertexId) const override;

  /* **************************************************************************
   * **************************************************************************
   *               Hypergraph property access methods
   * **************************************************************************
   * *************************************************************************/
  /// Get properties of the hypergraph.
  const HypergraphProperty* getHypergraphProperties() const override;
  /// Get properties of the hypergraph.
  HypergraphProperty* getHypergraphProperties_() const override;

  /* **************************************************************************
   * **************************************************************************
   *                Hypergraph hierarchy/Observer pattern
   * **************************************************************************
   * *************************************************************************/
  /// Get root hypergraph
  const DirectedHypergraphInterface<VertexTemplate_t, Hyperarc_t,
          VertexProperty, HyperarcProperty,
          HypergraphProperty>* getRootHypergraph() const override;

 protected:
  /// Observer update method
  void Update(const Observable& o, ObservableEvent e, void* arg) override;

  /* **************************************************************************
   * **************************************************************************
   *                        Protected methods
   * **************************************************************************
   * *************************************************************************/
 protected:
  /** \brief If the dtor of the root directed hypergraph was called we
   * invalidate all parents of its child hierarchy
   */
  void setParentOfChildsToNull();
  /**\brief Throw a exception if one tries to use a member function on a
   * subgraph whose parent was set to nullptr previously
   */
  void throwExceptionOnNullptrParent() const;
  /**\brief Adapt the content of container whitelistedVerticesList_ in
   * response to the call resetVertexIds in the root hypergraph
   */
  void updateVertexContainerUponResetVertexIds();
  /**\brief Adapt the content of container whitelistedHyperarcsList_ in
   * response to the call resetHyperarcIds in the root hypergraph
   */
  void updateHyperarcContainerUponResetHyperarcIds();
  /// Return pointer to vertices container
  VertexContainerPtr getRootVerticesContainerPtr() const override;
  /// Return pointer to hyperarc container
  HyperarcContainerPtr getRootHyperarcContainerPtr() const override;

  /* **************************************************************************
   * **************************************************************************
   *                              Data members
   * **************************************************************************
   * *************************************************************************/
 protected:
  /// Parent hypergraph
  DirectedHypergraphInterface<VertexTemplate_t, Hyperarc_t, VertexProperty,
          HyperarcProperty, HypergraphProperty>* parent_;
  /// Container of whitelisted vertices
  VertexContainerPtr whitelistedVerticesList_;
  /// Container of whitelisted hyperarcs
  HyperarcContainerPtr whitelistedHyperarcsList_;
  /// SubHypergraph property
  HypergraphProperty* subHypergraphProperty_;
};


/**
 * Factory method to create a directed sub-hypergraph.
 *
 * @tparam DirectedHypergraph_t
 * @param g
 * @param whitelistedVertexIds
 * @param whitelistedHyperarcIds
 * @return unique_ptr to directed sub-hypergraph of type
 * DirectedSubHypergraph<DirectedHypergraph_t>
 */
template <template <typename> class VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename HyperarcProperty,
        typename HypergraphProperty>
std::unique_ptr<DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t,
        VertexProperty, HyperarcProperty, HypergraphProperty>>
createDirectedSubHypergraph(DirectedHypergraphInterface<VertexTemplate_t,
        Hyperarc_t, VertexProperty, HyperarcProperty,
        HypergraphProperty>* parent,
                            const std::unordered_set<VertexIdType>&
                            whitelistedVertexIds,
                            const std::unordered_set<HyperedgeIdType>&
                            whitelistedHyperarcIds) {
  return std::make_unique<DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t,
          VertexProperty, HyperarcProperty, HypergraphProperty>>(
          parent, whitelistedVertexIds, whitelistedHyperarcIds);
}


/**
 * Factory method to copy a given directed sub-hypergraph
 *
 * @tparam DirectedHypergraph_t
 * @param g
 * @return unique_ptr to directed sub-hypergraph of type
 * DirectedSubHypergraph<DirectedHypergraph_t>
 */
template <template <typename> class VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename HyperarcProperty,
        typename HypergraphProperty>
std::unique_ptr<DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t,
        VertexProperty, HyperarcProperty, HypergraphProperty>>
copyDirectedSubHypergraph(
        const DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t,
                VertexProperty, HyperarcProperty, HypergraphProperty>& g) {
  return std::make_unique<DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t,
          VertexProperty, HyperarcProperty, HypergraphProperty>>(g);
}
}  // namespace hglib

#include "DirectedSubHypergraph.hpp"
#endif  // HGLIB_SUBDIRECTEDHYPERGRAPH_H
