/*Copyright (C) <2016-2017>  David Parsons, Martin Wannagat

This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU Affero General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/
//
// Created by Martin Wannagat on 18.09.17.
//

#ifndef HGLIB_HYPERGRAPHINTERFACE_H
#define HGLIB_HYPERGRAPHINTERFACE_H

#include <string>

#include "filtered_vector.h"

#include "hglib/utils/observation/Observable.h"
#include "hglib/utils/types.h"
#include "hglib/utils/HasInsertMemberFunction.h"
#include "hglib/vertex/ArgumentsToCreateVertex.h"

namespace hglib {

template <template <typename> class VertexTemplate_t,
        typename Hyperedge_t,
        typename VertexProperty,
        typename EdgeProperty,
        typename HypergraphProperty>
class HypergraphInterface : public Observable {
 public:
  /// Alias for the full vertex type
  using Vertex_t = VertexTemplate_t<Hyperedge_t>;
  /// Alias for VertexProperty
  using VertexProperty_t = VertexProperty;
  /// Alias for HypergraphProperty
  using HypergraphProperty_t = HypergraphProperty;
  /// Alias for conditional range of a container of Vertex_t ptr
  using VertexContainer = GraphElementContainer<Vertex_t*>;
  /// Alias for iterator over a container of vertices
  using vertex_iterator = typename GraphElementContainer<
          Vertex_t*>::const_iterator;
  /// Alias for unique_ptr of GraphElementContainer<Vertex_t*>
  using VertexContainerPtr = std::shared_ptr<GraphElementContainer<Vertex_t*>>;
  /// Alias for unique_ptr of GraphElementContainer<VertexProperty*>
  using VertexPropertyContainerPtr =
      std::shared_ptr<GraphElementContainer<VertexProperty*>>;

  /* **************************************************************************
   * **************************************************************************
   *                    Constructor/Destructor
   * **************************************************************************
   * *************************************************************************/
 protected:
  /// Constructor
  HypergraphInterface() : Observable() {}

 public:
  /// Destructor
  virtual ~HypergraphInterface() = default;

  /* **************************************************************************
   * **************************************************************************
   *                    Vertex access methods
   * **************************************************************************
   * *************************************************************************/
 public:
  /// Highest assigned vertex id of the hypergraph
  virtual size_t vertexContainerSize() const = 0;
  /// Number of vertices in the hypergraph
  virtual size_t nbVertices() const = 0;
  /// Search vertex list for a vertex with the provided name
  virtual const Vertex_t* vertexByName(
          const std::string& vertexName) const = 0;
  /// Search vertex list for a vertex with the provided id
  virtual const Vertex_t* vertexById(
          const VertexIdType & vertexId) const = 0;
  /// Const vertex iterator pointing to begin of vertices
  virtual vertex_iterator verticesBegin() const = 0;
  /// Const vertex iterator pointing to end of vertices
  virtual vertex_iterator verticesEnd() const = 0;
  /// Const vertex iterator pointing to begin and end of vertices
  virtual std::pair<vertex_iterator,
          vertex_iterator> verticesBeginEnd() const = 0;

  /// Return a reference to the container of vertices.
  virtual const VertexContainer& vertices() const = 0;

  /// Add a vertex
  virtual const Vertex_t* addVertex(
          const typename Vertex_t::SpecificAttributes& attributes = {}) = 0;
  /// Add a vertex with a given name
  virtual const Vertex_t* addVertex(
          const std::string& name,
          const typename Vertex_t::SpecificAttributes& attributes = {}) = 0;

  /// Get arguments to create the vertex
  virtual ArgumentsToCreateVertex<Vertex_t> argumentsToCreateVertex(
          const hglib::VertexIdType& vertexId) const = 0;

  /* **************************************************************************
   * **************************************************************************
   *                    Vertex property access methods
   * **************************************************************************
   * *************************************************************************/
  /// Get properties of the vertex with the provided id.
  virtual const VertexProperty* getVertexProperties(
          const VertexIdType& vertexId) const = 0;
  /// Get properties of the vertex with the provided id.
  virtual VertexProperty* getVertexProperties_(
          const VertexIdType& vertexId) const = 0;

  /* **************************************************************************
   * **************************************************************************
   *               Hypergraph_impl property access methods
   * **************************************************************************
   * *************************************************************************/
  /// Get properties of the hypergraph.
  virtual const HypergraphProperty* getHypergraphProperties() const = 0;
  /// Get properties of the hypergraph.
  virtual HypergraphProperty* getHypergraphProperties_() const = 0;

  /* **************************************************************************
   * **************************************************************************
   *                  Hypergraph hierarchy/Observer pattern
   * **************************************************************************
   * *************************************************************************/
 public:
  /// Return pointer to vertices container
  virtual VertexContainerPtr getRootVerticesContainerPtr() const = 0;
};


/* **************************************************************************
 * **************************************************************************
 *               Non-member functions
 * **************************************************************************
 * *************************************************************************/

/**
 * Add pointers to const vertex objects to container.
 *
 * An insert iterator of a container is provided as a parameter
 * to this function. After checking if the value_type of the
 * container is const Vertex_t<Hyperedge_t>*, and that the container has an
 * insert method, the vertices of the hypergraph are added to the
 * container via the insert iterator.
 *
 * Example calls:
 \verbatim
 hglib::DirectedHypergraph<> g;
 for (auto vertexName : vertices) {
   g.addVertex(vertexName);
 }

 std::vector<const DirectedVertex<Hyperarc>*> verticesContainer;
 hglib::vertices(g, std::back_inserter(verticesContainer));

 std::set<const DirectedVertex<Hyperarc>*> s;
 hglib::vertices(g, std::inserter(s, s.begin()));
 \endverbatim
 *
 * The following is not supported because a std::array does
 * not have an insert method:
 \verbatim
 std::array<const DirectedVertex<Hyperarc>*, 5> a;
 hglib::vertices(g, std::inserter(a, a.end()));
 \endverbatim
 *
 * Complexity: Linear
 *
 * @param inserter insert iterator of a container
 */
template <template <typename> class VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename EdgeProperty,
        typename HypergraphProperty, typename InsertIter>
void vertices(const HypergraphInterface<VertexTemplate_t, Hyperedge_t,
        VertexProperty, EdgeProperty, HypergraphProperty>& hypergraph,
              InsertIter inserter) {
  // desired value_type of the container
  typedef typename HypergraphInterface<VertexTemplate_t, Hyperedge_t,
          VertexProperty, EdgeProperty, HypergraphProperty>::Vertex_t T;
  // assert that the value_type of the container of the insert iterator
  // is const Vertex_t*
  static_assert(std::is_same<typename InsertIter::container_type::value_type,
                        const T*>::value,
                "Wrong Type: The value_type of the container must be "
                        "e.g. set<const DirectedVertex<Hyperarc>*>");
  // assert that the container C has a insert function that returns a
  // C::iterator
  static_assert(has_insert_member_function<typename InsertIter::container_type,
                        const T*>::value,
                "Provide a container with the 'insert' member function as in "
                        "std::vector. For example std::array is not"
                        " supported.");
  // insert pointers to vertices that are not nullptr
  for (const auto& vertex : hypergraph.vertices()) {
    inserter = vertex;
  }
}


/**
 * Add to the container the pointers to those const vertex objects
 * for which the lambda function evaluates to true
 *
 * An insert iterator of a container is provided as a first parameter
 * to this function. After checking if the value_type of the
 * container is const Vertex_t<Hyperedge_t>*, and that the container has an
 * insert method, the vertices, for which the lambda function (second parameter)
 * evaluates to true, are added to the container via the insert iterator.
 * The lambda function takes the index of the vertex (type VertexIdType) as
 * parameter.
 *
 * The method can be used to recover e.g. all vertices with a specific property:
 *
 * Example call:
 \verbatim
 enum Colour {
     red, blue, green, black
 };
 struct VertexProperty {
     string name = "Default";
     Colour colour = Colour::green;
 };
 struct EdgeProperty {
     string name = "Default";
     double weight = 0.0;
 };

 hglib::DirectedHypergraph<Hyperarc, DirectedVertex, VertexProperty,
          EdgeProperty> g;
 for (auto vertexName : vertices) {
   g.addVertex(vertexName);
 }

 // Get all (green) vertices
 std::vector<const DirectedVertex<Hyperarc>*> verticesContainer;
 hglib::vertices(g, std::back_inserter(verticesContainer),
                [&] (hglib::VertexIdType vertexId) -> bool {
                    return g.getVertexProperties(vertexId)->colour ==
                           Colour::green;});
 // ... change colour of some vertices
 // Get vertices by colour
 for ( int idx = red; idx != black; idx++ ) {
   verticesContainer.clear();  // empty container
   // Get vertices of specified colour
   Colour colour = static_cast<Colour>(idx);
   hglib::vertices(g, std::back_inserter(verticesContainer),
                  [&] (hglib::VertexIdType vertexId) -> bool {
                       return g.getVertexProperties(vertexId)->colour ==
                              colour;});
 }
 \endverbatim
 *
 * Complexity: O(n * O(func)). Complexity depends on the complexity of the
 * lambda function which is evaluated for each vertex.
 *
 * @param inserter insert iterator of a container
 * @param func lambda function that takes the index of a vertex (type IdType) as
 * parameter.
 */
template <template <typename> class VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename EdgeProperty,
        typename HypergraphProperty, typename InsertIter, typename Lambda>
void vertices(const HypergraphInterface<VertexTemplate_t, Hyperedge_t,
        VertexProperty, EdgeProperty, HypergraphProperty>& hypergraph,
         InsertIter inserter, Lambda&& func) {
  // desired value_type of the container
  typedef typename HypergraphInterface<VertexTemplate_t, Hyperedge_t,
          VertexProperty, EdgeProperty, HypergraphProperty>::Vertex_t T;
  // assert that the value_type of the container of the insert iterator
  // is const Vertex_t*
  static_assert(std::is_same<typename InsertIter::container_type::value_type,
                        const T*>::value,
                "Wrong Type: The value_type of the container must be "
                        "e.g. set<const DirectedVertex<Hyperarc>*>");
  // assert that the container C has a insert function that returns a
  // C::iterator
  static_assert(has_insert_member_function<typename InsertIter::container_type,
                        const T*>::value,
                "Provide a container with the 'insert' member function as "
                        "in std::vector. For example std::array is not "
                        "supported.");
  // insert pointers to vertices that are not nullptr and that fulfill the
  // lambda function
  for (const auto& vertex : hypergraph.vertices()) {
    if (func(vertex->id())) {
      inserter = vertex;
    }
  }
}
}  // namespace hglib
#endif  // HGLIB_HYPERGRAPHINTERFACE_H
