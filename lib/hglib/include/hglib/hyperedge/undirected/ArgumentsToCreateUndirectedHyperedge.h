/*Copyright (C) <2016-2017>  David Parsons, Martin Wannagat

This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU Affero General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/
//
// Created by Martin Wannagat on 03.10.17.
//

#ifndef HGLIB_ARGUMENTSTOCREATEHYPEREDGE_H
#define HGLIB_ARGUMENTSTOCREATEHYPEREDGE_H

#include <vector>

#include "hglib/utils/types.h"

namespace hglib {

/**
 * A wrapper class that holds the arguments needed to create a
 * hyperedge of the template parameter type.
 *
 * Every hyperedge type contains vertices, and attributes that are
 * specific to the hyperedge type (possibly empty).
 *
 * @tparam Hyperedge_t
 */
template<typename Hyperedge_t>
class ArgumentsToCreateUndirectedHyperedge {
  friend Hyperedge_t;

  /// Alias for data structure to provide tail and head ids
  typedef std::vector<hglib::VertexIdType> VerticesIds;

 protected:
  /// Constructor
  ArgumentsToCreateUndirectedHyperedge(
          const VerticesIds& verticesIds,
          const typename Hyperedge_t::SpecificAttributes& specificAttributes);

 public:
  /// Get ids of vertices
  const VerticesIds& verticesIds() const;
  /// Get specific arguments
  const typename Hyperedge_t::SpecificAttributes& specificAttributes() const;
  /// Set vertices ids
  void setVerticesIds(const VerticesIds&);
  /// Set arguments that are specific to the hyperedge type (Hyperedge_t)
  void setSpecificArguments(const typename Hyperedge_t::SpecificAttributes&);

 protected:
  /// Vertices ids
  VerticesIds verticesIds_;
  /// Specific arguments for the hyperedge type
  typename Hyperedge_t::SpecificAttributes specificAttributes_;
};
}  // namespace hglib

#include "ArgumentsToCreateUndirectedHyperedge.hpp"

#endif  // HGLIB_ARGUMENTSTOCREATEHYPEREDGE_H
