/*Copyright (C) <2016-2017>  David Parsons, Martin Wannagat

This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU Affero General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/
//
// Created by Martin Wannagat on 21.06.17.
//

#include "UndirectedHyperedgeBase.h"

#include <algorithm>
#include <vector>
#include <set>

#include "filtered_vector.h"

namespace hglib {

/**
 * UndirectedHyperedgeBase constructor
 *
 * @param id
 * @param vertices
 */
template <typename UndirectedVertex_t>
UndirectedHyperedgeBase<UndirectedVertex_t>::
UndirectedHyperedgeBase(
        HyperedgeIdType id,
        std::shared_ptr<GraphElementContainer<UndirectedVertex_t*>> vertices) :
        id_(id), vertices_(vertices) {}


/// Copy constructor
template <typename UndirectedVertex_t>
UndirectedHyperedgeBase<UndirectedVertex_t>::
UndirectedHyperedgeBase(const UndirectedHyperedgeBase& hyperedge) :
        id_(hyperedge.id_) {
  vertices_ = create_filtered_vector<UndirectedVertex_t*>();
}

/// Get Id of the hyperedge
template <typename UndirectedVertex_t>
HyperedgeIdType UndirectedHyperedgeBase<UndirectedVertex_t>::
id() const {
  return id_;
}

/// Print the hyperedge
template <typename UndirectedVertex_t>
void UndirectedHyperedgeBase<UndirectedVertex_t>::
print(std::ostream& out) const {
  static_cast<typename UndirectedVertex_t::HyperedgeType const&>(
          *this).print(out);
}

/* ****************************************************************************
 * ****************************************************************************
 *                        Protected methods
 * ****************************************************************************
 */
/// Add a pointer to vertex to the vertex list
template <typename UndirectedVertex_t>
void UndirectedHyperedgeBase<UndirectedVertex_t>::
addVertex(UndirectedVertex_t* vertex) {
  vertices_->push_back(vertex);
}

/// Remove a vertex from the hyperedge.
template <typename UndirectedVertex_t>
void UndirectedHyperedgeBase<UndirectedVertex_t>::
removeVertex(const UndirectedVertex_t& vertexToBeDeleted) {
  // We suppose that there is at most one occurrence of the given vertex
  // in the vertex list.
  auto newEnd = std::remove(vertices_->data(),
                            vertices_->data() + vertices_->size(),
                            &vertexToBeDeleted);
  // new end is different from the old one
  if (newEnd != (vertices_->data() + vertices_->size())) {
    vertices_->resize(vertices_->size() - 1);
  }
}

/// Compare if this hyperedge has the provided vertex names
template <typename UndirectedVertex_t>
bool UndirectedHyperedgeBase<UndirectedVertex_t>::
compare(const std::vector<const UndirectedVertex_t*>& vertices) const {
  if (vertices_->size() != vertices.size()) {
    return false;
  }
  // Compare names of vertices
  std::set<VertexIdType> vertexIds;
  for (auto& vertex : vertices) {
    vertexIds.insert(vertex->id());
  }
  for (const auto& vertex : *(vertices_.get())) {
    vertexIds.erase(vertex->id());
  }
  return vertexIds.empty();
}

/// Assign an ID to the hyperedge
template <typename UndirectedVertex_t>
void UndirectedHyperedgeBase<UndirectedVertex_t>::
setId(const HyperedgeIdType& id) {
  id_ = id;
}

/// Print vertex names of the hyperedge
template <typename UndirectedVertex_t>
void UndirectedHyperedgeBase<UndirectedVertex_t>::
printVertices(std::ostream& out) const {
  // print vertices
  out << '{';
  const char *padding = "";
  for (const auto &vertex : *(vertices_.get())) {
    out << padding << *vertex;
    padding = ", ";
  }
  out << '}';
}
}  // namespace hglib
