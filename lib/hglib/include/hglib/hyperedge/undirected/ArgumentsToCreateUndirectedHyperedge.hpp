/*Copyright (C) <2016-2017>  David Parsons, Martin Wannagat

This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU Affero General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/
//
// Created by Martin Wannagat on 03.10.17.
//

#include "ArgumentsToCreateUndirectedHyperedge.h"

namespace hglib {

/**
 * Constructor
 *
 * @param tailAndHeadIds
 * @param specificAttributes
 */
template<typename Hyperedge_t>
ArgumentsToCreateUndirectedHyperedge<Hyperedge_t>::
ArgumentsToCreateUndirectedHyperedge(
        const VerticesIds& verticesIds,
        const typename Hyperedge_t::SpecificAttributes& specificAttributes) :
        verticesIds_(verticesIds),
        specificAttributes_(specificAttributes) {}


/**
 * Get vertices ids
 *
 * @return vector<VertexIdType>
 */
template<typename Hyperedge_t>
const typename ArgumentsToCreateUndirectedHyperedge<Hyperedge_t>::VerticesIds&
ArgumentsToCreateUndirectedHyperedge<Hyperedge_t>::
verticesIds() const {
  return verticesIds_;
}


/**
 *
 *
 * @return
 */
template<typename Hyperedge_t>
const typename Hyperedge_t::SpecificAttributes&
ArgumentsToCreateUndirectedHyperedge<Hyperedge_t>::
specificAttributes() const {
  return specificAttributes_;
}


/**
 * Set vertices ids
 *
 * @param verticesIds
 */
template<typename Hyperedge_t>
void ArgumentsToCreateUndirectedHyperedge<Hyperedge_t>::
setVerticesIds(const typename ArgumentsToCreateUndirectedHyperedge<
        Hyperedge_t>::VerticesIds& verticesIds) {
  verticesIds_ = verticesIds;
}


/**
 * Set arguments that are specific to the hyperedge type (Hyperedge_t)
 *
 * @param args
 */
template<typename Hyperedge_t>
void ArgumentsToCreateUndirectedHyperedge<Hyperedge_t>::
setSpecificArguments(const typename Hyperedge_t::SpecificAttributes& args) {
  specificAttributes_ = args;
}
}  // namespace hglib
