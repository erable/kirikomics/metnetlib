/*Copyright (C) <2016-2017>  David Parsons, Martin Wannagat

This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU Affero General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/
//
// Created by Martin Wannagat on 19.06.17.
//

#ifndef HGLIB_DIRECTEDHYPEREDGE_H
#define HGLIB_DIRECTEDHYPEREDGE_H

#include <memory>
#include <tuple>
#include <utility>
#include <vector>
#include <string>

#include "DirectedHyperedgeBase.h"
#include "ArgumentsToCreateDirectedHyperedge.h"
#include "hglib/vertex/directed/DirectedVertex.h"
#include "hglib/utils/types.h"

namespace hglib {

/**
 * Hyperarc class
 */
class DirectedHyperedge final :
        public DirectedHyperedgeBase<DirectedVertex<DirectedHyperedge>>{
  template <template <typename> class DirectedVertex_t, typename Hyperarc_t,
          typename VertexProperty, typename EdgeProperty,
          typename HypergraphProperty>
  friend class DirectedHypergraph;
  template <template <typename> class DirectedVertex_t, typename Hyperarc_t,
          typename VertexProperty, typename EdgeProperty,
          typename HypergraphProperty>
  friend class DirectedHypergraphInterface;
  template <template <typename> class Vertex_t, typename Hyperarc_t,
        typename VertexProperty, typename EdgeProperty,
        typename HypergraphProperty>
  friend class DirectedSubHypergraph;

  /// Alias for the vertex type
  typedef DirectedVertex<DirectedHyperedge> Vertex_t;

 public:
  /**
   * SpecificAttributes class
   *
   * A basic hyperarc has no specific arguments, but the class is needed to
   * call the addHyperarc function of a Directed(Sub)Hypergraph.
   */
  class SpecificAttributes {};

 protected:
  /// Hyperarc constructor
  DirectedHyperedge(
          int id,
          std::shared_ptr<GraphElementContainer<Vertex_t*>> tails,
          std::shared_ptr<GraphElementContainer<Vertex_t*>> heads,
          const SpecificAttributes& specificAttributes);
  /// Copy constructor
  DirectedHyperedge(const DirectedHyperedge& hyperarc);
  /// Get arguments to create this Hyperarc
  ArgumentsToCreateDirectedHyperedge<
          DirectedHyperedge> argumentsToCreateHyperarc() const;

 public:
  /// Print the hyperarc
  void print(std::ostream& out) const;

 protected:
  /// Specific attributes (none)
  SpecificAttributes specificAttributes_;
};
}  // namespace hglib

#endif  // HGLIB_DIRECTEDHYPEREDGE_H
