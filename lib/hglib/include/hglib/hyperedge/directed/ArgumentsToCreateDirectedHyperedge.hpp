/*Copyright (C) <2016-2017>  David Parsons, Martin Wannagat

This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU Affero General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/
//
// Created by Martin Wannagat on 20.09.17.
//

#include "ArgumentsToCreateDirectedHyperedge.h"

namespace hglib {

/**
 * Constructor
 *
 * @param tailAndHeadIds
 * @param specificAttributes
 */
template<typename Hyperarc_t>
ArgumentsToCreateDirectedHyperedge<Hyperarc_t>::
ArgumentsToCreateDirectedHyperedge(
        const TailAndHeadIds& tailAndHeadIds,
        const typename Hyperarc_t::SpecificAttributes& specificAttributes) :
        tailAndHeadIds_(tailAndHeadIds),
        specificAttributes_(specificAttributes) {}


/**
 * Get tail and head ids
 *
 * @return pair<vector<VertexIdType>
 */
template<typename Hyperarc_t>
const typename ArgumentsToCreateDirectedHyperedge<Hyperarc_t>::TailAndHeadIds&
ArgumentsToCreateDirectedHyperedge<Hyperarc_t>::
tailAndHeadIds() const {
  return tailAndHeadIds_;
}

template<typename Hyperarc_t>
const typename Hyperarc_t::SpecificAttributes&
ArgumentsToCreateDirectedHyperedge<Hyperarc_t>::
specificAttributes() const {
  return specificAttributes_;
}


/**
 * Set tail and head ids
 *
 * @param tailHeadIds
 */
template<typename Hyperarc_t>
void ArgumentsToCreateDirectedHyperedge<Hyperarc_t>::
setTailsAndHeadIds(const typename ArgumentsToCreateDirectedHyperedge<
                Hyperarc_t>::TailAndHeadIds& tailHeadIds) {
  tailAndHeadIds_.first = tailHeadIds.first;
  tailAndHeadIds_.second = tailHeadIds.second;
}


/**
 * Set arguments that are specific to the hyperarc type (Hyperarc_t)
 *
 * @param args
 */
template<typename Hyperarc_t>
void ArgumentsToCreateDirectedHyperedge<Hyperarc_t>::
setSpecificArguments(const typename Hyperarc_t::SpecificAttributes& args) {
  specificAttributes_ = args;
}
}  // namespace hglib
