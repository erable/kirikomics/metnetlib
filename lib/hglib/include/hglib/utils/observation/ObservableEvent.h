/*Copyright (C) <2016-2017>  David Parsons, Martin Wannagat

This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU Affero General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/
//
// Created by Martin Wannagat on 31.08.17.
//

#ifndef HGLIB_OBSERVABLEEVENT_H
#define HGLIB_OBSERVABLEEVENT_H

#include "hglib/utils/types.h"

namespace hglib {

/**
 * Events that can be observed.
 */
enum ObservableEvent {
    VERTEX_REMOVED, HYPEREDGE_REMOVED, RESET_VERTEX_IDS, RESET_EDGE_IDS
};


/**
 * Struct to provide to the observer the arguments needed to remove
 * a vertex.
 */
struct Remove_vertex_args {
    VertexIdType vertexId_;
    bool removeImpliedHyperarcs_;
};
}  // namespace hglib
#endif  // HGLIB_OBSERVABLEEVENT_H
