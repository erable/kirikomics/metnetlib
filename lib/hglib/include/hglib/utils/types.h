/*Copyright (C) <2016-2017>  David Parsons, Martin Wannagat

This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU Affero General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/
//
// Created by Martin Wannagat on 18.09.17.
//

#ifndef HGLIB_TYPES_H
#define HGLIB_TYPES_H

#include <sys/types.h>

#include "filtered_vector.h"

namespace hglib {
  /// Alias for container wherein vertices, hyperedges/arcs etc. are stored
  template<typename T>
  using GraphElementContainer = FilteredVector<T>;

  /*/// Alias for container wherein vertices, hyperedges/arcs etc. are stored
  template <typename GraphElementContainer>
  using GraphElementContainer_shared_ptr = std::shared_ptr<GraphElementContainer>;*/

  /// Alias for the vertex Id type
  using VertexIdType = u_int16_t;

  /// Alias for the hyperedge Id type
  using HyperedgeIdType = u_int16_t;

  /// @cond
  /**
   * Empty default property struct.
   */
  struct emptyProperty{};
/// @endcond

  /**
   * Flag, used to distinguish if the vertices are given by id or by name
   * in the member function addHyperarc of a DirectedHypergraph.
   */
  constexpr u_int8_t NAME = 0;

}  // namespace hglib
#endif  // HGLIB_TYPES_H
