/*Copyright (C) <2016-2017>  David Parsons, Martin Wannagat

This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU Affero General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/
//
// Created by Martin Wannagat on 25.07.17.
//

#include "hglib/hypergraph/directed/DirectedHypergraph.h"
#include "hglib/hyperedge/directed/NamedDirectedHyperedge.h"
#include <hglib/hyperedge/undirected/NamedUndirectedHyperedge.h>

#include <iostream>
#include <fstream>
#include <string>
#include <utility>
#include <vector>
#include <sys/stat.h>

#include "gtest/gtest.h"

/**
 * Test fixture
 */
class Benchmark : public testing::Test {
protected:
  virtual void SetUp() {
    storedStreambuf_ = std::cerr.rdbuf();
    std::cerr.rdbuf(nullptr);
  }

  virtual void TearDown() {
    std::cerr.rdbuf(storedStreambuf_);
  }

private:
  std::streambuf* storedStreambuf_;
};


TEST_F(Benchmark, BenchmarkTest) {
  std::vector<unsigned int> nbVerticesList = {1000, 2000, 5000};
  std::vector<double> sparsityList = {0, 0.01, 0.05, 0.1, 0.15, 0.2, 0.25};
  int nbAccessPerItem(10000);
  std::string vertexPrefix("vertex_");
  std::string hyperarcPrefix("h_");

  // Get current directory
  char* cwd = getcwd( 0, 0 ) ; // **** microsoft specific ****
  std::string working_directory(cwd) ;
  std::free(cwd);
  std::string pattern = "hglib";
  std::string pathPrefix = working_directory.substr(0, working_directory.find(
          pattern) + pattern.length() + 1);

  // Check whether the directory exists and open the output file accordingly
  std::string outputDir("");
  struct stat sb;
  if (stat(pathPrefix.c_str(), &sb) == 0 && S_ISDIR(sb.st_mode)) {
    outputDir = pathPrefix;
  } else {
    outputDir = working_directory;
  }
  std::ofstream benchmarkFile;
  std::string outputFilename("benchmark.md");
  benchmarkFile.open(outputDir + outputFilename);
  std::cout << "Benchmark results will be printed to " << outputDir +
          outputFilename << std::endl;


  // Description of the table
  benchmarkFile << "# Benchmark for used container (hglib::GraphElementContainer)\n\n";
  benchmarkFile << "The columns in the below table correspond to the "
          "following:\n\n";
  benchmarkFile << "  * \\# vertices: Number of vertices that are added "
          "to the hypergraph at the beginning.\n";
  benchmarkFile << "  * sparsity: Percentage of vertices that are removed "
          "from the hypergraph.\n";
  benchmarkFile << "  * \\# hyperarcs: Number of hyperarcs. We first add as "
          "many hyperarcs as vertices with two random tail and two random "
          "head vertices. A hyperarc may be removed from the hypergraph when"
          " all its tail and head vertices were removed.\n";
  benchmarkFile << "  * time build hypergraph: Time elapsed to add/remove"
          " vertices and hyperarcs.\n";
  benchmarkFile << "  * time access all vertices/hyperarcs: Time elapsed to"
          " access " << nbAccessPerItem << " times all vertices and all"
          " hyperarcs that existed before the removal of vertices/hyperarcs."
          " This means that we may search for items that are not anymore in"
          " the data container.\n\n";
  benchmarkFile << "| # vertices | sparsity (in %) | # hyperarcs | time build "
          "hypergraph (sec) | time access all vertices/hyperarcs " <<
                nbAccessPerItem << " times (sec)|\n";
  benchmarkFile << "|---|---|---|---|---|\n";

  // run benchmark for given number of vertices and sparsity
  for (unsigned int nbVertices : nbVerticesList) {
    unsigned int nbHyperarcs(nbVertices);
    for (double sparsity : sparsityList) {
      // create the hypergraph
      clock_t startTime = clock();
      hglib::DirectedHypergraph<hglib::DirectedVertex,
              hglib::NamedDirectedHyperedge> g;
      // add n vertices
      for (unsigned int i = 0; i < nbVertices; ++i) {
        g.addVertex(vertexPrefix + std::to_string(i));
      }
      // add hyperarcs with two random tails and heads
      for (unsigned int i = 0; i < nbHyperarcs; ++i) {
        std::vector<std::string> tails, heads;
        tails.push_back(vertexPrefix + std::to_string(rand() % nbVertices));
        tails.push_back(vertexPrefix + std::to_string(rand() % nbVertices));
        heads.push_back(vertexPrefix + std::to_string(rand() % nbVertices));
        heads.push_back(vertexPrefix + std::to_string(rand() % nbVertices));
        g.addHyperarc({tails, heads},
                      hyperarcPrefix + std::to_string(i));
      }

      // remove vertices according to specified sparsity
      std::set<int> toBeRemovedVertices;
      while (toBeRemovedVertices.size() < (nbVertices * sparsity)) {
        toBeRemovedVertices.insert(rand() % nbVertices);
      }
      for (const auto &item : toBeRemovedVertices) {
        g.removeVertex(item);
      }

      clock_t endTime = clock();
      double timeBuildGraph = static_cast<double>(endTime - startTime) /
                              CLOCKS_PER_SEC;

      // access all vertices and hyperarcs n times
      startTime = clock();
      for (int nbRun = 0; nbRun < nbAccessPerItem; ++nbRun) {
        for (unsigned int i = 0; i < nbVertices; ++i) {
          try {
            g.vertexById(i);
          } catch (const std::out_of_range &e) {}
        }
        for (unsigned int i = 0; i < nbHyperarcs; ++i) {
          try {
            g.hyperarcById(i);
          } catch (const std::out_of_range &e) {}
        }
      }

      endTime = clock();
      double timeAccess = static_cast<double>(endTime - startTime) /
                          CLOCKS_PER_SEC;

      // output
      benchmarkFile << "| ";
      if (sparsity == 0.0) {
        benchmarkFile << g.nbVertices();
      }
      benchmarkFile << " | ";

      benchmarkFile << (sparsity * 100) << " | " << g.nbHyperarcs() << " | " <<
                timeBuildGraph << " | " << timeAccess
                << std::endl;
    }
  }
  benchmarkFile.close();
}
