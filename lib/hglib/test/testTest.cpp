#include "gtest/gtest.h"

#include "hglib.h"

class TestTest : public testing::Test {
 protected:
  virtual void SetUp() {
  }

  virtual void TearDown() {
  }

  int values[2] = {-1, 1};
};

TEST_F(TestTest, Test1) {
  ASSERT_EQ(values[1], hgl::max(values[0], values[1]));
}
