/*Copyright (C) <2016-2017>  David Parsons, Martin Wannagat

This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU Affero General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/
//
// Created by Martin Wannagat on 19.06.17.
//

#include "hglib/vertex/VertexBase.h"

namespace hglib {
/**
 * VertexBase constructor
 *
 * @param id Numerical identifier of the vertex
 * @param name Name of the vertex
 */
VertexBase::VertexBase(VertexIdType id, std::string name) :
        id_(id), name_(name) {}


/**
 * VertexBase constructor
 *
 * @param id Identifier of the vertex
 */
VertexBase::VertexBase(VertexIdType id) : id_(id),
                                          name_("v_" + std::to_string(id)) {}


/**
 * VertexBase copy constructor
 *
 * @param vertex Vertex to be copied
 */
VertexBase::VertexBase(const VertexBase& vertex) : id_(vertex.id_),
                                                   name_(vertex.name_) {}


/**
 * Assign an ID to the vertex
 *
 * @param id Id to be assigned
 */
void VertexBase::
setId(const VertexIdType& id) {
  id_ = id;
}


/**
 * Print the vertex
 *
 * @param out std::ostream
 */
void VertexBase::print(std::ostream& out) const {
  out << name_;
}
}  // namespace hglib
