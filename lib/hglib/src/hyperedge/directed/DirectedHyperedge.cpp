/*Copyright (C) <2016-2017>  David Parsons, Martin Wannagat

This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU Affero General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/
//
// Created by Martin Wannagat on 19.06.17.
//

#include "hglib/hyperedge/directed/DirectedHyperedge.h"

namespace hglib {

/**
 * Hyperarc constructor
 *
 * @param id Identifier of the hyperarc
 * @param tails tail vertices of the hyperarc
 * @param heads head vertices of the hyperarc
 */
DirectedHyperedge::DirectedHyperedge(
        int id,
        std::shared_ptr<GraphElementContainer<Vertex_t*>> tails,
        std::shared_ptr<GraphElementContainer<Vertex_t*>> heads,
        const SpecificAttributes& specificAttributes) :
        DirectedHyperedgeBase(id, tails, heads),
        specificAttributes_(specificAttributes) {}


/**
 * Copy constructor
 *
 * @param hyperarc Hyperarc to be copied
 */
DirectedHyperedge::DirectedHyperedge(const DirectedHyperedge& hyperarc) :
        DirectedHyperedgeBase(hyperarc),
        specificAttributes_(hyperarc.specificAttributes_) {}


/**
 * Get arguments to create this Hyperarc
 *
 * @return tuple<pair<vector<string>, vector<string>>>
 */
ArgumentsToCreateDirectedHyperedge<DirectedHyperedge> DirectedHyperedge::
argumentsToCreateHyperarc() const {
  std::vector<hglib::VertexIdType> tails, heads;
  for (const auto vertex : *(tails_.get())) {
    tails.push_back(vertex->id());
  }
  for (const auto vertex : *(heads_.get())) {
    heads.push_back(vertex->id());
  }
  return ArgumentsToCreateDirectedHyperedge<DirectedHyperedge>(
          std::make_pair(tails, heads), specificAttributes_);
}


/**
 * Print the hyperarc
 *
 * Print the hyperarc in the following format:
 * Tail_1 + ... + Tail_n -> Head_1 + ... + Head_m
 *
 * @param out std::ostream
 */
void DirectedHyperedge::
print(std::ostream& out) const {
  DirectedHyperedgeBase<Vertex_t>::printTailsAndHeads(out);
}
}  // namespace hglib
