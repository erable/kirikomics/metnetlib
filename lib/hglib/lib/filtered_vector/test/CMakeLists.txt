# ============================================================================
# Include googletest
# ============================================================================
add_subdirectory(gtest/gtest-1.7.0)

# ============================================================================
# Where to look for includes in gtest's include dir and in the project's src dir
# ============================================================================
include_directories(${gtest_SOURCE_DIR}/include)


# ============================================================================
# Make STDC MACROS available (for fixed width integers)
# ============================================================================
add_definitions(-D__STDC_FORMAT_MACROS -D__STDC_CONSTANT_MACROS)


# ============================================================================
# Use C++14
# Set compile flags for all subsequent targets
# ============================================================================
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -Wextra -std=c++1y")

# ============================================================================
# Require cpplint to check source files against Google code style
# ============================================================================
find_package(Cpplint) # sets CPPLINT_FOUND and CPPLINT_PROGRAM

# ============================================================================
# Add tests
# ============================================================================
# Libraries to link unit tests against
set(test_libs gtest_main)

# ============================================================================
# check source files against Google code style
# ============================================================================
if (${CPPLINT_FOUND})
    set(SOURCE_FILES
            ${PROJECT_SOURCE_DIR}/include/filtered_vector.h
            ${PROJECT_SOURCE_DIR}/include/filtered_vector/create_filtered_vector.h
            ${PROJECT_SOURCE_DIR}/include/filtered_vector/Filter.h
            ${PROJECT_SOURCE_DIR}/include/filtered_vector/Filter.hpp
            ${PROJECT_SOURCE_DIR}/include/filtered_vector/FilteredVector.h
            ${PROJECT_SOURCE_DIR}/include/filtered_vector/FilteredVector.hpp
            ${PROJECT_SOURCE_DIR}/include/filtered_vector/FilteredVectorBase.h
            ${PROJECT_SOURCE_DIR}/include/filtered_vector/FilteredVectorBase.hpp
            ${PROJECT_SOURCE_DIR}/include/filtered_vector/FilteredVectorDecorator.h
            ${PROJECT_SOURCE_DIR}/include/filtered_vector/FilteredVectorDecorator.hpp
            ${PROJECT_SOURCE_DIR}/include/filtered_vector/FilteredVectorIterator.h
            ${PROJECT_SOURCE_DIR}/include/filtered_vector/FilteredVectorIterator.hpp
            ${PROJECT_SOURCE_DIR}/include/filtered_vector/NullptrFilter.h
            ${PROJECT_SOURCE_DIR}/include/filtered_vector/NullptrFilter.hpp
            ${PROJECT_SOURCE_DIR}/include/filtered_vector/WhitelistFilter.h
            ${PROJECT_SOURCE_DIR}/include/filtered_vector/WhitelistFilter.hpp
            ${PROJECT_SOURCE_DIR}/test/testFilteredVector.cpp
            ${PROJECT_SOURCE_DIR}/test/testFilteredVectorIterator.cpp)

    foreach (FILE IN LISTS SOURCE_FILES)
        get_filename_component(FILENAME ${FILE} NAME)
        set(TEST_RUNNER cpplint_${FILENAME})
        set(CPPLINT_RUNNERS ${CPPLINT_RUNNERS} ${TEST_RUNNER})
        add_custom_target(${TEST_RUNNER}
                COMMAND cpplint
                # filter runtime/explicit: requires explicit one argument constructor
                # filter build/header_guard: requires header guards with full path
                # filter build/include: to avoid the following error
                # src/DirectedEdge.cpp should include its header file
                # src/DirectedEdge.h
                --filter=-runtime/explicit,-build/header_guard,-build/include,-readability/alt_tokens,-readability/casting
                ${FILE}
                DEPENDS ${FILE})
    endforeach (FILE)
endif (${CPPLINT_FOUND})

# ============================================================================
# Unit tests with or without valgrind (depends on system)
# ============================================================================
# List unit tests
set(TESTS testFilteredVector testFilteredVectorIterator)

# valgrind is not yet supported on mac os and we do not consider Windows for now
if(UNIX AND NOT APPLE)
    find_package(Valgrind REQUIRED) # sets VALGRIND_FOUND to TRUE
else()
    message("No check for memory leaks on this system.")
endif(UNIX AND NOT APPLE)

# Create a runner for each unit test
foreach (TEST IN LISTS TESTS)
    set(TEST_RUNNER  run_${TEST})
    set(TEST_RUNNERS ${TEST_RUNNERS} ${TEST_RUNNER})
    add_executable(${TEST} ${TEST}.cpp)
    target_link_libraries(${TEST} ${test_libs})
    if(VALGRIND_FOUND) # with valgrind
        # command 1: run valgrind on unit test file
        # command 2: run shell script to parse valgrind output for memory leaks
        # -> return 1 if a memory leak is detected
        # command 3: delete valgrind log file (done only if there is no memory leak)
        add_custom_target(${TEST_RUNNER}
                COMMAND valgrind
                --leak-check=full
                --show-reachable=yes
                --log-file=${CMAKE_CURRENT_BINARY_DIR}/valgrind.log.${TEST}
                ${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/${TEST}
                COMMAND ${CMAKE_SOURCE_DIR}/scripts/./parseValgrindOutput.sh
                ${CMAKE_CURRENT_BINARY_DIR}/valgrind.log.${TEST}
                COMMAND rm ${CMAKE_CURRENT_BINARY_DIR}/valgrind.log.${TEST}
                DEPENDS ${TEST}
                )
    else() # without valgrind
        add_custom_target(${TEST_RUNNER}
                COMMAND ${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/${TEST}
                DEPENDS ${TEST})
    endif(VALGRIND_FOUND)
endforeach(TEST)

# ============================================================================
# Doxygen
# ============================================================================
find_package(Doxygen)
if(DOXYGEN_FOUND)
    SET(DOXYGEN_LOGFILE ${CMAKE_CURRENT_BINARY_DIR}/doxygen.log)
    configure_file(${PROJECT_SOURCE_DIR}/docs/Doxyfile.in ${PROJECT_SOURCE_DIR}/docs/Doxyfile @ONLY)
    set(DOC_RUNNERS ${DOC_RUNNERS} doxygen)
    add_custom_target(doxygen
            COMMAND ${CMAKE_SOURCE_DIR}/scripts/./runDoxygen.sh
            ${PROJECT_SOURCE_DIR}/docs/Doxyfile
            ${DOXYGEN_LOGFILE}
            WORKING_DIRECTORY ${PROJECT_SOURCE_DIR}/docs
            COMMENT "Generating API documentation with Doxygen" VERBATIM
            )
endif(DOXYGEN_FOUND)

# Create meta-targets for all tests, unit tests and integration tests
add_custom_target(check DEPENDS utest cpplintTest doxygenTest)
add_custom_target(utest DEPENDS ${TEST_RUNNERS})
add_custom_target(cpplintTest DEPENDS ${CPPLINT_RUNNERS})
add_custom_target(doxygenTest DEPENDS ${DOC_RUNNERS})
#add_custom_target(itest ${CMAKE_CTEST_COMMAND} DEPENDS filtered_vector_lib)
