/*Copyright (C) <2016-2017>  David Parsons, Martin Wannagat

This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU Affero General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/
//
// Created by Martin Wannagat on 13.10.17.
//

#ifndef FILTERED_VECTOR_NULLPTR_FILTER_H
#define FILTERED_VECTOR_NULLPTR_FILTER_H

#include "Filter.h"

/**
 * Filter class that filter out nullptrs.
 *
 * The member function filter_out(T item) returns true if the given element
 * item is a nullptr, false otherwise. Thus, iterating over a filtered_vector
 * skips nullptrs.
 *
 * @tparam T Type of elements in a filtered_vector.
 */
template<typename T>
class NullptrFilter : public Filter<T> {
  /* **************************************************************************
   * **************************************************************************
   *                     Constructors/Destructor
   * **************************************************************************
   * *************************************************************************/
 public:
  /// Constructor
  NullptrFilter() = default;
  /// Copy constructor
  NullptrFilter(const NullptrFilter&) = default;
  /// Destructor
  virtual ~NullptrFilter() = default;

  /* **************************************************************************
   * **************************************************************************
   *                           Operators
   * **************************************************************************
   * *************************************************************************/
  /// Assignment operator
  NullptrFilter& operator=(const NullptrFilter&) = default;

  /* **************************************************************************
   * **************************************************************************
   *                        Public functions
   * **************************************************************************
   * *************************************************************************/
 public:
  /// Filter function
  bool filter_out(const T& item) const override;
};

#include "NullptrFilter.hpp"
#endif  // FILTERED_VECTOR_NULLPTR_FILTER_H
