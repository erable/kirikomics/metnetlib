/*Copyright (C) <2016-2017>  David Parsons, Martin Wannagat

This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU Affero General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/
//
// Created by Martin Wannagat on 12.10.17.
//

#ifndef FILTERED_VECTOR_FILTER_H
#define FILTERED_VECTOR_FILTER_H

/**
 * Base filter class.
 *
 * A filter is applied to a filtered_vector such that one can iterate over a
 * filtered_vector in different manners (according to the filter). The filter
 * decides which elements of the filtered_vector are skipped (in the
 * operator++() of the filtered_vector::iterator) via the filter_out member
 * function.
 * Items of type T (template parameter) can be added to (addItem(const T&)),
 * and removed from (removeItem(const T&)) the filter.
 *
 * This present class is a non-filter, it doesn't skip any item.
 * In other words, filter::filter_out(T) always returns false.
 *
 * @tparam T Type of elements in a filtered_vector.
 */
template<typename T>
class Filter {
  /* **************************************************************************
   * **************************************************************************
   *                           Constructors/Destructor
   * **************************************************************************
   * *************************************************************************/
 public:
  /// Constructor
  Filter() = default;
  /// Copy constructor
  Filter(const Filter&) = default;
  /// Destructor
  virtual ~Filter() = default;

  /* **************************************************************************
   * **************************************************************************
   *                           Operators
   * **************************************************************************
   * *************************************************************************/
  /// Assignment operator
  Filter& operator=(const Filter&) = default;

  /* **************************************************************************
   * **************************************************************************
   *                           Public functions
   * **************************************************************************
   * *************************************************************************/
 public:
  /// Filter function (return always true)
  virtual bool filter_out(const T&) const;
  /// Add item to the whitelist
  virtual void removeFilteredOutItem(const T&) {}
  /// Remove item from the whitelist
  virtual void addFilterOutItem(const T&) {}
};

#include "Filter.hpp"
#endif  // FILTERED_VECTOR_FILTER_H
