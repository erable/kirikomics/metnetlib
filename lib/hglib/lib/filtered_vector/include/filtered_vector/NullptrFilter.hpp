/*Copyright (C) <2016-2017>  David Parsons, Martin Wannagat

This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU Affero General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/
//
// Created by Martin Wannagat on 13.10.17.
//

#include "NullptrFilter.h"


/**
 * Filter function
 *
 * Filter function returns true if given element is a nullptr,
 * false otherwise.
 *
 * @param item Element to be checked
 * @return true if given element is a nullptr, false otherwise.
 */
template<typename T>
bool NullptrFilter<T>::
filter_out(const T& item) const {
  return item == nullptr;
}
