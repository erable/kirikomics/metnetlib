/*Copyright (C) <2016-2017>  David Parsons, Martin Wannagat

This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU Affero General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/
//
// Created by Martin Wannagat on 12.10.17.
//

#ifndef FILTERED_VECTOR_WHITELIST_FILTER_H
#define FILTERED_VECTOR_WHITELIST_FILTER_H

#include "Filter.h"

/**
 * Filter class that filter out elements that are not white-listed.
 *
 * The template parameter T is expected to be a pointer to a type which
 * possess a member function id().
 * The whitelist is implemented as a std::vector that may contain nullptrs.
 * The filter_out(T item) member function returns true if the whitelist entry
 * at a given position is a nullptr, or if the given position is greater or
 * equal to the size of the whitelist vector, false otherwise. The position
 * is determined by the Id of the function parameter item (the function
 * T::id() is called).
 *
 * This filter class allows to determine in constant time if a given element
 * is on the white list.
 *
 * @tparam T Type of elements in a filtered_vector.
 */
template<typename T>
class WhitelistFilter : public Filter<T>{
  // Check that the template parameter is a pointer
  static_assert(std::is_pointer<T>::value, "The template parameter is expected"
          " to be a pointer");

  /* **************************************************************************
   * **************************************************************************
   *                     Constructors/Destructor
   * **************************************************************************
   * *************************************************************************/
 public:
  /// Constructor
  explicit WhitelistFilter(const std::vector<T>&);
  /// Constructor
  explicit WhitelistFilter();
  /// Copy constructor
  WhitelistFilter(const WhitelistFilter&);
  /// Destructor
  virtual ~WhitelistFilter() = default;

  /* **************************************************************************
   * **************************************************************************
   *                           Operators
   * **************************************************************************
   * *************************************************************************/
 public:
  /// Assignment operator
  WhitelistFilter& operator=(const WhitelistFilter&);

  /* **************************************************************************
   * **************************************************************************
   *                           Public functions
   * **************************************************************************
   * *************************************************************************/
 public:
  /// Filter function
  bool filter_out(const T& item) const override;
  /// Add item to the end of the whitelist
  void removeFilteredOutItem(const T& item) override;
  /// Remove item from the whitelist
  void addFilterOutItem(const T& item) override;

  /* **************************************************************************
   * **************************************************************************
   *                              Data members
   * **************************************************************************
   * *************************************************************************/
 protected:
  /// Whitelist implemented as a filtered_vector
  std::vector<T> whitelist_;
};

#include "WhitelistFilter.hpp"
#endif  // FILTERED_VECTOR_WHITELIST_FILTER_H
