/*Copyright (C) <2016-2017>  David Parsons, Martin Wannagat

This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU Affero General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/
//
// Created by Martin Wannagat on 12.10.17.
//

#include "FilteredVectorIterator.h"

/// Constructor
template <typename T, typename Container>
FilteredVectorIterator<T, Container>::
FilteredVectorIterator(const pointer& ptr,
                       std::shared_ptr<const Container> cont) :
        current_(ptr), cont_(cont) {}

/// Equal operator
template <typename T, typename Container>
bool FilteredVectorIterator<T, Container>::
operator==(const FilteredVectorIterator& other) const noexcept {
  return current_ == other.current_;
}

/// Unequal operator
template <typename T, typename Container>
bool FilteredVectorIterator<T, Container>::
operator!=(const FilteredVectorIterator& other) const noexcept {
  return current_ != other.current_;
}

/// Dereference operator
template <typename T, typename Container>
typename FilteredVectorIterator<T, Container>::reference
FilteredVectorIterator<T, Container>::
operator*() const noexcept {
  return *current_;
}

/// Operator->
template <typename T, typename Container>
typename FilteredVectorIterator<T, Container>::pointer
FilteredVectorIterator<T, Container>::
operator->() const noexcept {
  return current_;
}

/// Increment operator
template <typename T, typename Container>
FilteredVectorIterator<T, Container>& FilteredVectorIterator<T, Container>::
operator++() noexcept {
  do {
    ++current_;
  } while (current_ != cont_->end().base() && cont_->filter_out(*current_));
  return *this;
}

/// Increment operator
template <typename T, typename Container>
FilteredVectorIterator<T, Container> FilteredVectorIterator<T, Container>::
operator++(int) noexcept {
  auto ret = *this;
  ++(*this);
  return ret;
}

/// Decrement operator
/**
 * @throws std::out_of_range if trying to go 'past' the beginning,
 * i.e. if the iterator is positioned on the first non-null value in the
 * container
 * @return filtered_vector_iterator
 */
template <typename T, typename Container>
FilteredVectorIterator<T, Container>& FilteredVectorIterator<T, Container>::
operator--() noexcept(false) {
  do {
    if (current_ == cont_->data()) {
      throw std::out_of_range("Trying to reach before container's beginning");
    }
    --current_;
  } while (cont_->filter_out(*current_));
  return *this;
}

/// Decrement operator
/**
 * @throws std::out_of_range if trying to go 'past' the beginning,
 * i.e. if the iterator is positioned on the first non-null value in the
 * container
 * @return filtered_vector_iterator
 */
template <typename T, typename Container>
FilteredVectorIterator<T, Container> FilteredVectorIterator<T, Container>::
operator--(int) noexcept(false) {
  auto ret = *this;
  --(*this);
  return ret;
}

/// Arithmetic operator
template<typename T, typename Container>
FilteredVectorIterator<T, Container> FilteredVectorIterator<T, Container>::
operator+(difference_type n) noexcept {
  auto ret = *this;
  while (n--) {
    ++ret;
  }
  return ret;
}

/// Arithmetic operator
template<typename T, typename Container>
FilteredVectorIterator<T, Container> FilteredVectorIterator<T, Container>::
operator-(difference_type n) noexcept {
  auto ret = *this;
  while (n--) {
    --ret;
  }
  return ret;
}

/// Arithmetic operator
template<typename T, typename Container>
typename FilteredVectorIterator<T, Container>::difference_type
FilteredVectorIterator<T, Container>::
operator-(FilteredVectorIterator<T, Container> rhs) noexcept {
  return current_ - rhs.current_;
}

/// Inequality comparison
template<typename T, typename Container>
bool FilteredVectorIterator<T, Container>::
operator<(FilteredVectorIterator<T, Container> rhs) noexcept {
  return current_ < rhs.current_;
}

/// Inequality comparison
template<typename T, typename Container>
bool FilteredVectorIterator<T, Container>::
operator>(FilteredVectorIterator<T, Container> rhs) noexcept {
  return current_ > rhs.current_;
}

/// Inequality comparison
template<typename T, typename Container>
bool FilteredVectorIterator<T, Container>::
operator<=(FilteredVectorIterator<T, Container> rhs) noexcept {
  return current_ <= rhs.current_;
}


/// Inequality comparison
template<typename T, typename Container>
bool FilteredVectorIterator<T, Container>::
operator>=(FilteredVectorIterator<T, Container> rhs) noexcept {
  return current_ >= rhs.current_;
}

/// Compound assignment operations +=
template <typename T, typename Container>
FilteredVectorIterator<T, Container>& FilteredVectorIterator<T, Container>::
operator+=(difference_type n) noexcept {
  current_ += n;
  return *this;
}

/// Compound assignment operations -=
template <typename T, typename Container>
FilteredVectorIterator<T, Container>& FilteredVectorIterator<T, Container>::
operator-=(difference_type n) noexcept {
  current_ -= n;
  return *this;
}

/// Offset dereference operator
template <typename T, typename Container>
typename FilteredVectorIterator<T, Container>::reference
FilteredVectorIterator<T, Container>::
operator[](difference_type n) const noexcept {
  return current_[n];
}

/// Get pointer to current element
template <typename T, typename Container>
const typename FilteredVectorIterator<T, Container>::pointer&
FilteredVectorIterator<T, Container>::
base() const noexcept {
  return current_;
}

/// Get pointer to constainer
template <typename T, typename Container>
std::shared_ptr<const Container> FilteredVectorIterator<T, Container>::
getContainerPtr() const noexcept {
  return cont_;
}
