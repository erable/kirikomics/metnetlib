/*Copyright (C) <2016-2017>  David Parsons, Martin Wannagat

This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU Affero General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/
//
// Created by Martin Wannagat on 01.12.17.
//

#include <set>

#include "gtest/gtest.h"

#include "metnetlib.h"

class TestMinPrecursorSets : public testing::Test {
 protected:
  virtual void SetUp() {
    storedStreambuf_ = std::cerr.rdbuf();
    std::cerr.rdbuf(nullptr);
  }

  virtual void TearDown() {
    std::cerr.rdbuf(storedStreambuf_);
  }

 private:
  std::streambuf* storedStreambuf_;
};

void print_preprocessed_data(
    const metnetlib::MinimalPrecursorSetsInput<
        metnetlib::MetabolicNetwork<metnetlib::Compound,
                                    metnetlib::Reaction>>& input,
    const metnetlib::MetabolicNetwork<metnetlib::Compound,
                                  metnetlib::Reaction>& network) {
  std::cout << "\n================================================\n";
  std::cout << "After the preprocessing your network contains "
            << network.nbCompounds() << " compounds, "
            << network.nbReactions() << " reactions, "
            << network.nbReversibleReactions()
            << " of them ";
  std::cout << (network.nbReversibleReactions() > 1 ? "are" : "is");
  std::cout << " reversible.\n";

  // Print targets
  std::cout << "The set of targets: ";
  auto itTargets = input.targetsBegin();
  auto endTargets = input.targetsEnd();
  const char *padding = "";
  for (; itTargets != endTargets; ++itTargets) {
    std::cout << padding << network.compoundById(
        *itTargets)->sbmlId();
    padding = ", ";
  }
  // Print sources
  std::cout << "\nThe set of sources: ";
  auto itSources = input.userDefinedSourcesBegin();
  auto endSource = input.userDefinedSourcesEnd();
  padding = "";
  for (; itSources != endSource; ++itSources) {
    std::cout << padding << network.compoundById(
        *itSources)->sbmlId();
    padding = ", ";
  }

  // Print bootstrap compounds
  std::cout << "\nThe set of bootstrap compounds: ";
  auto itBootstrap = input.bootstrapCompoundsBegin();
  auto endBootstrap = input.bootstrapCompoundsEnd();
  padding = "";
  for (; itBootstrap != endBootstrap; ++itBootstrap) {
    std::cout << padding << network.compoundById(
        *itBootstrap)->sbmlId();
    padding = ", ";
  }
  std::cout << "\n================================================\n";
}

#ifdef USE_CPLEX
TEST_F(TestMinPrecursorSets, Test_minimal_precursor_sets) {
  using MetNet = metnetlib::MetabolicNetwork<>;

  // Read a valid Sbml file
  auto metNet = metnetlib::SbmlFileReader<>::readFromFile(
      "data/metabolicNetworks/valid/minPS_1.xml");
  ASSERT_EQ(metNet->nbCompartments(), (size_t) 1);
  ASSERT_EQ(metNet->nbCompounds(), (size_t) 20);
  ASSERT_EQ(metNet->nbReactions(), (size_t) 17);

  // Define input
  // Declare targets
  std::set<metnetlib::CompoundIdType> targets;
  targets.insert(metNet->compoundBySbmlId("T")->id());
  // Declare sources (empty)
  std::set<metnetlib::CompoundIdType> sources;
  // Declare bootstrap compounds (empty)
  std::set<metnetlib::CompoundIdType> bootstrapCompounds;
  // Parameter flags
  bool considerTopologicalSources(true);
  bool considerBoundaryCompoundsAsSources(false);
  bool addFactoryReactions(true);

  auto input = metnetlib::MinimalPrecursorSetsInput<MetNet>(
      metNet,
      targets,
      sources,
      bootstrapCompounds,
      considerTopologicalSources,
      considerBoundaryCompoundsAsSources,
      metnetlib::ConstraintType::STEADYSTATE,
      0.1,
      addFactoryReactions);

  // Compute minimal precursor sets in Steady state
  testing::internal::CaptureStdout();
  using MetabolicNetwork_t = metnetlib::MetabolicNetwork<metnetlib::Compound,
      metnetlib::Reaction>;
  metnetlib::MinimalPrecursorSetEnumerator<MetabolicNetwork_t> minPsEnumerator(
      metnetlib::MIPSolver::CPLEX, input);
  auto minPrecursorSetsSteadyState = minPsEnumerator.enumerate();
  print_preprocessed_data(minPsEnumerator.prepocessed_input(),
                          minPsEnumerator.preprocessedNetwork());
  std::string output = testing::internal::GetCapturedStdout();
  std::string expectedStdout(
      "\n================================================\n"
          "After the preprocessing your network contains 20 compounds,"
          " 17 reactions, 0 of them is reversible.\n"
          "The set of targets: T\n"
          "The set of sources: S1, S2, S3, S4, S5, S6, S7\n"
          "The set of bootstrap compounds: \n"
          "================================================\n");
  ASSERT_EQ(output, expectedStdout);

  // Solutions = {{S1}, {S2}, {S5, S3}, {S5, S4}}
  std::vector<std::vector<std::string>> expectedSolutions = {
      {"S1"}, {"S2"}, {"S5", "S3"}, {"S5", "S4"}};
  ASSERT_EQ(minPrecursorSetsSteadyState.size(), expectedSolutions.size());
  // Check if we have found all expected precursor sets
  for (const auto& expectedPS : expectedSolutions) {
    bool foundSolution = false;
    for (const auto& ps : minPrecursorSetsSteadyState) {
      if (ps.nbPrecursors() == expectedPS.size()) {
        // Check if have found all expected precursors in the set
        bool foundAllSources = true;
        auto it = ps.precursorsBegin();
        auto end = ps.precursorsEnd();
        for (; it != end; ++it) {
          if (std::find(expectedPS.begin(), expectedPS.end(),
              (*it)->sbmlId()) == expectedPS.end()) {
            foundAllSources = false;
            break;
          }
        }
        if (foundAllSources) {
          foundSolution = true;
          break;
        }
      }
    }
    ASSERT_TRUE(foundSolution);
  }

  /*
  // Print solutions
  int i(0);
  for (const auto& solution : minPrecursorSetsSteadyState) {
    std::cout << std::to_string(++i) << ")\n" << solution
              << '\n';
  }*/

  // Compute minimal precursor sets allowing accumulation
  input.constraintType(metnetlib::ConstraintType::ACCUMULATION);
  testing::internal::CaptureStdout();
  metnetlib::MinimalPrecursorSetEnumerator<MetabolicNetwork_t> minPsEnumerator2(
      metnetlib::MIPSolver::CPLEX, input);
  auto minPrecursorSetsAccumulation = minPsEnumerator2.enumerate();
  print_preprocessed_data(minPsEnumerator2.prepocessed_input(),
                          minPsEnumerator2.preprocessedNetwork());
  output = testing::internal::GetCapturedStdout();
  expectedStdout = "\n================================================\n"
                     "After the preprocessing your network contains 20"
                     " compounds, 16 reactions, 0 of them is reversible.\n"
                     "The set of targets: T\n"
                     "The set of sources: S1, S2, S3, S4, S5, S6, S7\n"
                     "The set of bootstrap compounds: \n"
                     "================================================\n";
  ASSERT_EQ(output, expectedStdout);

  // Solutions = {{S1}, {S2}, {S5, S3}, {S5, S4}, {S6, S4}, {S3, S6}}
  expectedSolutions = {
      {"S1"}, {"S2"}, {"S5", "S3"}, {"S5", "S4"}, {"S6", "S4"}, {"S3", "S6"}};
  ASSERT_EQ(minPrecursorSetsAccumulation.size(), expectedSolutions.size());
  // Check if we have found all expected precursor sets
  for (const auto& expectedPS : expectedSolutions) {
    bool foundSolution = false;
    for (const auto& ps : minPrecursorSetsAccumulation) {
      if (ps.nbPrecursors() == expectedPS.size()) {
        // Check if have found all expected precursors in the set
        bool foundAllSources = true;
        auto it = ps.precursorsBegin();
        auto end = ps.precursorsEnd();
        for (; it != end; ++it) {
          if (std::find(expectedPS.begin(), expectedPS.end(),
                        (*it)->sbmlId()) == expectedPS.end()) {
            foundAllSources = false;
            break;
          }
        }
        if (foundAllSources) {
          foundSolution = true;
          break;
        }
      }
    }
    ASSERT_TRUE(foundSolution);
  }

  /*
  // Print solutions
  i = 0;
  for (const auto& solution : minPrecursorSetsAccumulation) {
    std::cout << std::to_string(++i) << ")\n" << solution
              << '\n';
  }*/
}
#endif  // USE_CPLEX

#ifdef USE_CPLEX
TEST_F(TestMinPrecursorSets, Test_inputReader) {
  auto input = metnetlib::MinimalPrecursorSetInputReader<>::read(
      "data/metabolicNetworks/valid/minPS_1.xml",
      "data/minimalPrecursorSetsInput/minPsInput.xml");

  // Check the network file
  ASSERT_EQ(input.metabolicNetwork()->nbCompartments(), (size_t) 1);
  ASSERT_EQ(input.metabolicNetwork()->nbCompounds(), (size_t) 20);
  ASSERT_EQ(input.metabolicNetwork()->nbReactions(), (size_t) 17);

  // Check expected targets
  std::set<std::string> expectedTargets = {"T"};
  auto it = input.targetsBegin();
  auto end = input.targetsEnd();
  size_t nbTargets(0);
  for (; it != end; ++it) {
    ASSERT_NE(expectedTargets.find(input.metabolicNetwork()->compoundById(*it)->
        sbmlId()), expectedTargets.end());
    ++nbTargets;
  }
  ASSERT_EQ(it, end);
  ASSERT_EQ(nbTargets, expectedTargets.size());

  // Check expected user defined sources
  std::set<std::string> expectedSources = {"S1", "S2", "S3", "S4", "S5", "S6",
                                           "S7"};
  it = input.userDefinedSourcesBegin();
  end = input.userDefinedSourcesEnd();
  size_t nbSources(0);
  for (; it != end; ++it) {
    ASSERT_NE(expectedSources.find(input.metabolicNetwork()->compoundById(*it)->
        sbmlId()), expectedSources.end());
    ++nbSources;
  }
  ASSERT_EQ(it, end);
  ASSERT_EQ(nbSources, expectedSources.size());

  // Check expected bootstrap compounds
  std::set<std::string> expectedBootstraps = {};
  it = input.bootstrapCompoundsBegin();
  end = input.bootstrapCompoundsEnd();
  size_t nbBootstraps(0);
  for (; it != end; ++it) {
    ASSERT_NE(expectedBootstraps.find(input.metabolicNetwork()->
        compoundById(*it)->sbmlId()), expectedBootstraps.end());
    ++nbBootstraps;
  }
  ASSERT_EQ(it, end);
  ASSERT_EQ(nbBootstraps, expectedBootstraps.size());

  // Compute minimal precursor sets
  // Change some input parameter
  input.considerTopologicalSources(true);
  input.constraintType(metnetlib::ConstraintType::ACCUMULATION);
  testing::internal::CaptureStdout();
  using MetabolicNetwork_t = metnetlib::MetabolicNetwork<metnetlib::Compound,
      metnetlib::Reaction>;
  metnetlib::MinimalPrecursorSetEnumerator<MetabolicNetwork_t> minPsEnumerator(
      metnetlib::MIPSolver::CPLEX, input);
  auto minPrecursorSetsAccumulation = minPsEnumerator.enumerate();
  print_preprocessed_data(minPsEnumerator.prepocessed_input(),
                          minPsEnumerator.preprocessedNetwork());
  std::string output = testing::internal::GetCapturedStdout();
  std::string expectedStdout =
      "\n================================================\n"
      "After the preprocessing your network contains 20"
      " compounds, 16 reactions, 0 of them is reversible.\n"
      "The set of targets: T\n"
      "The set of sources: S1, S2, S3, S4, S5, S6, S7\n"
      "The set of bootstrap compounds: \n"
      "================================================\n";
  ASSERT_EQ(output, expectedStdout);

  // Print solutions
  /*int i(0);
  for (const auto& solution : minPrecursorSetsAccumulation) {
    std::cout << std::to_string(++i) << ")\n" << solution
              << '\n';
  }*/

  // Solutions = {{S1}, {S2}, {S5, S3}, {S5, S4}, {S6, S4}, {S3, S6}}
  std::vector<std::vector<std::string>> expectedSolutions = {
      {"S1"}, {"S2"}, {"S5", "S3"}, {"S5", "S4"}, {"S6", "S4"}, {"S3", "S6"}};
  ASSERT_EQ(minPrecursorSetsAccumulation.size(), expectedSolutions.size());
  // Check if we have found all expected precursor sets
  for (const auto& expectedPS : expectedSolutions) {
    bool foundSolution = false;
    for (const auto& ps : minPrecursorSetsAccumulation) {
      if (ps.nbPrecursors() == expectedPS.size()) {
        // Check if have found all expected precursors in the set
        bool foundAllSources = true;
        auto it = ps.precursorsBegin();
        auto end = ps.precursorsEnd();
        for (; it != end; ++it) {
          if (std::find(expectedPS.begin(), expectedPS.end(),
                        (*it)->sbmlId()) == expectedPS.end()) {
            foundAllSources = false;
            break;
          }
        }
        if (foundAllSources) {
          foundSolution = true;
          break;
        }
      }
    }
    ASSERT_TRUE(foundSolution);
  }
}
#endif  // USE_CPLEX
