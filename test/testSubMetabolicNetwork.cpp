/*Copyright (C) <2016-2017>  David Parsons, Martin Wannagat

This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU Affero General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/
//
// Created by Martin Wannagat on 16.11.17.
//

#include <string>

#include "gtest/gtest.h"

#include "metnetlib.h"

class TestSubMetabolicNetwork : public testing::Test {
 protected:
  virtual void SetUp() {
    storedStreambuf_ = std::cerr.rdbuf();
    std::cerr.rdbuf(nullptr);
  }

  virtual void TearDown() {
    std::cerr.rdbuf(storedStreambuf_);
  }

  // a compartment
  std::tuple<std::string, double, bool>
      compartment = std::make_tuple("c", 1.0, false);

 private:
  std::streambuf* storedStreambuf_;
};

TEST_F(TestSubMetabolicNetwork, Test_ctor) {
  metnetlib::MetabolicNetwork<> metNet;
  // Add two compartments
  auto compartment = metNet.addCompartment("c1", 1.0, false);
  metNet.addCompartment("c2", 1.0, false);

  // Add some compounds
  metNet.addCompound("C1", {compartment, true, false, true});
  metNet.addCompound("C2", {compartment, true, false, true});
  metNet.addCompound("C3", {compartment, true, false, true});
  metNet.addCompound("C4", {compartment, true, false, true});
  metNet.addCompound("C5", {compartment, true, false, true});
  metNet.addCompound("C6", {compartment, true, false, true});
  metNet.addCompound("C7", {compartment, true, false, true});

  // add some reactions
  metNet.addReaction(hglib::NAME, {{"C1", "C2"}, {"C3"}},
                     {"R1", true, false, {{1, true}, {2, true}}, {{1, true}},
                      -1000, 1000});
  metNet.addReaction(hglib::NAME, {{"C4", "C2"}, {"C3"}},
                     {"R2", true, false, {{1, true}, {2, true}}, {{1, true}},
                      -1000, 1000});
  metNet.addReaction(hglib::NAME, {{"C6", "C4"}, {"C2"}},
                     {"R3", true, false, {{1, true}, {2, true}}, {{1, true}},
                      -1000, 1000});
  metNet.addReaction(hglib::NAME, {{"C7", "C1"}, {"C5"}},
                     {"R4", true, false, {{1, true}, {2, true}}, {{1, true}},
                      -1000, 1000});
  metNet.addReaction(hglib::NAME, {{"C5", "C3"}, {"C6"}},
                     {"R5", true, false, {{1, true}, {2, true}}, {{1, true}},
                      -1000, 1000});

  // Create sub-metabolic network
  std::unordered_set<metnetlib::CompoundIdType>
      whitelistedCompounds = {1, 2, 3};
  std::unordered_set<metnetlib::ReactionIdType>
      whitelistedReactions = {1, 2};
  auto subMetNet = metnetlib::SubMetabolicNetwork<>(&metNet,
                                                    whitelistedCompounds,
                                                    whitelistedReactions);

  // Check that all compartments are in the sub metabolic network
  for (const auto& compartment : metNet.compartments()) {
    ASSERT_EQ(subMetNet.compartmentById(compartment->id()), compartment);
  }

  // check that only whitelisted compounds are in the sub metabolic network
  for (const auto& compound : metNet.compounds()) {
    if (std::find(whitelistedCompounds.begin(), whitelistedCompounds.end(),
                  compound->id()) == whitelistedCompounds.end()) {
      ASSERT_EQ(subMetNet.compoundById(compound->id()), nullptr);
    } else {
      ASSERT_EQ(subMetNet.compoundById(compound->id()), compound);
    }
  }
  // check that only whitelisted reactions are in the sub metabolic network
  for (const auto& reaction : metNet.reactions()) {
    if (std::find(whitelistedReactions.begin(), whitelistedReactions.end(),
                  reaction->id()) == whitelistedReactions.end()) {
      ASSERT_EQ(subMetNet.reactionById(reaction->id()), nullptr);
    } else {
      ASSERT_EQ(subMetNet.reactionById(reaction->id()), reaction);
    }
  }

  // sub-sub-metabolic network
  whitelistedCompounds = {1};
  whitelistedReactions = {1};
  auto subsubMetNet = metnetlib::SubMetabolicNetwork<>(&subMetNet,
                                                       whitelistedCompounds,
                                                       whitelistedReactions);

  // Check that all compartments are in the sub metabolic network
  for (const auto& compartment : metNet.compartments()) {
    ASSERT_EQ(subsubMetNet.compartmentById(compartment->id()), compartment);
  }

  // check that only whitelisted compounds are in the sub-sub-metabolic network
  for (const auto& compound : metNet.compounds()) {
    if (std::find(whitelistedCompounds.begin(), whitelistedCompounds.end(),
                  compound->id()) == whitelistedCompounds.end()) {
      ASSERT_EQ(subsubMetNet.compoundById(compound->id()), nullptr);
    } else {
      ASSERT_EQ(subsubMetNet.compoundById(compound->id()), compound);
    }
  }
  // check that only whitelisted reactions are in the sub metabolic network
  for (const auto& reaction : metNet.reactions()) {
    if (std::find(whitelistedReactions.begin(), whitelistedReactions.end(),
                  reaction->id()) == whitelistedReactions.end()) {
      ASSERT_EQ(subsubMetNet.reactionById(reaction->id()), nullptr);
    } else {
      ASSERT_EQ(subsubMetNet.reactionById(reaction->id()), reaction);
    }
  }

  // check that only compartments/compounds/reactions that are declared in the
  // parent network are added to the sub-networks
  metNet.removeCompartment(1, false);
  metNet.removeCompound(1, false);
  metNet.removeReaction(1);
  whitelistedCompounds = {1, 2, 3, 1000};
  whitelistedReactions = {1, 2, 1000};
  auto subMetNet2 = metnetlib::SubMetabolicNetwork<>(&metNet,
                                                     whitelistedCompounds,
                                                     whitelistedReactions);
  // verify compartments
  ASSERT_NE(subMetNet2.compartmentById(0), nullptr);
  ASSERT_EQ(subMetNet2.compartmentById(1), nullptr);
  // verify compounds
  ASSERT_EQ(subMetNet2.compoundById(1), nullptr);
  ASSERT_THROW(subMetNet2.compoundById(1000), std::out_of_range);
  ASSERT_NE(subMetNet2.compoundById(2), nullptr);
  ASSERT_NE(subMetNet2.compoundById(3), nullptr);
  // verify reactions
  ASSERT_EQ(subMetNet2.reactionById(1), nullptr);
  ASSERT_THROW(subMetNet2.reactionById(1000), std::out_of_range);
  ASSERT_NE(subMetNet2.reactionById(2), nullptr);
}

TEST_F(TestSubMetabolicNetwork, Test_copyCtor) {
  enum Colour {red, blue, green};
  struct CompoundProperty {
      std::string label = "Default";
      Colour colour = Colour::green;
  };
  struct ReactionProperty {
      std::string label = "Default";
      double weight = 0.0;
  };
  struct MetNetProperty {
      std::string label = "Default";
      Colour colour = Colour::red;
  };
  metnetlib::MetabolicNetwork<metnetlib::Compound,
                              metnetlib::Reaction,
                              CompoundProperty,
                              ReactionProperty,
                              MetNetProperty> metNet;
  // Add a compartment
  auto compartment = metNet.addCompartment("c1", 1.0, false);

  // Add some compounds
  metNet.addCompound("C1", {compartment, true, false, true});
  metNet.addCompound("C2", {compartment, true, false, true});
  metNet.addCompound("C3", {compartment, true, false, true});
  metNet.addCompound("C4", {compartment, true, false, true});
  metNet.addCompound("C5", {compartment, true, false, true});
  metNet.addCompound("C6", {compartment, true, false, true});
  metNet.addCompound("C7", {compartment, true, false, true});

  // add some reactions
  metNet.addReaction(hglib::NAME, {{"C1", "C2"}, {"C3"}},
                     {"R1", true, false, {{1, true}, {2, true}}, {{1, true}},
                      -1000, 1000});
  metNet.addReaction(hglib::NAME, {{"C4", "C2"}, {"C3"}},
                     {"R2", true, false, {{1, true}, {2, true}}, {{1, true}},
                      -1000, 1000});
  metNet.addReaction(hglib::NAME, {{"C6", "C4"}, {"C2"}},
                     {"R3", true, false, {{1, true}, {2, true}}, {{1, true}},
                      -1000, 1000});
  metNet.addReaction(hglib::NAME, {{"C7", "C1"}, {"C5"}},
                     {"R4", true, false, {{1, true}, {2, true}}, {{1, true}},
                      -1000, 1000});
  metNet.addReaction(hglib::NAME, {{"C5", "C3"}, {"C6"}},
                     {"R5", true, false, {{1, true}, {2, true}}, {{1, true}},
                      -1000, 1000});

  // Create sub-metabolic network
  std::unordered_set<metnetlib::CompoundIdType> whitelistedCompounds =
      {1, 2, 3};
  std::unordered_set<metnetlib::ReactionIdType> whitelistedReactions =
      {1, 2};
  auto subMetNet = metnetlib::SubMetabolicNetwork<
      metnetlib::Compound,
      metnetlib::Reaction,
      CompoundProperty,
      ReactionProperty,
      MetNetProperty>(&metNet,
                      whitelistedCompounds,
                      whitelistedReactions);

  // Make some changes in the sub-network
  subMetNet.removeCompound(1, false);
  // change some default properties in sub-network
  subMetNet.getCompoundProperties_(2)->label = "Test";
  metnetlib::ReactionIdType reactionId(2);  // reaction "R2"
  subMetNet.getReactionProperties_(reactionId)->label = "TestReaction";
  subMetNet.getReactionProperties_(reactionId)->weight = 1.0;
  subMetNet.getMetabolicNetworkProperties_()->colour = Colour::blue;

  // Make a copy of subMetNet
  auto subMetNetCopy = metnetlib::SubMetabolicNetwork<
      metnetlib::Compound,
      metnetlib::Reaction,
      CompoundProperty,
      ReactionProperty,
      MetNetProperty>(subMetNet);

  // Check that both sub-networks are identical
  ASSERT_EQ(subMetNet.nbCompartments(), subMetNetCopy.nbCompartments());
  ASSERT_EQ(subMetNet.nbCompounds(), subMetNetCopy.nbCompounds());
  ASSERT_EQ(subMetNet.nbReactions(), subMetNetCopy.nbReactions());
  ASSERT_EQ(subMetNet.nbReversibleReactions(),
            subMetNetCopy.nbReversibleReactions());

  // Compare compartments
  for (const auto& compartmentInSub : subMetNet.compartments()) {
    const auto& compartmentInCopy = subMetNetCopy.compartmentById(
        compartmentInSub->id());
    ASSERT_EQ(compartmentInSub, compartmentInCopy);
  }

  // compare compounds
  for (const auto& compound : subMetNet.compounds()) {
    auto compoundId = compound->id();
    const auto& compoundCopy = subMetNetCopy.compoundById(compoundId);
    ASSERT_EQ(compound, compoundCopy);

    // compare consuming reactions
    ASSERT_EQ(subMetNet.isConsumed(compoundId),
              subMetNetCopy.isConsumed(compoundId));
    ASSERT_EQ(subMetNet.nbConsumingReactions(compoundId),
              subMetNetCopy.nbConsumingReactions(compoundId));
    auto consumingReactions = subMetNet.consumingReactions(compoundId);
    auto it = subMetNetCopy.consumingReactionsBegin(compoundId);
    for (const auto& reaction : *consumingReactions) {
      ASSERT_EQ(reaction, *it);
      ++it;
    }

    // compare producing reactions
    ASSERT_EQ(subMetNet.isProduced(compoundId),
              subMetNetCopy.isProduced(compoundId));
    ASSERT_EQ(subMetNet.nbProducingReactions(compoundId),
              subMetNetCopy.nbProducingReactions(compoundId));
    auto producingReactions = subMetNet.producingReactions(compoundId);
    it = subMetNetCopy.producingReactionsBegin(compoundId);
    for (const auto& reaction : *producingReactions) {
      ASSERT_EQ(reaction, *it);
      ++it;
    }
  }

  // Check reactions
  for (const auto& reaction : subMetNet.reactions()) {
    auto reactionId = reaction->id();
    const auto& reactionCopy = subMetNetCopy.reactionById(reactionId);
    ASSERT_EQ(reaction, reactionCopy);

    ASSERT_EQ(subMetNet.isReversible(reactionId),
              subMetNetCopy.isReversible(reactionId));
    ASSERT_EQ(subMetNet.reversibleReaction(reactionId),
              subMetNetCopy.reversibleReaction(reactionId));

    // compare substrates
    ASSERT_EQ(subMetNet.hasSubstrates(reactionId),
              subMetNetCopy.hasSubstrates(reactionId));
    ASSERT_EQ(subMetNet.nbSubstrates(reactionId),
              subMetNetCopy.nbSubstrates(reactionId));
    auto substrates = subMetNet.substrates(reactionId);
    auto it = subMetNetCopy.substratesBegin(reactionId);
    for (const auto& compound : *substrates) {
      ASSERT_EQ(compound, *it);
      ASSERT_EQ(subMetNet.substrateStoichiometry(compound->id(), reactionId),
                subMetNetCopy.substrateStoichiometry(compound->id(),
                                                     reactionId));
      ++it;
    }

    // compare products
    ASSERT_EQ(subMetNet.hasProducts(reactionId),
              subMetNetCopy.hasProducts(reactionId));
    ASSERT_EQ(subMetNet.nbProducts(reactionId),
              subMetNetCopy.nbProducts(reactionId));
    auto products = subMetNet.products(reactionId);
    it = subMetNetCopy.productsBegin(reactionId);
    for (const auto& compound : *products) {
      ASSERT_EQ(compound, *it);
      ASSERT_EQ(subMetNet.productStoichiometry(compound->id(), reactionId),
                subMetNetCopy.productStoichiometry(compound->id(), reactionId));
      ++it;
    }
  }

  // change sub-network and check that this change is
  // independent from the copied sub-network
  subMetNet.getMetabolicNetworkProperties_()->colour = Colour::green;
  subMetNet.getMetabolicNetworkProperties_()->label = "Original sub-network";
  ASSERT_EQ(subMetNetCopy.getMetabolicNetworkProperties_()->colour,
            Colour::blue);
  ASSERT_EQ(subMetNetCopy.getMetabolicNetworkProperties_()->label.compare(
      "Default"), 0);

  size_t nbExpectedCompounds(subMetNetCopy.nbCompounds());
  size_t nbExpectedReactions(subMetNetCopy.nbReactions());
  subMetNet.removeCompound(2, false);
  subMetNet.removeReaction(1);
  ASSERT_EQ(nbExpectedCompounds, subMetNetCopy.nbCompounds());
  ASSERT_EQ(nbExpectedReactions, subMetNetCopy.nbReactions());
}

TEST_F(TestSubMetabolicNetwork, Test_subMetNet_AssignmentOperator) {
  enum Colour {
      red, blue, green
  };
  struct CompoundProperty {
      std::string name = "Default";
      Colour colour = Colour::green;
  };
  struct ReactionProperty {
      std::string name = "Default";
      double weight = 0.0;
  };
  struct MetabolicNetworkProperty {
      std::string name = "Default";
      Colour colour = Colour::blue;
  };

  metnetlib::MetabolicNetwork<metnetlib::Compound,
                              metnetlib::Reaction,
                              CompoundProperty,
                              ReactionProperty,
                              MetabolicNetworkProperty> metNet;
  // Add a compartment
  auto compartment = metNet.addCompartment("c1", 1.0, false);

  // Add some compounds
  metNet.addCompound("C1", {compartment, true, false, true});
  metNet.addCompound("C2", {compartment, true, false, true});
  metNet.addCompound("C3", {compartment, true, false, true});
  metNet.addCompound("C4", {compartment, true, false, true});
  metNet.addCompound("C5", {compartment, true, false, true});
  metNet.addCompound("C6", {compartment, true, false, true});
  metNet.addCompound("C7", {compartment, true, false, true});

  // add some reactions
  metNet.addReaction(hglib::NAME, {{"C1", "C2"}, {"C3"}},
                     {"R1", true, false, {{1, true}, {2, true}}, {{1, true}},
                      -1000, 1000});
  metNet.addReaction(hglib::NAME, {{"C4", "C2"}, {"C3"}},
                     {"R2", true, false, {{1, true}, {2, true}}, {{1, true}},
                      -1000, 1000});
  metNet.addReaction(hglib::NAME, {{"C6", "C4"}, {"C2"}},
                     {"R3", true, false, {{1, true}, {2, true}}, {{1, true}},
                      -1000, 1000});
  metNet.addReaction(hglib::NAME, {{"C7", "C1"}, {"C5"}},
                     {"R4", true, false, {{1, true}, {2, true}}, {{1, true}},
                      -1000, 1000});
  metNet.addReaction(hglib::NAME, {{"C5", "C3"}, {"C6"}},
                     {"R5", true, false, {{1, true}, {2, true}}, {{1, true}},
                      -1000, 1000});

  // Three sub- and sub-sub-metabolic networks
  std::unordered_set<metnetlib::CompoundIdType> whitelistedCompounds =
      {1, 2, 3};
  std::unordered_set<metnetlib::ReactionIdType> whitelistedReactions =
      {1, 2};
  auto subMetNet = metnetlib::SubMetabolicNetwork<
      metnetlib::Compound,
      metnetlib::Reaction,
      CompoundProperty,
      ReactionProperty,
      MetabolicNetworkProperty>(&metNet,
                                whitelistedCompounds,
                                whitelistedReactions);
  whitelistedCompounds = {1, 2, 3};
  whitelistedReactions = {2};
  auto subsubMetNet = metnetlib::SubMetabolicNetwork<
      metnetlib::Compound,
      metnetlib::Reaction,
      CompoundProperty,
      ReactionProperty,
      MetabolicNetworkProperty>(&subMetNet,
                                whitelistedCompounds,
                                whitelistedReactions);

  // second sub- and sub-sub-graph
  whitelistedCompounds = {1, 2, 3, 4};
  whitelistedReactions = {2};
  auto subMetNet2 = metnetlib::SubMetabolicNetwork<
      metnetlib::Compound,
      metnetlib::Reaction,
      CompoundProperty,
      ReactionProperty,
      MetabolicNetworkProperty>(&metNet,
                                whitelistedCompounds,
                                whitelistedReactions);
  whitelistedCompounds = {1, 2};
  whitelistedReactions = {2};
  auto subsubMetNet2 = metnetlib::SubMetabolicNetwork<
      metnetlib::Compound,
      metnetlib::Reaction,
      CompoundProperty,
      ReactionProperty,
      MetabolicNetworkProperty>(&subMetNet2,
                                whitelistedCompounds,
                                whitelistedReactions);

  // third sub- and sub-sub-graph
  whitelistedCompounds = {0, 3, 5};
  whitelistedReactions = {0};
  auto subMetNet3 = metnetlib::SubMetabolicNetwork<
      metnetlib::Compound,
      metnetlib::Reaction,
      CompoundProperty,
      ReactionProperty,
      MetabolicNetworkProperty>(&metNet,
                                whitelistedCompounds,
                                whitelistedReactions);
  whitelistedCompounds = {5};
  whitelistedReactions = {0};
  auto subsubMetNet3 = metnetlib::SubMetabolicNetwork<
      metnetlib::Compound,
      metnetlib::Reaction,
      CompoundProperty,
      ReactionProperty,
      MetabolicNetworkProperty>(&subMetNet3,
                                whitelistedCompounds,
                                whitelistedReactions);

  // Change properties in the first sub-network
  subMetNet.getMetabolicNetworkProperties_()->name = "Test";
  subMetNet.getMetabolicNetworkProperties_()->colour = Colour::red;
  subMetNet.removeCompound(1, false);
  subMetNet.removeReaction(1);

  // Assignments
  subMetNet = subMetNet;  // nothing happens in a self assignment
  // The following assignment should invalidate subsubMetNet2 and
  // subsubMetNet3
  subMetNet2 = subMetNet3 = subMetNet;
  ASSERT_THROW(subsubMetNet2.nbCompounds(), std::invalid_argument);
  ASSERT_THROW(subsubMetNet3.nbCompounds(), std::invalid_argument);

  // check if all sub-hypergraphs are identical
  ASSERT_EQ(subMetNet.nbCompounds(), subMetNet2.nbCompounds());
  ASSERT_EQ(subMetNet.nbCompounds(), subMetNet3.nbCompounds());
  for (const auto& compound : subMetNet.compounds()) {
    ASSERT_EQ(subMetNet2.compoundById(compound->id()), compound);
    ASSERT_EQ(subMetNet3.compoundById(compound->id()), compound);
  }
  ASSERT_EQ(subMetNet.nbReactions(), subMetNet2.nbReactions());
  ASSERT_EQ(subMetNet.nbReactions(), subMetNet3.nbReactions());
  for (const auto& reaction : subMetNet.reactions()) {
    ASSERT_EQ(subMetNet2.reactionById(reaction->id()), reaction);
    ASSERT_EQ(subMetNet3.reactionById(reaction->id()), reaction);
  }
  // Check that the subsubMetNet is not a child of all sub-networks and that
  // all sub-networks are independent from each other
  subMetNet2.removeReaction(2);
  ASSERT_NE(subMetNet.nbReactions(), subMetNet2.nbReactions());
  ASSERT_NE(subMetNet.nbReactions(), subMetNet2.nbReactions());
  // Reaction 2 is still in the subsubMetNet as its parent is subMetNet
  // and not subMetNet2
  ASSERT_NE(subsubMetNet.reactionById(2), nullptr);

  metnetlib::MetabolicNetwork<metnetlib::Compound,
                              metnetlib::Reaction,
                              CompoundProperty,
                              ReactionProperty,
                              MetabolicNetworkProperty> metNet2;
  // The following assignment invalidates the whole sub-network hierarchy of
  // metNet
  metNet = metNet2;
  ASSERT_THROW(subMetNet.nbCompounds(), std::invalid_argument);
  ASSERT_THROW(subsubMetNet.nbCompounds(), std::invalid_argument);
  ASSERT_THROW(subMetNet2.nbCompounds(), std::invalid_argument);
  ASSERT_THROW(subMetNet3.nbCompounds(), std::invalid_argument);
}

TEST_F(TestSubMetabolicNetwork, Test_addCompartment) {
  metnetlib::MetabolicNetwork<> metNet;
  // add a compartment
  const auto& compartment = metNet.addCompartment("c1", 1.0, false);

  // Add some compounds
  metNet.addCompound("C1", {compartment, true, false, true});
  metNet.addCompound("C2", {compartment, true, false, true});
  metNet.addCompound("C3", {compartment, true, false, true});
  metNet.addCompound("C4", {compartment, true, false, true});
  metNet.addCompound("C5", {compartment, true, false, true});
  metNet.addCompound("C6", {compartment, true, false, true});
  metNet.addCompound("C7", {compartment, true, false, true});

  // add some reactions
  metNet.addReaction(hglib::NAME, {{"C1", "C2"}, {"C3"}},
                     {"R1", true, false, {{1, true}, {2, true}}, {{1, true}},
                      -1000, 1000});
  metNet.addReaction(hglib::NAME, {{"C4", "C2"}, {"C3"}},
                     {"R2", true, false, {{1, true}, {2, true}}, {{1, true}},
                      -1000, 1000});
  metNet.addReaction(hglib::NAME, {{"C6", "C4"}, {"C2"}},
                     {"R3", true, false, {{1, true}, {2, true}}, {{1, true}},
                      -1000, 1000});
  metNet.addReaction(hglib::NAME, {{"C7", "C1"}, {"C5"}},
                     {"R4", true, false, {{1, true}, {2, true}}, {{1, true}},
                      -1000, 1000});
  metNet.addReaction(hglib::NAME, {{"C5", "C3"}, {"C6"}},
                     {"R5", true, false, {{1, true}, {2, true}}, {{1, true}},
                      -1000, 1000});

  // build sub-metabolic networks
  std::unordered_set<metnetlib::CompoundIdType> whitelistedCompounds =
      {1, 2, 3};
  std::unordered_set<metnetlib::ReactionIdType> whitelistedReactions =
      {1, 2};
  auto subMetNet = metnetlib::SubMetabolicNetwork<>(&metNet,
                                                    whitelistedCompounds,
                                                    whitelistedReactions);
  whitelistedCompounds = {1, 2, 3};
  whitelistedReactions = {2};
  auto subsubMetNet = metnetlib::SubMetabolicNetwork<>(&subMetNet,
                                                       whitelistedCompounds,
                                                       whitelistedReactions);

  // Add compartments in the sub-networks
  const auto& compartment2 = subMetNet.addCompartment("c2", 2.0, true);
  const auto& compartment3 = subsubMetNet.addCompartment("c3", 2.0, true);

  // Check that compartment c3 is at each level
  ASSERT_EQ(metNet.compartmentBySbmlId("c3"), compartment3);
  ASSERT_EQ(subMetNet.compartmentBySbmlId("c3"), compartment3);
  ASSERT_EQ(subsubMetNet.compartmentBySbmlId("c3"), compartment3);

  // Check that compartment c2 is in the two top levels
  ASSERT_EQ(metNet.compartmentBySbmlId("c2"), compartment2);
  ASSERT_EQ(subMetNet.compartmentBySbmlId("c2"), compartment2);
  ASSERT_EQ(subsubMetNet.compartmentBySbmlId("c2"), nullptr);

  // can't add a compartment with a name that is already present in the root
  // network
  ASSERT_THROW(subMetNet.addCompartment("c2", 3.0, true),
               std::invalid_argument);
  ASSERT_THROW(subsubMetNet.addCompartment("c2", 3.0, true),
               std::invalid_argument);

  // Test exception throw on invalid compartment names
  // Check in sub-network
  ASSERT_THROW(subMetNet.addCompartment("", 1.0, true),
               std::invalid_argument);
  ASSERT_THROW(subMetNet.addCompartment("1", 1.0, true),
               std::invalid_argument);
  ASSERT_THROW(subMetNet.addCompartment("1_c", 1.0, true),
               std::invalid_argument);
  ASSERT_THROW(subMetNet.addCompartment("-c", 1.0, true),
               std::invalid_argument);
  // Check in sub-sub-network
  ASSERT_THROW(subsubMetNet.addCompartment("", 1.0, true),
               std::invalid_argument);
  ASSERT_THROW(subsubMetNet.addCompartment("1", 1.0, true),
               std::invalid_argument);
  ASSERT_THROW(subsubMetNet.addCompartment("1_c", 1.0, true),
               std::invalid_argument);
  ASSERT_THROW(subsubMetNet.addCompartment("-c", 1.0, true),
               std::invalid_argument);
  // Test exception throw on negative size
  // Check in sub-network
  ASSERT_THROW(subMetNet.addCompartment("negSize", -1.0, true),
               std::invalid_argument);
  // Check in sub-sub-network
  ASSERT_THROW(subsubMetNet.addCompartment("negSize", -1.0, true),
               std::invalid_argument);

  // Test some valid compartment names
  // According to SBML specification, the name must fit this regular
  // expression: [a-zA-Z_][a-zA-Z0-9_]*
  // Check in sub-network
  ASSERT_NO_THROW(subMetNet.addCompartment("_c", 1.0, true));
  ASSERT_NO_THROW(subMetNet.addCompartment("__c", 1.0, true));
  ASSERT_NO_THROW(subMetNet.addCompartment("c_", 1.0, true));
  ASSERT_NO_THROW(subMetNet.addCompartment("C", 1.0, true));
  ASSERT_NO_THROW(subMetNet.addCompartment("C_", 1.0, true));
  ASSERT_NO_THROW(subMetNet.addCompartment("c_1", 1.0, true));
  ASSERT_NO_THROW(subMetNet.addCompartment("_c_a_b_c_0", 1.0, true));
  ASSERT_NO_THROW(subMetNet.addCompartment("_", 1.0, true));
  ASSERT_NO_THROW(subMetNet.addCompartment("__", 1.0, true));
  // Check in sub-sub-network
  ASSERT_NO_THROW(subsubMetNet.addCompartment("_c2", 1.0, true));
  ASSERT_NO_THROW(subsubMetNet.addCompartment("__c2", 1.0, true));
  ASSERT_NO_THROW(subsubMetNet.addCompartment("c2_", 1.0, true));
  ASSERT_NO_THROW(subsubMetNet.addCompartment("C2", 1.0, true));
  ASSERT_NO_THROW(subsubMetNet.addCompartment("C2_", 1.0, true));
  ASSERT_NO_THROW(subsubMetNet.addCompartment("c_12", 1.0, true));
  ASSERT_NO_THROW(subsubMetNet.addCompartment("_c_a_b_c_02", 1.0, true));
}

TEST_F(TestSubMetabolicNetwork, Test_compartmentAccess) {
  metnetlib::MetabolicNetwork<> metNet;
  // add a compartment
  const auto& compartment = metNet.addCompartment("c1", 1.0, false);

  // Add some compounds
  metNet.addCompound("C1", {compartment, true, false, true});
  metNet.addCompound("C2", {compartment, true, false, true});
  metNet.addCompound("C3", {compartment, true, false, true});
  metNet.addCompound("C4", {compartment, true, false, true});
  metNet.addCompound("C5", {compartment, true, false, true});
  metNet.addCompound("C6", {compartment, true, false, true});
  metNet.addCompound("C7", {compartment, true, false, true});

  // add some reactions
  metNet.addReaction(hglib::NAME, {{"C1", "C2"}, {"C3"}},
                     {"R1", true, false, {{1, true}, {2, true}}, {{1, true}},
                      -1000, 1000});
  metNet.addReaction(hglib::NAME, {{"C4", "C2"}, {"C3"}},
                     {"R2", true, false, {{1, true}, {2, true}}, {{1, true}},
                      -1000, 1000});
  metNet.addReaction(hglib::NAME, {{"C6", "C4"}, {"C2"}},
                     {"R3", true, false, {{1, true}, {2, true}}, {{1, true}},
                      -1000, 1000});
  metNet.addReaction(hglib::NAME, {{"C7", "C1"}, {"C5"}},
                     {"R4", true, false, {{1, true}, {2, true}}, {{1, true}},
                      -1000, 1000});
  metNet.addReaction(hglib::NAME, {{"C5", "C3"}, {"C6"}},
                     {"R5", true, false, {{1, true}, {2, true}}, {{1, true}},
                      -1000, 1000});

  // build sub-metabolic networks
  std::unordered_set<metnetlib::CompoundIdType> whitelistedCompounds =
      {1, 2, 3};
  std::unordered_set<metnetlib::ReactionIdType> whitelistedReactions =
      {1, 2};
  auto subMetNet = metnetlib::SubMetabolicNetwork<>(&metNet,
                                                    whitelistedCompounds,
                                                    whitelistedReactions);
  whitelistedCompounds = {1, 2, 3};
  whitelistedReactions = {2};
  auto subsubMetNet = metnetlib::SubMetabolicNetwork<>(&subMetNet,
                                                       whitelistedCompounds,
                                                       whitelistedReactions);
  // Check number of compartments
  size_t expectedCompartmentNbInRootMetNet(1);
  size_t expectedCompartmentNbInSubMetNet(1);
  size_t expectedCompartmentNbInSubSubMetNet(1);
  ASSERT_EQ(metNet.nbCompartments(), expectedCompartmentNbInRootMetNet);
  ASSERT_EQ(subMetNet.nbCompartments(), expectedCompartmentNbInSubMetNet);
  ASSERT_EQ(subsubMetNet.nbCompartments(), expectedCompartmentNbInSubSubMetNet);

  // Check compartment iterator in metNet
  metnetlib::MetabolicNetwork<>::compartment_iterator itComp, endComp;
  itComp = metNet.compartmentsBegin();
  endComp = metNet.compartmentsEnd();
  for (const auto& comp : metNet.compartments()) {
    ASSERT_EQ(comp, *itComp);
    ++itComp;
  }
  ASSERT_EQ(itComp, endComp);

  // Check compartment iterator in subMetNet
  metnetlib::SubMetabolicNetwork<>::compartment_iterator it, end;
  it = subMetNet.compartmentsBegin();
  end = subMetNet.compartmentsEnd();
  for (const auto& comp : subMetNet.compartments()) {
    ASSERT_EQ(comp, *it);
    ++it;
  }
  ASSERT_EQ(it, end);
  // Check compartment iterator in subsubMetNet
  it = subsubMetNet.compartmentsBegin();
  end = subsubMetNet.compartmentsEnd();
  for (const auto& comp : subsubMetNet.compartments()) {
    ASSERT_EQ(comp, *it);
    ++it;
  }
  ASSERT_EQ(it, end);

  // Add compartments in the sub-networks
  const auto& compartment2 = subMetNet.addCompartment("c2", 2.0, true);
  // Check number of compartments
  ASSERT_EQ(metNet.nbCompartments(), ++expectedCompartmentNbInRootMetNet);
  ASSERT_EQ(subMetNet.nbCompartments(), ++expectedCompartmentNbInSubMetNet);
  ASSERT_EQ(subsubMetNet.nbCompartments(), expectedCompartmentNbInSubSubMetNet);

  // Check compartment iterator in metNet
  itComp = metNet.compartmentsBegin();
  endComp = metNet.compartmentsEnd();
  for (const auto& comp : metNet.compartments()) {
    ASSERT_EQ(comp, *itComp);
    ++itComp;
  }
  ASSERT_EQ(itComp, endComp);

  // Check compartment iterator in subMetNet
  it = subMetNet.compartmentsBegin();
  end = subMetNet.compartmentsEnd();
  for (const auto& comp : subMetNet.compartments()) {
    ASSERT_EQ(comp, *it);
    ++it;
  }
  ASSERT_EQ(it, end);
  // Check compartment iterator in subsubMetNet
  it = subsubMetNet.compartmentsBegin();
  end = subsubMetNet.compartmentsEnd();
  for (const auto& comp : subsubMetNet.compartments()) {
    ASSERT_EQ(comp, *it);
    ++it;
  }
  ASSERT_EQ(it, end);

  const auto& compartment3 = subsubMetNet.addCompartment("c3", 2.0, true);
  // Check number of compartments
  ASSERT_EQ(metNet.nbCompartments(), ++expectedCompartmentNbInRootMetNet);
  ASSERT_EQ(subMetNet.nbCompartments(), ++expectedCompartmentNbInSubMetNet);
  ASSERT_EQ(subsubMetNet.nbCompartments(),
            ++expectedCompartmentNbInSubSubMetNet);

  // Check compartment iterator in metNet
  itComp = metNet.compartmentsBegin();
  endComp = metNet.compartmentsEnd();
  for (const auto& comp : metNet.compartments()) {
    ASSERT_EQ(comp, *itComp);
    ++itComp;
  }
  ASSERT_EQ(itComp, endComp);

  // Check compartment iterator in subMetNet
  it = subMetNet.compartmentsBegin();
  end = subMetNet.compartmentsEnd();
  for (const auto& comp : subMetNet.compartments()) {
    ASSERT_EQ(comp, *it);
    ++it;
  }
  ASSERT_EQ(it, end);
  // Check compartment iterator in subsubMetNet
  it = subsubMetNet.compartmentsBegin();
  end = subsubMetNet.compartmentsEnd();
  for (const auto& comp : subsubMetNet.compartments()) {
    ASSERT_EQ(comp, *it);
    ++it;
  }
  ASSERT_EQ(it, end);

  // Check the compartments in subMetNet and subsubMetNet
  ASSERT_EQ(subMetNet.compartmentById(0), compartment);
  ASSERT_EQ(subMetNet.compartmentById(1), compartment2);
  ASSERT_EQ(subMetNet.compartmentById(2), compartment3);
  ASSERT_EQ(subMetNet.compartmentBySbmlId("c1"), compartment);
  ASSERT_EQ(subMetNet.compartmentBySbmlId("c2"), compartment2);
  ASSERT_EQ(subMetNet.compartmentBySbmlId("c3"), compartment3);

  ASSERT_EQ(subsubMetNet.compartmentById(0), compartment);
  ASSERT_EQ(subsubMetNet.compartmentById(1), nullptr);
  ASSERT_EQ(subsubMetNet.compartmentById(2), compartment3);
  ASSERT_EQ(subsubMetNet.compartmentBySbmlId("c1"), compartment);
  ASSERT_EQ(subsubMetNet.compartmentBySbmlId("c2"), nullptr);
  ASSERT_EQ(subsubMetNet.compartmentBySbmlId("c3"), compartment3);

  // Test exceptions
  ASSERT_THROW(subMetNet.compartmentById(3), std::out_of_range);
  ASSERT_THROW(subsubMetNet.compartmentById(3), std::out_of_range);
  // Test unknown compartment Sbml Id
  ASSERT_EQ(subMetNet.compartmentBySbmlId("unknwownCompartment"), nullptr);
  ASSERT_EQ(subsubMetNet.compartmentBySbmlId("unknwownCompartment"), nullptr);
}

TEST_F(TestSubMetabolicNetwork, Test_Compounds) {
  metnetlib::MetabolicNetwork<> metNet;
  // add a compartment
  const auto &compartment = metNet.addCompartment("c1", 1.0, false);

  // Add some compounds
  metNet.addCompound("C1", {compartment, true, false, true});
  metNet.addCompound("C2", {compartment, true, false, true});
  metNet.addCompound("C3", {compartment, true, false, true});
  metNet.addCompound("C4", {compartment, true, false, true});
  metNet.addCompound("C5", {compartment, true, false, true});
  metNet.addCompound("C6", {compartment, true, false, true});
  metNet.addCompound("C7", {compartment, true, false, true});

  // add some reactions
  metNet.addReaction(hglib::NAME, {{"C1", "C2"},
                                   {"C3"}},
                     {"R1", true, false, {{1, true}, {2, true}}, {{1, true}},
                      -1000, 1000});
  metNet.addReaction(hglib::NAME, {{"C4", "C2"},
                                   {"C3"}},
                     {"R2", true, false, {{1, true}, {2, true}}, {{1, true}},
                      -1000, 1000});
  metNet.addReaction(hglib::NAME, {{"C6", "C4"},
                                   {"C2"}},
                     {"R3", true, false, {{1, true}, {2, true}}, {{1, true}},
                      -1000, 1000});
  metNet.addReaction(hglib::NAME, {{"C7", "C1"},
                                   {"C5"}},
                     {"R4", true, false, {{1, true}, {2, true}}, {{1, true}},
                      -1000, 1000});
  metNet.addReaction(hglib::NAME, {{"C5", "C3"},
                                   {"C6"}},
                     {"R5", true, false, {{1, true}, {2, true}}, {{1, true}},
                      -1000, 1000});

  // build sub-metabolic networks
  std::unordered_set<metnetlib::CompoundIdType> whitelistedCompounds =
      {1, 2, 3};
  std::unordered_set<metnetlib::ReactionIdType> whitelistedReactions =
      {1, 2};
  auto subMetNet = metnetlib::SubMetabolicNetwork<>(&metNet,
                                                    whitelistedCompounds,
                                                    whitelistedReactions);
  whitelistedCompounds = {1, 2, 3};
  whitelistedReactions = {2};
  auto subsubMetNet = metnetlib::SubMetabolicNetwork<>(&subMetNet,
                                                       whitelistedCompounds,
                                                       whitelistedReactions);

  // Check number of compounds
  size_t expectedCompoundNbInRootMetNet(7);
  size_t expectedCompoundNbInSubMetNet(3);
  size_t expectedCompoundNbInSubSubMetNet(3);
  ASSERT_EQ(metNet.nbCompounds(), expectedCompoundNbInRootMetNet);
  ASSERT_EQ(subMetNet.nbCompounds(), expectedCompoundNbInSubMetNet);
  ASSERT_EQ(subsubMetNet.nbCompounds(), expectedCompoundNbInSubSubMetNet);

  // Check compound iterators
  // in root met net
  metnetlib::MetabolicNetwork<>::compound_iterator itComp, endComp;
  itComp = metNet.compoundsBegin();
  endComp = metNet.compoundsEnd();
  for (const auto& compound : metNet.compounds()) {
    ASSERT_EQ(compound, *itComp);
    ++itComp;
  }
  ASSERT_EQ(itComp, endComp);

  // in sub met net
  metnetlib::SubMetabolicNetwork<>::compound_iterator it, end;
  it = subMetNet.compoundsBegin();
  end = subMetNet.compoundsEnd();
  for (const auto& compound : subMetNet.compounds()) {
    ASSERT_EQ(compound, *it);
    ++it;
  }
  ASSERT_EQ(it, end);

  // in sub-sub met net
  it = subsubMetNet.compoundsBegin();
  end = subsubMetNet.compoundsEnd();
  for (const auto& compound : subsubMetNet.compounds()) {
    ASSERT_EQ(compound, *it);
    ++it;
  }
  ASSERT_EQ(it, end);

  // Add compounds in the sub-networks
  const auto& compound1 = subMetNet.addCompound(
      "C8", {compartment, true, false, true});
  ASSERT_EQ(metNet.nbCompounds(), ++expectedCompoundNbInRootMetNet);
  ASSERT_EQ(subMetNet.nbCompounds(), ++expectedCompoundNbInSubMetNet);
  ASSERT_EQ(subsubMetNet.nbCompounds(), expectedCompoundNbInSubSubMetNet);

  // Check compound iterators
  // in root met net
  itComp = metNet.compoundsBegin();
  endComp = metNet.compoundsEnd();
  for (const auto& compound : metNet.compounds()) {
    ASSERT_EQ(compound, *itComp);
    ++itComp;
  }
  ASSERT_EQ(itComp, endComp);

  // in sub met net
  it = subMetNet.compoundsBegin();
  end = subMetNet.compoundsEnd();
  for (const auto& compound : subMetNet.compounds()) {
    ASSERT_EQ(compound, *it);
    ++it;
  }
  ASSERT_EQ(it, end);

  // in sub-sub met net
  it = subsubMetNet.compoundsBegin();
  end = subsubMetNet.compoundsEnd();
  for (const auto& compound : subsubMetNet.compounds()) {
    ASSERT_EQ(compound, *it);
    ++it;
  }
  ASSERT_EQ(it, end);

  const auto& compound2 = subsubMetNet.addCompound(
      "C9", {compartment, true, false, true});
  ASSERT_EQ(metNet.nbCompounds(), ++expectedCompoundNbInRootMetNet);
  ASSERT_EQ(subMetNet.nbCompounds(), ++expectedCompoundNbInSubMetNet);
  ASSERT_EQ(subsubMetNet.nbCompounds(), ++expectedCompoundNbInSubSubMetNet);

  // Check compound iterators
  // in root met net
  itComp = metNet.compoundsBegin();
  endComp = metNet.compoundsEnd();
  for (const auto& compound : metNet.compounds()) {
    ASSERT_EQ(compound, *itComp);
    ++itComp;
  }
  ASSERT_EQ(itComp, endComp);

  // in sub met net
  it = subMetNet.compoundsBegin();
  end = subMetNet.compoundsEnd();
  for (const auto& compound : subMetNet.compounds()) {
    ASSERT_EQ(compound, *it);
    ++it;
  }
  ASSERT_EQ(it, end);

  // in sub-sub met net
  it = subsubMetNet.compoundsBegin();
  end = subsubMetNet.compoundsEnd();
  for (const auto& compound : subsubMetNet.compounds()) {
    ASSERT_EQ(compound, *it);
    ++it;
  }
  ASSERT_EQ(it, end);

  // Check that compound C8 is in the two top levels
  ASSERT_EQ(metNet.compoundBySbmlId("C8"), compound1);
  ASSERT_EQ(subMetNet.compoundBySbmlId("C8"), compound1);
  ASSERT_EQ(subsubMetNet.compoundBySbmlId("C8"), nullptr);
  // Check via Id
  ASSERT_EQ(metNet.compoundById(compound1->id()), compound1);
  ASSERT_EQ(subMetNet.compoundById(compound1->id()), compound1);
  ASSERT_EQ(subsubMetNet.compoundById(compound1->id()), nullptr);

  // Check that compound C9 is at each level
  ASSERT_EQ(metNet.compoundBySbmlId("C9"), compound2);
  ASSERT_EQ(subMetNet.compoundBySbmlId("C9"), compound2);
  ASSERT_EQ(subsubMetNet.compoundBySbmlId("C9"), compound2);
  // Check via Id
  ASSERT_EQ(metNet.compoundById(compound2->id()), compound2);
  ASSERT_EQ(subMetNet.compoundById(compound2->id()), compound2);
  ASSERT_EQ(subsubMetNet.compoundById(compound2->id()), compound2);

  // Check exceptions
  // Compound with the same name already exists
  ASSERT_THROW(subMetNet.addCompound("C8", {compartment, false, true, false}),
               std::invalid_argument);
  ASSERT_THROW(subsubMetNet.addCompound("C9",
                                        {compartment, false, true, false}),
               std::invalid_argument);
  // Exception throw on invalid compound names
  ASSERT_THROW(subMetNet.addCompound("", {compartment, false, true, false}),
               std::invalid_argument);
  ASSERT_THROW(subMetNet.addCompound("1", {compartment, false, true, false}),
               std::invalid_argument);
  ASSERT_THROW(subMetNet.addCompound("1_c", {compartment, false, true, false}),
               std::invalid_argument);
  ASSERT_THROW(subMetNet.addCompound("-c", {compartment, false, true, false}),
               std::invalid_argument);
  ASSERT_THROW(subsubMetNet.addCompound("", {compartment, false, true, false}),
               std::invalid_argument);
  ASSERT_THROW(subsubMetNet.addCompound("1", {compartment, false, true, false}),
               std::invalid_argument);
  ASSERT_THROW(subsubMetNet.addCompound("1_c",
                                        {compartment, false, true, false}),
               std::invalid_argument);
  ASSERT_THROW(subsubMetNet.addCompound("-c",
                                        {compartment, false, true, false}),
               std::invalid_argument);
  // Exception on invalid compartment pointer
  ASSERT_THROW(subMetNet.addCompound("C10", {nullptr, false, true, false}),
               std::invalid_argument);
  ASSERT_THROW(subsubMetNet.addCompound("C11", {nullptr, false, true, false}),
               std::invalid_argument);

  // compartment pointer taken from a different network
  metnetlib::MetabolicNetwork<> metNet2;
  // Add a compartment (identical to the one in metNet)
  auto compartment2 = metNet2.addCompartment("c1", 1.0, false);
  // Add a compartment
  auto compartment3 = metNet2.addCompartment("c2", 1.0, false);

  // Try to add compounds with a wrong compartment
  ASSERT_THROW(subMetNet.addCompound("C12", {compartment2, false, true, false}),
               std::invalid_argument);
  ASSERT_THROW(subMetNet.addCompound("C13", {compartment3, false, true, false}),
               std::out_of_range);
  ASSERT_THROW(subsubMetNet.addCompound("C14",
                                        {compartment2, false, true, false}),
               std::invalid_argument);
  ASSERT_THROW(subsubMetNet.addCompound("C15",
                                        {compartment3, false, true, false}),
               std::out_of_range);

  // build another sub-network, add a compartment to it and then try to add a
  // compound to the first sub-network
  whitelistedCompounds = {1, 2, 3, 4};
  whitelistedReactions = {1, 2, 3};
  auto subMetNet2 = metnetlib::SubMetabolicNetwork<>(&metNet,
                                                     whitelistedCompounds,
                                                     whitelistedReactions);
  // The compartment is added also to the root met net but not to all
  // sub-networks
  const auto& compartment4 = subMetNet2.addCompartment("c4", 2.0, true);

  ASSERT_THROW(subMetNet.addCompound("C16", {compartment4, false, true, false}),
               std::invalid_argument);
  ASSERT_THROW(subsubMetNet.addCompound("C17",
                                        {compartment4, false, true, false}),
               std::invalid_argument);
}

TEST_F(TestSubMetabolicNetwork, Test_Reactions) {
  metnetlib::MetabolicNetwork<> metNet;
  // add a compartment
  const auto &compartment = metNet.addCompartment("c1", 1.0, false);

  // Add some compounds
  metNet.addCompound("C1", {compartment, true, false, true});
  metNet.addCompound("C2", {compartment, true, false, true});
  metNet.addCompound("C3", {compartment, true, false, true});
  metNet.addCompound("C4", {compartment, true, false, true});
  metNet.addCompound("C5", {compartment, true, false, true});
  metNet.addCompound("C6", {compartment, true, false, true});
  metNet.addCompound("C7", {compartment, true, false, true});

  // add some reactions
  metNet.addReaction(hglib::NAME, {{"C1", "C2"},
                                   {"C3"}},
                     {"R1", true, false, {{1, true}, {2, true}}, {{1, true}},
                      -1000, 1000});
  metNet.addReaction(hglib::NAME, {{"C4", "C2"},
                                   {"C3"}},
                     {"R2", true, false, {{1, true}, {2, true}}, {{1, true}},
                      -1000, 1000});
  metNet.addReaction(hglib::NAME, {{"C6", "C4"},
                                   {"C2"}},
                     {"R3", true, false, {{1, true}, {2, true}}, {{1, true}},
                      -1000, 1000});

  // build sub-metabolic networks
  std::unordered_set<metnetlib::CompoundIdType> whitelistedCompounds =
      {1, 2, 3};
  std::unordered_set<metnetlib::ReactionIdType> whitelistedReactions =
      {1, 2};
  auto subMetNet = metnetlib::SubMetabolicNetwork<>(&metNet,
                                                    whitelistedCompounds,
                                                    whitelistedReactions);
  whitelistedCompounds = {1, 2, 3};
  whitelistedReactions = {2};
  auto subsubMetNet = metnetlib::SubMetabolicNetwork<>(&subMetNet,
                                                       whitelistedCompounds,
                                                       whitelistedReactions);

  // Check number of reactions
  size_t nbReactionsRootMetNet(3);
  size_t nbReactionsSubMetNet(2);
  size_t nbReactionsSubSubMetNet(1);
  ASSERT_EQ(metNet.nbReactions(), nbReactionsRootMetNet);
  ASSERT_EQ(subMetNet.nbReactions(), nbReactionsSubMetNet);
  ASSERT_EQ(subsubMetNet.nbReactions(), nbReactionsSubSubMetNet);

  // Check number of reversible reactions
  size_t nbRevReactionsRootMetNet(3);
  size_t nbRevReactionsSubMetNet(0);
  size_t nbRevReactionsSubSubMetNet(0);
  ASSERT_EQ(metNet.nbReversibleReactions(), nbRevReactionsRootMetNet);
  ASSERT_EQ(subMetNet.nbReversibleReactions(), nbRevReactionsSubMetNet);
  ASSERT_EQ(subsubMetNet.nbReversibleReactions(), nbRevReactionsSubSubMetNet);

  // Check reaction iterators...
  // ...in root network
  metnetlib::MetabolicNetwork<>::reaction_iterator iterReac(
      metNet.reactionsBegin());
  int nbSplittedReaction(0);
  int expectedNbSplittedReaction(6);
  for (const auto& reaction : metNet.reactions()) {
    ASSERT_EQ(reaction, *iterReac);
    ++iterReac;
    ++nbSplittedReaction;
  }
  ASSERT_EQ(iterReac, metNet.reactionsEnd());
  ASSERT_EQ(nbSplittedReaction, expectedNbSplittedReaction);

  // ...in sub-network
  metnetlib::SubMetabolicNetwork<>::reaction_iterator it(
      subMetNet.reactionsBegin());
  nbSplittedReaction = 0;
  expectedNbSplittedReaction = 2;
  for (const auto& reaction : subMetNet.reactions()) {
    ASSERT_EQ(reaction, *it);
    ++it;
    ++nbSplittedReaction;
  }
  ASSERT_EQ(it, subMetNet.reactionsEnd());
  ASSERT_EQ(nbSplittedReaction, expectedNbSplittedReaction);


  // ...in sub-sub-network
  it = subsubMetNet.reactionsBegin();
  nbSplittedReaction = 0;
  expectedNbSplittedReaction = 1;
  for (const auto& reaction : subsubMetNet.reactions()) {
    ASSERT_EQ(reaction, *it);
    ++it;
    ++nbSplittedReaction;
  }
  ASSERT_EQ(it, subsubMetNet.reactionsEnd());
  ASSERT_EQ(nbSplittedReaction, expectedNbSplittedReaction);


  /* ***************************************************************************
   * ***************************************************************************
   *             Add a reversible reaction in subMetNet
   * ***************************************************************************
   ****************************************************************************/
  const auto& reaction1 = subMetNet.addReaction(
      hglib::NAME, {{"C2"}, {"C3"}},
      {"R_test1", true, false, {{2, true}}, {{1, true}}, -1000, 1000});

  // Check reaction iterators...
  // ...in root network
  iterReac = metNet.reactionsBegin();
  nbSplittedReaction = 0;
  expectedNbSplittedReaction = 8;
  for (const auto& reaction : metNet.reactions()) {
    ASSERT_EQ(reaction, *iterReac);
    ++iterReac;
    ++nbSplittedReaction;
  }
  ASSERT_EQ(iterReac, metNet.reactionsEnd());
  ASSERT_EQ(nbSplittedReaction, expectedNbSplittedReaction);

  // ...in sub-network
  it = subMetNet.reactionsBegin();
  nbSplittedReaction = 0;
  expectedNbSplittedReaction = 4;
  for (const auto& reaction : subMetNet.reactions()) {
    ASSERT_EQ(reaction, *it);
    ++it;
    ++nbSplittedReaction;
  }
  ASSERT_EQ(it, subMetNet.reactionsEnd());
  ASSERT_EQ(nbSplittedReaction, expectedNbSplittedReaction);


  // ...in sub-sub-network
  it = subsubMetNet.reactionsBegin();
  nbSplittedReaction = 0;
  expectedNbSplittedReaction = 1;
  for (const auto& reaction : subsubMetNet.reactions()) {
    ASSERT_EQ(reaction, *it);
    ++it;
    ++nbSplittedReaction;
  }
  ASSERT_EQ(it, subsubMetNet.reactionsEnd());
  ASSERT_EQ(nbSplittedReaction, expectedNbSplittedReaction);

  // Check number of reactions
  ASSERT_EQ(metNet.nbReactions(), ++nbReactionsRootMetNet);
  ASSERT_EQ(subMetNet.nbReactions(), ++nbReactionsSubMetNet);
  ASSERT_EQ(subsubMetNet.nbReactions(), nbReactionsSubSubMetNet);

  // Check number of reversible reactions
  ASSERT_EQ(metNet.nbReversibleReactions(), ++nbRevReactionsRootMetNet);
  ASSERT_EQ(subMetNet.nbReversibleReactions(), ++nbRevReactionsSubMetNet);
  ASSERT_EQ(subsubMetNet.nbReversibleReactions(), nbRevReactionsSubSubMetNet);

  // Check if added reaction is seen as reversible in the sub-network
  ASSERT_TRUE(subMetNet.isReversible(reaction1->id()));
  // Check reversible reaction
  ASSERT_NE(subMetNet.reversibleReaction(reaction1->id()), nullptr);
  ASSERT_EQ(metNet.reversibleReaction(reaction1->id()),
            subMetNet.reversibleReaction(reaction1->id()));

  // Check that reaction R_test1 is in metNet and subMetNet but not in
  // subsubMetNet
  ASSERT_EQ(metNet.reactionBySbmlId("R_test1"), reaction1);
  ASSERT_EQ(subMetNet.reactionBySbmlId("R_test1"), reaction1);
  ASSERT_EQ(subsubMetNet.reactionBySbmlId("R_test1"), nullptr);
  // Check that there is also the reversible reaction
  ASSERT_NE(metNet.reactionBySbmlId(std::string("R_test1") +
                                        metnetlib::suffixReverseReaction),
            nullptr);
  ASSERT_NE(subMetNet.reactionBySbmlId(std::string("R_test1") +
                                           metnetlib::suffixReverseReaction),
            nullptr);
  ASSERT_EQ(subsubMetNet.reactionBySbmlId(std::string("R_test1") +
                                              metnetlib::suffixReverseReaction),
            nullptr);

  /* ***************************************************************************
   * ***************************************************************************
   *  Remove from sub-network one sense from the last added reversible reaction
   * ***************************************************************************
   ****************************************************************************/
  subMetNet.removeReaction(reaction1->id(), false);
  // Check number of reversible reactions
  ASSERT_EQ(metNet.nbReversibleReactions(), nbRevReactionsRootMetNet);
  ASSERT_EQ(subMetNet.nbReversibleReactions(), --nbRevReactionsSubMetNet);
  ASSERT_EQ(subsubMetNet.nbReversibleReactions(), nbRevReactionsSubSubMetNet);

  // Check that the reaction is seen as irreversible now in the sub-network
  // Get reverse reaction
  const auto& revReaction = metNet.reversibleReaction(reaction1->id());
  ASSERT_NE(revReaction, nullptr);
  ASSERT_FALSE(subMetNet.isReversible(revReaction->id()));
  // Check that the reverse sense is a nullptr
  ASSERT_EQ(subMetNet.reversibleReaction(revReaction->id()), nullptr);
  // But the reaction is still reversible in the root network
  ASSERT_TRUE(metNet.isReversible(reaction1->id()));
  ASSERT_TRUE(metNet.isReversible(revReaction->id()));

  // Check reaction iterators...
  // ...in root network
  iterReac = metNet.reactionsBegin();
  nbSplittedReaction = 0;
  expectedNbSplittedReaction = 8;
  for (const auto& reaction : metNet.reactions()) {
    ASSERT_EQ(reaction, *iterReac);
    ++iterReac;
    ++nbSplittedReaction;
  }
  ASSERT_EQ(iterReac, metNet.reactionsEnd());
  ASSERT_EQ(nbSplittedReaction, expectedNbSplittedReaction);

  // ...in sub-network
  it = subMetNet.reactionsBegin();
  nbSplittedReaction = 0;
  expectedNbSplittedReaction = 3;
  for (const auto& reaction : subMetNet.reactions()) {
    ASSERT_EQ(reaction, *it);
    ++it;
    ++nbSplittedReaction;
  }
  ASSERT_EQ(it, subMetNet.reactionsEnd());
  ASSERT_EQ(nbSplittedReaction, expectedNbSplittedReaction);


  // ...in sub-sub-network
  it = subsubMetNet.reactionsBegin();
  nbSplittedReaction = 0;
  expectedNbSplittedReaction = 1;
  for (const auto& reaction : subsubMetNet.reactions()) {
    ASSERT_EQ(reaction, *it);
    ++it;
    ++nbSplittedReaction;
  }
  ASSERT_EQ(it, subsubMetNet.reactionsEnd());
  ASSERT_EQ(nbSplittedReaction, expectedNbSplittedReaction);


  /* ***************************************************************************
   * ***************************************************************************
   *             Add an irreversible reaction in subsubMetNet
   * ***************************************************************************
   ****************************************************************************/
  const auto& reaction2 = subsubMetNet.addReaction(
      hglib::NAME, {{"C3"}, {"C4"}},
      {"R_test2", false, false, {{2, true}}, {{1, true}}, 0, 1000});

  // Check reaction iterators...
  // ...in root network
  iterReac = metNet.reactionsBegin();
  nbSplittedReaction = 0;
  expectedNbSplittedReaction = 9;
  for (const auto& reaction : metNet.reactions()) {
    ASSERT_EQ(reaction, *iterReac);
    ++iterReac;
    ++nbSplittedReaction;
  }
  ASSERT_EQ(iterReac, metNet.reactionsEnd());
  ASSERT_EQ(nbSplittedReaction, expectedNbSplittedReaction);

  // ...in sub-network
  it = subMetNet.reactionsBegin();
  nbSplittedReaction = 0;
  expectedNbSplittedReaction = 4;
  for (const auto& reaction : subMetNet.reactions()) {
    ASSERT_EQ(reaction, *it);
    ++it;
    ++nbSplittedReaction;
  }
  ASSERT_EQ(it, subMetNet.reactionsEnd());
  ASSERT_EQ(nbSplittedReaction, expectedNbSplittedReaction);


  // ...in sub-sub-network
  it = subsubMetNet.reactionsBegin();
  nbSplittedReaction = 0;
  expectedNbSplittedReaction = 2;
  for (const auto& reaction : subsubMetNet.reactions()) {
    ASSERT_EQ(reaction, *it);
    ++it;
    ++nbSplittedReaction;
  }
  ASSERT_EQ(it, subsubMetNet.reactionsEnd());
  ASSERT_EQ(nbSplittedReaction, expectedNbSplittedReaction);

  ASSERT_EQ(metNet.reactionBySbmlId("R_test2"), reaction2);
  ASSERT_EQ(subMetNet.reactionBySbmlId("R_test2"), reaction2);
  ASSERT_EQ(subsubMetNet.reactionBySbmlId("R_test2"), reaction2);

  // Check number of reactions
  ASSERT_EQ(metNet.nbReactions(), ++nbReactionsRootMetNet);
  ASSERT_EQ(subMetNet.nbReactions(), ++nbReactionsSubMetNet);
  ASSERT_EQ(subsubMetNet.nbReactions(), ++nbReactionsSubSubMetNet);

  // Check number of reversible reactions
  ASSERT_EQ(metNet.nbReversibleReactions(), nbRevReactionsRootMetNet);
  ASSERT_EQ(subMetNet.nbReversibleReactions(), nbRevReactionsSubMetNet);
  ASSERT_EQ(subsubMetNet.nbReversibleReactions(), nbRevReactionsSubSubMetNet);

  // Check if added reaction is seen as irreversible in the sub-networks
  ASSERT_FALSE(subMetNet.isReversible(reaction2->id()));
  ASSERT_FALSE(subsubMetNet.isReversible(reaction2->id()));
  // Check that the reverse sense is a nullptr
  ASSERT_EQ(subMetNet.reversibleReaction(reaction2->id()), nullptr);
  ASSERT_EQ(subsubMetNet.reversibleReaction(reaction2->id()), nullptr);

  // Check exceptions of invalid reactions
  // Add a reaction with an already existing Sbml Id
  ASSERT_THROW(subMetNet.addReaction(hglib::NAME, {{"C4"}, {"C7"}},
                                  {"R2", true, true, {{1, true}}, {{2, true}},
                                   -100, 100}),
               std::invalid_argument);
  ASSERT_THROW(subsubMetNet.addReaction(
      hglib::NAME, {{"C4"}, {"C7"}},
      {"R2", true, true, {{1, true}}, {{2, true}}, -100, 100}),
               std::invalid_argument);
  // Add a reaction with undefined compound (substrate)
  ASSERT_THROW(subMetNet.addReaction(hglib::NAME, {{"C8"}, {"C7"}},
                                     {"R4", true, true, {{1, true}},
                                      {{2, true}}, -100, 100}),
               std::invalid_argument);
  ASSERT_THROW(subsubMetNet.addReaction(hglib::NAME, {{"C8"}, {"C7"}},
                                        {"R4", true, true, {{1, true}},
                                         {{2, true}}, -100, 100}),
               std::invalid_argument);
  // Add a reaction with undefined compound (product)
  ASSERT_THROW(subMetNet.addReaction(hglib::NAME, {{"C7"}, {"C8"}},
                                  {"R4", true, true, {{1, true}}, {{2, true}},
                                   -100, 100}),
               std::invalid_argument);
  ASSERT_THROW(subsubMetNet.addReaction(hglib::NAME, {{"C7"}, {"C8"}},
                                        {"R4", true, true, {{1, true}},
                                         {{2, true}}, -100, 100}),
               std::invalid_argument);

  // Add a reaction R4 with too less substrate stoichiometry entries
  ASSERT_THROW(subMetNet.addReaction(hglib::NAME, {{"C4", "C8"}, {"C2", "C7"}},
                                  {"R4", false, false, {{2, true}},
                                   {{3, true}, {1, true}}, 0, 1000}),
               std::invalid_argument);
  ASSERT_THROW(subsubMetNet.addReaction(hglib::NAME, {{"C4", "C8"},
                                                      {"C2", "C7"}},
                                        {"R4", false, false, {{2, true}},
                                         {{3, true}, {1, true}}, 0, 1000}),
               std::invalid_argument);
  // Add a reaction R4 with too many substrate stoichiometry entries
  ASSERT_THROW(subMetNet.addReaction(hglib::NAME, {{"C4", "C8"}, {"C2", "C7"}},
                                     {"R4", false, false,
                                      {{1, true}, {2, true}, {2, true}},
                                      {{3, true}, {1, true}}, 0, 1000}),
               std::invalid_argument);
  ASSERT_THROW(subsubMetNet.addReaction(hglib::NAME, {{"C4", "C8"},
                                                      {"C2", "C7"}},
                                        {"R4", false, false, {{1, true},
                                                              {2, true},
                                                              {2, true}},
                                         {{3, true}, {1, true}}, 0, 1000}),
               std::invalid_argument);
  // Add a reaction R4 with too less product stoichiometry entries
  ASSERT_THROW(subMetNet.addReaction(hglib::NAME, {{"C4", "C8"}, {"C2", "C7"}},
                                  {"R4", false, false, {{1, true}, {2, true}},
                                   {{1, true}}, 0, 1000}),
               std::invalid_argument);
  ASSERT_THROW(subsubMetNet.addReaction(hglib::NAME, {{"C4", "C8"},
                                                      {"C2", "C7"}},
                                        {"R4", false, false, {{1, true},
                                                              {2, true}},
                                         {{1, true}}, 0, 1000}),
               std::invalid_argument);
  // Add a reaction R4 with too many product stoichiometry entries
  ASSERT_THROW(subMetNet.addReaction(hglib::NAME, {{"C4", "C8"}, {"C2", "C7"}},
                                     {"R4", false, false,
                                      {{2, true}, {2, true}}, {{3, true},
                                                               {1, true},
                                                               {2, true}}, 0,
                                      1000}),
               std::invalid_argument);
  ASSERT_THROW(subsubMetNet.addReaction(hglib::NAME, {{"C4", "C8"},
                                                      {"C2", "C7"}},
                                        {"R4", false, false,
                                         {{2, true}, {2, true}}, {{3, true},
                                                                  {1, true},
                                                                  {2, true}}, 0,
                                         1000}),
               std::invalid_argument);
  // Add R4 with the right number of stoichiometry entries.
  subsubMetNet.addReaction(hglib::NAME, {{"C2", "C3"}, {"C2", "C4"}},
                           {"R4", false, false, {{2, true}, {2, true}},
                            {{3, true}, {1, true}}, 0, 1000});
  ASSERT_NE(metNet.reactionBySbmlId("R4"), nullptr);
  ASSERT_NE(subMetNet.reactionBySbmlId("R4"), nullptr);
  ASSERT_NE(subsubMetNet.reactionBySbmlId("R4"), nullptr);

  // Add a irreversible reaction but with negative bound
  ASSERT_THROW(subMetNet.addReaction(hglib::NAME, {{"C4"}, {"C2"}},
                                     {"R5", false, false, {{2, true}},
                                      {{3, true}}, -10, 1000}),
               std::invalid_argument);
  ASSERT_THROW(subsubMetNet.addReaction(hglib::NAME, {{"C4"}, {"C2"}},
                                        {"R5", false, false, {{2, true}},
                                         {{3, true}}, -10, 1000}),
               std::invalid_argument);

  // Check exception on invalid Sbml Ids
  ASSERT_THROW(subMetNet.addReaction(hglib::NAME, {{"C4"}, {"C2"}},
                                     {"", false, false, {{2, true}},
                                      {{3, true}}, 0, 1000}),
               std::invalid_argument);
  ASSERT_THROW(subsubMetNet.addReaction(hglib::NAME, {{"C4"}, {"C2"}},
                                        {"", false, false, {{2, true}},
                                         {{3, true}}, 0, 1000}),
               std::invalid_argument);

  ASSERT_THROW(subMetNet.addReaction(hglib::NAME, {{"C4"}, {"C2"}},
                                     {"5", false, false, {{2, true}},
                                      {{3, true}}, 0, 1000}),
               std::invalid_argument);
  ASSERT_THROW(subsubMetNet.addReaction(hglib::NAME, {{"C4"}, {"C2"}},
                                        {"5", false, false, {{2, true}},
                                         {{3, true}}, 0, 1000}),
               std::invalid_argument);

  ASSERT_THROW(subMetNet.addReaction(hglib::NAME, {{"C4"}, {"C2"}},
                                     {"5_c", false, false, {{2, true}},
                                      {{3, true}}, 0, 1000}),
               std::invalid_argument);
  ASSERT_THROW(subsubMetNet.addReaction(hglib::NAME, {{"C4"}, {"C2"}},
                                        {"5_c", false, false, {{2, true}},
                                         {{3, true}}, 0, 1000}),
               std::invalid_argument);

  ASSERT_THROW(subMetNet.addReaction(hglib::NAME, {{"C4"}, {"C2"}},
                                     {"-R5", false, false, {{2, true}},
                                      {{3, true}}, 0, 1000}),
               std::invalid_argument);
  ASSERT_THROW(subsubMetNet.addReaction(hglib::NAME, {{"C4"}, {"C2"}},
                                        {"-R5", false, false, {{2, true}},
                                         {{3, true}}, 0, 1000}),
               std::invalid_argument);
}

TEST_F(TestSubMetabolicNetwork, Test_removeReaction) {
  metnetlib::MetabolicNetwork<> metNet;
  // add a compartment
  const auto &compartment = metNet.addCompartment("c1", 1.0, false);

  // Add some compounds
  metNet.addCompound("C1", {compartment, true, false, true});
  metNet.addCompound("C2", {compartment, true, false, true});
  metNet.addCompound("C3", {compartment, true, false, true});
  metNet.addCompound("C4", {compartment, true, false, true});
  metNet.addCompound("C5", {compartment, true, false, true});
  metNet.addCompound("C6", {compartment, true, false, true});
  metNet.addCompound("C7", {compartment, true, false, true});

  // add some reactions
  metNet.addReaction(hglib::NAME, {{"C1", "C2"},
                                   {"C3"}},
                     {"R1", true, false, {{1, true}, {2, true}}, {{1, true}},
                      -1000, 1000});
  metNet.addReaction(hglib::NAME, {{"C4", "C2"},
                                   {"C3"}},
                     {"R2", true, false, {{1, true}, {2, true}}, {{1, true}},
                      -1000, 1000});
  metNet.addReaction(hglib::NAME, {{"C6", "C4"},
                                   {"C2"}},
                     {"R3", true, false, {{1, true}, {2, true}}, {{1, true}},
                      -1000, 1000});

  // build sub-metabolic networks
  std::unordered_set<metnetlib::CompoundIdType> whitelistedCompounds =
      {1, 2, 3};
  std::unordered_set<metnetlib::ReactionIdType> whitelistedReactions =
      {0, 1, 2, 3};
  auto subMetNet = metnetlib::SubMetabolicNetwork<>(&metNet,
                                                    whitelistedCompounds,
                                                    whitelistedReactions);
  whitelistedCompounds = {1, 2, 3};
  whitelistedReactions = {0, 2, 3};
  auto subsubMetNet = metnetlib::SubMetabolicNetwork<>(&subMetNet,
                                                       whitelistedCompounds,
                                                       whitelistedReactions);

  // Remove reaction (not the reverse sense) from root met net
  metNet.removeReaction(0, false);
  ASSERT_EQ(metNet.reactionById(0), nullptr);
  // Check that the reverse sense is still there
  ASSERT_NE(metNet.reactionById(1), nullptr);
  // Check that the reaction with Id 0 is not anymore in the sub-networks
  ASSERT_EQ(subMetNet.reactionById(0), nullptr);
  ASSERT_EQ(subsubMetNet.reactionById(0), nullptr);
  // Check that the reverse sense is still in the sub-network
  ASSERT_NE(subMetNet.reactionById(1), nullptr);


  // remove reaction from subnetwork
  subMetNet.removeReaction(1, false);
  // Check that the reaction is still in the root network
  ASSERT_NE(metNet.reactionById(1), nullptr);
  // But the reaction is not anymore in the sub-network
  ASSERT_EQ(subMetNet.reactionById(1), nullptr);

  // remove reaction in subnetwork in its subnetworks
  subMetNet.removeReaction(2, true);
  // Check that the reaction and the reverse reaction is not anymore in the
  // sub-networks
  ASSERT_EQ(subMetNet.reactionById(2), nullptr);
  ASSERT_EQ(subMetNet.reactionById(3), nullptr);
  ASSERT_EQ(subsubMetNet.reactionById(2), nullptr);
  ASSERT_EQ(subsubMetNet.reactionById(3), nullptr);
  // But the reactions are still in the root network
  ASSERT_NE(metNet.reactionById(2), nullptr);
  ASSERT_NE(metNet.reactionById(3), nullptr);
}

TEST_F(TestSubMetabolicNetwork, Test_substrates) {
  metnetlib::MetabolicNetwork<> metNet;
  // add a compartment
  const auto &compartment = metNet.addCompartment("c1", 1.0, false);

  // Add some compounds
  metNet.addCompound("C1", {compartment, true, false, true});
  metNet.addCompound("C2", {compartment, true, false, true});
  metNet.addCompound("C3", {compartment, true, false, true});
  metNet.addCompound("C4", {compartment, true, false, true});
  metNet.addCompound("C5", {compartment, true, false, true});

  // add some reactions
  metNet.addReaction(hglib::NAME, {{"C1", "C2"},
                                   {"C3"}},
                     {"R1", true, false, {{1, true}, {2, true}}, {{1, true}},
                      -1000, 1000});
  metNet.addReaction(hglib::NAME, {{"C4", "C2"},
                                   {"C3"}},
                     {"R2", true, false, {{1, true}, {2, true}}, {{1, true}},
                      -1000, 1000});
  metNet.addReaction(hglib::NAME, {{"C5", "C4"},
                                   {"C2"}},
                     {"R3", true, false, {{1, true}, {2, true}}, {{1, true}},
                      -1000, 1000});

  // build sub-metabolic network
  std::unordered_set<metnetlib::CompoundIdType> whitelistedCompounds =
      {0, 1, 2};
  std::unordered_set<metnetlib::ReactionIdType> whitelistedReactions =
      {0, 1, 2, 3, 4};
  auto subMetNet = metnetlib::SubMetabolicNetwork<>(&metNet,
                                                    whitelistedCompounds,
                                                    whitelistedReactions);

  // Check substrates
  // Check if a reaction has substrates
  ASSERT_TRUE(subMetNet.hasSubstrates(0));
  ASSERT_TRUE(subMetNet.hasSubstrates(1));
  ASSERT_TRUE(subMetNet.hasSubstrates(2));
  ASSERT_TRUE(subMetNet.hasSubstrates(3));
  ASSERT_FALSE(subMetNet.hasSubstrates(4));
  // Check exceptions
  ASSERT_THROW(subMetNet.hasSubstrates(5), std::invalid_argument);
  ASSERT_THROW(subMetNet.hasSubstrates(10), std::out_of_range);

  // Check number of substrates (compared to the root network)
  ASSERT_EQ(subMetNet.nbSubstrates(0), (size_t) 2);
  ASSERT_EQ(metNet.nbSubstrates(0), (size_t) 2);
  ASSERT_EQ(subMetNet.nbSubstrates(1), (size_t) 1);
  ASSERT_EQ(metNet.nbSubstrates(1), (size_t) 1);
  ASSERT_EQ(subMetNet.nbSubstrates(2), (size_t) 1);
  ASSERT_EQ(metNet.nbSubstrates(2), (size_t) 2);
  ASSERT_EQ(subMetNet.nbSubstrates(3), (size_t) 1);
  ASSERT_EQ(metNet.nbSubstrates(3), (size_t) 1);
  ASSERT_EQ(subMetNet.nbSubstrates(4), (size_t) 0);
  ASSERT_EQ(metNet.nbSubstrates(4), (size_t) 2);
  // Check exceptions
  ASSERT_THROW(subMetNet.nbSubstrates(5), std::invalid_argument);
  ASSERT_THROW(subMetNet.nbSubstrates(10), std::out_of_range);

  // Check isSubstrateOfReaction
  ASSERT_TRUE(subMetNet.isSubstrateOfReaction(0, 0));
  ASSERT_TRUE(subMetNet.isSubstrateOfReaction(1, 0));
  ASSERT_TRUE(subMetNet.isSubstrateOfReaction(2, 1));
  ASSERT_TRUE(subMetNet.isSubstrateOfReaction(1, 2));
  ASSERT_TRUE(subMetNet.isSubstrateOfReaction(2, 3));
  ASSERT_FALSE(subMetNet.isSubstrateOfReaction(0, 3));
  // Check exceptions
  ASSERT_THROW(subMetNet.isSubstrateOfReaction(3, 2), std::invalid_argument);
  ASSERT_THROW(subMetNet.isSubstrateOfReaction(3, 4), std::invalid_argument);
  ASSERT_THROW(subMetNet.isSubstrateOfReaction(4, 4), std::invalid_argument);
  ASSERT_THROW(subMetNet.isSubstrateOfReaction(1, 5), std::invalid_argument);
  ASSERT_THROW(subMetNet.isSubstrateOfReaction(10, 2), std::out_of_range);
  ASSERT_THROW(subMetNet.isSubstrateOfReaction(2, 10), std::out_of_range);

  // Check stoichiometry
  ASSERT_EQ(subMetNet.substrateStoichiometry(0, 0), 1);
  ASSERT_EQ(subMetNet.substrateStoichiometry(1, 0), 2);
  ASSERT_EQ(subMetNet.substrateStoichiometry(2, 1), 1);
  ASSERT_EQ(subMetNet.substrateStoichiometry(1, 2), 2);
  ASSERT_EQ(subMetNet.substrateStoichiometry(2, 3), 1);
  // Check exceptions
  ASSERT_THROW(subMetNet.substrateStoichiometry(0, 3), std::invalid_argument);
  ASSERT_THROW(subMetNet.substrateStoichiometry(3, 2), std::invalid_argument);
  ASSERT_THROW(subMetNet.substrateStoichiometry(3, 4), std::invalid_argument);
  ASSERT_THROW(subMetNet.substrateStoichiometry(4, 4), std::invalid_argument);
  ASSERT_THROW(subMetNet.substrateStoichiometry(1, 5), std::invalid_argument);
  ASSERT_THROW(subMetNet.substrateStoichiometry(10, 2), std::out_of_range);
  ASSERT_THROW(subMetNet.substrateStoichiometry(2, 10), std::out_of_range);

  // Check substrate iterators
  // of reaction 0
  metnetlib::ReactionIdType reactionId = 0;
  metnetlib::SubMetabolicNetwork<>::compound_iterator it =
      subMetNet.substratesBegin(reactionId);
  int expectedNbSubstrates = 2;
  int nbSubstrates = 0;
  auto substratesPtr = subMetNet.substrates(reactionId);
  for (const auto& substrate : *substratesPtr) {
    ASSERT_EQ(substrate, *it);
    ++it;
    ++nbSubstrates;
  }
  ASSERT_EQ(it, subMetNet.substratesEnd(reactionId));
  ASSERT_EQ(nbSubstrates, expectedNbSubstrates);
  // of reaction 1
  reactionId = 1;
  it = subMetNet.substratesBegin(reactionId);
  expectedNbSubstrates = 1;
  nbSubstrates = 0;
  substratesPtr = subMetNet.substrates(reactionId);
  for (const auto& substrate : *substratesPtr) {
    ASSERT_EQ(substrate, *it);
    ++it;
    ++nbSubstrates;
  }
  ASSERT_EQ(it, subMetNet.substratesEnd(reactionId));
  ASSERT_EQ(nbSubstrates, expectedNbSubstrates);
  // of reaction 2
  reactionId = 2;
  it = subMetNet.substratesBegin(reactionId);
  expectedNbSubstrates = 1;
  nbSubstrates = 0;
  substratesPtr = subMetNet.substrates(reactionId);
  for (const auto& substrate : *substratesPtr) {
    ASSERT_EQ(substrate, *it);
    ++it;
    ++nbSubstrates;
  }
  ASSERT_EQ(it, subMetNet.substratesEnd(reactionId));
  ASSERT_EQ(nbSubstrates, expectedNbSubstrates);
  // of reaction 3
  reactionId = 3;
  it = subMetNet.substratesBegin(reactionId);
  expectedNbSubstrates = 1;
  nbSubstrates = 0;
  substratesPtr = subMetNet.substrates(reactionId);
  for (const auto& substrate : *substratesPtr) {
    ASSERT_EQ(substrate, *it);
    ++it;
    ++nbSubstrates;
  }
  ASSERT_EQ(it, subMetNet.substratesEnd(reactionId));
  ASSERT_EQ(nbSubstrates, expectedNbSubstrates);
  // of reaction 4
  reactionId = 4;
  it = subMetNet.substratesBegin(reactionId);
  expectedNbSubstrates = 0;
  nbSubstrates = 0;
  substratesPtr = subMetNet.substrates(reactionId);
  for (const auto& substrate : *substratesPtr) {
    ASSERT_EQ(substrate, *it);
    ++it;
    ++nbSubstrates;
  }
  ASSERT_EQ(it, subMetNet.substratesEnd(reactionId));
  ASSERT_EQ(nbSubstrates, expectedNbSubstrates);

  // Check exceptions of iterators
  ASSERT_THROW(subMetNet.substratesBegin(5), std::invalid_argument);
  ASSERT_THROW(subMetNet.substratesBegin(10), std::out_of_range);
  ASSERT_THROW(subMetNet.substratesEnd(5), std::invalid_argument);
  ASSERT_THROW(subMetNet.substratesEnd(10), std::out_of_range);
  ASSERT_THROW(subMetNet.substrates(5), std::invalid_argument);
  ASSERT_THROW(subMetNet.substrates(10), std::out_of_range);
}

TEST_F(TestSubMetabolicNetwork, Test_products) {
  metnetlib::MetabolicNetwork<> metNet;
  // add a compartment
  const auto &compartment = metNet.addCompartment("c1", 1.0, false);

  // Add some compounds
  metNet.addCompound("C1", {compartment, true, false, true});
  metNet.addCompound("C2", {compartment, true, false, true});
  metNet.addCompound("C3", {compartment, true, false, true});
  metNet.addCompound("C4", {compartment, true, false, true});
  metNet.addCompound("C5", {compartment, true, false, true});

  // add some reactions
  metNet.addReaction(hglib::NAME, {{"C1", "C2"},
                                   {"C3"}},
                     {"R1", true, false, {{1, true}, {2, true}}, {{1, true}},
                      -1000, 1000});
  metNet.addReaction(hglib::NAME, {{"C4", "C2"},
                                   {"C3"}},
                     {"R2", true, false, {{1, true}, {2, true}}, {{1, true}},
                      -1000, 1000});
  metNet.addReaction(hglib::NAME, {{"C5", "C4"},
                                   {"C2"}},
                     {"R3", true, false, {{1, true}, {2, true}}, {{1, true}},
                      -1000, 1000});

  // build sub-metabolic network
  std::unordered_set<metnetlib::CompoundIdType> whitelistedCompounds =
      {0, 1};
  std::unordered_set<metnetlib::ReactionIdType> whitelistedReactions =
      {0, 1, 3};
  auto subMetNet = metnetlib::SubMetabolicNetwork<>(&metNet,
                                                    whitelistedCompounds,
                                                    whitelistedReactions);

  // Check products
  // Check if a reaction has products
  ASSERT_FALSE(subMetNet.hasProducts(0));
  ASSERT_TRUE(subMetNet.hasProducts(1));
  ASSERT_TRUE(subMetNet.hasProducts(3));
  // Check exceptions
  ASSERT_THROW(subMetNet.hasProducts(5), std::invalid_argument);
  ASSERT_THROW(subMetNet.hasProducts(10), std::out_of_range);

  // Check number of products (compared to the root network)
  ASSERT_EQ(subMetNet.nbProducts(0), (size_t) 0);
  ASSERT_EQ(metNet.nbProducts(0), (size_t) 1);
  ASSERT_EQ(subMetNet.nbProducts(1), (size_t) 2);
  ASSERT_EQ(metNet.nbProducts(1), (size_t) 2);
  ASSERT_EQ(subMetNet.nbProducts(3), (size_t) 1);
  ASSERT_EQ(metNet.nbProducts(3), (size_t) 2);
  // Check exceptions
  ASSERT_THROW(subMetNet.nbProducts(5), std::invalid_argument);
  ASSERT_THROW(subMetNet.nbProducts(10), std::out_of_range);

    // Check isProductOfReaction
  ASSERT_TRUE(subMetNet.isProductOfReaction(0, 1));
  ASSERT_TRUE(subMetNet.isProductOfReaction(1, 1));
  ASSERT_TRUE(subMetNet.isProductOfReaction(1, 3));
  ASSERT_FALSE(subMetNet.isProductOfReaction(0, 0));
  ASSERT_FALSE(subMetNet.isProductOfReaction(1, 0));
  ASSERT_FALSE(subMetNet.isProductOfReaction(0, 3));
  // Check exceptions
  ASSERT_THROW(subMetNet.isProductOfReaction(2, 0), std::invalid_argument);
  ASSERT_THROW(subMetNet.isProductOfReaction(3, 3), std::invalid_argument);
  ASSERT_THROW(subMetNet.isProductOfReaction(2, 2), std::invalid_argument);
  ASSERT_THROW(subMetNet.isProductOfReaction(1, 5), std::invalid_argument);
  ASSERT_THROW(subMetNet.isProductOfReaction(10, 0), std::out_of_range);
  ASSERT_THROW(subMetNet.isProductOfReaction(0, 10), std::out_of_range);

  // Check stoichiometry
  ASSERT_EQ(subMetNet.productStoichiometry(0, 1), 1);
  ASSERT_EQ(subMetNet.productStoichiometry(1, 1), 2);
  ASSERT_EQ(subMetNet.productStoichiometry(1, 3), 2);
  // Check exceptions
  ASSERT_THROW(subMetNet.productStoichiometry(2, 0), std::invalid_argument);
  ASSERT_THROW(subMetNet.productStoichiometry(3, 3), std::invalid_argument);
  ASSERT_THROW(subMetNet.productStoichiometry(2, 2), std::invalid_argument);
  ASSERT_THROW(subMetNet.productStoichiometry(1, 5), std::invalid_argument);
  ASSERT_THROW(subMetNet.productStoichiometry(10, 0), std::out_of_range);
  ASSERT_THROW(subMetNet.productStoichiometry(0, 10), std::out_of_range);

  // Check product iterators
  // of reaction 0
  metnetlib::ReactionIdType reactionId = 0;
  metnetlib::SubMetabolicNetwork<>::compound_iterator it =
      subMetNet.productsBegin(reactionId);
  int expectedNbProducts = 0;
  int nbProducts = 0;
  auto productsPtr = subMetNet.products(reactionId);
  for (const auto& product : *productsPtr) {
    ASSERT_EQ(product, *it);
    ++it;
    ++nbProducts;
  }
  ASSERT_EQ(it, subMetNet.productsEnd(reactionId));
  ASSERT_EQ(nbProducts, expectedNbProducts);
  // of reaction 1
  reactionId = 1;
  it = subMetNet.productsBegin(reactionId);
  expectedNbProducts = 2;
  nbProducts = 0;
  productsPtr = subMetNet.products(reactionId);
  for (const auto& product : *productsPtr) {
    ASSERT_EQ(product, *it);
    ++it;
    ++nbProducts;
  }
  ASSERT_EQ(it, subMetNet.productsEnd(reactionId));
  ASSERT_EQ(nbProducts, expectedNbProducts);
  // of reaction 3
  reactionId = 3;
  it = subMetNet.productsBegin(reactionId);
  expectedNbProducts = 1;
  nbProducts = 0;
  productsPtr = subMetNet.products(reactionId);
  for (const auto& product : *productsPtr) {
    ASSERT_EQ(product, *it);
    ++it;
    ++nbProducts;
  }
  ASSERT_EQ(it, subMetNet.productsEnd(reactionId));
  ASSERT_EQ(nbProducts, expectedNbProducts);

  // Check exceptions of iterators
  ASSERT_THROW(subMetNet.productsBegin(5), std::invalid_argument);
  ASSERT_THROW(subMetNet.productsBegin(10), std::out_of_range);
  ASSERT_THROW(subMetNet.productsEnd(5), std::invalid_argument);
  ASSERT_THROW(subMetNet.productsEnd(10), std::out_of_range);
  ASSERT_THROW(subMetNet.products(5), std::invalid_argument);
  ASSERT_THROW(subMetNet.products(10), std::out_of_range);
}

TEST_F(TestSubMetabolicNetwork, Test_consumingReactions) {
  metnetlib::MetabolicNetwork<> metNet;
  // add a compartment
  const auto &compartment = metNet.addCompartment("c1", 1.0, false);

  // Add some compounds
  metNet.addCompound("C1", {compartment, true, false, true});
  metNet.addCompound("C2", {compartment, true, false, true});
  metNet.addCompound("C3", {compartment, true, false, true});
  metNet.addCompound("C4", {compartment, true, false, true});
  metNet.addCompound("C5", {compartment, true, false, true});

  // add some reactions
  metNet.addReaction(hglib::NAME, {{"C1", "C2"},
                                   {"C3"}},
                     {"R1", true, false, {{1, true}, {2, true}}, {{1, true}},
                      -1000, 1000});
  metNet.addReaction(hglib::NAME, {{"C4", "C2"},
                                   {"C3"}},
                     {"R2", true, false, {{1, true}, {2, true}}, {{1, true}},
                      -1000, 1000});
  metNet.addReaction(hglib::NAME, {{"C5", "C4"},
                                   {"C2"}},
                     {"R3", true, false, {{1, true}, {2, true}}, {{1, true}},
                      -1000, 1000});

  // build sub-metabolic network
  std::unordered_set<metnetlib::CompoundIdType> whitelistedCompounds =
      {0, 1, 2, 3};
  std::unordered_set<metnetlib::ReactionIdType> whitelistedReactions =
      {0, 2};
  auto subMetNet = metnetlib::SubMetabolicNetwork<>(&metNet,
                                                    whitelistedCompounds,
                                                    whitelistedReactions);

  // Check consuming reactions
  // Check if a compound has consuming reactions
  ASSERT_TRUE(subMetNet.isConsumed(0));
  ASSERT_TRUE(subMetNet.isConsumed(1));
  ASSERT_FALSE(subMetNet.isConsumed(2));
  ASSERT_TRUE(subMetNet.isConsumed(3));
  // Check exceptions
  ASSERT_THROW(subMetNet.isConsumed(4), std::invalid_argument);
  ASSERT_THROW(subMetNet.isConsumed(10), std::out_of_range);

  // Check nbConsumingReactions
  ASSERT_EQ(subMetNet.nbConsumingReactions(0), (size_t) 1);
  ASSERT_EQ(metNet.nbConsumingReactions(0), (size_t) 1);
  ASSERT_EQ(subMetNet.nbConsumingReactions(1), (size_t) 2);
  ASSERT_EQ(metNet.nbConsumingReactions(1), (size_t) 3);
  ASSERT_EQ(subMetNet.nbConsumingReactions(2), (size_t) 0);
  ASSERT_EQ(metNet.nbConsumingReactions(2), (size_t) 2);
  ASSERT_EQ(subMetNet.nbConsumingReactions(3), (size_t) 1);
  ASSERT_EQ(metNet.nbConsumingReactions(3), (size_t) 2);
  // Check exceptions
  ASSERT_THROW(subMetNet.nbProducts(4), std::invalid_argument);
  ASSERT_THROW(subMetNet.nbProducts(10), std::out_of_range);

  // Check consuming reactions iterators
  // of compound 0
  metnetlib::CompoundIdType compoundId = 0;
  metnetlib::SubMetabolicNetwork<>::reaction_iterator it =
      subMetNet.consumingReactionsBegin(compoundId);
  int expectedNbConsumingReactions = 1;
  int nbConsumingReactions = 0;
  auto consumingReactionsPtr = subMetNet.consumingReactions(compoundId);
  for (const auto& reaction : *consumingReactionsPtr) {
    ASSERT_EQ(reaction, *it);
    ++it;
    ++nbConsumingReactions;
  }
  ASSERT_EQ(it, subMetNet.consumingReactionsEnd(compoundId));
  ASSERT_EQ(nbConsumingReactions, expectedNbConsumingReactions);
  // of compound 1
  compoundId = 1;
  it = subMetNet.consumingReactionsBegin(compoundId);
  expectedNbConsumingReactions = 2;
  nbConsumingReactions = 0;
  consumingReactionsPtr = subMetNet.consumingReactions(compoundId);
  for (const auto& reaction : *consumingReactionsPtr) {
    ASSERT_EQ(reaction, *it);
    ++it;
    ++nbConsumingReactions;
  }
  ASSERT_EQ(it, subMetNet.consumingReactionsEnd(compoundId));
  ASSERT_EQ(nbConsumingReactions, expectedNbConsumingReactions);
  // of compound 2
  compoundId = 2;
  it = subMetNet.consumingReactionsBegin(compoundId);
  expectedNbConsumingReactions = 0;
  nbConsumingReactions = 0;
  consumingReactionsPtr = subMetNet.consumingReactions(compoundId);
  for (const auto& reaction : *consumingReactionsPtr) {
    ASSERT_EQ(reaction, *it);
    ++it;
    ++nbConsumingReactions;
  }
  ASSERT_EQ(it, subMetNet.consumingReactionsEnd(compoundId));
  ASSERT_EQ(nbConsumingReactions, expectedNbConsumingReactions);
  // of compound 3
  compoundId = 3;
  it = subMetNet.consumingReactionsBegin(compoundId);
  expectedNbConsumingReactions = 1;
  nbConsumingReactions = 0;
  consumingReactionsPtr = subMetNet.consumingReactions(compoundId);
  for (const auto& reaction : *consumingReactionsPtr) {
    ASSERT_EQ(reaction, *it);
    ++it;
    ++nbConsumingReactions;
  }
  ASSERT_EQ(it, subMetNet.consumingReactionsEnd(compoundId));
  ASSERT_EQ(nbConsumingReactions, expectedNbConsumingReactions);

  // Check exceptions of iterators
  ASSERT_THROW(subMetNet.consumingReactionsBegin(4), std::invalid_argument);
  ASSERT_THROW(subMetNet.consumingReactionsBegin(10), std::out_of_range);
  ASSERT_THROW(subMetNet.consumingReactionsEnd(4), std::invalid_argument);
  ASSERT_THROW(subMetNet.consumingReactionsEnd(10), std::out_of_range);
  ASSERT_THROW(subMetNet.consumingReactions(4), std::invalid_argument);
  ASSERT_THROW(subMetNet.consumingReactions(10), std::out_of_range);
}

TEST_F(TestSubMetabolicNetwork, Test_producingReactions) {
  metnetlib::MetabolicNetwork<> metNet;
  // add a compartment
  const auto &compartment = metNet.addCompartment("c1", 1.0, false);

  // Add some compounds
  metNet.addCompound("C1", {compartment, true, false, true});
  metNet.addCompound("C2", {compartment, true, false, true});
  metNet.addCompound("C3", {compartment, true, false, true});
  metNet.addCompound("C4", {compartment, true, false, true});
  metNet.addCompound("C5", {compartment, true, false, true});

  // add some reactions
  metNet.addReaction(hglib::NAME, {{"C1", "C2"},
                                   {"C3"}},
                     {"R1", true, false, {{1, true}, {2, true}}, {{1, true}},
                      -1000, 1000});
  metNet.addReaction(hglib::NAME, {{"C4", "C2"},
                                   {"C3"}},
                     {"R2", true, false, {{1, true}, {2, true}}, {{1, true}},
                      -1000, 1000});
  metNet.addReaction(hglib::NAME, {{"C5", "C4"},
                                   {"C2"}},
                     {"R3", true, false, {{1, true}, {2, true}}, {{1, true}},
                      -1000, 1000});

  // build sub-metabolic network
  std::unordered_set<metnetlib::CompoundIdType> whitelistedCompounds =
      {0, 1, 2, 3};
  std::unordered_set<metnetlib::ReactionIdType> whitelistedReactions =
      {1, 3};
  auto subMetNet = metnetlib::SubMetabolicNetwork<>(&metNet,
                                                    whitelistedCompounds,
                                                    whitelistedReactions);

  // Check producing reactions
  // Check if a compound has producing reactions
  ASSERT_TRUE(subMetNet.isProduced(0));
  ASSERT_TRUE(subMetNet.isProduced(1));
  ASSERT_FALSE(subMetNet.isProduced(2));
  ASSERT_TRUE(subMetNet.isProduced(3));
  // Check exceptions
  ASSERT_THROW(subMetNet.isProduced(4), std::invalid_argument);
  ASSERT_THROW(subMetNet.isProduced(10), std::out_of_range);

  // Check nbProducingReactions
  ASSERT_EQ(subMetNet.nbProducingReactions(0), (size_t) 1);
  ASSERT_EQ(metNet.nbProducingReactions(0), (size_t) 1);
  ASSERT_EQ(subMetNet.nbProducingReactions(1), (size_t) 2);
  ASSERT_EQ(metNet.nbProducingReactions(1), (size_t) 3);
  ASSERT_EQ(subMetNet.nbProducingReactions(2), (size_t) 0);
  ASSERT_EQ(metNet.nbProducingReactions(2), (size_t) 2);
  ASSERT_EQ(subMetNet.nbProducingReactions(3), (size_t) 1);
  ASSERT_EQ(metNet.nbProducingReactions(3), (size_t) 2);
  // Check exceptions
  ASSERT_THROW(subMetNet.nbProducingReactions(4), std::invalid_argument);
  ASSERT_THROW(subMetNet.nbProducingReactions(10), std::out_of_range);

  // Check producing reactions iterators
  // of compound 0
  metnetlib::CompoundIdType compoundId = 0;
  metnetlib::SubMetabolicNetwork<>::reaction_iterator it =
      subMetNet.producingReactionsBegin(compoundId);
  int expectedNbProducingReactions = 1;
  int nbProducingReactions = 0;
  auto producingReactionsPtr = subMetNet.producingReactions(compoundId);
  for (const auto& reaction : *producingReactionsPtr) {
    ASSERT_EQ(reaction, *it);
    ++it;
    ++nbProducingReactions;
  }
  ASSERT_EQ(it, subMetNet.producingReactionsEnd(compoundId));
  ASSERT_EQ(nbProducingReactions, expectedNbProducingReactions);
  // of compound 1
  compoundId = 1;
  it = subMetNet.producingReactionsBegin(compoundId);
  expectedNbProducingReactions = 2;
  nbProducingReactions = 0;
  producingReactionsPtr = subMetNet.producingReactions(compoundId);
  for (const auto& reaction : *producingReactionsPtr) {
    ASSERT_EQ(reaction, *it);
    ++it;
    ++nbProducingReactions;
  }
  ASSERT_EQ(it, subMetNet.producingReactionsEnd(compoundId));
  ASSERT_EQ(nbProducingReactions, expectedNbProducingReactions);
  // of compound 2
  compoundId = 2;
  it = subMetNet.producingReactionsBegin(compoundId);
  expectedNbProducingReactions = 0;
  nbProducingReactions = 0;
  producingReactionsPtr = subMetNet.producingReactions(compoundId);
  for (const auto& reaction : *producingReactionsPtr) {
    ASSERT_EQ(reaction, *it);
    ++it;
    ++nbProducingReactions;
  }
  ASSERT_EQ(it, subMetNet.producingReactionsEnd(compoundId));
  ASSERT_EQ(nbProducingReactions, expectedNbProducingReactions);
  // of compound 3
  compoundId = 3;
  it = subMetNet.producingReactionsBegin(compoundId);
  expectedNbProducingReactions = 1;
  nbProducingReactions = 0;
  producingReactionsPtr = subMetNet.producingReactions(compoundId);
  for (const auto& reaction : *producingReactionsPtr) {
    ASSERT_EQ(reaction, *it);
    ++it;
    ++nbProducingReactions;
  }
  ASSERT_EQ(it, subMetNet.producingReactionsEnd(compoundId));
  ASSERT_EQ(nbProducingReactions, expectedNbProducingReactions);

  // Check exceptions of iterators
  ASSERT_THROW(subMetNet.producingReactionsBegin(4), std::invalid_argument);
  ASSERT_THROW(subMetNet.producingReactionsBegin(10), std::out_of_range);
  ASSERT_THROW(subMetNet.producingReactionsEnd(4), std::invalid_argument);
  ASSERT_THROW(subMetNet.producingReactionsEnd(10), std::out_of_range);
  ASSERT_THROW(subMetNet.producingReactions(4), std::invalid_argument);
  ASSERT_THROW(subMetNet.producingReactions(10), std::out_of_range);
}

TEST_F(TestSubMetabolicNetwork, Test_properties) {
  enum Colour {
      red, blue, green
  };
  struct CompoundProperty {
      std::string name = "Default";
      Colour colour = Colour::green;
  };
  struct ReactionProperty {
      std::string name = "Default";
      double weight = 0.0;
  };
  struct MetabolicNetworkProperty {
      std::string name;
      Colour colour = Colour::red;
  };

  typedef metnetlib::MetabolicNetwork<metnetlib::Compound,
                                      metnetlib::Reaction,
                                      CompoundProperty,
                                      ReactionProperty,
                                      MetabolicNetworkProperty> MetNetwork;
  MetNetwork metNet;

  // add a compartment
  const auto &compartment = metNet.addCompartment("c1", 1.0, false);

  // Add three compounds
  metNet.addCompound("C1", {compartment, true, false, true});
  metNet.addCompound("C2", {compartment, true, false, true});
  metNet.addCompound("C3", {compartment, true, false, true});

  // add some reactions
  metNet.addReaction(hglib::NAME, {{"C1"}, {"C3"}}, {"R1", true, false,
                                                     {{1, true}}, {{1, true}},
                                                     -1000, 1000});
  metNet.addReaction(hglib::NAME, {{"C2"}, {"C1"}}, {"R2", true, false,
                                                     {{1, true}}, {{1, true}},
                                                     -1000, 1000});

  // Change some properties
  metNet.getCompoundProperties_(0)->colour = Colour::blue;
  metNet.getCompoundProperties_(0)->name = "Compound_C1";
  metNet.getReactionProperties_(0)->name = "Reaction_R1";
  metNet.getReactionProperties_(0)->weight = 2.0;
  metNet.getMetabolicNetworkProperties_()->name = "MyNetwork";
  metNet.getMetabolicNetworkProperties_()->colour = Colour::green;

  // build sub-metabolic network
  std::unordered_set<metnetlib::CompoundIdType> whitelistedCompounds = {0, 1};
  std::unordered_set<metnetlib::ReactionIdType> whitelistedReactions = {0, 1};
  typedef metnetlib::SubMetabolicNetwork<metnetlib::Compound,
                              metnetlib::Reaction,
                              CompoundProperty,
                              ReactionProperty,
                              MetabolicNetworkProperty> SubMetNet;
  SubMetNet subMetNet = SubMetNet(&metNet,
                                  whitelistedCompounds,
                                  whitelistedReactions);

  // Check compound properties
  ASSERT_EQ(subMetNet.getCompoundProperties(0)->colour, Colour::blue);
  ASSERT_EQ(subMetNet.getCompoundProperties(0)->name.compare("Compound_C1"), 0);
  ASSERT_EQ(subMetNet.getCompoundProperties_(0)->colour, Colour::blue);
  ASSERT_EQ(subMetNet.getCompoundProperties_(0)->name.compare("Compound_C1"),
            0);
  ASSERT_EQ(subMetNet.getCompoundProperties(1)->colour, Colour::green);
  ASSERT_EQ(subMetNet.getCompoundProperties(1)->name.compare("Default"), 0);
  ASSERT_EQ(subMetNet.getCompoundProperties_(1)->colour, Colour::green);
  ASSERT_EQ(subMetNet.getCompoundProperties_(1)->name.compare("Default"), 0);
  // check exceptions
  ASSERT_THROW(subMetNet.getCompoundProperties(2), std::invalid_argument);
  ASSERT_THROW(subMetNet.getCompoundProperties_(2), std::invalid_argument);
  ASSERT_THROW(subMetNet.getCompoundProperties(10), std::out_of_range);
  ASSERT_THROW(subMetNet.getCompoundProperties_(10), std::out_of_range);

  // Check reaction properties
  ASSERT_EQ(subMetNet.getReactionProperties(0)->weight, 2.0);
  ASSERT_EQ(subMetNet.getReactionProperties(0)->name.compare("Reaction_R1"), 0);
  ASSERT_EQ(subMetNet.getReactionProperties_(0)->weight, 2.0);
  ASSERT_EQ(subMetNet.getReactionProperties_(0)->name.compare("Reaction_R1"),
            0);
  ASSERT_EQ(subMetNet.getReactionProperties(1)->weight, 0.0);
  ASSERT_EQ(subMetNet.getReactionProperties(1)->name.compare("Default"), 0);
  ASSERT_EQ(subMetNet.getReactionProperties_(1)->weight, 0.0);
  ASSERT_EQ(subMetNet.getReactionProperties_(1)->name.compare("Default"), 0);
  // check exceptions
  ASSERT_THROW(subMetNet.getReactionProperties(2), std::invalid_argument);
  ASSERT_THROW(subMetNet.getReactionProperties_(2), std::invalid_argument);
  ASSERT_THROW(subMetNet.getReactionProperties(10), std::out_of_range);
  ASSERT_THROW(subMetNet.getReactionProperties_(10), std::out_of_range);

  // Check metabolic network property
  ASSERT_EQ(subMetNet.getMetabolicNetworkProperties()->colour, Colour::green);
  ASSERT_EQ(
      subMetNet.getMetabolicNetworkProperties()->name.compare("MyNetwork"), 0);
  ASSERT_EQ(subMetNet.getMetabolicNetworkProperties_()->colour, Colour::green);
  ASSERT_EQ(
      subMetNet.getMetabolicNetworkProperties_()->name.compare("MyNetwork"), 0);
}
