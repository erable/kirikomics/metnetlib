/*Copyright (C) <2016-2017>  David Parsons, Martin Wannagat

This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU Affero General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/
//
// Created by Martin Wannagat on 14.12.17.
//

#include <set>

#include "gtest/gtest.h"

#include "metnetlib.h"

class TestMinPrecursorSets2 : public testing::Test {
 protected:
  virtual void SetUp() {
    storedStreambuf_ = std::cerr.rdbuf();
    std::cerr.rdbuf(nullptr);
  }

  virtual void TearDown() {
    std::cerr.rdbuf(storedStreambuf_);
  }

 private:
  std::streambuf* storedStreambuf_;
};

#ifdef USE_CPLEX
TEST_F(TestMinPrecursorSets2, Test_CFT_experiment1) {
  auto input = metnetlib::MinimalPrecursorSetInputReader<>::read(
      "data/metabolicNetworks/experiments/Ecoli_CFT073_ic_1306.xml",
      "data/minimalPrecursorSetsInput/experiments/experiment1/CFT/"
          "input_paper_minimal_media_auxothrophies_o2.xml");

  // Compute minimal precursor sets
  input.epsilon(0.5);
  input.constraintType(metnetlib::ConstraintType::ACCUMULATION);
  // compute minimal precursor sets
  testing::internal::CaptureStdout();
  using MetabolicNetwork_t =
      metnetlib::MetabolicNetwork<metnetlib::Compound,
                                  metnetlib::Reaction>;
  metnetlib::MinimalPrecursorSetEnumerator<MetabolicNetwork_t>
      minPsEnumerator(metnetlib::MIPSolver::CPLEX, input);
  auto solutions = minPsEnumerator.enumerate();
  testing::internal::GetCapturedStdout();
  std::vector<std::vector<std::string>> expectedSolutions = {
      {"M_zn2_e", "M_cobalt2_e", "M_k_e", "M_cu2_e", "M_so4_e", "M_pi_e",
          "M_mobd_e", "M_ca2_e", "M_trp_DASH_L_e", "M_ni2_e", "M_mn2_e",
          "M_mg2_e", "M_fe3_e", "M_cl_e"},
      {"M_zn2_e", "M_cobalt2_e", "M_k_e", "M_cu2_e", "M_so4_e", "M_pi_e",
          "M_o2_e", "M_fe2_e", "M_mobd_e", "M_ca2_e", "M_trp_DASH_L_e",
          "M_ni2_e", "M_mn2_e", "M_mg2_e", "M_cl_e"}};
  ASSERT_EQ(solutions.size(), expectedSolutions.size());
  // Check if we have found all expected precursor sets
  for (const auto& expectedPS : expectedSolutions) {
    bool foundSolution = false;
    for (const auto& ps : solutions) {
      if (ps.nbPrecursors() == expectedPS.size()) {
        // Check if have found all expected precursors in the set
        bool foundAllSources = true;
        auto it = ps.precursorsBegin();
        auto end = ps.precursorsEnd();
        for (; it != end; ++it) {
          if (std::find(expectedPS.begin(), expectedPS.end(),
                        (*it)->sbmlId()) == expectedPS.end()) {
            foundAllSources = false;
            break;
          }
        }
        if (foundAllSources) {
          foundSolution = true;
          break;
        }
      }
    }
    ASSERT_TRUE(foundSolution);
  }

  // Print solutions
  /*int i = 0;
  for (const auto& solution : solutions) {
    std::cout << std::to_string(++i) << ")\n" << solution
              << '\n';
  }*/
}
#endif  // USE_CPLEX

#ifdef USE_CPLEX
TEST_F(TestMinPrecursorSets2, Test_EDL_experiment1) {
  auto input = metnetlib::MinimalPrecursorSetInputReader<>::read(
      "data/metabolicNetworks/experiments/Ecoli_EDL993_iZ_1308.xml",
      "data/minimalPrecursorSetsInput/experiments/experiment1/EDL_MG_Sakai/"
          "input_paper_minimal_media_auxothrophies_o2.xml");

  // Compute minimal precursor sets
  input.epsilon(0.5);
  input.constraintType(metnetlib::ConstraintType::ACCUMULATION);
  // compute minimal precursor sets
  testing::internal::CaptureStdout();
  using MetabolicNetwork_t =
      metnetlib::MetabolicNetwork<metnetlib::Compound, metnetlib::Reaction>;
  metnetlib::MinimalPrecursorSetEnumerator<MetabolicNetwork_t>
      minPsEnumerator(metnetlib::MIPSolver::CPLEX, input);
  auto solutions = minPsEnumerator.enumerate();
  ASSERT_EQ(solutions.size(), (size_t) 2);
  testing::internal::GetCapturedStdout();
}
#endif  // USE_CPLEX
