/*Copyright (C) <2016-2017>  David Parsons, Martin Wannagat

This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU Affero General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/
//
// Created by Martin Wannagat on 29.11.17.
//

#include "gtest/gtest.h"

#include "ortools/linear_solver/linear_solver.h"
#include "ortools/linear_solver/linear_solver.pb.h"

using operations_research::MPSolver;
using operations_research::MPVariable;
using operations_research::MPObjective;
using operations_research::MPConstraint;

class TestOrtools : public testing::Test {
 protected:
  virtual void SetUp() {
    storedStreambuf_ = std::cerr.rdbuf();
    std::cerr.rdbuf(nullptr);
  }

  virtual void TearDown() {
    std::cerr.rdbuf(storedStreambuf_);
  }

 private:
  std::streambuf* storedStreambuf_;
};

TEST_F(TestOrtools, Test_ortools) {
  #ifdef USE_CPLEX  // USE_CPLEX
    MPSolver solver("LinearExample", MPSolver::CPLEX_LINEAR_PROGRAMMING);
  #else  // USE_GLOP by default
    MPSolver solver("LinearExample", MPSolver::GLOP_LINEAR_PROGRAMMING);
  #endif
  const double infinity = solver.infinity();
  // x and y are non-negative variables.
  MPVariable* const x = solver.MakeNumVar(0.0, infinity, "x");
  MPVariable* const y = solver.MakeNumVar(0.0, infinity, "y");
  // Objective function: 3x + 4y.
  MPObjective* const objective = solver.MutableObjective();
  objective->SetCoefficient(x, 3);
  objective->SetCoefficient(y, 4);
  objective->SetMaximization();
  // x + 2y <= 14.
  MPConstraint* const c0 = solver.MakeRowConstraint(-infinity, 14.0);
  c0->SetCoefficient(x, 1);
  c0->SetCoefficient(y, 2);

  // 3x - y >= 0.
  MPConstraint* const c1 = solver.MakeRowConstraint(0.0, infinity);
  c1->SetCoefficient(x, 3);
  c1->SetCoefficient(y, -1);

  // x - y <= 2.
  MPConstraint* const c2 = solver.MakeRowConstraint(-infinity, 2.0);
  c2->SetCoefficient(x, 1);
  c2->SetCoefficient(y, -1);

  // Check number of variables
  ASSERT_EQ(solver.NumVariables(), 2);
  // Check number of constraints
  ASSERT_EQ(solver.NumConstraints(), 3);
  // Solve
  solver.Solve();

  // Check the value of each variable in the solution.
  ASSERT_DOUBLE_EQ(x->solution_value(), 6.0);
  ASSERT_DOUBLE_EQ(y->solution_value(), 4.0);
  // The objective value of the solution.
  ASSERT_DOUBLE_EQ(objective->Value(), 34.0);
}
