/*Copyright (C) <2016-2017>  David Parsons, Martin Wannagat

This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU Affero General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/
//
// Created by Martin Wannagat on 07.11.17.
//

#include "Compound.h"

namespace metnetlib {

/// Constructor
template <typename Reaction_t>
Compound<Reaction_t>::
Compound(const CompoundIdType& id, const std::string& sbmlId,
         const SpecificAttributes& specificAttributes) :
    hglib::DirectedVertex<Reaction_t>(id, sbmlId, {}),
    specificAttributes_(specificAttributes) {
}

/// Constructor
template <typename Reaction_t>
Compound<Reaction_t>::
Compound(const CompoundIdType& id,
         const SpecificAttributes& specificAttributes) :
    Compound(id, "c_" + std::to_string(id), specificAttributes) {
}

/// Copy constructor
template <typename Reaction_t>
Compound<Reaction_t>::
Compound(const Compound& compound) :
    hglib::DirectedVertex<Reaction_t>(compound),
    specificAttributes_(compound.specificAttributes_) {
}

/// Get internal Id of the compartment
template <typename Reaction_t>
CompoundIdType Compound<Reaction_t>::
id() const {
  return this->id_;
}

/// Get the Sbml Id of the compound.
template <typename Reaction_t>
std::string Compound<Reaction_t>::
sbmlId() const {
  return this->name_;
}

/// Get pointer to compartment this compound lies in
template <typename Reaction_t>
const Compartment* Compound<Reaction_t>::
getCompartment() const {
  return specificAttributes_.compartment_;
}

/// Get the value of the hasOnlySubstanceUnits attribute flag
template <typename Reaction_t>
bool Compound<Reaction_t>::
hasOnlySubstanceUnits() const {
  return specificAttributes_.hasOnlySubstanceUnits_;
}

/// Check whether the boundary condition is set
template <typename Reaction_t>
bool Compound<Reaction_t>::
hasBoundaryCondition() const {
  return specificAttributes_.boundaryCondition_;
}

/// Check whether the amount of a compound can vary
template <typename Reaction_t>
bool Compound<Reaction_t>::
isConstant() const {
  return specificAttributes_.constant_;
}

/// Set boundary condition flag
template <typename Reaction_t>
void Compound<Reaction_t>::
setBoundaryCondition(bool boundaryCondition) {
  specificAttributes_.boundaryCondition_ = boundaryCondition;
}

/// Get arguments needed to create this compound
template <typename Reaction_t>
hglib::ArgumentsToCreateVertex<Compound<Reaction_t>> Compound<Reaction_t>::
argumentsToCreateVertex() const {
  return hglib::ArgumentsToCreateVertex<Compound<Reaction_t>>(
      this->name_, specificAttributes_);
}

/// Set the compartment pointer
template <typename Reaction_t>
void Compound<Reaction_t>::
setCompartment(const Compartment* compartment) {
  specificAttributes_.compartment_ = compartment;
}
}  // namespace metnetlib
