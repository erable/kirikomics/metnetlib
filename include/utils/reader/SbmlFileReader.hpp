/*Copyright (C) <2016-2017>  David Parsons, Martin Wannagat

This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU Affero General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/
//
// Created by Martin Wannagat on 27.11.17.
//

#include "SbmlFileReader.h"

#include "utils/types.h"

namespace metnetlib {

template <typename CompoundProperty, typename ReactionProperty,
    typename MetabolicNetworkProperty>
std::shared_ptr<
    typename SbmlFileReader<CompoundProperty,
                            ReactionProperty,
                            MetabolicNetworkProperty>::MetNetwork>
SbmlFileReader<CompoundProperty,
               ReactionProperty,
               MetabolicNetworkProperty>::
readFromFile(const char* filename) {
  LIBSBML_CPP_NAMESPACE::SBMLDocument* document =
      LIBSBML_CPP_NAMESPACE::readSBML(filename);
  if (document->getErrorLog()->getNumErrors() > 0) {
    // build error message for exception
    std::ostream errorMessage(nullptr);
    std::stringbuf str;
    errorMessage.rdbuf(&str);
    document->printErrors(errorMessage);
    // delete pointer to document (libsbml)
    delete document;
    throw std::invalid_argument("Error reading your Sbml file:\n"+ str.str());
  }

  // create metabolic network
  std::shared_ptr<MetNetwork> network = std::make_shared<MetNetwork>();
  // parse file
  addCompartments(network, document);  // add compartments
  addCompounds(network, document);  // add compounds
  addReactions(network, document);  // add reactions
  // delete pointer to document (libsbml)
  delete document;
  // Return the metabolic network
  return network;
}

/// Add compartments
template <typename CompoundProperty, typename ReactionProperty,
    typename MetabolicNetworkProperty>
void SbmlFileReader<CompoundProperty, ReactionProperty,
    MetabolicNetworkProperty>::
addCompartments(const std::shared_ptr<MetNetwork>& network,
                LIBSBML_CPP_NAMESPACE::SBMLDocument* document) {
  try {
    std::string errorMsg("");
    LIBSBML_CPP_NAMESPACE::ListOfCompartments *listCompartments =
        document->getModel()->getListOfCompartments();
    for (unsigned int i = 0; i < listCompartments->size(); ++i) {
      auto compartment = listCompartments->get(i);
      try {
        network->addCompartment(compartment->getId(), compartment->getSize(),
                                compartment->getConstant());
      } catch (std::invalid_argument &e) {
        errorMsg += e.what();
      }
    }
    if (errorMsg.compare("") != 0) {
      throw std::invalid_argument(errorMsg);
    }
  } catch (std::invalid_argument& e) {
    delete document;
    throw e;
  }
}


/// Add compounds
template <typename CompoundProperty, typename ReactionProperty,
    typename MetabolicNetworkProperty>
void SbmlFileReader<CompoundProperty, ReactionProperty,
    MetabolicNetworkProperty>::
addCompounds(const std::shared_ptr<MetNetwork>& network,
             LIBSBML_CPP_NAMESPACE::SBMLDocument* document) {
  try {
    std::string errorMsg("");
    LIBSBML_CPP_NAMESPACE::ListOfSpecies *listOfCompounds =
      document->getModel()->getListOfSpecies();
    for (unsigned int i = 0; i < listOfCompounds->size(); ++i) {
      auto compound = listOfCompounds->get(i);
      try {
        const auto &compartment = network->compartmentBySbmlId(
            compound->getCompartment());
        if (compartment == nullptr) {  // undefined compartment
          std::string errorMessage = "The compartment Sbml Id ";
          errorMessage += compound->getCompartment();
          errorMessage += " is not defined.\n";
          throw std::invalid_argument(errorMessage);
        }
        network->addCompound(compound->getId(),
                             {compartment, compound->getHasOnlySubstanceUnits(),
                              compound->getBoundaryCondition(),
                              compound->getConstant()});
      } catch (std::invalid_argument &e) {
        errorMsg += e.what();
      }
    }
    if (errorMsg.compare("") != 0) {
      throw std::invalid_argument(errorMsg);
    }
  } catch (std::invalid_argument& e) {
    delete document;
    throw e;
  }
}


/// Add reactions
template <typename CompoundProperty, typename ReactionProperty,
    typename MetabolicNetworkProperty>
void SbmlFileReader<CompoundProperty, ReactionProperty,
    MetabolicNetworkProperty>::
addReactions(const std::shared_ptr<MetNetwork>& network,
             LIBSBML_CPP_NAMESPACE::SBMLDocument* document) {
  try {
    std::string errorMsg("");
    LIBSBML_CPP_NAMESPACE::ListOfReactions *listOfReactions =
        document->getModel()->getListOfReactions();
    for (unsigned int i = 0; i < listOfReactions->size(); ++i) {
      const auto &reaction = listOfReactions->get(i);
      std::vector<std::string> substrateNames, productNames;
      std::vector<std::pair<double, bool>> substrates, products;
      LIBSBML_CPP_NAMESPACE::ListOfSpeciesReferences *listOfSubstrates =
          reaction->getListOfReactants();
      for (unsigned int j = 0; j < listOfSubstrates->size(); ++j) {
        substrateNames.push_back(reaction->getReactant(j)->getSpecies());
        substrates.push_back({reaction->getReactant(j)->getStoichiometry(),
                              reaction->getReactant(j)->getConstant()});
      }
      LIBSBML_CPP_NAMESPACE::ListOfSpeciesReferences *listOfProducts =
        reaction->getListOfProducts();
      for (unsigned int j = 0; j < listOfProducts->size(); ++j) {
        productNames.push_back(reaction->getProduct(j)->getSpecies());
        products.push_back({reaction->getProduct(j)->getStoichiometry(),
                            reaction->getProduct(j)->getConstant()});
      }
      double lowerBound = 0.0, upperBound = 0.0;
      if (reaction->getKineticLaw() != nullptr and
          reaction->getKineticLaw()->getListOfParameters() != nullptr and
          reaction->getKineticLaw()->getListOfParameters()->get(
              "LOWER_BOUND") != nullptr and
          reaction->getKineticLaw()->getListOfParameters()->get(
              "UPPER_BOUND") != nullptr) {
        lowerBound = reaction->getKineticLaw()->getListOfParameters()->get(
            "LOWER_BOUND")->getValue();
        upperBound = reaction->getKineticLaw()->getListOfParameters()->get(
            "UPPER_BOUND")->getValue();
      } else {  // default values
        lowerBound = reaction->getReversible() ?
                     defaultLowerBound : 0.0;
        upperBound = defaultUpperBound;
      }
      try {
        network->addReaction(hglib::NAME, {substrateNames, productNames},
                             {reaction->getId(), reaction->getReversible(),
                              reaction->getFast(),
                              substrates, products, lowerBound, upperBound});
      } catch (std::invalid_argument& e) {
        errorMsg += e.what();
      }
    }
    if (errorMsg.compare("") != 0) {
      throw std::invalid_argument(errorMsg);
    }
  } catch (std::invalid_argument& e) {
    delete document;
    throw e;
  }
}
}  // namespace metnetlib
