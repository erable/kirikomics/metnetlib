/*Copyright (C) <2016-2017>  David Parsons, Martin Wannagat

This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU Affero General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/
//
// Created by Martin Wannagat on 27.11.17.
//

#ifndef PROJECT_SBMLFILEREADER_H
#define PROJECT_SBMLFILEREADER_H

#include <memory>

#include <sbml/SBMLTypes.h>
#include <sbml/SBMLDocument.h>

#include "metabolicNetwork/MetabolicNetwork.h"

namespace metnetlib {

template <typename CompoundProperty = hglib::emptyProperty,
    typename ReactionProperty = hglib::emptyProperty,
    typename MetabolicNetworkProperty = hglib::emptyProperty>
class SbmlFileReader {
  /* **************************************************************************
   * **************************************************************************
   *                    Alias declarations
   * **************************************************************************
   * *************************************************************************/
 public:
  using MetNetwork = MetabolicNetwork<Compound, Reaction, CompoundProperty,
      ReactionProperty, MetabolicNetworkProperty>;

  /* **************************************************************************
   * **************************************************************************
   *                    Constructor/Destructor
   * **************************************************************************
   * *************************************************************************/
 public:
  /// Default constructor
  SbmlFileReader() = delete;
  /// Destructor
  virtual ~SbmlFileReader() = delete;
  /// Copy constructor
  SbmlFileReader(const SbmlFileReader&) = delete;
  /// Move constructor
  SbmlFileReader(SbmlFileReader&&) = delete;
  /// Copy assignment
  SbmlFileReader& operator=(const SbmlFileReader&) = delete;
  /// Move assignment
  SbmlFileReader& operator=(SbmlFileReader&&) = delete;


  /* **************************************************************************
   * **************************************************************************
   *                    Public member functions
   * **************************************************************************
   * *************************************************************************/
 public:
  /// Parse given metabolic network file
  static std::shared_ptr<MetNetwork> readFromFile(const char*);

  /* **************************************************************************
   * **************************************************************************
   *                    Protected and pure virtual member functions
   * **************************************************************************
   * *************************************************************************/
 protected:
  /// Add compartments
  static void addCompartments(const std::shared_ptr<MetNetwork>&,
                              LIBSBML_CPP_NAMESPACE::SBMLDocument*);
  /// Add compounds
  static void addCompounds(const std::shared_ptr<MetNetwork>&,
                           LIBSBML_CPP_NAMESPACE::SBMLDocument*);
  /// Add reactions
  static void addReactions(const std::shared_ptr<MetNetwork>&,
                           LIBSBML_CPP_NAMESPACE::SBMLDocument*);
};
}  // namespace metnetlib

#include "SbmlFileReader.hpp"
#endif  // PROJECT_SBMLFILEREADER_H
