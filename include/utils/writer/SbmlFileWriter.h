/*Copyright (C) <2016-2017>  David Parsons, Martin Wannagat

This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU Affero General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/
//
// Created by Martin Wannagat on 18.12.17.
//

#ifndef METNETLIB_SBMLFILEWRITER_H
#define METNETLIB_SBMLFILEWRITER_H

namespace metnetlib {

template <typename MetabolicNetwork_t>
class SbmlFileWriter {
  /* **************************************************************************
   * **************************************************************************
   *                    Constructor/Destructor
   * **************************************************************************
   * *************************************************************************/
 public:
  /// Default constructor
  SbmlFileWriter() = delete;
  /// Destructor
  virtual ~SbmlFileWriter() = delete;
  /// Copy constructor
  SbmlFileWriter(const SbmlFileWriter&) = delete;
  /// Move constructor
  SbmlFileWriter(SbmlFileWriter&&) = delete;
  /// Copy assignment
  SbmlFileWriter& operator=(const SbmlFileWriter&) = delete;
  /// Move assignment
  SbmlFileWriter& operator=(SbmlFileWriter&&) = delete;

  /* **************************************************************************
   * **************************************************************************
   *                    Public member functions
   * **************************************************************************
   * *************************************************************************/
 public:
  /// Write given metabolic network to given file
  static void writeMetabolicNetworkToFile(const MetabolicNetwork_t&,
                                          const char*);

 protected:
  /// Write XML header
  static void writeHeader(std::ofstream&);
  /// Write list of compartments
  static void writeListOfCompartments(std::ofstream&,
                                      const MetabolicNetwork_t&);
  /// Write list of compounds
  static void writeListOfCompounds(std::ofstream&, const MetabolicNetwork_t&);
  /// Write list of reactions
  static void writeListOfReactions(std::ofstream&, const MetabolicNetwork_t&);
  /// Write XML tail
  static void writeTail(std::ofstream&);
};
}  // namespace metnetlib

#include "SbmlFileWriter.hpp"
#endif  // METNETLIB_SBMLFILEWRITER_H
