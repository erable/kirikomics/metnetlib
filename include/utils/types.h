/*Copyright (C) <2016-2017>  David Parsons, Martin Wannagat

This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU Affero General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/
//
// Created by Martin Wannagat on 07.11.17.
//

#ifndef METNET_TYPES_H
#define METNET_TYPES_H

#include <cstring>

#include <string>
#include <sys/types.h>

#include "hglib/utils/types.h"

namespace metnetlib {
/// Alias for the compartment Id type
using CompartmentIdType = u_int8_t;

/// Alias for compound Id type
using CompoundIdType = hglib::VertexIdType;

/// Alias for reaction Id type
using ReactionIdType = hglib::HyperedgeIdType;

/// Alias for container wherein compounds, reactions etc. are stored
template<typename T>
using MetNetElementContainer = hglib::GraphElementContainer<T>;


/// \brief Suffix of the name of the reverse sense of a reaction. Will be added
/// to the name (Sbml Id) of the forward reaction.
constexpr char suffixReverseReaction[] = "_REV";
constexpr auto suffixReverseReactionLength = sizeof(suffixReverseReaction) - 1;

/// Default value for upper bound
constexpr double defaultUpperBound(1000.0);

/// Default value for lower bound if the reaction is reversible
constexpr double defaultLowerBound(-defaultUpperBound);
}  // namespace metnetlib
#endif  // METNET_TYPES_H
