/*Copyright (C) <2016-2017>  David Parsons, Martin Wannagat

This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU Affero General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/
//
// Created by Martin Wannagat on 07.11.17.
//

#ifndef METNET_COMPARTMENT_H
#define METNET_COMPARTMENT_H

#include <string>

#include "utils/types.h"

namespace metnetlib {

/**
 * Class to model a compartment.
 */
class Compartment {
  // MetabolicNetwork is a friend class
  template <template <typename> class Compound_t, typename Reac_t,
      typename CompoundProperty, typename ReactionProperty,
      typename MetNetProperty>
  friend class MetabolicNetwork;

  /* **************************************************************************
   * **************************************************************************
   *                        Constructor/Destructor (Rule of five)
   * **************************************************************************
   ***************************************************************************/
 protected:
  /// Constructor
  Compartment(const CompartmentIdType&, const std::string&, const double&,
              bool);
  /// Default constructor
  Compartment() = delete;
  /// Destructor
  ~Compartment() = default;
  /// Copy constructor
  Compartment(const Compartment&) = default;
  /// Move constructor
  Compartment(Compartment&&) = default;
  /// Copy assignment
  Compartment& operator=(const Compartment&) = default;
  /// Move assignment
  Compartment& operator=(Compartment&&) = default;

  /* **************************************************************************
   * **************************************************************************
   *                        Member functions
   * **************************************************************************
   ***************************************************************************/
 public:
  /// Get internal Id of the compartment
  inline const auto& id() const;
  /// Get Sbml Id of the compartment
  inline const auto& sbmlId() const;
  /// Get size of the compartment
  inline const auto& size() const;
  /// Is the size of the compartment constant?
  inline bool isConstant() const;

  /// Set size if the compartment is not constant
  void setSize(const double&);

 protected:
  /// Set Id of the compartment
  void setId(const CompartmentIdType&);

  /* **************************************************************************
   * **************************************************************************
   *                        Member variables
   * **************************************************************************
   ***************************************************************************/
 protected:
  /// Internal numerical Id
  CompartmentIdType id_;
  /// Sbml Id
  std::string sbmlId_;
  /// Size of the compartment
  double size_;
  /// Flag to show whether the compartment is constant
  bool constant_;
};

/// Get internal Id of the compartment
const auto& Compartment::id() const {
  return id_;
}

/// Get Sbml Id of the compartment
const auto& Compartment::sbmlId() const {
  return sbmlId_;
}

/// Get size of the compartment
const auto& Compartment::size() const {
  return size_;
}

/// Is the size of the compartment constant?
bool Compartment::isConstant() const {
  return constant_;
}

}  // namespace metnetlib
#endif  // METNET_COMPARTMENT_H
