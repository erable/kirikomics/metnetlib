/*Copyright (C) <2016-2017>  David Parsons, Martin Wannagat

This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU Affero General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/
//
// Created by Martin Wannagat on 30.11.17.
//

#ifndef METNETLIB_ALGORITHMUTILS_H
#define METNETLIB_ALGORITHMUTILS_H

#include "are_identical_reactions.hpp"
#include "ConstraintType.h"
#include "MIPSolver.h"
#include "remove_duplicate_reactions.hpp"
#include "remove_isolated_compounds.hpp"
#include "remove_reactions_without_substrate.hpp"
#include "remove_reactions_without_substrate_or_product.hpp"
#include "topological_sources.hpp"

#endif  // METNETLIB_ALGORITHMUTILS_H
