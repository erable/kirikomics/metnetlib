/*Copyright (C) <2016-2017>  David Parsons, Martin Wannagat

This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU Affero General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/
//
// Created by Martin Wannagat on 30.11.17.
//


#ifndef METNETLIB_TOPOLOGICAL_SOURCES_H
#define METNETLIB_TOPOLOGICAL_SOURCES_H

#include <list>

#include "metabolicNetwork/metabolicNetworks.h"

namespace metnetlib {

/**
 * Get all topological sources of a metabolic network
 *
 * A compound is considered to be a topological source if it is either not
 * produced by a reaction, or if produced then it is consumed and produced by
 * exactly one reversible reaction.
 *
 * @tparam MetabolicNetwork
 * @param network
 * @return
 */
template<typename MetabolicNetwork>
std::list<typename MetabolicNetwork::Compound_t*>
topological_sources(const MetabolicNetwork& network) {
  std::list<typename MetabolicNetwork::Compound_t*> topologicalSources;
  for (const auto& compound : network.compounds()) {
    const auto& id(compound->id());
    // compound is not produced
    if (not network.isProduced(id)) {
      topologicalSources.push_back(compound);
    } else if (network.nbConsumingReactions(id) == 1 and
        network.nbProducingReactions(id) == 1) {
      // The compound is consumed and produced by exactly one reversible
      // reaction.
      // Get the consuming reaction.
      const auto& reaction = *network.consumingReactionsBegin(id);
      // Is this reaction reversible?
      if (network.isReversible(reaction->id())) {
        topologicalSources.push_back(compound);
      }
    }
  }
  return topologicalSources;
}
}  // namespace metnetlib
#endif  // METNETLIB_TOPOLOGICAL_SOURCES_H
