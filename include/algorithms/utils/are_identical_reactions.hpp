/*Copyright (C) <2016-2017>  David Parsons, Martin Wannagat

This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU Affero General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/
//
// Created by Martin Wannagat on 30.11.17.
//

#ifndef METNETLIB_ARE_IDENTICAL_REACTIONS_H
#define METNETLIB_ARE_IDENTICAL_REACTIONS_H

#include <unordered_map>

namespace metnetlib {

/**
 * Check if the given reactions are identical
 *
 * Identity is checked on the substrates, products and the stoichiometry. The
 * reactions are splitted in our library, thus the reaction
 * R1: 1 A + 2 B -> 1 C and the forward reaction of R2: 1 A + 2 B <-> 1 C are
 * identical.
 *
 * @tparam MetabolicNetwork
 * @param network
 * @param r1
 * @param r2
 * @return
 */
template<typename MetabolicNetwork>
bool are_identical_reactions(const MetabolicNetwork& network,
                             const typename MetabolicNetwork::Reaction_t& r1,
                             const typename MetabolicNetwork::Reaction_t& r2) {
  // Check first if both reactions are from the given network
  try {
    if (network.reactionById(r1.id()) != &r1 or
        network.reactionById(r2.id()) != &r2) {
      throw std::invalid_argument("The provided reactions are not part of the"
                                      " network");
    }
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in are_duplicate_reactions: "
        "index1 = " << r1.id() << ", index2 = " << r2.id() << '\n';
    throw;
  }

  // compare substrates
  // add substrates and stoichiometries of r1 to a map
  std::unordered_map<CompoundIdType, double> compoundIds;
  auto it = network.substratesBegin(r1.id());
  auto end = network.substratesEnd(r1.id());
  for (; it != end; ++it) {
    compoundIds.insert({(*it)->id(),
                        network.substrateStoichiometry((*it)->id(), r1.id())});
  }

  // remove substrates of r2 from the list
  it = network.substratesBegin(r2.id());
  end = network.substratesEnd(r2.id());
  for (; it != end; ++it) {
    // check if substrate is also in r1 and if yes compare the stoichiometry
    auto entry = compoundIds.find((*it)->id());
    if (entry != compoundIds.end() and
        (*entry).second == network.substrateStoichiometry((*it)->id(),
                                                           r2.id())) {
      compoundIds.erase((*it)->id());
    } else {  // substrate not in r1 or different stoichiometry
      return false;
    }
  }
  // In case r1 has more substrates than r2
  if (not compoundIds.empty()) {
    return false;
  }

  // compare products
  // add products and stoichiometries of r1 to a map
  it = network.productsBegin(r1.id());
  end = network.productsEnd(r1.id());
  for (; it != end; ++it) {
    compoundIds.insert({(*it)->id(),
                        network.productStoichiometry((*it)->id(), r1.id())});
  }

  // remove products of r2 from the list
  it = network.productsBegin(r2.id());
  end = network.productsEnd(r2.id());
  for (; it != end; ++it) {
    // check if product is also in r1 and if yes compare the stoichiometry
    auto entry = compoundIds.find((*it)->id());
    if (entry != compoundIds.end() and
        (*entry).second == network.productStoichiometry((*it)->id(),
                                                         r2.id())) {
      compoundIds.erase((*it)->id());
    } else {  // product not in r1 or different stoichiometry
      return false;
    }
  }
  // False, if r1 has more products than r2
  return compoundIds.empty();
}

}  // namespace metnetlib
#endif  // METNETLIB_ARE_IDENTICAL_REACTIONS_H
