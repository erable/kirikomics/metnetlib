/*Copyright (C) <2016-2017>  David Parsons, Martin Wannagat

This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU Affero General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/
//
// Created by Martin Wannagat on 11.12.17.
//

#ifndef METNETLIB_CONSTRAINTTYPE_H
#define METNETLIB_CONSTRAINTTYPE_H

namespace metnetlib {

/**
 * Type of constraints put on compounds that are neither sources nor bootstrap
 * compounds.
 * ACCUMULATION: (Sv)_c >= 0
 * STEADYSTATE: (Sv)_c = 0
 * DUPMACH: (Sv)_c > 0 OR all reactions consuming the compound c have a zero
 *          flux
 *
 * S ... Stoichiometric matrix
 * v ... flux vector
 * c ... a compound
 * (Sv)_c ... vector of net production of compound 'c' in the network for a
 *            flux v
 */
enum ConstraintType {
    STEADYSTATE, ACCUMULATION, DUPMACH
};
}  // namespace metnetlib
#endif  // METNETLIB_CONSTRAINTTYPE_H
