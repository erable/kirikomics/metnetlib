/*Copyright (C) <2016-2017>  David Parsons, Martin Wannagat

This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU Affero General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/
//
// Created by Martin Wannagat on 11.12.17.
//

#ifndef METNETLIB_MINPRECURSORSETINPUTREADER_H
#define METNETLIB_MINPRECURSORSETINPUTREADER_H

#include "metabolicNetwork/MetabolicNetwork.h"
#include "MinimalPrecursorSetsInput.h"

namespace metnetlib {

/**
 * Read input parameters from files.
 *
 * Read the metabolic network from a SBML file. The MetabolicNetwork object has
 * as compound (reaction) type the class Compound<Reaction> (Reaction).
 *
 * The other input parameters are read from a second file in XML format:
 * \code
 <?xml version="1.0"?>
 <inputs date="2017-12-11" comment="Test input"
   considerTopologicalSources="false" considerBoundaryCompoundsAsSources="false"
   constraintType="STEADYSTATE" epsilon="0.1" addFactoryReactions="false"/>
    <bootstrap-compounds>
      <species id="A" comment= />
      <species id="B" comment= />
    </bootstrap-compounds>
    <precursor-compounds>
      <species id="S1" comment= />
      <species id="S2" comment= />
      <species id="S3" comment= />
    </precursor-compounds>
    <target-compounds>
      <species id="T" comment=""/>
    </target-compounds>
 </inputs>
 * \endcode
 *
 *
 * @tparam CompoundProperty
 * @tparam ReactionProperty
 * @tparam MetabolicNetworkProperty
 */
template <typename CompoundProperty = hglib::emptyProperty,
    typename ReactionProperty = hglib::emptyProperty,
    typename MetabolicNetworkProperty = hglib::emptyProperty>
class MinimalPrecursorSetInputReader {
  /* **************************************************************************
   * **************************************************************************
   *                    Alias declarations
   * **************************************************************************
   * *************************************************************************/
 public:
  using MetNetwork = MetabolicNetwork<Compound, Reaction, CompoundProperty,
      ReactionProperty, MetabolicNetworkProperty>;

  /* **************************************************************************
   * **************************************************************************
   *                    Constructor/Destructor
   * **************************************************************************
   * *************************************************************************/
 public:
  /// Default constructor
  MinimalPrecursorSetInputReader() = delete;
  /// Destructor
  virtual ~MinimalPrecursorSetInputReader() = delete;
  /// Copy constructor
  MinimalPrecursorSetInputReader(
      const MinimalPrecursorSetInputReader&) = delete;
  /// Move constructor
  MinimalPrecursorSetInputReader(MinimalPrecursorSetInputReader&&) = delete;
  /// Copy assignment
  MinimalPrecursorSetInputReader& operator=(
      const MinimalPrecursorSetInputReader&) = delete;
  /// Move assignment
  MinimalPrecursorSetInputReader& operator=(
      MinimalPrecursorSetInputReader&&) = delete;


  /* **************************************************************************
   * **************************************************************************
   *                    Public member functions
   * **************************************************************************
   * *************************************************************************/
 public:
  /// Parse given metabolic network file
  static MinimalPrecursorSetsInput<MetNetwork> read(const char*, const char*);
};
}  // namespace metnetlib

#include "MinimalPrecursorSetInputReader.hpp"
#endif  // METNETLIB_MINPRECURSORSETINPUTREADER_H
