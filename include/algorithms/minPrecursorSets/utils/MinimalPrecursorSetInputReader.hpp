/*Copyright (C) <2016-2017>  David Parsons, Martin Wannagat

This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU Affero General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/
//
// Created by Martin Wannagat on 11.12.17.
//

#include "MinimalPrecursorSetInputReader.h"

#include <algorithm>
#include <cctype>
#include <fstream>
#include <iostream>
#include <string>

#include "MinimalPrecursorSetsInput.h"
#include "utils/reader/SbmlFileReader.h"

namespace metnetlib {

template <typename CompoundProperty,
          typename ReactionProperty,
          typename MetabolicNetworkProperty>
MinimalPrecursorSetsInput<
    typename MinimalPrecursorSetInputReader<
        CompoundProperty,
        ReactionProperty,
        MetabolicNetworkProperty>::MetNetwork>
MinimalPrecursorSetInputReader<CompoundProperty,
                               ReactionProperty,
                               MetabolicNetworkProperty>::
read(const char* metabolicNetworkFile,
     const char* minimalPrecursorSetsParamFile) {
  // Get metabolic network from file
  auto network = SbmlFileReader<CompoundProperty,
                                ReactionProperty,
                                MetabolicNetworkProperty>::
      readFromFile(metabolicNetworkFile);

  // Build return object with empty targets, sources, bootstrap and default
  // values
  auto minPrecursorSetsParams = MinimalPrecursorSetsInput<MetNetwork>(
      network, {}, {}, {});
  // Get input parameter from xml file
  std::string line;
  std::ifstream file(minimalPrecursorSetsParamFile);
  if (file.is_open()) {
    // Error message to keep track of input errors
    std::string errorMsg("");
    // Type of current compound
    enum InputCompoundType {TARGET, SOURCE, BOOTSTRAP};
    InputCompoundType compoundType;
    // Read the file
    while (std::getline(file, line)) {
      // remove \s from line
      line.erase(std::remove_if(line.begin(), line.end(), isspace), line.end());
      // target compounds will follow
      if (line.find("<target-compounds>") != std::string::npos) {
        compoundType = InputCompoundType::TARGET;
      } else if (line.find("<precursor-compounds>") != std::string::npos) {
        // user defined sources will follow
        compoundType = InputCompoundType::SOURCE;
      } else if (line.find("<bootstrap-compounds>") != std::string::npos) {
        // bootstrap compounds will follow
        compoundType = InputCompoundType::BOOTSTRAP;
      } else if (line.rfind("<species", 0) == 0) {
        // Get the Sbml Id of the actual compound
        std::string idPrefix = "id=\"";
        auto startPos = line.find(idPrefix);
        auto endPos = line.find("\"", startPos + idPrefix.size());
        auto sbmlId = line.substr(startPos + idPrefix.size(),
                                  endPos - startPos - idPrefix.size());
        // Get the Id of the compound in the network
        const auto& compound = network->compoundBySbmlId(sbmlId);
        if (compound != nullptr) {  // Add Id to targets, source or bootstrap
          switch (compoundType) {
            case InputCompoundType::TARGET: {
              minPrecursorSetsParams.addTarget(compound->id());
              break;
            }
            case InputCompoundType::SOURCE: {
              minPrecursorSetsParams.addUserDefinedSource(compound->id());
              break;
            }
            case InputCompoundType::BOOTSTRAP: {
              minPrecursorSetsParams.addBootstrapCompound(compound->id());
              break;
            }
          }
        } else {
          std::cerr << "Ignore Sbml Id: '" << sbmlId << "' because it is not"
              " found in the metabolic network.\n";
        }
      } else if (line.find("<inputs") != std::string::npos) {
        // Get other parameter
        // Get flag considerTopologicalSources
        std::string attributeName = "considerTopologicalSources=\"";
        auto startPos = line.find(attributeName);
        if (startPos != std::string::npos) {
          auto endPos = line.find("\"", startPos + attributeName.size());
          auto flag = line.substr(startPos + attributeName.size(),
                                  endPos - startPos - attributeName.size());

          if (flag.compare("false") == 0) {
            minPrecursorSetsParams.considerTopologicalSources(false);
          } else if (flag.compare("true") == 0) {
            minPrecursorSetsParams.considerTopologicalSources(true);
          } else {
            errorMsg += "\nProvide true or false for the attribute"
                " considerTopologicalSources.";
          }
        }

        // Get flag considerBoundaryCompoundsAsSources
        attributeName = "considerBoundaryCompoundsAsSources=\"";
        startPos = line.find(attributeName);
        if (startPos != std::string::npos) {
          auto endPos = line.find("\"", startPos + attributeName.size());
          auto flag = line.substr(startPos + attributeName.size(),
                                  endPos - startPos - attributeName.size());

          if (flag.compare("false") == 0) {
            minPrecursorSetsParams.considerBoundaryCompoundsAsSources(false);
          } else if (flag.compare("true") == 0) {
            minPrecursorSetsParams.considerBoundaryCompoundsAsSources(true);
          } else {
            errorMsg += "\nProvide true or false for the attribute"
                " considerBoundaryCompoundsAsSources.";
          }
        }

        // Get constraintType
        attributeName = "constraintType=\"";
        startPos = line.find(attributeName);
        if (startPos != std::string::npos) {
          auto endPos = line.find("\"", startPos + attributeName.size());
          auto attributeValue = line.substr(startPos + attributeName.size(),
                                            endPos - startPos -
                                            attributeName.size());

          if (attributeValue.compare("STEADYSTATE") == 0) {
            minPrecursorSetsParams.constraintType(ConstraintType::STEADYSTATE);
          } else if (attributeValue.compare("ACCUMULATION") == 0) {
            minPrecursorSetsParams.constraintType(ConstraintType::ACCUMULATION);
          } else if (attributeValue.compare("DUPMACH") == 0) {
            minPrecursorSetsParams.constraintType(ConstraintType::DUPMACH);
          } else {
            errorMsg += "\nProvide [STEADYSTATE|ACCUMULATION|DUPMACH] for"
                " the attribute constraintType.";
          }
        }

        // Get epsilon
        attributeName = "epsilon=\"";
        startPos = line.find(attributeName);
        if (startPos != std::string::npos) {
          auto endPos = line.find("\"", startPos + attributeName.size());
          auto attributeValue = line.substr(startPos + attributeName.size(),
                                            endPos - startPos -
                                            attributeName.size());
          try {
            double value = std::stod(attributeValue);
            minPrecursorSetsParams.epsilon(value);
          } catch (const std::exception& e) {
            errorMsg += '\n';
            errorMsg += e.what();
          }
        }

        // Get flag addFactoryReactions
        attributeName = "addFactoryReactions=\"";
        startPos = line.find(attributeName);
        if (startPos != std::string::npos) {
          auto endPos = line.find("\"", startPos + attributeName.size());
          auto flag = line.substr(startPos + attributeName.size(),
                                  endPos - startPos - attributeName.size());

          if (flag.compare("false") == 0) {
            minPrecursorSetsParams.addFactoryReactions(false);
          } else if (flag.compare("true") == 0) {
            minPrecursorSetsParams.addFactoryReactions(true);
          } else {
            errorMsg += "\nProvide true or false for the attribute"
                " addFactoryReactions.";
          }
        }
      }
    }
    file.close();
    // throw an exception if the input format was not respected
    if (errorMsg.compare("") != 0) {
      throw std::invalid_argument(errorMsg);
    }
  } else {
    std::string errorMsg("Can't open input file: ");
    errorMsg += minimalPrecursorSetsParamFile;
    throw std::invalid_argument(errorMsg);
  }
  // return parameters
  return minPrecursorSetsParams;
}
}  // namespace metnetlib
