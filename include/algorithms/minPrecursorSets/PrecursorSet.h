/*Copyright (C) <2016-2017>  David Parsons, Martin Wannagat

This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU Affero General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/
//
// Created by Martin Wannagat on 30.11.17.
//

#ifndef METNETLIB_PRECURSORSET_H
#define METNETLIB_PRECURSORSET_H

#include <set>

#include "compound/Compound.h"
#include "reaction/Reaction.h"

namespace metnetlib {

template <typename Compound_t = Compound<Reaction>,
    typename Reaction_t = Reaction>
class PrecursorSet {
  /* **************************************************************************
   * **************************************************************************
   *                        Constructor/Destructor (Rule of five)
   * **************************************************************************
   ***************************************************************************/
 public:
  /// Default constructor
  PrecursorSet() = default;
  /// Destructor
  virtual ~PrecursorSet() = default;
  /// Copy constructor
  PrecursorSet(const PrecursorSet&);
  /// Move constructor
  PrecursorSet(PrecursorSet&&) = delete;
  /// Copy assignment
  PrecursorSet& operator=(const PrecursorSet&);
  /// Move assignment
  PrecursorSet& operator=(PrecursorSet&&) = delete;

  /* **************************************************************************
   * **************************************************************************
   *                        Member functions
   * **************************************************************************
   ***************************************************************************/
 public:
  /// Add a precursor to the precursor set
  void addPrecursor(const Compound_t&);
  /// Add a compound as a bootstrap compound to the precursor set
  void addBootstrapCompound(const Compound_t&);
  /// Add a reaction as part of the factory from the precursor set to the target
  void addFactoryReaction(const Reaction_t&);
  /// Get begin of precursors
  typename std::set<const Compound_t*>::const_iterator precursorsBegin() const;
  /// Get end of precursors
  typename std::set<const Compound_t*>::const_iterator precursorsEnd() const;
  /// Get number of precursors
  size_t nbPrecursors() const;
  /// Get begin of bootstrap compounds
  typename std::set<
      const Compound_t*>::const_iterator bootstrapCompoundsBegin() const;
  /// Get end of bootstrap compounds
  typename std::set<
      const Compound_t*>::const_iterator bootstrapCompoundsEnd() const;
  /// Get begin of factory reactions
  typename std::set<
      const Reaction_t*>::const_iterator factoryReactionsBegin() const;
  /// Get end of factory reactions
  typename std::set<
      const Reaction_t*>::const_iterator factoryReactionsEnd() const;

  /* **************************************************************************
   * **************************************************************************
   *                        Data variables
   * **************************************************************************
   ***************************************************************************/
 protected:
  /// Precursors
  std::set<const Compound_t*> precursors_;
  /// Bootstrap compounds
  std::set<const Compound_t*> bootstrapCompounds_;
  /// Reactions that are part of one factory from the precursors to the target
  std::set<const Reaction_t*> factoryReactions_;
};

/// Operator<<
template <typename Compound_t, typename Reaction_t>
inline std::ostream& operator<< (
    std::ostream& out, const PrecursorSet<Compound_t, Reaction_t>& ps) {
  // Print precursors
  out << "Precursors = {";
  const char* padding = "";
  auto it = ps.precursorsBegin();
  auto end = ps.precursorsEnd();
  for (; it != end; ++it) {
    out << padding << *(*it);
    padding = ", ";
  }
  out << "}\n";

  // Print bootstrap compounds
  out << "Supplementary bootstrap compounds = {";
  padding = "";
  it = ps.bootstrapCompoundsBegin();
  end = ps.bootstrapCompoundsEnd();
  for (; it != end; ++it) {
    out << padding << *(*it);
    padding = ", ";
  }
  out << "}\n";

  // Print factory reactions
  out << "Factory reactions = {";
  padding = "";
  auto itFactoryReac = ps.factoryReactionsBegin();
  auto factoryReactionEnd = ps.factoryReactionsEnd();
  for (; itFactoryReac != factoryReactionEnd; ++itFactoryReac) {
    out << padding << (*itFactoryReac)->sbmlId();
    padding = ", ";
  }
  out << "}";
  return out;
}

}  // namespace metnetlib

#include "PrecursorSet.hpp"
#endif  // METNETLIB_PRECURSORSET_H
