/*Copyright (C) <2016-2017>  David Parsons, Martin Wannagat

This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU Affero General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/
//
// Created by Martin Wannagat on 13.12.17.
//

#include "MinimalPrecursorSetEnumerator.h"

#ifdef USE_CPLEX
#include <ilcplex/ilocplex.h>
#endif  // USE_CPLEX

#include "metabolicNetwork/metabolicNetworks.h"
#include "algorithms/utils/ConstraintType.h"
#include "algorithms/utils/remove_duplicate_reactions.hpp"
#include "algorithms/utils/remove_isolated_compounds.hpp"
#include "algorithms/utils/remove_reactions_without_substrate.hpp"
#include "algorithms/utils/remove_reactions_without_substrate_or_product.hpp"
#include "algorithms/utils/topological_sources.hpp"
#include "utils/types.h"

namespace metnetlib {

/// Constructor
template<typename MetabolicNetwork>
MinimalPrecursorSetEnumerator<MetabolicNetwork>::
MinimalPrecursorSetEnumerator(
    const MIPSolver& mipSolver,
    const MinimalPrecursorSetsInput<MetabolicNetwork>& input) :
    mipSolver_(mipSolver),
    input_(input),
    prepocessedInput_(input),
    preprocessedNetwork_(*(input.metabolicNetwork())) {
}

/// Copy constructor
template<typename MetabolicNetwork>
MinimalPrecursorSetEnumerator<MetabolicNetwork>::
MinimalPrecursorSetEnumerator(const MinimalPrecursorSetEnumerator& other) :
    mipSolver_(other.mipSolver_),
    input_(other.input_),
    prepocessedInput_(other.prepocessedInput_),
    preprocessedNetwork_(other.preprocessedNetwork_) {
}

/// Get optimization problem type
template<typename MetabolicNetwork>
const MIPSolver& MinimalPrecursorSetEnumerator<
    MetabolicNetwork>::
mipSolver() const {
  return mipSolver_;
}


/// Set optimization problem type
template<typename MetabolicNetwork>
void MinimalPrecursorSetEnumerator<MetabolicNetwork>::
setMipSolver(const MIPSolver & mipSolver) {
  mipSolver_ = mipSolver;
}

/// Enumerate all minimal precursor sets for the given input
template<typename MetabolicNetwork>
std::vector<
    typename MinimalPrecursorSetEnumerator<MetabolicNetwork>::PrecursorSet_t>
MinimalPrecursorSetEnumerator<MetabolicNetwork>::
enumerate() {
  // * Pre-process the network
  // ** Remove duplicate reactions
  // ** Remove reactions without substrate or product if constraint type is
  //    not STEADYSTATE
  // ** Remove reactions without substrate if constraint type is
  //    STEADYSTATE (reactions without a product are kept as they may be used
  //    to export a compound to maintain steady-state)
  // ** Remove isolated compounds
  // * If considerTopologicalSources == true, add topological sources to the
  //   list of sources
  // * Build initial model
  // * Solve and extend model if there are further solutions

  /* ***************************************************************************
   * ***************************************************************************
   *                            Preprocessing
   * ***************************************************************************
   ****************************************************************************/
  // pre-process the network
  preprocessingMinimalPrecursorSets();

  /* ***************************************************************************
   * ***************************************************************************
   *                          Build and solve the model
   * ***************************************************************************
   ****************************************************************************/
  switch (mipSolver_) {
    // Use cplex version
    #ifdef USE_CPLEX
    case MIPSolver::CPLEX: {
      return enumerateWithCplex();
    }
    #endif  // USE_CPLEX
    default: throw std::invalid_argument(
          "PLease use a supported mixed integer programming solver from the"
              " enum MIPSolver, e.g. MIPSolver::CPLEX");
  }
}

/// Preprocessing
template<typename MetabolicNetwork>
void MinimalPrecursorSetEnumerator<MetabolicNetwork>::
preprocessingMinimalPrecursorSets() {
  // In constraint models there is no constraint put on boundary compounds,
  // that is they can be imported and exported. In our model we ignore the
  // boundary flag. Instead, we add en export reaction for each boundary
  // compound. If the boundary compound 'A' is neither source nor target, the
  // compound 'A' is added to the set of sources or bootstrap compounds
  // depending on the value of the flag considerBoundaryCompoundsAsSources. (An
  // import reaction will be added to the model for each source and bootstrap
  // compound)
  for (const auto& compound : preprocessedNetwork_.compounds()) {
    if (compound->hasBoundaryCondition()) {
      compound->setBoundaryCondition(false);
      // Is it a target compound?
      bool isTarget = std::find(prepocessedInput_.targetsBegin(),
                                prepocessedInput_.targetsEnd(),
                                compound->id()) !=
                      prepocessedInput_.targetsEnd();
      // Add an export reaction if it is not a target
      if (not isTarget) {
        std::string reactionName = "ExportReaction_" +
                                   std::to_string(compound->id());
        preprocessedNetwork_.addDefaultIrreversibleReaction(
            reactionName, {compound->sbmlId()}, {});
      }
      // It is neither a source nor a target
      if (not isTarget and
          std::find(prepocessedInput_.userDefinedSourcesBegin(),
                    prepocessedInput_.userDefinedSourcesEnd(),
                    compound->id()) ==
          prepocessedInput_.userDefinedSourcesEnd()) {
        // It is a ordinary boundary compound
        // Add it to the set of sources or to the bootstrap compounds dependent
        // of the flag considerBoundaryCompoundsAsSources
        if (prepocessedInput_.considerBoundaryCompoundsAsSources()) {
          prepocessedInput_.addUserDefinedSource(compound->id());
        } else {
          prepocessedInput_.addBootstrapCompound(compound->id());
        }
      }
    }
  }

  /* ***************************************************************************
   * ***************************************************************************
   *                   Remove reactions without substrate/product
   * ***************************************************************************
   ****************************************************************************/
  // Remove reactions without substrate or product if constraint type is NOT
  // STEADYSTATE
  if (prepocessedInput_.constraintType() != ConstraintType::STEADYSTATE) {
    remove_reactions_without_substrate_or_product(&preprocessedNetwork_);
  } else {
    // The substrates of a reversible reaction without products
    // (e.g. R1: A <-> ) are considered as sources.
    // * Declare these compounds as sources
    // * Remove reactions without substrate (e.g. backward reaction of R1).
    //   This avoids the import of a compound for 'free' because if the
    //   compound 'A' is not declared as source we do not put a binary variable
    //   constraint on 'A'

    // * Declare substrates of reversible reactions without product as sources
    if (prepocessedInput_.considerTopologicalSources()) {
      for (const auto &reaction : preprocessedNetwork_.reactions()) {
        if (preprocessedNetwork_.isReversible(reaction->id()) and
            not preprocessedNetwork_.hasProducts(reaction->id())) {
          // insert all substrate ids
          auto substratesPtr = preprocessedNetwork_.substrates(
              reaction->id());
          for (const auto &substrate : *substratesPtr) {
            prepocessedInput_.addUserDefinedSource(substrate->id());
          }
        }
      }
    }
    // * Remove reactions without substrate
    remove_reactions_without_substrate(&preprocessedNetwork_);
  }

  /* ***************************************************************************
   * ***************************************************************************
   *                       Remove isolated compounds
   * ***************************************************************************
   ****************************************************************************/
  remove_isolated_compounds(&preprocessedNetwork_);

  /* ***************************************************************************
   * ***************************************************************************
   *                       Remove duplicate reactions
   * ***************************************************************************
   ****************************************************************************/
  remove_duplicate_reactions(&preprocessedNetwork_);

  /* ***************************************************************************
   * ***************************************************************************
   *              Add topological sources to the list of sources
   * ***************************************************************************
   ****************************************************************************/
  if (prepocessedInput_.considerTopologicalSources()) {
    auto topologicalSourceCompounds = topological_sources(preprocessedNetwork_);
    for (const auto& topologicalSource : topologicalSourceCompounds) {
      prepocessedInput_.addUserDefinedSource(topologicalSource->id());
    }
  }

  /* ***************************************************************************
   * ***************************************************************************
   * Remove compounds from the set of sources that are also bootstrap compounds
   * ***************************************************************************
   ****************************************************************************/
  auto it = prepocessedInput_.bootstrapCompoundsBegin();
  auto end = prepocessedInput_.bootstrapCompoundsEnd();
  for (; it != end; ++it) {
    prepocessedInput_.removeUserDefinedSource(*it);
  }

  // Remove compounds from the target, source, bootstrap set if they do not
  // belong to the network
  std::list<CompoundIdType> compoundsToBeRemoved;
  it = prepocessedInput_.targetsBegin();
  end = prepocessedInput_.targetsEnd();
  for (; it != end; ++it) {
    if (preprocessedNetwork_.compoundById(*it) == nullptr) {
      compoundsToBeRemoved.push_back(*it);
    }
  }
  for (const auto& id : compoundsToBeRemoved) {
    std::cout << "Remove target from list: " << id << '\n';
    prepocessedInput_.removeTarget(id);
  }
  compoundsToBeRemoved.clear();
  it = prepocessedInput_.userDefinedSourcesBegin();
  end = prepocessedInput_.userDefinedSourcesEnd();
  for (; it != end; ++it) {
    if (preprocessedNetwork_.compoundById(*it) == nullptr) {
      compoundsToBeRemoved.push_back(*it);
    }
  }
  for (const auto& id : compoundsToBeRemoved) {
    std::cout << "Remove source from list: " << id << '\n';
    prepocessedInput_.removeUserDefinedSource(id);
  }
  compoundsToBeRemoved.clear();
  it = prepocessedInput_.bootstrapCompoundsBegin();
  end = prepocessedInput_.bootstrapCompoundsEnd();
  for (; it != end; ++it) {
    if (preprocessedNetwork_.compoundById(*it) == nullptr) {
      compoundsToBeRemoved.push_back(*it);
    }
  }
  for (const auto& id : compoundsToBeRemoved) {
    std::cout << "Remove bootstrap from list: " << id << '\n';
    prepocessedInput_.removeBootstrapCompound(id);
  }
}

#ifdef USE_CPLEX
/// Enumerate with cplex
template<typename MetabolicNetwork>
std::vector<
    typename MinimalPrecursorSetEnumerator<MetabolicNetwork>::PrecursorSet_t>
MinimalPrecursorSetEnumerator<MetabolicNetwork>::
enumerateWithCplex() const {
  std::vector<PrecursorSet_t> solutions;  // minimal precursor sets solutions
  IloEnv env;  // Cplex environment
  try {
    /* *************************************************************************
     * *************************************************************************
     *                          Build the model
     * *************************************************************************
     **************************************************************************/
    IloModel model(env);

    /* *************************************************************************
     * *************************************************************************
     *                          Create the variables
     * *************************************************************************
     **************************************************************************/

    // =========================================================================
    // One continuous variable for each network reaction; use filtered_vector
    // for constant access via the reaction id
    // =========================================================================
    std::string prefixReactionVar("R_");
    std::shared_ptr<FilteredVector<IloNumVar *>> reactionFluxVars =
        create_filtered_vector<IloNumVar *>(
            create_filtered_vector<IloNumVar *>(),
            std::make_unique<NullptrFilter<IloNumVar *>>());
    for (const auto &reaction : preprocessedNetwork_.reactions()) {
      if (reaction->id() > reactionFluxVars->size()) {
        // fill gaps with nullptrs and add then continuous variable
        size_t nbGaps = reaction->id() - reactionFluxVars->size();
        while (nbGaps-- > 0) {
          reactionFluxVars->push_back(nullptr);
        }
      }
      // Add continuous variable
      std::string varName = prefixReactionVar + std::to_string(reaction->id());
      reactionFluxVars->push_back(new IloNumVar(env,
                                                reaction->lowerBound(),
                                                reaction->upperBound(),
                                                varName.c_str()));
    }

    // =========================================================================
    // Continuous variables for source imports (source-pool reactions)
    // =========================================================================
    std::shared_ptr<FilteredVector<IloNumVar *>> sourcePoolFluxVars =
        create_filtered_vector<IloNumVar *>(
            create_filtered_vector<IloNumVar *>(),
            std::make_unique<NullptrFilter<IloNumVar *>>());
    // Integer variables for source imports (source-pool reactions)
    std::shared_ptr<FilteredVector<IloNumVar *>> sourcePoolIndicatorVars =
        create_filtered_vector<IloNumVar *>(
            create_filtered_vector<IloNumVar *>(),
            std::make_unique<NullptrFilter<IloNumVar *>>());
    // Create the objective function in the same run: minimise the sum of the
    // binary variables
    IloObjective objective = IloMinimize(env);
    if (prepocessedInput_.userDefinedSourcesBegin() !=
        prepocessedInput_.userDefinedSourcesEnd()) {
      // Get highest source id and initialise sourcePoolFluxVars and
      // sourcePoolIndicatorVars with nullptrs
      CompoundIdType maxSourceId(0);
      for (auto it = prepocessedInput_.userDefinedSourcesBegin();
           it != prepocessedInput_.userDefinedSourcesEnd(); ++it) {
        const auto &sourceId = *it;
        if (sourceId > maxSourceId) {
          maxSourceId = sourceId;
        }
      }
      // Init filtered_vectors
      for (CompoundIdType i = 0; i <= maxSourceId; ++i) {
        sourcePoolFluxVars->push_back(nullptr);
        sourcePoolIndicatorVars->push_back(nullptr);
      }
      // Fill filtered_vectors
      for (auto it = prepocessedInput_.userDefinedSourcesBegin();
           it != prepocessedInput_.userDefinedSourcesEnd();
           ++it) {
        const auto &source = *it;
        // Replace the nullptr at position 'source' with a continuous variable
        std::string varName = "importS_" + std::to_string(source);
        sourcePoolFluxVars->operator[](source) = new IloNumVar(
            env, 0, defaultUpperBound, varName.c_str());

        // Replace the nullptr at position 'source' with a boolean variable
        sourcePoolIndicatorVars->operator[](source) = new IloNumVar(
            env, 0, 1, IloNumVar::Int, std::to_string(source).c_str());
        // Add boolean variable to objective function
        objective.setLinearCoef(*(sourcePoolIndicatorVars->operator[](source)),
                                1);
      }
    }

    // =========================================================================
    // Continuous variables for import reactions of bootstrap compounds
    // =========================================================================
    std::shared_ptr<FilteredVector<IloNumVar *>> bootstrapImportFluxVars =
        create_filtered_vector<IloNumVar *>(
            create_filtered_vector<IloNumVar *>(),
            std::make_unique<NullptrFilter<IloNumVar *>>());
    if (prepocessedInput_.bootstrapCompoundsBegin() !=
        prepocessedInput_.bootstrapCompoundsEnd()) {
      // Get highest bootstrap id and initialise bootstrapImportFluxVars with
      // nullptrs
      CompoundIdType maxBootstrapId(0);
      for (auto it = prepocessedInput_.bootstrapCompoundsBegin();
           it != prepocessedInput_.bootstrapCompoundsEnd();
           ++it) {
        const auto &bootstrapId = *it;
        if (bootstrapId > maxBootstrapId) {
          maxBootstrapId = bootstrapId;
        }
      }
      // Init bootstrapImportFluxVars
      for (CompoundIdType i = 0; i <= maxBootstrapId; ++i) {
        bootstrapImportFluxVars->push_back(nullptr);
      }
      for (auto it = prepocessedInput_.bootstrapCompoundsBegin();
           it != prepocessedInput_.bootstrapCompoundsEnd();
           ++it) {
        const auto &cmpId = *it;
        bootstrapImportFluxVars->operator[](cmpId) = new IloNumVar(
            env, 0, defaultUpperBound, std::to_string(cmpId).c_str());
      }
    }

    /* *************************************************************************
     * *************************************************************************
     *              Build the model by row (constraint per compound)
     * *************************************************************************
     **************************************************************************/
    // Define first the default lower and upper bounds
    double defaultLowerBoundOfConstraint(0.0);
    double defaultUpperBoundOfConstraint =
        (prepocessedInput_.constraintType() == ConstraintType::STEADYSTATE ?
         0.0 : IloInfinity);
    // Build constraint per compound
    for (const auto &compound : preprocessedNetwork_.compounds()) {
      const auto &compoundId = compound->id();
      IloRange constraint = IloRange(env, defaultLowerBoundOfConstraint,
                                     defaultUpperBoundOfConstraint,
                                     compound->sbmlId().c_str());
      // Build right hand side
      // It's a target -> constraint >= epsilon
      if (std::find(prepocessedInput_.targetsBegin(),
                    prepocessedInput_.targetsEnd(),
                    compoundId) != prepocessedInput_.targetsEnd()) {
        constraint.setLB(prepocessedInput_.epsilon());
        constraint.setUB(IloInfinity);
      }

      // Build left hand side
      // producing reactions
      auto it = preprocessedNetwork_.producingReactionsBegin(compoundId);
      auto end = preprocessedNetwork_.producingReactionsEnd(compoundId);
      for (; it != end; ++it) {
        auto stoichiometry = preprocessedNetwork_.productStoichiometry(
            compoundId, (*it)->id());
        constraint.setLinearCoef(*(reactionFluxVars->operator[]((*it)->id())),
                                 stoichiometry);
      }

      // consuming reactions
      it = preprocessedNetwork_.consumingReactionsBegin(compoundId);
      end = preprocessedNetwork_.consumingReactionsEnd(compoundId);
      for (; it != end; ++it) {
        auto stoichiometry = -preprocessedNetwork_.substrateStoichiometry(
            compoundId, (*it)->id());
        constraint.setLinearCoef(*(reactionFluxVars->operator[]((*it)->id())),
                                 stoichiometry);
      }

      // Source?
      if (std::find(prepocessedInput_.userDefinedSourcesBegin(),
                    prepocessedInput_.userDefinedSourcesEnd(),
                    compoundId) != prepocessedInput_.userDefinedSourcesEnd()) {
        // Add import reaction
        constraint.setLinearCoef(*(sourcePoolFluxVars->operator[](compoundId)),
                                 1);

        // Add indicator constraint b_i = 0 <=> v_i <= 0
        IloNumVar b_i = *(sourcePoolIndicatorVars->operator[](compoundId));
        IloNumVar v_i = *(sourcePoolFluxVars->operator[](compoundId));
        std::string indConstName = "IND_1_" + compound->sbmlId();
        model.add(IloIfThen(env, (b_i <= 0), (v_i <= 0), indConstName.c_str()));
        indConstName = "IND_2_" + compound->sbmlId();
        model.add(IloIfThen(env, (v_i <= 0), (b_i <= 0), indConstName.c_str()));
      } else if (std::find(prepocessedInput_.bootstrapCompoundsBegin(),
                           prepocessedInput_.bootstrapCompoundsEnd(),
                           compoundId) !=
                 prepocessedInput_.bootstrapCompoundsEnd()) {
        // Bootstrap compound? Yes, add import reaction
        constraint.setLinearCoef(
            *(bootstrapImportFluxVars->operator[](compoundId)), 1);
      }
      // TODO(mw) Add DUPMACH constraint

      model.add(constraint);  // Add constraint to the model
    }

    // Objective function
    model.add(objective);

    /* *************************************************************************
     * *************************************************************************
     * Solve the model and add a constraint per solution (to exclude supersets)
     * until there are no minimal solutions anymore
     * *************************************************************************
     **************************************************************************/
    IloCplex cplex(model);
    // Set some parameter to obtaint he full solution pool
    cplex.setParam(IloCplex::Param::MIP::Pool::AbsGap, 0.0);
    cplex.setParam(IloCplex::Param::MIP::Pool::Intensity, 4);
    cplex.setParam(IloCplex::Param::MIP::Limits::Populate, 2100000000);
    cplex.setParam(IloCplex::Param::Emphasis::Numerical, true);
    cplex.setParam(IloCplex::Param::MIP::Tolerances::Integrality, 1e-9);
    cplex.setParam(IloCplex::Param::Simplex::Tolerances::Feasibility, 1e-9);
    // No output
    cplex.setOut(env.getNullStream());
    // cplex.exportModel("test.lp");
    int numsol(0);
    do {
      // Solve
      cplex.populate();
      numsol = cplex.getSolnPoolNsolns();  // Number of solutions
      std::vector<PrecursorSet_t> nextSolutions;
      for (int i = 0; i < numsol; i++) {
        // Create precursor set object
        PrecursorSet_t nextSolution;
        // Add precursors
        for (const auto &var : *sourcePoolIndicatorVars) {
          if (IloRound(cplex.getValue(*var, i)) == 1) {
            const auto &compound =
                prepocessedInput_.metabolicNetwork()->compoundById(
                    std::stoi(var->getName()));
            // Add source to precursor set
            nextSolution.addPrecursor(*compound);
          }
        }
        // Add bootstrap compounds
        for (const auto &var : *bootstrapImportFluxVars) {
          if (cplex.getValue(*var, i) > 0) {
            const auto &compound =
                prepocessedInput_.metabolicNetwork()->compoundById(
                    std::stoi(var->getName()));
            nextSolution.addBootstrapCompound(*compound);
          }
        }
        // Add reactions if wanted
        if (prepocessedInput_.addFactoryReactions()) {
          for (const auto &var : *reactionFluxVars) {
            if (cplex.getValue(*var, i) > 0) {
              // Retrieve the Id of the reaction
              std::string reactionIdString(var->getName());
              // Remove the prefix
              reactionIdString.erase(0, prefixReactionVar.length());
              const auto &reaction =
                  prepocessedInput_.metabolicNetwork()->reactionById(
                      std::stoi(reactionIdString));
              nextSolution.addFactoryReaction(*reaction);
            }
          }
        }
        // Add solution to return vector
        nextSolutions.push_back(nextSolution);
      }

      for (const auto &nextSolution : nextSolutions) {
        // If non-empty solution S, add constraint to the model to exclude
        // supersets of the solution S: sum of b_i <= |S| - 1
        if (nextSolution.precursorsBegin() != nextSolution.precursorsEnd()) {
          IloRange excludeSupersets = IloRange(
              env, 0, nextSolution.nbPrecursors() - 1);
          auto it = nextSolution.precursorsBegin();
          auto end = nextSolution.precursorsEnd();
          for (; it != end; ++it) {
            excludeSupersets.setLinearCoef(
                *(sourcePoolIndicatorVars->operator[]((*it)->id())), 1.0);
          }
          model.add(excludeSupersets);
        }
        solutions.push_back(nextSolution);
      }
    } while (numsol > 0);
    // de-allocate pointers to cplex variables
    for (const auto& var : *reactionFluxVars) {
      delete var;
    }
    for (const auto& var : *sourcePoolFluxVars) {
      delete var;
    }
    for (const auto& var : *sourcePoolIndicatorVars) {
      delete var;
    }
    for (const auto& var : *bootstrapImportFluxVars) {
      delete var;
    }
  } catch (const IloException &e) {
    std::cout << "Concert exception caught: " << e << '\n';
  } catch (...) {
    std::cout << "Unknown exception caught\n";
  }
  env.end();
  return solutions;
}
#endif  // USE_CPLEX

}  // namespace metnetlib
