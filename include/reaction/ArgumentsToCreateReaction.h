/*Copyright (C) <2016-2017>  David Parsons, Martin Wannagat

This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU Affero General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/
//
// Created by Martin Wannagat on 04.12.17.
//

#ifndef METNETLIB_ARGUMENTSTOCREATEREACTION_H
#define METNETLIB_ARGUMENTSTOCREATEREACTION_H

namespace metnetlib {

template<typename Reaction_t>
class ArgumentsToCreateReaction {
  friend Reaction_t;

  /* **************************************************************************
   * **************************************************************************
   *                        Alias declarations
   * **************************************************************************
   ***************************************************************************/
  /// Alias for data structure to provide substrate and product names
  typedef std::pair <std::vector<std::string>,
  std::vector<std::string>> SubstrateAndProductNames;

  /* **************************************************************************
   * **************************************************************************
   *                       Constructor
   * **************************************************************************
   ***************************************************************************/
 protected:
  /// Constructor
  ArgumentsToCreateReaction(const SubstrateAndProductNames&,
                            const typename Reaction_t::SpecificAttributes&);

  /* **************************************************************************
   * **************************************************************************
   *                        Member functions
   * **************************************************************************
   ***************************************************************************/
 public:
  /// Get substrate and product names
  const SubstrateAndProductNames& substrateAndProductNames() const;

  /// Get specific arguments
  const typename Reaction_t::SpecificAttributes& specificAttributes() const;

  /// Set substrate and product names
  void setSubstrateAndProductNames(const SubstrateAndProductNames&);

  /// Set arguments that are specific to the reaction type (Reaction_t)
  void setSpecificArguments(const typename Reaction_t::SpecificAttributes&);

  /* **************************************************************************
   * **************************************************************************
   *                        Data members
   * **************************************************************************
   ***************************************************************************/
 protected:
  /// Substrate and product names
  SubstrateAndProductNames substrateAndProductNames_;
  /// Specific arguments for the reaction type
  typename Reaction_t::SpecificAttributes specificAttributes_;
};
}  // namespace metnetlib

#include "ArgumentsToCreateReaction.hpp"

#endif  // METNETLIB_ARGUMENTSTOCREATEREACTION_H
