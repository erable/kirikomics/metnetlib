/*Copyright (C) <2016-2017>  David Parsons, Martin Wannagat

This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU Affero General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/
//
// Created by Martin Wannagat on 16.11.17.
//

#include "SubMetabolicNetwork.h"

#include <regex>

#include "MetabolicNetworkObservableEvents.h"
#include "reaction/ArgumentsToCreateReaction.h"

namespace metnetlib {

/// Constructor
template <template <typename> class CompoundTemplate_t, typename Reaction_type,
    typename CompoundProperty, typename ReactionProperty,
    typename MetabolicNetworkProperty>
SubMetabolicNetwork<CompoundTemplate_t, Reaction_type, CompoundProperty,
    ReactionProperty, MetabolicNetworkProperty>::
SubMetabolicNetwork(MetabolicNetworkInterface<CompoundTemplate_t, Reaction_type,
                    CompoundProperty, ReactionProperty,
                    MetabolicNetworkProperty>* parentMetabolicNetwork,
                    const std::unordered_set<
                        CompoundIdType>& whitelistedCompoundIds,
                    const std::unordered_set<
                        ReactionIdType>& whitelistedReactionIds) :
    MetabolicNetworkInterface<CompoundTemplate_t, Reaction_type,
        CompoundProperty, ReactionProperty, MetabolicNetworkProperty>(),
    MetabolicNetworkObserver(),
    parentMetabolicNetwork_(parentMetabolicNetwork) {
  // build whitelisted compartment FilteredVector
  // All parent compartments are part of the sub-metabolic network
  std::vector<Compartment*> whitelistFilterCompartments;
  CompartmentIdType expectedNextId(0);
  for (const auto& compartment : parentMetabolicNetwork_->compartments()) {
    auto compartmentId = compartment->id();
    if (expectedNextId != compartmentId) {
      do {
        whitelistFilterCompartments.push_back(nullptr);
        --compartmentId;
      } while (compartmentId > expectedNextId);
    }
    whitelistFilterCompartments.push_back(
        const_cast<Compartment*>(compartment));
    ++expectedNextId;
  }

  // create actual whitelisted compartment list
  whitelistedCompartmentList_ = create_filtered_vector<Compartment*>(
      parentMetabolicNetwork_->getRootCompartmentContainerPtr(),
      std::make_unique<WhitelistFilter<Compartment*>>(
          whitelistFilterCompartments));

  // Init underlying DirectedSubHypergraph
  directedSubHypergraph_ = new DirectedSubHypergraph_t(
      parentMetabolicNetwork_->getUnderlyingDirectedHypergraph(),
      whitelistedCompoundIds, whitelistedReactionIds);

  // Create MetabolicNetworkProperty object
  subMetabolicNetworkProperty_ = new MetabolicNetworkProperty(
      *parentMetabolicNetwork_->getMetabolicNetworkProperties_());

  // add itself as observer of the parent
  parentMetabolicNetwork_->AddObserver(
      this, MetabolicNetworkObservableEvent::COMPARTMENT_REMOVED);
  parentMetabolicNetwork_->AddObserver(
      this, MetabolicNetworkObservableEvent::RESET_COMPARTMENT_IDS);
}


/// Copy constructor
template <template <typename> class CompoundTemplate_t, typename Reaction_type,
    typename CompoundProperty, typename ReactionProperty,
    typename MetabolicNetworkProperty>
SubMetabolicNetwork<CompoundTemplate_t, Reaction_type, CompoundProperty,
    ReactionProperty, MetabolicNetworkProperty>::
SubMetabolicNetwork(const SubMetabolicNetwork& other) :
    MetabolicNetworkObserver(),
    parentMetabolicNetwork_(other.parentMetabolicNetwork_) {
  // copy whitelisted compartment FilteredVector
  std::vector<Compartment*> whitelistFilterCompartments;
  // Init with nullptrs
  for (size_t i = 0; i < parentMetabolicNetwork_->compartmentContainerSize();
       ++i) {
    whitelistFilterCompartments.push_back(nullptr);
  }
  // Insert compartments
  for (const auto& compartment : other.compartments()) {
    whitelistFilterCompartments[compartment->id()] =
        const_cast<Compartment*>(compartment);
  }
  // create actual compartment list
  whitelistedCompartmentList_ = create_filtered_vector<Compartment*>(
      parentMetabolicNetwork_->getRootCompartmentContainerPtr(),
      std::make_unique<WhitelistFilter<Compartment*>>(
          whitelistFilterCompartments));

  // Init underlying DirectedSubHypergraph
  directedSubHypergraph_ =
      new DirectedSubHypergraph_t(*(other.directedSubHypergraph_));

  // Create MetabolicNetworkProperty object
  subMetabolicNetworkProperty_ = new MetabolicNetworkProperty(
      *(other.getMetabolicNetworkProperties_()));

  // add itself as observer of the parent
  parentMetabolicNetwork_->AddObserver(
      this, MetabolicNetworkObservableEvent::COMPARTMENT_REMOVED);
  parentMetabolicNetwork_->AddObserver(
      this, MetabolicNetworkObservableEvent::RESET_COMPARTMENT_IDS);
}


/// Copy assignment
template <template <typename> class CompoundTemplate_t, typename Reaction_type,
    typename CompoundProperty, typename ReactionProperty,
    typename MetabolicNetworkProperty>
SubMetabolicNetwork<CompoundTemplate_t, Reaction_type, CompoundProperty,
    ReactionProperty, MetabolicNetworkProperty>& SubMetabolicNetwork<
    CompoundTemplate_t, Reaction_type, CompoundProperty, ReactionProperty,
    MetabolicNetworkProperty>::
operator=(const SubMetabolicNetwork<CompoundTemplate_t, Reaction_type,
    CompoundProperty, ReactionProperty, MetabolicNetworkProperty>& source) {
  if (this != &source) {
    // Set the parent of all childs in the sub-network-hierarchy to null
    std::unordered_set<SubMetabolicNetwork<CompoundTemplate_t, Reaction_type,
        CompoundProperty, ReactionProperty,
        MetabolicNetworkProperty>*> observers;
    for (auto itEvent = this->observers_.begin();
         itEvent != this->observers_.end(); ++itEvent) {
      for (auto itObserver : itEvent->second) {
        auto subnetwork = dynamic_cast<SubMetabolicNetwork<CompoundTemplate_t,
            Reaction_type, CompoundProperty, ReactionProperty,
            MetabolicNetworkProperty>*>(itObserver);
        observers.insert(subnetwork);
      }
    }
    for (auto& subnetwork : observers) {
      subnetwork->setParentOfChildsToNull();
      this->DeleteObserver(
          subnetwork, MetabolicNetworkObservableEvent::COMPARTMENT_REMOVED);
      this->DeleteObserver(
          subnetwork, MetabolicNetworkObservableEvent::RESET_COMPARTMENT_IDS);
    }

    // Detach itself as observer from parent graph
    parentMetabolicNetwork_->DeleteObserver(
        this, MetabolicNetworkObservableEvent::COMPARTMENT_REMOVED);
    parentMetabolicNetwork_->DeleteObserver(
        this, MetabolicNetworkObservableEvent::RESET_COMPARTMENT_IDS);

    // Assign parent
    parentMetabolicNetwork_ = source.parentMetabolicNetwork_;

    // copy whitelisted compartments FilteredVector
    std::vector<Compartment*> whitelistFilterCompartments;
    for (size_t i = 0; i < parentMetabolicNetwork_->compartmentContainerSize();
         ++i) {
      whitelistFilterCompartments.push_back(nullptr);
    }
    for (const auto& compartment : source.compartments()) {
      whitelistFilterCompartments[compartment->id()] =
          const_cast<Compartment*>(compartment);
    }
    // create actual compartments list
    whitelistedCompartmentList_ = create_filtered_vector<Compartment*>(
        parentMetabolicNetwork_->getRootCompartmentContainerPtr(),
        std::make_unique<WhitelistFilter<Compartment*>>(
            whitelistFilterCompartments));

    // Init underlying DirectedSubHypergraph
    delete directedSubHypergraph_;
    directedSubHypergraph_ =
        new DirectedSubHypergraph_t(*(source.directedSubHypergraph_));

    // init hypergraph property
    delete subMetabolicNetworkProperty_;
    subMetabolicNetworkProperty_ = new MetabolicNetworkProperty(
        *source.getMetabolicNetworkProperties_());

    // Attach itself as observer to parent graph
    parentMetabolicNetwork_->AddObserver(
        this, MetabolicNetworkObservableEvent::COMPARTMENT_REMOVED);
    parentMetabolicNetwork_->AddObserver(
        this, MetabolicNetworkObservableEvent::RESET_COMPARTMENT_IDS);
  }
  return *this;
}


/// Destructor
template <template <typename> class CompoundTemplate_t, typename Reaction_type,
    typename CompoundProperty, typename ReactionProperty,
    typename MetabolicNetworkProperty>
SubMetabolicNetwork<CompoundTemplate_t, Reaction_type, CompoundProperty,
    ReactionProperty, MetabolicNetworkProperty>::
~SubMetabolicNetwork() {
  delete directedSubHypergraph_;
  delete subMetabolicNetworkProperty_;

  // Detach itself as observer of the parent network
  if (parentMetabolicNetwork_ != nullptr) {
    parentMetabolicNetwork_->DeleteObserver(
        this, MetabolicNetworkObservableEvent::COMPARTMENT_REMOVED);
    parentMetabolicNetwork_->DeleteObserver(
        this, MetabolicNetworkObservableEvent::RESET_COMPARTMENT_IDS);

    // Sub-networks whose parent (this sub-network) becomes the current
    // grand-parent.
    // Add sub-networks as observers to the current grand-parent.
    // Clear list of observers of this sub-network.
    for (auto itEvent = this->observers_.begin();
         itEvent != this->observers_.end(); ++itEvent) {
      for (auto itObserver = itEvent->second.begin();
           itObserver != itEvent->second.end(); ++itObserver) {
        auto subNetwork = dynamic_cast<SubMetabolicNetwork<CompoundTemplate_t,
            Reaction_type, CompoundProperty, ReactionProperty,
            MetabolicNetworkProperty>*>(*itObserver);
        subNetwork->parentMetabolicNetwork_ = parentMetabolicNetwork_;
        subNetwork->parentMetabolicNetwork_->AddObserver(subNetwork,
                                                         itEvent->first);
      }
      this->observers_[itEvent->first].clear();
    }
  }
}

/* **************************************************************************
 * **************************************************************************
 *                        Member functions
 * **************************************************************************
 ***************************************************************************/
// related to compartment
/// Add a compartment
template <template <typename> class CompoundTemplate_t, typename Reaction_type,
    typename CompoundProperty, typename ReactionProperty,
    typename MetabolicNetworkProperty>
const Compartment* SubMetabolicNetwork<CompoundTemplate_t, Reaction_type,
    CompoundProperty, ReactionProperty, MetabolicNetworkProperty>::
addCompartment(const std::string& sbmlId, double size, bool constant) {
  throwExceptionOnNullptrParent();
  try {
    // Add new compartment to the parent metabolic network
    const auto compartment = parentMetabolicNetwork_->addCompartment(
        sbmlId, size, constant);
    whitelistedCompartmentList_->removeFilteredOutItem(
        const_cast<Compartment*>(compartment));
    return compartment;
  } catch (const std::invalid_argument& ia) {
    throw std::invalid_argument(ia.what());
  }
}


/// Get pointer to compartment
template <template <typename> class CompoundTemplate_t, typename Reaction_type,
    typename CompoundProperty, typename ReactionProperty,
    typename MetabolicNetworkProperty>
const Compartment* SubMetabolicNetwork<CompoundTemplate_t, Reaction_type,
    CompoundProperty, ReactionProperty, MetabolicNetworkProperty>::
compartmentBySbmlId(const std::string& sbmlId) const {
  throwExceptionOnNullptrParent();
  // Search for compartment with Sbml Id
  for (const auto& compartment : *whitelistedCompartmentList_) {
    if (compartment->sbmlId().compare(sbmlId) == 0) {
      return compartment;
    }
  }
  return nullptr;
}


/// Get pointer to compartment
template <template <typename> class CompoundTemplate_t, typename Reaction_type,
    typename CompoundProperty, typename ReactionProperty,
    typename MetabolicNetworkProperty>
const Compartment* SubMetabolicNetwork<CompoundTemplate_t, Reaction_type,
    CompoundProperty, ReactionProperty, MetabolicNetworkProperty>::
compartmentById(const CompartmentIdType& id) const {
  throwExceptionOnNullptrParent();
  try {
    const auto& compartment = whitelistedCompartmentList_->at(id);
    return whitelistedCompartmentList_->filter_out(compartment) ?
           nullptr : compartment;
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in SubMetabolicNetwork::"
        "compartmentById: "
        "index = " << id << "; min = 0; max = "
              << whitelistedCompartmentList_->size() << '\n';
    throw;
  }
}

/// Get number of compartments
template <template <typename> class CompoundTemplate_t, typename Reaction_type,
    typename CompoundProperty, typename ReactionProperty,
    typename MetabolicNetworkProperty>
size_t SubMetabolicNetwork<CompoundTemplate_t, Reaction_type, CompoundProperty,
    ReactionProperty, MetabolicNetworkProperty>::
nbCompartments() const {
  throwExceptionOnNullptrParent();
  size_t nbCompartments(0);
  for (auto it = whitelistedCompartmentList_->begin();
       it != whitelistedCompartmentList_->end(); ++it) {
    ++nbCompartments;
  }
  return nbCompartments;
}


/// Const compartment iterator pointing to begin of the compartments
template <template <typename> class CompoundTemplate_t, typename Reaction_type,
    typename CompoundProperty, typename ReactionProperty,
    typename MetabolicNetworkProperty>
typename SubMetabolicNetwork<CompoundTemplate_t, Reaction_type,
    CompoundProperty, ReactionProperty,
    MetabolicNetworkProperty>::compartment_iterator SubMetabolicNetwork<
    CompoundTemplate_t, Reaction_type, CompoundProperty, ReactionProperty,
    MetabolicNetworkProperty>::
compartmentsBegin() const {
  throwExceptionOnNullptrParent();
  return whitelistedCompartmentList_->cbegin();
}


/// Const compartment iterator pointing to end of the compartments
template <template <typename> class CompoundTemplate_t, typename Reaction_type,
    typename CompoundProperty, typename ReactionProperty,
    typename MetabolicNetworkProperty>
typename SubMetabolicNetwork<CompoundTemplate_t, Reaction_type,
    CompoundProperty, ReactionProperty,
    MetabolicNetworkProperty>::compartment_iterator SubMetabolicNetwork<
    CompoundTemplate_t, Reaction_type, CompoundProperty, ReactionProperty,
    MetabolicNetworkProperty>::
compartmentsEnd() const {
  throwExceptionOnNullptrParent();
  return whitelistedCompartmentList_->cend();
}


/// Return a reference to the container of compartments.
template <template <typename> class CompoundTemplate_t, typename Reaction_type,
    typename CompoundProperty, typename ReactionProperty,
    typename MetabolicNetworkProperty>
const typename SubMetabolicNetwork<CompoundTemplate_t, Reaction_type,
    CompoundProperty, ReactionProperty,
    MetabolicNetworkProperty>::CompartmentContainer& SubMetabolicNetwork<
    CompoundTemplate_t, Reaction_type, CompoundProperty, ReactionProperty,
    MetabolicNetworkProperty>::
compartments() const {
  throwExceptionOnNullptrParent();
  return *whitelistedCompartmentList_;
}


/// Remove a compartment and all its compounds from the network
template <template <typename> class CompoundTemplate_t, typename Reaction_type,
    typename CompoundProperty, typename ReactionProperty,
    typename MetabolicNetworkProperty>
void SubMetabolicNetwork<CompoundTemplate_t, Reaction_type,
    CompoundProperty, ReactionProperty, MetabolicNetworkProperty>::
removeCompartment(const CompartmentIdType& id,
                  bool removeImpliedReactions) {
  throwExceptionOnNullptrParent();
  /*
   * Check if there exists a compartment in the sub-network with the given id.
   * If yes, remove it from the whitelist.
   * If no, an exception is thrown.
   */
  try {
    const auto compartment = compartmentById(id);
    if (compartment == nullptr) {
      std::string errorMessage("Called SubMetabolicNetwork::removeCompartment"
                                   " with the compartment id ");
      errorMessage += std::to_string(id);
      errorMessage += " which does not exists in the sub-network."
          "\n";
      throw std::invalid_argument(errorMessage);
    }

    // Remove all compounds in the compartment
    std::vector<Compound_t*> removeCompounds;
    for (const auto& compound : directedSubHypergraph_->vertices()) {
      if (compound->getCompartment() == compartment) {
        removeCompounds.push_back(compound);
      }
    }
    for (const auto& compound : removeCompounds) {
      directedSubHypergraph_->removeVertex(compound->id(),
                                           removeImpliedReactions);
    }

    CompartmentIdType idx(id);
    // Notify observers of change
    Remove_compartment_args args;
    args.compartmentId_ = idx;
    args.removeImpliedReactions = removeImpliedReactions;
    this->NotifyObservers(MetabolicNetworkObservableEvent::COMPARTMENT_REMOVED,
                          &args);

    // Filter out compartment from this sub-network
    whitelistedCompartmentList_->addFilteredOutItem(
        const_cast<Compartment*>(compartment));
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in DirectedSubHypergraph::"
        "removeCompartment: "
        "index = " << id << "; min = 0; max = " <<
              whitelistedCompartmentList_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& ia) {
    throw;
  }
}


/// Highest assigned compartment id of the network
template <template <typename> class CompoundTemplate_t, typename Reaction_type,
    typename CompoundProperty, typename ReactionProperty,
    typename MetabolicNetworkProperty>
size_t SubMetabolicNetwork<CompoundTemplate_t, Reaction_type,
    CompoundProperty, ReactionProperty, MetabolicNetworkProperty>::
compartmentContainerSize() const {
  throwExceptionOnNullptrParent();
  return parentMetabolicNetwork_->compartmentContainerSize();
}


// related to compounds
/// \brief Add a compound with a given Sbml Id and several additional
/// arguments specific to the compound type
template <template <typename> class CompoundTemplate_t, typename Reaction_type,
    typename CompoundProperty, typename ReactionProperty,
    typename MetabolicNetworkProperty>
const typename SubMetabolicNetwork<CompoundTemplate_t, Reaction_type,
    CompoundProperty, ReactionProperty,
    MetabolicNetworkProperty>::Compound_t* SubMetabolicNetwork<
    CompoundTemplate_t, Reaction_type, CompoundProperty, ReactionProperty,
    MetabolicNetworkProperty>::
addCompound(const std::string& sbmlId,
            const typename Compound_t::SpecificAttributes& attributes) {
  throwExceptionOnNullptrParent();

  // * Check if sbmlId follows the SBML specification
  // * Check if there exists the compartment ptr (inside specificAttributes)
  // in this network
  // * Add compound to underlying directed sub-hypergraph
  // * Return pointer to compound

  // Check if sbmlId follows the SBML specification
  std::regex regEx("[a-zA-Z_][a-zA-Z0-9_]*");
  if (not std::regex_match(sbmlId.begin(), sbmlId.end(), regEx)) {
    std::string errorMessage = "The compound Sbml Id ";
    errorMessage += sbmlId;
    errorMessage += " is not valid. The Sbml Id must match the following"
        " regular expression: [a-zA-Z_][a-zA-Z0-9_]*";
    throw std::invalid_argument(errorMessage);
  }

  // Check if there exists the compartment ptr (inside specificAttributes)
  // in this network
  try {
    if (attributes.compartment_ == nullptr) {
      throw std::invalid_argument("Exception in SubMetabolicNetwork::"
                                      "addCompound:"
                                      " Provide a valid compartment pointer."
                                      " Not a nullptr!\n");
    }
    auto compartmentPtr = attributes.compartment_;
    if (compartmentById(compartmentPtr->id()) != compartmentPtr) {
      throw std::invalid_argument("Exception in SubMetabolicNetwork::"
                                      "addCompound:"
                                      " Provide a valid compartment pointer"
                                      " from this metabolic network.\n");
    }
  } catch (const std::out_of_range& oor) {
    std::cerr << "Exception in SubMetabolicNetwork::addCompound: "
        "Provide a valid compartment pointer from this network.\n";
    throw oor;
  }

  return directedSubHypergraph_->addVertex(sbmlId, attributes);
}


/// Get pointer to compound
template <template <typename> class CompoundTemplate_t, typename Reaction_type,
    typename CompoundProperty, typename ReactionProperty,
    typename MetabolicNetworkProperty>
const typename SubMetabolicNetwork<CompoundTemplate_t, Reaction_type,
    CompoundProperty, ReactionProperty,
    MetabolicNetworkProperty>::Compound_t* SubMetabolicNetwork<
    CompoundTemplate_t, Reaction_type, CompoundProperty, ReactionProperty,
    MetabolicNetworkProperty>::
compoundById(const CompoundIdType& id) const {
  throwExceptionOnNullptrParent();
  return directedSubHypergraph_->vertexById(id);
}


/// Get pointer to compound
template <template <typename> class CompoundTemplate_t, typename Reaction_type,
    typename CompoundProperty, typename ReactionProperty,
    typename MetabolicNetworkProperty>
const typename SubMetabolicNetwork<CompoundTemplate_t, Reaction_type,
    CompoundProperty, ReactionProperty,
    MetabolicNetworkProperty>::Compound_t* SubMetabolicNetwork<
    CompoundTemplate_t, Reaction_type, CompoundProperty, ReactionProperty,
    MetabolicNetworkProperty>::
compoundBySbmlId(const std::string& sbmlId) const {
  throwExceptionOnNullptrParent();
  return directedSubHypergraph_->vertexByName(sbmlId);
}


/// Get number of compounds
template <template <typename> class CompoundTemplate_t, typename Reaction_type,
    typename CompoundProperty, typename ReactionProperty,
    typename MetabolicNetworkProperty>
size_t SubMetabolicNetwork<CompoundTemplate_t, Reaction_type,
    CompoundProperty, ReactionProperty, MetabolicNetworkProperty>::
nbCompounds() const {
  throwExceptionOnNullptrParent();
  return directedSubHypergraph_->nbVertices();
}


/// Const compound iterator pointing to begin of the compounds
template <template <typename> class CompoundTemplate_t, typename Reaction_type,
    typename CompoundProperty, typename ReactionProperty,
    typename MetabolicNetworkProperty>
typename SubMetabolicNetwork<CompoundTemplate_t, Reaction_type,
    CompoundProperty, ReactionProperty,
    MetabolicNetworkProperty>::compound_iterator SubMetabolicNetwork<
    CompoundTemplate_t, Reaction_type, CompoundProperty, ReactionProperty,
    MetabolicNetworkProperty>::
compoundsBegin() const {
  throwExceptionOnNullptrParent();
  return directedSubHypergraph_->verticesBegin();
}


/// Const compound iterator pointing to end of the compounds
template <template <typename> class CompoundTemplate_t, typename Reaction_type,
    typename CompoundProperty, typename ReactionProperty,
    typename MetabolicNetworkProperty>
typename SubMetabolicNetwork<CompoundTemplate_t, Reaction_type,
    CompoundProperty, ReactionProperty,
    MetabolicNetworkProperty>::compound_iterator SubMetabolicNetwork<
    CompoundTemplate_t, Reaction_type, CompoundProperty, ReactionProperty,
    MetabolicNetworkProperty>::
compoundsEnd() const {
  throwExceptionOnNullptrParent();
  return directedSubHypergraph_->verticesEnd();
}


/// Return a reference to the container of compounds.
template <template <typename> class CompoundTemplate_t, typename Reaction_type,
    typename CompoundProperty, typename ReactionProperty,
    typename MetabolicNetworkProperty>
const typename SubMetabolicNetwork<CompoundTemplate_t, Reaction_type,
    CompoundProperty, ReactionProperty,
    MetabolicNetworkProperty>::CompoundContainer& SubMetabolicNetwork<
    CompoundTemplate_t, Reaction_type, CompoundProperty, ReactionProperty,
    MetabolicNetworkProperty>::
compounds() const {
  throwExceptionOnNullptrParent();
  return directedSubHypergraph_->vertices();
}


/// Get arguments to create the compound
template <template <typename> class CompoundTemplate_t, typename Reaction_type,
    typename CompoundProperty, typename ReactionProperty,
    typename MetabolicNetworkProperty>
hglib::ArgumentsToCreateVertex<
    typename SubMetabolicNetwork<CompoundTemplate_t,
                                 Reaction_type,
                                 CompoundProperty,
                                 ReactionProperty,
                                 MetabolicNetworkProperty>::Compound_t>
SubMetabolicNetwork<CompoundTemplate_t,
                    Reaction_type,
                    CompoundProperty,
                    ReactionProperty,
                    MetabolicNetworkProperty>::
argumentsToCreateCompound(const CompoundIdType& compoundId) const {
  throwExceptionOnNullptrParent();
  return directedSubHypergraph_->argumentsToCreateVertex(compoundId);
}

/// Remove a compound with the given Id
template <template <typename> class CompoundTemplate_t, typename Reaction_type,
    typename CompoundProperty, typename ReactionProperty,
    typename MetabolicNetworkProperty>
void SubMetabolicNetwork<CompoundTemplate_t, Reaction_type, CompoundProperty,
    ReactionProperty, MetabolicNetworkProperty>::
removeCompound(const CompoundIdType& id,
               bool removeImpliedReactions) {
  throwExceptionOnNullptrParent();
  directedSubHypergraph_->removeVertex(id, removeImpliedReactions);
}


// consuming reactions of a compound
/// Check whether the compound with given Id is consumed by a reaction
template <template <typename> class CompoundTemplate_t, typename Reaction_type,
    typename CompoundProperty, typename ReactionProperty,
    typename MetabolicNetworkProperty>
bool SubMetabolicNetwork<CompoundTemplate_t, Reaction_type, CompoundProperty,
    ReactionProperty, MetabolicNetworkProperty>::
isConsumed(const CompoundIdType& id) const {
  throwExceptionOnNullptrParent();
  try {
    return directedSubHypergraph_->hasOutHyperarcs(id);
  } catch (const std::out_of_range& oor) {
    std::string errorMsg = "IndexOutOfRangeException in SubMetabolicNetwork::";
    errorMsg += "isConsumed: compound index = ";
    errorMsg += std::to_string(id);
    errorMsg += "; min = 0; max = ";
    errorMsg += std::to_string(directedSubHypergraph_->vertexContainerSize());
    errorMsg += '\n';
    throw std::out_of_range(errorMsg);
  } catch (const std::invalid_argument& oor) {
    std::string errorMessage = "Called SubMetabolicNetwork::";
    errorMessage += "isConsumed, but the compound id ";
    errorMessage += std::to_string(id);
    errorMessage += " does not exists in the sub-metabolic network.\n";
    throw std::invalid_argument(errorMessage);
  }
}


/// Get number of consuming reactions
template <template <typename> class CompoundTemplate_t, typename Reaction_type,
    typename CompoundProperty, typename ReactionProperty,
    typename MetabolicNetworkProperty>
size_t SubMetabolicNetwork<CompoundTemplate_t, Reaction_type, CompoundProperty,
    ReactionProperty, MetabolicNetworkProperty>::
nbConsumingReactions(const CompoundIdType& id) const {
  throwExceptionOnNullptrParent();
  try {
    return directedSubHypergraph_->outDegree(id);
  } catch (const std::out_of_range& oor) {
    std::string errorMsg = "IndexOutOfRangeException in SubMetabolicNetwork::";
    errorMsg += "nbConsumingReactions: compound index = ";
    errorMsg += std::to_string(id);
    errorMsg += "; min = 0; max = ";
    errorMsg += std::to_string(directedSubHypergraph_->vertexContainerSize());
    errorMsg += '\n';
    throw std::out_of_range(errorMsg);
  } catch (const std::invalid_argument& oor) {
    std::string errorMessage = "Called SubMetabolicNetwork::";
    errorMessage += "nbConsumingReactions, but the compound id ";
    errorMessage += std::to_string(id);
    errorMessage += " does not exists in the sub-metabolic network.\n";
    throw std::invalid_argument(errorMessage);
  }
}


/// \brief Const iterator pointing to begin of the consuming reactions of the
/// compound with given Id
template <template <typename> class CompoundTemplate_t, typename Reaction_type,
    typename CompoundProperty, typename ReactionProperty,
    typename MetabolicNetworkProperty>
typename SubMetabolicNetwork<CompoundTemplate_t, Reaction_type,
    CompoundProperty, ReactionProperty,
    MetabolicNetworkProperty>::reaction_iterator SubMetabolicNetwork<
    CompoundTemplate_t, Reaction_type, CompoundProperty, ReactionProperty,
    MetabolicNetworkProperty>::
consumingReactionsBegin(const CompoundIdType& id) const {
  throwExceptionOnNullptrParent();
  try {
    return directedSubHypergraph_->outHyperarcsBegin(id);
  } catch (const std::out_of_range& oor) {
    std::string errorMsg = "IndexOutOfRangeException in SubMetabolicNetwork::";
    errorMsg += "consumingReactionsBegin: compound index = ";
    errorMsg += std::to_string(id);
    errorMsg += "; min = 0; max = ";
    errorMsg += std::to_string(directedSubHypergraph_->vertexContainerSize());
    errorMsg += '\n';
    throw std::out_of_range(errorMsg);
  } catch (const std::invalid_argument& oor) {
    std::string errorMessage = "Called SubMetabolicNetwork::";
    errorMessage += "consumingReactionsBegin, but the compound id ";
    errorMessage += std::to_string(id);
    errorMessage += " does not exists in the sub-metabolic network.\n";
    throw std::invalid_argument(errorMessage);
  }
}


/// \brief Const iterator pointing to end of the consuming reactions of the
/// compound with given Id
template <template <typename> class CompoundTemplate_t, typename Reaction_type,
    typename CompoundProperty, typename ReactionProperty,
    typename MetabolicNetworkProperty>
typename SubMetabolicNetwork<CompoundTemplate_t, Reaction_type,
    CompoundProperty, ReactionProperty,
    MetabolicNetworkProperty>::reaction_iterator SubMetabolicNetwork<
    CompoundTemplate_t, Reaction_type, CompoundProperty, ReactionProperty,
    MetabolicNetworkProperty>::
consumingReactionsEnd(const CompoundIdType& id) const {
  throwExceptionOnNullptrParent();
  try {
    return directedSubHypergraph_->outHyperarcsEnd(id);
  } catch (const std::out_of_range& oor) {
    std::string errorMsg = "IndexOutOfRangeException in SubMetabolicNetwork::";
    errorMsg += "consumingReactionsEnd: compound index = ";
    errorMsg += std::to_string(id);
    errorMsg += "; min = 0; max = ";
    errorMsg += std::to_string(directedSubHypergraph_->vertexContainerSize());
    errorMsg += '\n';
    throw std::out_of_range(errorMsg);
  } catch (const std::invalid_argument& oor) {
    std::string errorMessage = "Called SubMetabolicNetwork::";
    errorMessage += "consumingReactionsEnd, but the compound id ";
    errorMessage += std::to_string(id);
    errorMessage += " does not exists in the sub-metabolic network.\n";
    throw std::invalid_argument(errorMessage);
  }
}


/// \brief Return a shared_ptr to the container of consuming reactions of the
/// compound with given Id
template <template <typename> class CompoundTemplate_t, typename Reaction_type,
    typename CompoundProperty, typename ReactionProperty,
    typename MetabolicNetworkProperty>
std::shared_ptr<const hglib::GraphElementContainer<typename SubMetabolicNetwork<
    CompoundTemplate_t, Reaction_type, CompoundProperty, ReactionProperty,
    MetabolicNetworkProperty>::Reaction_t*>> SubMetabolicNetwork<
    CompoundTemplate_t, Reaction_type, CompoundProperty, ReactionProperty,
    MetabolicNetworkProperty>::
consumingReactions(const CompoundIdType& id) const {
  throwExceptionOnNullptrParent();
  try {
    return directedSubHypergraph_->outHyperarcs(id);
  } catch (const std::out_of_range& oor) {
    std::string errorMsg = "IndexOutOfRangeException in SubMetabolicNetwork::";
    errorMsg += "consumingReactions: compound index = ";
    errorMsg += std::to_string(id);
    errorMsg += "; min = 0; max = ";
    errorMsg += std::to_string(directedSubHypergraph_->vertexContainerSize());
    errorMsg += '\n';
    throw std::out_of_range(errorMsg);
  } catch (const std::invalid_argument& oor) {
    std::string errorMessage = "Called SubMetabolicNetwork::";
    errorMessage += "consumingReactions, but the compound id ";
    errorMessage += std::to_string(id);
    errorMessage += " does not exists in the sub-metabolic network.\n";
    throw std::invalid_argument(errorMessage);
  }
}


// producing reactions of a compound
/// Check whether the compound with given Id is produced by a reaction
template <template <typename> class CompoundTemplate_t, typename Reaction_type,
    typename CompoundProperty, typename ReactionProperty,
    typename MetabolicNetworkProperty>
bool SubMetabolicNetwork<CompoundTemplate_t, Reaction_type, CompoundProperty,
    ReactionProperty, MetabolicNetworkProperty>::
isProduced(const CompoundIdType& id) const {
  throwExceptionOnNullptrParent();
  try {
    return directedSubHypergraph_->hasInHyperarcs(id);
  } catch (const std::out_of_range& oor) {
    std::string errorMsg = "IndexOutOfRangeException in SubMetabolicNetwork::";
    errorMsg += "isProduced: compound index = ";
    errorMsg += std::to_string(id);
    errorMsg += "; min = 0; max = ";
    errorMsg += std::to_string(directedSubHypergraph_->vertexContainerSize());
    errorMsg += '\n';
    throw std::out_of_range(errorMsg);
  } catch (const std::invalid_argument& oor) {
    std::string errorMessage = "Called SubMetabolicNetwork::";
    errorMessage += "isProduced, but the compound id ";
    errorMessage += std::to_string(id);
    errorMessage += " does not exists in the sub-metabolic network.\n";
    throw std::invalid_argument(errorMessage);
  }
}


/// Get number of producing reactions
template <template <typename> class CompoundTemplate_t, typename Reaction_type,
    typename CompoundProperty, typename ReactionProperty,
    typename MetabolicNetworkProperty>
size_t SubMetabolicNetwork<CompoundTemplate_t, Reaction_type, CompoundProperty,
    ReactionProperty, MetabolicNetworkProperty>::
nbProducingReactions(const CompoundIdType& id) const {
  throwExceptionOnNullptrParent();
  try {
    return directedSubHypergraph_->inDegree(id);
  } catch (const std::out_of_range& oor) {
    std::string errorMsg = "IndexOutOfRangeException in SubMetabolicNetwork::";
    errorMsg += "nbProducingReactions: compound index = ";
    errorMsg += std::to_string(id);
    errorMsg += "; min = 0; max = ";
    errorMsg += std::to_string(directedSubHypergraph_->vertexContainerSize());
    errorMsg += '\n';
    throw std::out_of_range(errorMsg);
  } catch (const std::invalid_argument& oor) {
    std::string errorMessage = "Called SubMetabolicNetwork::";
    errorMessage += "nbProducingReactions, but the compound id ";
    errorMessage += std::to_string(id);
    errorMessage += " does not exists in the sub-metabolic network.\n";
    throw std::invalid_argument(errorMessage);
  }
}


/// \brief Const iterator pointing to begin of the producing reactions of the
/// compound with given Id
template <template <typename> class CompoundTemplate_t, typename Reaction_type,
    typename CompoundProperty, typename ReactionProperty,
    typename MetabolicNetworkProperty>
typename SubMetabolicNetwork<CompoundTemplate_t, Reaction_type,
    CompoundProperty, ReactionProperty,
    MetabolicNetworkProperty>::reaction_iterator SubMetabolicNetwork<
    CompoundTemplate_t, Reaction_type, CompoundProperty, ReactionProperty,
    MetabolicNetworkProperty>::
producingReactionsBegin(const CompoundIdType& id) const {
  throwExceptionOnNullptrParent();
  try {
    return directedSubHypergraph_->inHyperarcsBegin(id);
  } catch (const std::out_of_range& oor) {
    std::string errorMsg = "IndexOutOfRangeException in SubMetabolicNetwork::";
    errorMsg += "producingReactionsBegin: compound index = ";
    errorMsg += std::to_string(id);
    errorMsg += "; min = 0; max = ";
    errorMsg += std::to_string(directedSubHypergraph_->vertexContainerSize());
    errorMsg += '\n';
    throw std::out_of_range(errorMsg);
  } catch (const std::invalid_argument& oor) {
    std::string errorMessage = "Called SubMetabolicNetwork::";
    errorMessage += "producingReactionsBegin, but the compound id ";
    errorMessage += std::to_string(id);
    errorMessage += " does not exists in the sub-metabolic network.\n";
    throw std::invalid_argument(errorMessage);
  }
}


/// \brief Const iterator pointing to end of the producing reactions of the
/// compound with given Id
template <template <typename> class CompoundTemplate_t, typename Reaction_type,
    typename CompoundProperty, typename ReactionProperty,
    typename MetabolicNetworkProperty>
typename SubMetabolicNetwork<CompoundTemplate_t, Reaction_type,
    CompoundProperty, ReactionProperty,
    MetabolicNetworkProperty>::reaction_iterator SubMetabolicNetwork<
    CompoundTemplate_t, Reaction_type, CompoundProperty, ReactionProperty,
    MetabolicNetworkProperty>::
producingReactionsEnd(const CompoundIdType& id) const {
  throwExceptionOnNullptrParent();
  try {
    return directedSubHypergraph_->inHyperarcsEnd(id);
  } catch (const std::out_of_range& oor) {
    std::string errorMsg = "IndexOutOfRangeException in SubMetabolicNetwork::";
    errorMsg += "producingReactionsEnd: compound index = ";
    errorMsg += std::to_string(id);
    errorMsg += "; min = 0; max = ";
    errorMsg += std::to_string(directedSubHypergraph_->vertexContainerSize());
    errorMsg += '\n';
    throw std::out_of_range(errorMsg);
  } catch (const std::invalid_argument& oor) {
    std::string errorMessage = "Called SubMetabolicNetwork::";
    errorMessage += "producingReactionsEnd, but the compound id ";
    errorMessage += std::to_string(id);
    errorMessage += " does not exists in the sub-metabolic network.\n";
    throw std::invalid_argument(errorMessage);
  }
}


/// \brief Return a shared_ptr to the container of producing reactions of the
/// compound with given Id
template <template <typename> class CompoundTemplate_t, typename Reaction_type,
    typename CompoundProperty, typename ReactionProperty,
    typename MetabolicNetworkProperty>
std::shared_ptr<const hglib::GraphElementContainer<typename SubMetabolicNetwork<
    CompoundTemplate_t, Reaction_type, CompoundProperty, ReactionProperty,
    MetabolicNetworkProperty>::Reaction_t*>> SubMetabolicNetwork<
    CompoundTemplate_t, Reaction_type, CompoundProperty, ReactionProperty,
    MetabolicNetworkProperty>::
producingReactions(const CompoundIdType& id) const {
  throwExceptionOnNullptrParent();
  try {
    return directedSubHypergraph_->inHyperarcs(id);
  } catch (const std::out_of_range& oor) {
    std::string errorMsg = "IndexOutOfRangeException in SubMetabolicNetwork::";
    errorMsg += "producingReactions: compound index = ";
    errorMsg += std::to_string(id);
    errorMsg += "; min = 0; max = ";
    errorMsg += std::to_string(directedSubHypergraph_->vertexContainerSize());
    errorMsg += '\n';
    throw std::out_of_range(errorMsg);
  } catch (const std::invalid_argument& oor) {
    std::string errorMessage = "Called SubMetabolicNetwork::";
    errorMessage += "producingReactions, but the compound id ";
    errorMessage += std::to_string(id);
    errorMessage += " does not exists in the sub-metabolic network.\n";
    throw std::invalid_argument(errorMessage);
  }
}


// Compound property getters
/// Get properties of the compound with the provided id.
template <template <typename> class CompoundTemplate_t, typename Reaction_type,
    typename CompoundProperty, typename ReactionProperty,
    typename MetabolicNetworkProperty>
const CompoundProperty* SubMetabolicNetwork<CompoundTemplate_t, Reaction_type,
    CompoundProperty, ReactionProperty, MetabolicNetworkProperty>::
getCompoundProperties(const CompoundIdType& id) const {
  throwExceptionOnNullptrParent();
  try {
    return directedSubHypergraph_->getVertexProperties(id);
  } catch (const std::out_of_range& oor) {
    std::string errorMsg = "IndexOutOfRangeException in SubMetabolicNetwork::";
    errorMsg += "getCompoundProperties: compound index = ";
    errorMsg += std::to_string(id);
    errorMsg += "; min = 0; max = ";
    errorMsg += std::to_string(directedSubHypergraph_->vertexContainerSize());
    errorMsg += '\n';
    throw std::out_of_range(errorMsg);
  } catch (const std::invalid_argument& oor) {
    std::string errorMessage = "Called SubMetabolicNetwork::";
    errorMessage += "getCompoundProperties, but the compound id ";
    errorMessage += std::to_string(id);
    errorMessage += " does not exists in the sub-metabolic network.\n";
    throw std::invalid_argument(errorMessage);
  }
}


/// Get properties of the compound with the provided id.
template <template <typename> class CompoundTemplate_t, typename Reaction_type,
    typename CompoundProperty, typename ReactionProperty,
    typename MetabolicNetworkProperty>
CompoundProperty* SubMetabolicNetwork<CompoundTemplate_t, Reaction_type,
    CompoundProperty, ReactionProperty, MetabolicNetworkProperty>::
getCompoundProperties_(const CompoundIdType& id) const {
  throwExceptionOnNullptrParent();
  try {
    return directedSubHypergraph_->getVertexProperties_(id);
  } catch (const std::out_of_range& oor) {
    std::string errorMsg = "IndexOutOfRangeException in SubMetabolicNetwork::";
    errorMsg += "getCompoundProperties_: compound index = ";
    errorMsg += std::to_string(id);
    errorMsg += "; min = 0; max = ";
    errorMsg += std::to_string(directedSubHypergraph_->vertexContainerSize());
    errorMsg += '\n';
    throw std::out_of_range(errorMsg);
  } catch (const std::invalid_argument& oor) {
    std::string errorMessage = "Called SubMetabolicNetwork::";
    errorMessage += "getCompoundProperties_, but the compound id ";
    errorMessage += std::to_string(id);
    errorMessage += " does not exists in the sub-metabolic network.\n";
    throw std::invalid_argument(errorMessage);
  }
}


// related to reactions
/**
 * \brief Add a reaction with the provided substrates, products, and
 * additional parameters specific to the reaction type to the
 * network
 */
template <template <typename> class CompoundTemplate_t, typename Reaction_type,
    typename CompoundProperty, typename ReactionProperty,
    typename MetabolicNetworkProperty>
const typename SubMetabolicNetwork<CompoundTemplate_t, Reaction_type,
    CompoundProperty, ReactionProperty,
    MetabolicNetworkProperty>::Reaction_t* SubMetabolicNetwork<
    CompoundTemplate_t, Reaction_type, CompoundProperty, ReactionProperty,
    MetabolicNetworkProperty>::
addReaction(decltype(hglib::NAME),
            const SubstrateAndProductNames& substrateProductNames,
            const typename Reaction_t::SpecificAttributes& attributes) {
  throwExceptionOnNullptrParent();

  // * Check that the Sbml Id follows the Sbml specifications
  // * Check that the number of substrates corresponds to the number of
  // substrate stoichiometry entries
  // * Check that the number of products corresponds to the number of
  // product stoichiometry entries
  // * Check that the lower bound is greater than or equal to zero if the
  // reaction is not reversible
  // * If the reaction is irreversible, add the reaction and return pointer to
  // it
  // * If the reaction is reversible, add a forward and backward reaction and
  // return a pointer to the forward reaction

  // Check that the Sbml Id follows the Sbml specifications
  std::regex regEx("[a-zA-Z_][a-zA-Z0-9_]*");
  std::string sbmlId = attributes.sbmlId_;
  if (not std::regex_match(sbmlId.begin(), sbmlId.end(), regEx)) {
    std::string errorMessage = "The reaction Sbml Id ";
    errorMessage += sbmlId;
    errorMessage += " is not valid. The Sbml Id must match the following"
        " regular expression: [a-zA-Z_][a-zA-Z0-9_]*";
    throw std::invalid_argument(errorMessage);
  }

  // Check that there is no reaction in the network with the same Sbml Id
  for (const auto& reaction :
        directedSubHypergraph_->getRootHypergraph()->hyperarcs()) {
    if (reaction->sbmlId().compare(sbmlId) == 0) {
      std::string errorMessage = "The reaction Sbml Id ";
      errorMessage += sbmlId;
      errorMessage += " already exists in the network.\n";
      throw std::invalid_argument(errorMessage);
    }
  }

  // Check that the number of substrates corresponds to the number of
  // substrate stoichiometry entries
  auto nbStoichioEntries = attributes.substrateStoichiometries().size();
  if (substrateProductNames.first.size() != nbStoichioEntries) {
    std::string errorMessage = "Number of substrates (";
    errorMessage += std::to_string(substrateProductNames.first.size());
    errorMessage += ") and number of substrate stoichiometry entries (";
    errorMessage += std::to_string(nbStoichioEntries);
    errorMessage += ") must be equal.\n";
    throw std::invalid_argument(errorMessage);
  }

  // Check that the number of products corresponds to the number of
  // product stoichiometry entries
  nbStoichioEntries = attributes.productStoichiometries().size();
  if (substrateProductNames.second.size() != nbStoichioEntries) {
    std::string errorMessage = "Number of products (";
    errorMessage += std::to_string(substrateProductNames.second.size());
    errorMessage += ") and number of product stoichiometry entries (";
    errorMessage += std::to_string(nbStoichioEntries);
    errorMessage += ") must be equal.\n";
    throw std::invalid_argument(errorMessage);
  }

  // Check that the lower bound is greater than or equal to zero if the
  // reaction is not reversible
  if (attributes.reversible_ == false and attributes.lowerBound_ < 0) {
    std::string errorMessage = "The lower flux bound of an irreversible"
        " reaction must be greater than or equal to zero: ";
    errorMessage += attributes.sbmlId_;
    errorMessage += " -> " + std::to_string(attributes.lowerBound_) + "\n";
    throw std::invalid_argument(errorMessage);
  }

  // Create reaction or forward and backward reaction if reversible
  try {
    // If the reaction is irreversible, add the reaction
    if (attributes.reversible_ == false) {
      return directedSubHypergraph_->addHyperarc(
          hglib::NAME, substrateProductNames, attributes);
    } else {
      const Reaction_t* forwardReaction;
      // As we split reversible reactions, we set the lower bound to zero if a
      // negative value is provided.
      if (attributes.lowerBound_ < 0) {
        // A copy is needed as the attributes are passed by const ref
        auto attributesCopy(attributes);
        attributesCopy.lowerBound_ = 0;
        forwardReaction = directedSubHypergraph_->addHyperarc(
            hglib::NAME, substrateProductNames, attributesCopy);
      } else {
        forwardReaction = directedSubHypergraph_->addHyperarc(
            hglib::NAME, substrateProductNames, attributes);
      }

      // Get arguments to create backward reaction (contain substrate/product
      // names and reaction type specific attributes)
      auto argsForBackwardReaction =
          forwardReaction->argumentsToCreateReverseReaction();
      // Recover reaction type specific attributes of the backward reaction
      auto backwardReactionSpecificAttributes =
          argsForBackwardReaction.specificAttributes();
      // set lower and upper bound of the backward reaction
      backwardReactionSpecificAttributes.lowerBound_ = 0;
      if (attributes.lowerBound_ >= 0) {
        backwardReactionSpecificAttributes.upperBound_ = 0;
      } else {
        backwardReactionSpecificAttributes.upperBound_ =
            -attributes.lowerBound_;
      }
      // Set arguments to create the backward reaction
      argsForBackwardReaction.setSpecificArguments(
          backwardReactionSpecificAttributes);
      // create the backward reaction
      const auto& backwardReaction =
          directedSubHypergraph_->addHyperarc(
              argsForBackwardReaction.tailAndHeadIds(),
              argsForBackwardReaction.specificAttributes());
      // Add each other as reverse reaction
      auto nonConstForwardReaction = const_cast<Reaction_type *>(
          forwardReaction);
      auto nonConstBackwardReaction = const_cast<Reaction_type *>(
          backwardReaction);
      nonConstForwardReaction->setReversibleReaction(nonConstBackwardReaction);
      nonConstBackwardReaction->setReversibleReaction(nonConstForwardReaction);

      // return pointer to forward reaction
      return directedSubHypergraph_->hyperarcById(forwardReaction->id());
    }
  } catch (const std::invalid_argument& ia) {
    // An exception is only thrown if a substrate or product is not declared in
    // the network. In this case no reaction is created. Thus, also in the case
    // of a reversible reaction, it is not possible to create the forward but
    // not the backward reaction. No pointer to a reaction has to be deleted.
    throw ia;
  }
}


/// Get pointer to reaction
template <template <typename> class CompoundTemplate_t, typename Reaction_type,
    typename CompoundProperty, typename ReactionProperty,
    typename MetabolicNetworkProperty>
const typename SubMetabolicNetwork<CompoundTemplate_t, Reaction_type,
    CompoundProperty, ReactionProperty,
    MetabolicNetworkProperty>::Reaction_t* SubMetabolicNetwork<
    CompoundTemplate_t, Reaction_type, CompoundProperty, ReactionProperty,
    MetabolicNetworkProperty>::
reactionById(const ReactionIdType& id) const {
  throwExceptionOnNullptrParent();
  return directedSubHypergraph_->hyperarcById(id);
}


/// Get pointer to reaction
template <template <typename> class CompoundTemplate_t, typename Reaction_type,
    typename CompoundProperty, typename ReactionProperty,
    typename MetabolicNetworkProperty>
const typename SubMetabolicNetwork<CompoundTemplate_t, Reaction_type,
    CompoundProperty, ReactionProperty,
    MetabolicNetworkProperty>::Reaction_t* SubMetabolicNetwork<
    CompoundTemplate_t, Reaction_type, CompoundProperty, ReactionProperty,
    MetabolicNetworkProperty>::
reactionBySbmlId(const std::string& sbmlId) const {
  throwExceptionOnNullptrParent();
  for (const auto& reaction : directedSubHypergraph_->hyperarcs()) {
    if (reaction->sbmlId().compare(sbmlId) == 0) {
      return reaction;
    }
  }
  return nullptr;
}


/// Get number of reactions
template <template <typename> class CompoundTemplate_t, typename Reaction_type,
    typename CompoundProperty, typename ReactionProperty,
    typename MetabolicNetworkProperty>
size_t SubMetabolicNetwork<CompoundTemplate_t, Reaction_type,
    CompoundProperty, ReactionProperty, MetabolicNetworkProperty>::
nbReactions() const {
  throwExceptionOnNullptrParent();
  // number of all reactions (reversible reactions are counted twice)
  size_t nbReac(directedSubHypergraph_->nbHyperarcs());
  // Get number of reversible reactions
  size_t nbRevReac(nbReversibleReactions());
  return (nbReac - nbRevReac);
}


/// Get number of reversible reactions
template <template <typename> class CompoundTemplate_t, typename Reaction_type,
    typename CompoundProperty, typename ReactionProperty,
    typename MetabolicNetworkProperty>
size_t SubMetabolicNetwork<CompoundTemplate_t, Reaction_type,
    CompoundProperty, ReactionProperty, MetabolicNetworkProperty>::
nbReversibleReactions() const {
  throwExceptionOnNullptrParent();
  // count reversible reactions only if both senses are in the subnetwork
  size_t nbRevReac(0);
  for (const auto& reac : directedSubHypergraph_->hyperarcs()) {
    if (reac->isReversible() and
        directedSubHypergraph_->hyperarcById(
            reac->reversibleReaction()->id()) != nullptr) {
      ++nbRevReac;
    }
  }
  return (nbRevReac / 2);
}


/// Is the reaction reversible?
template <template <typename> class CompoundTemplate_t, typename Reaction_type,
    typename CompoundProperty, typename ReactionProperty,
    typename MetabolicNetworkProperty>
bool SubMetabolicNetwork<CompoundTemplate_t, Reaction_type,
    CompoundProperty, ReactionProperty, MetabolicNetworkProperty>::
isReversible(const ReactionIdType& id) const {
  throwExceptionOnNullptrParent();
  // The reaction is reversible if
  // (1) it is reversible in the root metabolic network, AND
  // (2) when the reverse reaction is also part of the sub-network
  try {
    const auto& reaction = directedSubHypergraph_->hyperarcById(id);
    if (reaction == nullptr) {
      std::string errorMessage = "Error in SubMetabolicNetwork::";
      errorMessage += "isReversible: The reaction with";
      errorMessage += " given Id: ";
      errorMessage += std::to_string(id);
      errorMessage += " is not in the metabolic network.\n";
      throw std::invalid_argument(errorMessage);
    }
    // Reversible in the root metabolic network?
    if (reaction->isReversible()) {
      // Get reverse reaction
      const auto& revReaction = reaction->reversibleReaction();
      // Check if reverse reaction is in the sub metabolic network
      return directedSubHypergraph_->hyperarcById(revReaction->id()) != nullptr;
    } else {
      return false;
    }
  } catch (const std::out_of_range& oor) {
    throw oor;
  } catch (const std::invalid_argument& ia) {
    throw ia;
  }
}


/// Get reverse reaction
template <template <typename> class CompoundTemplate_t, typename Reaction_type,
    typename CompoundProperty, typename ReactionProperty,
    typename MetabolicNetworkProperty>
const typename SubMetabolicNetwork<CompoundTemplate_t, Reaction_type,
    CompoundProperty, ReactionProperty,
    MetabolicNetworkProperty>::Reaction_t* SubMetabolicNetwork<
    CompoundTemplate_t, Reaction_type, CompoundProperty, ReactionProperty,
    MetabolicNetworkProperty>::
reversibleReaction(const ReactionIdType& id) const {
  throwExceptionOnNullptrParent();
  try {
    const auto& reaction = directedSubHypergraph_->hyperarcById(id);
    if (reaction == nullptr) {
      std::string errorMessage = "Error in SubMetabolicNetwork::";
      errorMessage += "reversibleReaction: The reaction with";
      errorMessage += " given Id: ";
      errorMessage += std::to_string(id);
      errorMessage += " is not in the metabolic network.\n";
      throw std::invalid_argument(errorMessage);
    }
    // Get reverse reaction
    const auto& revReaction = reaction->reversibleReaction();
    // Check if reverse reaction is also in the sub metabolic network
    if (revReaction != nullptr) {
      // ... it is not
      if (directedSubHypergraph_->hyperarcById(revReaction->id()) == nullptr) {
        return nullptr;
      }
    }
    // return a nullptr if the reaction is not reversible or the pointer to the
    // reverse reaction
    return reaction->reversibleReaction();
  } catch (const std::out_of_range& oor) {
    throw oor;
  } catch (const std::invalid_argument& ia) {
    throw ia;
  }
}


/// Const reaction iterator pointing to begin of the reactions
template <template <typename> class CompoundTemplate_t, typename Reaction_type,
    typename CompoundProperty, typename ReactionProperty,
    typename MetabolicNetworkProperty>
typename SubMetabolicNetwork<CompoundTemplate_t, Reaction_type,
    CompoundProperty, ReactionProperty,
    MetabolicNetworkProperty>::reaction_iterator SubMetabolicNetwork<
    CompoundTemplate_t, Reaction_type, CompoundProperty, ReactionProperty,
    MetabolicNetworkProperty>::
reactionsBegin() const {
  throwExceptionOnNullptrParent();
  return directedSubHypergraph_->hyperarcsBegin();
}


/// Const reaction iterator pointing to end of the reactions
template <template <typename> class CompoundTemplate_t, typename Reaction_type,
    typename CompoundProperty, typename ReactionProperty,
    typename MetabolicNetworkProperty>
typename SubMetabolicNetwork<CompoundTemplate_t, Reaction_type,
    CompoundProperty, ReactionProperty,
    MetabolicNetworkProperty>::reaction_iterator SubMetabolicNetwork<
    CompoundTemplate_t, Reaction_type, CompoundProperty, ReactionProperty,
    MetabolicNetworkProperty>::
reactionsEnd() const {
  throwExceptionOnNullptrParent();
  return directedSubHypergraph_->hyperarcsEnd();
}


/// Return a reference to the container of reactions.
template <template <typename> class CompoundTemplate_t, typename Reaction_type,
    typename CompoundProperty, typename ReactionProperty,
    typename MetabolicNetworkProperty>
const typename SubMetabolicNetwork<CompoundTemplate_t, Reaction_type,
    CompoundProperty, ReactionProperty,
    MetabolicNetworkProperty>::ReactionContainer& SubMetabolicNetwork<
    CompoundTemplate_t, Reaction_type, CompoundProperty, ReactionProperty,
    MetabolicNetworkProperty>::
reactions() const {
  throwExceptionOnNullptrParent();
  return directedSubHypergraph_->hyperarcs();
}


/// Add a default irreversible reaction
template <template <typename> class CompoundTemplate_t, typename Reaction_type,
    typename CompoundProperty, typename ReactionProperty,
    typename MetabolicNetworkProperty>
const typename SubMetabolicNetwork<
    CompoundTemplate_t, Reaction_type, CompoundProperty, ReactionProperty,
    MetabolicNetworkProperty>::Reaction_t* SubMetabolicNetwork<
    CompoundTemplate_t, Reaction_type, CompoundProperty, ReactionProperty,
    MetabolicNetworkProperty>::
addDefaultIrreversibleReaction(const std::string& reactionName,
                               const std::vector<std::string>& substrateNames,
                               const std::vector<std::string>& productNames) {
  throwExceptionOnNullptrParent();

  // Check that the Sbml Id (reaction name) follows the Sbml specifications
  std::regex regEx("[a-zA-Z_][a-zA-Z0-9_]*");
  if (not std::regex_match(reactionName.begin(), reactionName.end(), regEx)) {
    std::string errorMessage = "The reaction Sbml Id ";
    errorMessage += reactionName;
    errorMessage += " is not valid. The Sbml Id must match the following"
        " regular expression: [a-zA-Z_][a-zA-Z0-9_]*";
    throw std::invalid_argument(errorMessage);
  }

  // Check that there is no reaction in the network with the same Sbml Id
  for (const auto& reaction :
      directedSubHypergraph_->getRootHypergraph()->hyperarcs()) {
    if (reaction->sbmlId().compare(reactionName) == 0) {
      std::string errorMessage = "The reaction Sbml Id ";
      errorMessage += reactionName;
      errorMessage += " already exists in the network.\n";
      throw std::invalid_argument(errorMessage);
    }
  }

  // Get default arguments and add the reaction
  try {
    auto args = Reaction_t::argumentsToCreateDefaultIrreversibleReaction(
        reactionName, substrateNames, productNames);
    return directedSubHypergraph_->addHyperarc(
          hglib::NAME, args.substrateAndProductNames(),
          args.specificAttributes());
  } catch (const std::invalid_argument& ia) {
    // An exception is only thrown if a substrate or product is not declared in
    // the network. In this case no reaction is created.
    throw ia;
  }
}


/// Remove a reaction with the given id from the network.
template <template <typename> class CompoundTemplate_t, typename Reaction_type,
    typename CompoundProperty, typename ReactionProperty,
    typename MetabolicNetworkProperty>
void SubMetabolicNetwork<CompoundTemplate_t, Reaction_type,
    CompoundProperty, ReactionProperty, MetabolicNetworkProperty>::
removeReaction(const ReactionIdType& id,
               bool removeReverseReaction) {
  throwExceptionOnNullptrParent();
  // * Get reaction with provided Id
  // * If the reaction is reversible and
  //      * removeReverseReaction == true, and
  //      * the reverse sense reaction is in the sub-network, then the reverse
  //        reaction is removed from the network
  // * Remove the reaction with the given Id from the network
  try {
    // Get reaction with provided Id
    const auto& reaction = directedSubHypergraph_->hyperarcById(id);
    if (reaction == nullptr) {
      std::string errorMessage("Called SubMetabolicNetwork::removeReaction"
                                   " with "
                                   "the reaction id ");
      errorMessage += std::to_string(id);
      errorMessage += " which does not exists in the sub metabolic network.\n";
      throw std::invalid_argument(errorMessage);
    }

    // If the reaction is reversible and
    if (reaction->isReversible()) {
      auto reversibleReaction = const_cast<Reaction_t*>(
          reaction->reversibleReaction());
      // * removeReverseReaction == true, and
      // * the reverse sense reaction is in the sub-network, then the reverse
      // reaction is removed from the network
      if (removeReverseReaction and
          directedSubHypergraph_->hyperarcById(reversibleReaction->id()) !=
              nullptr) {
        directedSubHypergraph_->removeHyperarc(reversibleReaction->id());
      }
    }
    // Remove the reaction with the given Id from the network
    directedSubHypergraph_->removeHyperarc(id);
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in SubMetabolicNetwork::"
        "removeReaction: index = " << id << "; min = 0; max = "
              << (directedSubHypergraph_->nbHyperarcs() - 1) << '\n';
    throw;
  } catch (const std::invalid_argument& invalid_argument) {
    throw;
  }
}


// substrates of a reaction
/// Check whether the reaction with given Id has substrates
template <template <typename> class CompoundTemplate_t, typename Reaction_type,
    typename CompoundProperty, typename ReactionProperty,
    typename MetabolicNetworkProperty>
bool SubMetabolicNetwork<CompoundTemplate_t, Reaction_type,
    CompoundProperty, ReactionProperty, MetabolicNetworkProperty>::
hasSubstrates(const ReactionIdType& id) const {
  throwExceptionOnNullptrParent();
  try {
    return directedSubHypergraph_->hasTailVertices(id);
  } catch (const std::out_of_range& oor) {
    std::string errorMsg = "IndexOutOfRangeException in SubMetabolicNetwork::";
    errorMsg += "hasSubstrates: compound index = ";
    errorMsg += std::to_string(id);
    errorMsg += "; min = 0; max = ";
    errorMsg += std::to_string(directedSubHypergraph_->vertexContainerSize());
    errorMsg += '\n';
    throw std::out_of_range(errorMsg);
  } catch (const std::invalid_argument& oor) {
    std::string errorMessage = "Called SubMetabolicNetwork::";
    errorMessage += "hasSubstrates, but the compound id ";
    errorMessage += std::to_string(id);
    errorMessage += " does not exists in the sub-metabolic network.\n";
    throw std::invalid_argument(errorMessage);
  }
}


/// Get number of substrates of the reaction with given Id
template <template <typename> class CompoundTemplate_t, typename Reaction_type,
    typename CompoundProperty, typename ReactionProperty,
    typename MetabolicNetworkProperty>
size_t SubMetabolicNetwork<CompoundTemplate_t, Reaction_type,
    CompoundProperty, ReactionProperty, MetabolicNetworkProperty>::
nbSubstrates(const ReactionIdType& id) const {
  throwExceptionOnNullptrParent();
  try {
    return directedSubHypergraph_->nbTailVertices(id);
  } catch (const std::out_of_range& oor) {
    std::string errorMsg = "IndexOutOfRangeException in SubMetabolicNetwork::";
    errorMsg += "nbSubstrates: compound index = ";
    errorMsg += std::to_string(id);
    errorMsg += "; min = 0; max = ";
    errorMsg += std::to_string(directedSubHypergraph_->vertexContainerSize());
    errorMsg += '\n';
    throw std::out_of_range(errorMsg);
  } catch (const std::invalid_argument& oor) {
    std::string errorMessage = "Called SubMetabolicNetwork::";
    errorMessage += "nbSubstrates, but the compound id ";
    errorMessage += std::to_string(id);
    errorMessage += " does not exists in the sub-metabolic network.\n";
    throw std::invalid_argument(errorMessage);
  }
}


/// \brief Const compound iterator pointing to the begin of the substrates
/// of the reaction with the given id
template <template <typename> class CompoundTemplate_t, typename Reaction_type,
    typename CompoundProperty, typename ReactionProperty,
    typename MetabolicNetworkProperty>
typename SubMetabolicNetwork<CompoundTemplate_t, Reaction_type,
    CompoundProperty, ReactionProperty,
    MetabolicNetworkProperty>::compound_iterator SubMetabolicNetwork<
    CompoundTemplate_t, Reaction_type, CompoundProperty, ReactionProperty,
    MetabolicNetworkProperty>::
substratesBegin(const ReactionIdType& id) const {
  throwExceptionOnNullptrParent();
  try {
    return directedSubHypergraph_->tailsBegin(id);
  } catch (const std::out_of_range& oor) {
    std::string errorMsg = "IndexOutOfRangeException in SubMetabolicNetwork::";
    errorMsg += "substratesBegin: compound index = ";
    errorMsg += std::to_string(id);
    errorMsg += "; min = 0; max = ";
    errorMsg += std::to_string(directedSubHypergraph_->vertexContainerSize());
    errorMsg += '\n';
    throw std::out_of_range(errorMsg);
  } catch (const std::invalid_argument& oor) {
    std::string errorMessage = "Called SubMetabolicNetwork::";
    errorMessage += "substratesBegin, but the compound id ";
    errorMessage += std::to_string(id);
    errorMessage += " does not exists in the sub-metabolic network.\n";
    throw std::invalid_argument(errorMessage);
  }
}


/// \briefConst compound iterator pointing to the end of the substrates
/// of the reaction with the given id
template <template <typename> class CompoundTemplate_t, typename Reaction_type,
    typename CompoundProperty, typename ReactionProperty,
    typename MetabolicNetworkProperty>
typename SubMetabolicNetwork<CompoundTemplate_t, Reaction_type,
    CompoundProperty, ReactionProperty,
    MetabolicNetworkProperty>::compound_iterator SubMetabolicNetwork<
    CompoundTemplate_t, Reaction_type, CompoundProperty, ReactionProperty,
    MetabolicNetworkProperty>::
substratesEnd(const ReactionIdType& id) const {
  throwExceptionOnNullptrParent();
  try {
    return directedSubHypergraph_->tailsEnd(id);
  } catch (const std::out_of_range& oor) {
    std::string errorMsg = "IndexOutOfRangeException in SubMetabolicNetwork::";
    errorMsg += "substratesEnd: compound index = ";
    errorMsg += std::to_string(id);
    errorMsg += "; min = 0; max = ";
    errorMsg += std::to_string(directedSubHypergraph_->vertexContainerSize());
    errorMsg += '\n';
    throw std::out_of_range(errorMsg);
  } catch (const std::invalid_argument& oor) {
    std::string errorMessage = "Called SubMetabolicNetwork::";
    errorMessage += "substratesEnd, but the compound id ";
    errorMessage += std::to_string(id);
    errorMessage += " does not exists in the sub-metabolic network.\n";
    throw std::invalid_argument(errorMessage);
  }
}


/// Get shared_ptr of container of substrates of given reaction id
template <template <typename> class CompoundTemplate_t, typename Reaction_type,
    typename CompoundProperty, typename ReactionProperty,
    typename MetabolicNetworkProperty>
std::shared_ptr<const hglib::GraphElementContainer<typename SubMetabolicNetwork<
    CompoundTemplate_t, Reaction_type, CompoundProperty, ReactionProperty,
    MetabolicNetworkProperty>::Compound_t*>> SubMetabolicNetwork<
    CompoundTemplate_t, Reaction_type, CompoundProperty, ReactionProperty,
    MetabolicNetworkProperty>::
substrates(const ReactionIdType& id) const {
  throwExceptionOnNullptrParent();
  try {
    return directedSubHypergraph_->tails(id);
  } catch (const std::out_of_range& oor) {
    std::string errorMsg = "IndexOutOfRangeException in SubMetabolicNetwork::";
    errorMsg += "substrates: compound index = ";
    errorMsg += std::to_string(id);
    errorMsg += "; min = 0; max = ";
    errorMsg += std::to_string(directedSubHypergraph_->vertexContainerSize());
    errorMsg += '\n';
    throw std::out_of_range(errorMsg);
  } catch (const std::invalid_argument& oor) {
    std::string errorMessage = "Called SubMetabolicNetwork::";
    errorMessage += "substrates, but the compound id ";
    errorMessage += std::to_string(id);
    errorMessage += " does not exists in the sub-metabolic network.\n";
    throw std::invalid_argument(errorMessage);
  }
}


/// Check if given compound is a substrate of given reaction.
template <template <typename> class CompoundTemplate_t, typename Reaction_type,
    typename CompoundProperty, typename ReactionProperty,
    typename MetabolicNetworkProperty>
bool SubMetabolicNetwork<CompoundTemplate_t, Reaction_type,
    CompoundProperty, ReactionProperty, MetabolicNetworkProperty>::
isSubstrateOfReaction(const CompoundIdType& compoundId,
                      const ReactionIdType& reactionId) const {
  throwExceptionOnNullptrParent();
  try {
    return directedSubHypergraph_->isTailVertexOfHyperarc(compoundId,
                                                          reactionId);
  } catch (const std::out_of_range& oor) {
    std::string errorMsg = "IndexOutOfRangeException in SubMetabolicNetwork::";
    errorMsg += "isSubstrateOfReaction: compound index = ";
    errorMsg += std::to_string(compoundId);
    errorMsg += "; min = 0; max = ";
    errorMsg += std::to_string(directedSubHypergraph_->vertexContainerSize());
    errorMsg += "; reaction id = ";
    errorMsg += std::to_string(reactionId);
    errorMsg += "; min = 0; max = ";
    errorMsg += std::to_string(directedSubHypergraph_->hyperarcContainerSize());
    errorMsg += '\n';
    throw std::out_of_range(errorMsg);
  } catch (const std::invalid_argument& oor) {
    std::string errorMessage = "Called SubMetabolicNetwork::";
    errorMessage += "isSubstrateOfReaction, but either the compound id ";
    errorMessage += std::to_string(compoundId);
    errorMessage += " or the reaction id ";
    errorMessage += std::to_string(reactionId);
    errorMessage += " does not exists in the sub-metabolic network.\n";
    throw std::invalid_argument(errorMessage);
  }
}


/// Get substrate stoichiometry of given compound in given reaction
template <template <typename> class CompoundTemplate_t, typename Reaction_type,
    typename CompoundProperty, typename ReactionProperty,
    typename MetabolicNetworkProperty>
double SubMetabolicNetwork<CompoundTemplate_t, Reaction_type,
    CompoundProperty, ReactionProperty, MetabolicNetworkProperty>::
substrateStoichiometry(const CompoundIdType& compoundId,
                       const ReactionIdType& reactionId) const {
  throwExceptionOnNullptrParent();
  // * Check first if the compound is a substrate of given reaction in this sub-
  // metabolic network
  // * Retrieve then the given compound and reaction
  // * Call the function to retrieve the substrate stoichiometry
  try {
    if (isSubstrateOfReaction(compoundId, reactionId)) {
      const auto& compound = directedSubHypergraph_->vertexById(compoundId);
      if (compound == nullptr) {
        std::string errorMessage = "Error in SubMetabolicNetwork::";
        errorMessage += "substrateStoichiometry: The compound with";
        errorMessage += " given Id: ";
        errorMessage += std::to_string(compoundId);
        errorMessage += " is not in the sub metabolic network.\n";
        throw std::invalid_argument(errorMessage);
      }
      const auto& reaction = directedSubHypergraph_->hyperarcById(reactionId);
      if (reaction == nullptr) {
        std::string errorMessage = "Error in SubMetabolicNetwork::";
        errorMessage += "substrateStoichiometry: The reaction with";
        errorMessage += " given Id: ";
        errorMessage += std::to_string(reactionId);
        errorMessage += " is not in the sub metabolic network.\n";
        throw std::invalid_argument(errorMessage);
      }
      // Return stoichiometry
      return reaction->substrateStoichiometry(compound);
    } else {
      std::string errorMessage = "Error in SubMetabolicNetwork::";
      errorMessage += "substrateStoichiometry: The compound with";
      errorMessage += " given Id: ";
      errorMessage += std::to_string(compoundId);
      errorMessage += " is not a substrate of the reaction with given Id: ";
      errorMessage += std::to_string(reactionId);
      errorMessage += " in this sub network.\n";
      throw std::invalid_argument(errorMessage);
    }
  } catch (const std::out_of_range& oor) {
    throw oor;
  } catch (const std::invalid_argument& ia) {
    throw ia;
  }
}


// products of a reaction
/// Check whether the reaction with given Id has products
template <template <typename> class CompoundTemplate_t, typename Reaction_type,
    typename CompoundProperty, typename ReactionProperty,
    typename MetabolicNetworkProperty>
bool SubMetabolicNetwork<CompoundTemplate_t, Reaction_type,
    CompoundProperty, ReactionProperty, MetabolicNetworkProperty>::
hasProducts(const ReactionIdType& id) const {
  throwExceptionOnNullptrParent();
  try {
    return directedSubHypergraph_->hasHeadVertices(id);
  } catch (const std::out_of_range& oor) {
    std::string errorMsg = "IndexOutOfRangeException in SubMetabolicNetwork::";
    errorMsg += "hasProducts: compound index = ";
    errorMsg += std::to_string(id);
    errorMsg += "; min = 0; max = ";
    errorMsg += std::to_string(directedSubHypergraph_->vertexContainerSize());
    errorMsg += '\n';
    throw std::out_of_range(errorMsg);
  } catch (const std::invalid_argument& oor) {
    std::string errorMessage = "Called SubMetabolicNetwork::";
    errorMessage += "hasProducts, but the compound id ";
    errorMessage += std::to_string(id);
    errorMessage += " does not exists in the sub-metabolic network.\n";
    throw std::invalid_argument(errorMessage);
  }
}


/// Get number of products of the reaction with given Id
template <template <typename> class CompoundTemplate_t, typename Reaction_type,
    typename CompoundProperty, typename ReactionProperty,
    typename MetabolicNetworkProperty>
size_t SubMetabolicNetwork<CompoundTemplate_t, Reaction_type,
    CompoundProperty, ReactionProperty, MetabolicNetworkProperty>::
nbProducts(const ReactionIdType& id) const {
  throwExceptionOnNullptrParent();
  try {
    return directedSubHypergraph_->nbHeadVertices(id);
  } catch (const std::out_of_range& oor) {
    std::string errorMsg = "IndexOutOfRangeException in SubMetabolicNetwork::";
    errorMsg += "nbProducts: compound index = ";
    errorMsg += std::to_string(id);
    errorMsg += "; min = 0; max = ";
    errorMsg += std::to_string(directedSubHypergraph_->vertexContainerSize());
    errorMsg += '\n';
    throw std::out_of_range(errorMsg);
  } catch (const std::invalid_argument& oor) {
    std::string errorMessage = "Called SubMetabolicNetwork::";
    errorMessage += "nbProducts, but the compound id ";
    errorMessage += std::to_string(id);
    errorMessage += " does not exists in the sub-metabolic network.\n";
    throw std::invalid_argument(errorMessage);
  }
}


/// \brief Const compound iterator pointing to the begin of the products
/// of the reaction with the given id
template <template <typename> class CompoundTemplate_t, typename Reaction_type,
    typename CompoundProperty, typename ReactionProperty,
    typename MetabolicNetworkProperty>
typename SubMetabolicNetwork<CompoundTemplate_t, Reaction_type,
    CompoundProperty, ReactionProperty,
    MetabolicNetworkProperty>::compound_iterator SubMetabolicNetwork<
    CompoundTemplate_t, Reaction_type, CompoundProperty, ReactionProperty,
    MetabolicNetworkProperty>::
productsBegin(const ReactionIdType& id) const {
  throwExceptionOnNullptrParent();
  try {
    return directedSubHypergraph_->headsBegin(id);
  } catch (const std::out_of_range& oor) {
    std::string errorMsg = "IndexOutOfRangeException in SubMetabolicNetwork::";
    errorMsg += "productsBegin: compound index = ";
    errorMsg += std::to_string(id);
    errorMsg += "; min = 0; max = ";
    errorMsg += std::to_string(directedSubHypergraph_->vertexContainerSize());
    errorMsg += '\n';
    throw std::out_of_range(errorMsg);
  } catch (const std::invalid_argument& oor) {
    std::string errorMessage = "Called SubMetabolicNetwork::";
    errorMessage += "productsBegin, but the compound id ";
    errorMessage += std::to_string(id);
    errorMessage += " does not exists in the sub-metabolic network.\n";
    throw std::invalid_argument(errorMessage);
  }
}


/// \briefConst compound iterator pointing to the end of the products
/// of the reaction with the given id
template <template <typename> class CompoundTemplate_t, typename Reaction_type,
    typename CompoundProperty, typename ReactionProperty,
    typename MetabolicNetworkProperty>
typename SubMetabolicNetwork<CompoundTemplate_t, Reaction_type,
    CompoundProperty, ReactionProperty,
    MetabolicNetworkProperty>::compound_iterator SubMetabolicNetwork<
    CompoundTemplate_t, Reaction_type, CompoundProperty, ReactionProperty,
    MetabolicNetworkProperty>::
productsEnd(const ReactionIdType& id) const {
  throwExceptionOnNullptrParent();
  try {
    return directedSubHypergraph_->headsEnd(id);
  } catch (const std::out_of_range& oor) {
    std::string errorMsg = "IndexOutOfRangeException in SubMetabolicNetwork::";
    errorMsg += "productsEnd: compound index = ";
    errorMsg += std::to_string(id);
    errorMsg += "; min = 0; max = ";
    errorMsg += std::to_string(directedSubHypergraph_->vertexContainerSize());
    errorMsg += '\n';
    throw std::out_of_range(errorMsg);
  } catch (const std::invalid_argument& oor) {
    std::string errorMessage = "Called SubMetabolicNetwork::";
    errorMessage += "productsEnd, but the compound id ";
    errorMessage += std::to_string(id);
    errorMessage += " does not exists in the sub-metabolic network.\n";
    throw std::invalid_argument(errorMessage);
  }
}


/// Get shared_ptr of container of products of given reaction id
template <template <typename> class CompoundTemplate_t, typename Reaction_type,
    typename CompoundProperty, typename ReactionProperty,
    typename MetabolicNetworkProperty>
std::shared_ptr<const hglib::GraphElementContainer<typename SubMetabolicNetwork<
    CompoundTemplate_t, Reaction_type, CompoundProperty, ReactionProperty,
    MetabolicNetworkProperty>::Compound_t*>> SubMetabolicNetwork<
    CompoundTemplate_t, Reaction_type, CompoundProperty, ReactionProperty,
    MetabolicNetworkProperty>::
products(const ReactionIdType& id) const {
  throwExceptionOnNullptrParent();
  try {
    return directedSubHypergraph_->heads(id);
  } catch (const std::out_of_range& oor) {
    std::string errorMsg = "IndexOutOfRangeException in SubMetabolicNetwork::";
    errorMsg += "products: compound index = ";
    errorMsg += std::to_string(id);
    errorMsg += "; min = 0; max = ";
    errorMsg += std::to_string(directedSubHypergraph_->vertexContainerSize());
    errorMsg += '\n';
    throw std::out_of_range(errorMsg);
  } catch (const std::invalid_argument& oor) {
    std::string errorMessage = "Called SubMetabolicNetwork::";
    errorMessage += "products, but the compound id ";
    errorMessage += std::to_string(id);
    errorMessage += " does not exists in the sub-metabolic network.\n";
    throw std::invalid_argument(errorMessage);
  }
}


/// Check if given compound is a product of given reaction.
template <template <typename> class CompoundTemplate_t, typename Reaction_type,
    typename CompoundProperty, typename ReactionProperty,
    typename MetabolicNetworkProperty>
bool SubMetabolicNetwork<CompoundTemplate_t, Reaction_type,
    CompoundProperty, ReactionProperty, MetabolicNetworkProperty>::
isProductOfReaction(const CompoundIdType& compoundId,
                    const ReactionIdType& reactionId) const {
  throwExceptionOnNullptrParent();
  try {
    return directedSubHypergraph_->isHeadVertexOfHyperarc(compoundId,
                                                          reactionId);
  } catch (const std::out_of_range& oor) {
    std::string errorMsg = "IndexOutOfRangeException in SubMetabolicNetwork::";
    errorMsg += "isProductOfReaction: compound index = ";
    errorMsg += std::to_string(compoundId);
    errorMsg += "; min = 0; max = ";
    errorMsg += std::to_string(directedSubHypergraph_->vertexContainerSize());
    errorMsg += "; reaction id = ";
    errorMsg += std::to_string(reactionId);
    errorMsg += "; min = 0; max = ";
    errorMsg += std::to_string(directedSubHypergraph_->hyperarcContainerSize());
    errorMsg += '\n';
    throw std::out_of_range(errorMsg);
  } catch (const std::invalid_argument& oor) {
    std::string errorMessage = "Called SubMetabolicNetwork::";
    errorMessage += "isProductOfReaction, but either the compound id ";
    errorMessage += std::to_string(compoundId);
    errorMessage += " or the reaction id ";
    errorMessage += std::to_string(reactionId);
    errorMessage += " does not exists in the sub-metabolic network.\n";
    throw std::invalid_argument(errorMessage);
  }
}


/// Get product stoichiometry of given compound in given reaction
template <template <typename> class CompoundTemplate_t, typename Reaction_type,
    typename CompoundProperty, typename ReactionProperty,
    typename MetabolicNetworkProperty>
double SubMetabolicNetwork<CompoundTemplate_t, Reaction_type,
    CompoundProperty, ReactionProperty, MetabolicNetworkProperty>::
productStoichiometry(const CompoundIdType& compoundId,
                     const ReactionIdType& reactionId) const {
  throwExceptionOnNullptrParent();
  // * Check first if the compound is a product of given reaction in this sub-
  // metabolic network
  // * Retrieve then the given compound and reaction
  // * Call the function to retrieve the product stoichiometry
  try {
    if (isProductOfReaction(compoundId, reactionId)) {
      const auto& compound = directedSubHypergraph_->vertexById(compoundId);
      if (compound == nullptr) {
        std::string errorMessage = "Error in SubMetabolicNetwork::";
        errorMessage += "productStoichiometry: The compound with";
        errorMessage += " given Id: ";
        errorMessage += std::to_string(compoundId);
        errorMessage += " is not in the metabolic sub-network.\n";
        throw std::invalid_argument(errorMessage);
      }
      const auto &reaction = directedSubHypergraph_->hyperarcById(reactionId);
      if (reaction == nullptr) {
        std::string errorMessage = "Error in SubMetabolicNetwork::";
        errorMessage += "productStoichiometry: The reaction with";
        errorMessage += " given Id: ";
        errorMessage += std::to_string(reactionId);
        errorMessage += " is not in the metabolic sub-network.\n";
        throw std::invalid_argument(errorMessage);
      }
      // Return stoichiometry
      return reaction->productStoichiometry(compound);
    } else {
      std::string errorMessage = "Error in SubMetabolicNetwork::";
      errorMessage += "productStoichiometry: The compound with";
      errorMessage += " given Id: ";
      errorMessage += std::to_string(compoundId);
      errorMessage += " is not a product of the reaction with given Id: ";
      errorMessage += std::to_string(reactionId);
      errorMessage += " in this sub network.\n";
      throw std::invalid_argument(errorMessage);
    }
  } catch (const std::out_of_range& oor) {
    throw oor;
  } catch (const std::invalid_argument& ia) {
    throw ia;
  }
}

// reaction property related
/// Get properties of the reaction with the provided id.
template <template <typename> class CompoundTemplate_t, typename Reaction_type,
    typename CompoundProperty, typename ReactionProperty,
    typename MetabolicNetworkProperty>
const ReactionProperty* SubMetabolicNetwork<CompoundTemplate_t, Reaction_type,
    CompoundProperty, ReactionProperty, MetabolicNetworkProperty>::
getReactionProperties(const ReactionIdType& id) const {
  throwExceptionOnNullptrParent();
  try {
    return directedSubHypergraph_->getHyperarcProperties(id);
  } catch (const std::out_of_range& oor) {
    std::string errorMsg = "IndexOutOfRangeException in SubMetabolicNetwork::";
    errorMsg += "getReactionProperties: reaction index = ";
    errorMsg += std::to_string(id);
    errorMsg += "; min = 0; max = ";
    errorMsg += std::to_string(directedSubHypergraph_->hyperarcContainerSize());
    errorMsg += '\n';
    throw std::out_of_range(errorMsg);
  } catch (const std::invalid_argument& oor) {
    std::string errorMessage = "Called SubMetabolicNetwork::";
    errorMessage += "getReactionProperties, but the reaction id ";
    errorMessage += std::to_string(id);
    errorMessage += " does not exists in the sub-metabolic network.\n";
    throw std::invalid_argument(errorMessage);
  }
}


/// Get properties of the reaction with the provided id.
template <template <typename> class CompoundTemplate_t, typename Reaction_type,
    typename CompoundProperty, typename ReactionProperty,
    typename MetabolicNetworkProperty>
ReactionProperty* SubMetabolicNetwork<CompoundTemplate_t, Reaction_type,
    CompoundProperty, ReactionProperty, MetabolicNetworkProperty>::
getReactionProperties_(const ReactionIdType& id) const {
  throwExceptionOnNullptrParent();
  try {
    return directedSubHypergraph_->getHyperarcProperties_(id);
  } catch (const std::out_of_range& oor) {
    std::string errorMsg = "IndexOutOfRangeException in SubMetabolicNetwork::";
    errorMsg += "getReactionProperties_: reaction index = ";
    errorMsg += std::to_string(id);
    errorMsg += "; min = 0; max = ";
    errorMsg += std::to_string(directedSubHypergraph_->hyperarcContainerSize());
    errorMsg += '\n';
    throw std::out_of_range(errorMsg);
  } catch (const std::invalid_argument& oor) {
    std::string errorMessage = "Called SubMetabolicNetwork::";
    errorMessage += "getReactionProperties_, but the reaction id ";
    errorMessage += std::to_string(id);
    errorMessage += " does not exists in the sub-metabolic network.\n";
    throw std::invalid_argument(errorMessage);
  }
}


// Related to the metabolic network property
/// Get properties of the metabolic network.
template <template <typename> class CompoundTemplate_t, typename Reaction_type,
    typename CompoundProperty, typename ReactionProperty,
    typename MetabolicNetworkProperty>
const MetabolicNetworkProperty* SubMetabolicNetwork<CompoundTemplate_t,
    Reaction_type, CompoundProperty, ReactionProperty,
    MetabolicNetworkProperty>::
getMetabolicNetworkProperties() const {
  throwExceptionOnNullptrParent();
  return subMetabolicNetworkProperty_;
}


/// Get properties of the metabolic network.
template <template <typename> class CompoundTemplate_t, typename Reaction_type,
    typename CompoundProperty, typename ReactionProperty,
    typename MetabolicNetworkProperty>
MetabolicNetworkProperty* SubMetabolicNetwork<CompoundTemplate_t, Reaction_type,
    CompoundProperty, ReactionProperty, MetabolicNetworkProperty>::
getMetabolicNetworkProperties_() const {
  throwExceptionOnNullptrParent();
  return const_cast<MetabolicNetworkProperty*>(getMetabolicNetworkProperties());
}

/* **************************************************************************
 * **************************************************************************
 *                        Protected member functions
 * **************************************************************************
 ***************************************************************************/
/**
 * Get underlying directed sub-hypergraph
 *
 * @return Pointer to DirectedHypergraphInterface
 */
template <template <typename> class CompoundTemplate_t, typename Reaction_type,
    typename CompoundProperty, typename ReactionProperty,
    typename MetabolicNetworkProperty>
typename SubMetabolicNetwork<CompoundTemplate_t, Reaction_type,
    CompoundProperty, ReactionProperty,
    MetabolicNetworkProperty>::DirectedHypergraphInterface* SubMetabolicNetwork<
    CompoundTemplate_t, Reaction_type, CompoundProperty, ReactionProperty,
    MetabolicNetworkProperty>::
getUnderlyingDirectedHypergraph() const {
  throwExceptionOnNullptrParent();
  return directedSubHypergraph_;
}


/** \brief Adapt the content of container whitelistedCompartmentList_ in
   * response to the call resetCompartmentIds in the root network
   */
template <template <typename> class CompoundTemplate_t, typename Reaction_type,
    typename CompoundProperty, typename ReactionProperty,
    typename MetabolicNetworkProperty>
void SubMetabolicNetwork<CompoundTemplate_t, Reaction_type, CompoundProperty,
    ReactionProperty, MetabolicNetworkProperty>::
updateCompartmentContainerUponResetCompartmentIds() {
  throwExceptionOnNullptrParent();
  /*
   * Consider the filtered_vectors of compartments in the root and sub-networks,
   * where X corresponds to a nullptr and a number to the id of the compartment
   * pointed by the entry:
   *
   * network:          |X|1|2|3|X|
   * subnetwork1:      |X|X|2|3|X|
   * subsubnetwork1_1: |X|X|X|3|X|
   * subnetwork2:      |X|1|X|X|X|
   *
   * If the method resetCompartmentIds was called on the network we expect the
   * following:
   *
   * network:          |0|1|2|
   * subnetwork1:      |X|1|2|
   * subsubnetwork1_1: |X|X|2|
   * subnetwork2:      |0|X|X|
   *
   * The method updateCompartmentContainerUponResetCompartmentIds in
   * SubMetabolicNetwork notify its subnetworks about this change,
   * before changing its own filtered_vector of compartments.
   */

  // Notify subnetworks of this change
  this->NotifyObservers(MetabolicNetworkObservableEvent::RESET_COMPARTMENT_IDS);

  if (whitelistedCompartmentList_->empty()) {
    return;
  }
  std::vector<Compartment*> newWhitelistedCompartmentContainer;
  auto it = whitelistedCompartmentList_->data();
  auto end = whitelistedCompartmentList_->data() +
      whitelistedCompartmentList_->size();
  for (; it != end; ++it) {
    const auto& compartment = *it;
    if (compartment != nullptr) {
      if (whitelistedCompartmentList_->filter_out(compartment)) {
        newWhitelistedCompartmentContainer.push_back(nullptr);
      } else {
        newWhitelistedCompartmentContainer.push_back(compartment);
      }
    }
  }

  whitelistedCompartmentList_ = create_filtered_vector<Compartment*>(
      parentMetabolicNetwork_->getRootCompartmentContainerPtr(),
      std::make_unique<WhitelistFilter<Compartment*>>(
          newWhitelistedCompartmentContainer));
}


/**
 * \brief If the dtor of the root network was called we invalidate all parents
 * of its child hierarchy
 */
template <template <typename> class CompoundTemplate_t, typename Reaction_type,
    typename CompoundProperty, typename ReactionProperty,
    typename MetabolicNetworkProperty>
void SubMetabolicNetwork<CompoundTemplate_t, Reaction_type,
    CompoundProperty, ReactionProperty, MetabolicNetworkProperty>::
setParentOfChildsToNull() {
  throwExceptionOnNullptrParent();
  std::unordered_set<MetabolicNetworkObserver*> observers;
  for (auto itEvent = this->observers_.begin();
       itEvent != this->observers_.end(); ++itEvent) {
    for (auto itObserver : itEvent->second) {
      observers.insert(itObserver);
    }
  }
  for (auto& observer : observers) {
    auto subnetwork = dynamic_cast<SubMetabolicNetwork<CompoundTemplate_t,
        Reaction_type, CompoundProperty, ReactionProperty,
        MetabolicNetworkProperty>*>(observer);
    subnetwork->setParentOfChildsToNull();
  }
  parentMetabolicNetwork_ = nullptr;
}


/**\brief Throw an exception if one tries to use a member function on a
   * sub-network whose parent was set to nullptr previously
   */
template <template <typename> class CompoundTemplate_t, typename Reaction_type,
    typename CompoundProperty, typename ReactionProperty,
    typename MetabolicNetworkProperty>
void SubMetabolicNetwork<CompoundTemplate_t, Reaction_type,
    CompoundProperty, ReactionProperty, MetabolicNetworkProperty>::
throwExceptionOnNullptrParent() const {
  if (parentMetabolicNetwork_ == nullptr) {
    throw std::invalid_argument("Previously another network was assigned"
                                    " to the original parent of this"
                                    " sub-network. This sub-network"
                                    " has no parent network anymore and"
                                    " is thus in an invalid state.");
  }
}


/// Return pointer to compartment container in the root metabolic network
template <template <typename> class CompoundTemplate_t, typename Reaction_type,
    typename CompoundProperty, typename ReactionProperty,
    typename MetabolicNetworkProperty>
typename SubMetabolicNetwork<CompoundTemplate_t, Reaction_type,
    CompoundProperty, ReactionProperty,
    MetabolicNetworkProperty>::CompartmentContainerPtr SubMetabolicNetwork<
    CompoundTemplate_t, Reaction_type, CompoundProperty, ReactionProperty,
    MetabolicNetworkProperty>::
getRootCompartmentContainerPtr() const {
  throwExceptionOnNullptrParent();
  return parentMetabolicNetwork_->getRootCompartmentContainerPtr();
}

/// Observer update method
template <template <typename> class CompoundTemplate_t, typename Reaction_type,
    typename CompoundProperty, typename ReactionProperty,
    typename MetabolicNetworkProperty>
void SubMetabolicNetwork<CompoundTemplate_t, Reaction_type,
    CompoundProperty, ReactionProperty,
    MetabolicNetworkProperty>::
Update(const MetabolicNetworkObservable& o, MetabolicNetworkObservableEvent e,
       void* arg) {
  throwExceptionOnNullptrParent();
  if (&o == parentMetabolicNetwork_) {
    switch (e) {
      case MetabolicNetworkObservableEvent::COMPARTMENT_REMOVED: {
        Remove_compartment_args *args =
            static_cast<Remove_compartment_args *>(arg);
        try {
          const auto &compartment = whitelistedCompartmentList_->at(
              args->compartmentId_);
          if (not whitelistedCompartmentList_->filter_out(compartment)) {
            removeCompartment(args->compartmentId_,
                              args->removeImpliedReactions);
          }
        } catch (const std::out_of_range &oor) {
          std::cerr << "IndexOutOfRangeException in"
              " SubMetabolicNetwork::Update: "
              "index = " << args->compartmentId_ << "; min = 0; max = " <<
                    whitelistedCompartmentList_->size() << '\n';
          throw;
        }
        break;
      }
      case MetabolicNetworkObservableEvent::RESET_COMPARTMENT_IDS: {
        updateCompartmentContainerUponResetCompartmentIds();
        break;
      }
    }
  }
}
}  // namespace metnetlib
