/*Copyright (C) <2016-2017>  David Parsons, Martin Wannagat

This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU Affero General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/
//
// Created by Martin Wannagat on 07.11.17.
//

#ifndef METNET_METABOLICNETWORKINTERFACE_H
#define METNET_METABOLICNETWORKINTERFACE_H

#include "hglib/hypergraph/directed/DirectedHypergraphInterface.h"

#include "MetabolicNetworkObservable.h"

namespace metnetlib {

template <template <typename> class CompoundTemplate_t,
    typename Reaction_type,
    typename CompoundProperty,
    typename ReactionProperty,
    typename MetabolicNetworkProperty>
class MetabolicNetworkInterface : public MetabolicNetworkObservable {
  // TODO(mw) enforce that the Reaction_type has certain member variables

  /* **************************************************************************
   * **************************************************************************
   *                    Alias declarations
   * **************************************************************************
   * *************************************************************************/
 public:
  /// Alias for the full compound type
  using Compound_t = CompoundTemplate_t<Reaction_type>;
  /// Alias for conditional range of a container of Compartment ptr
  using CompartmentContainer = MetNetElementContainer<Compartment*>;
  /// Alias for container to store pointers to compartments
  using CompartmentContainerPtr = std::shared_ptr<CompartmentContainer>;
  /// Alias for iterator over a container of compartments
  using compartment_iterator = CompartmentContainer::const_iterator;
  /// Alias for conditional range of a container of Compound_t ptr
  using CompoundContainer = MetNetElementContainer<Compound_t*>;
  /// Alias for iterator over a container of compounds
  using compound_iterator = typename CompoundContainer::const_iterator;
  /// Alias for reaction type
  using Reaction_t = Reaction_type;
  /// Alias for conditional range of a container of Reaction_t ptr
  using ReactionContainer = MetNetElementContainer<Reaction_t*>;
  /// Alias for iterator over a container of reactions
  using reaction_iterator = typename ReactionContainer::const_iterator;
  /// Alias for data structire to provide substarte and product names
  using SubstrateAndProductNames = std::pair<std::vector<std::string>,
                                             std::vector<std::string>>;
  /// Alias for compound property type
  using CompoundProperty_t = CompoundProperty;
  /// Alias for reaction property type
  using ReactionProperty_t = ReactionProperty;
  /// Alias for metabolic network property type
  using MetabolicNetworkProperty_t = MetabolicNetworkProperty;

  /* **************************************************************************
   * **************************************************************************
   *                    Destructor
   * **************************************************************************
   * *************************************************************************/
 public:
  /// Destructor
  virtual ~MetabolicNetworkInterface() {}

  /* **************************************************************************
   * **************************************************************************
   *                        Member functions
   * **************************************************************************
   ***************************************************************************/
  // related to compartment
  /// Add a compartment
  virtual const Compartment* addCompartment(
      const std::string&, double, bool) = 0;
  /// Get pointer to compartment
  virtual const Compartment* compartmentBySbmlId(const std::string&) const = 0;
  /// Get pointer to compartment
  virtual const Compartment* compartmentById(
      const CompartmentIdType&) const = 0;
  /// Get number of compartments
  virtual size_t nbCompartments() const = 0;
  /// Const compartment iterator pointing to begin of the compartments
  virtual compartment_iterator compartmentsBegin() const = 0;
  /// Const compartment iterator pointing to end of the compartments
  virtual compartment_iterator compartmentsEnd() const = 0;
  /// Return a reference to the container of compartments.
  virtual const CompartmentContainer& compartments() const = 0;

  /// Remove a compartment and all its compounds from the network
  virtual void removeCompartment(const CompartmentIdType&, bool) = 0;
  /// Highest assigned compartment id of the network
  virtual size_t compartmentContainerSize() const = 0;

  // related to compounds
  /// \brief Add a compound with a given Sbml Id and several additional
  /// arguments specific to the compound type
  virtual const Compound_t* addCompound(
      const std::string&, const typename Compound_t::SpecificAttributes&) = 0;
  /// Get pointer to compound
  virtual const Compound_t* compoundById(const CompoundIdType&) const = 0;
  /// Get pointer to compound
  virtual const Compound_t* compoundBySbmlId(const std::string&) const = 0;
  /// Get number of compounds
  virtual size_t nbCompounds() const = 0;
  /// Const compound iterator pointing to begin of the compounds
  virtual compound_iterator compoundsBegin() const = 0;
  /// Const compound iterator pointing to end of the compounds
  virtual compound_iterator compoundsEnd() const = 0;
  /// Return a reference to the container of compounds.
  virtual const CompoundContainer& compounds() const = 0;

  /// Get arguments to create the compound
  virtual hglib::ArgumentsToCreateVertex<Compound_t>
  argumentsToCreateCompound(const CompoundIdType&) const = 0;

  /// Remove a compound with the given Id
  virtual void removeCompound(const CompoundIdType&,
                              bool removeImpliedReactions) = 0;

  // consuming reactions of a compound
  /// Check whether the compound with given Id is consumed by a reaction
  virtual bool isConsumed(const CompoundIdType&) const = 0;
  /// Get number of consuming reactions
  virtual size_t nbConsumingReactions(const CompoundIdType&) const = 0;
  /// \brief Const iterator pointing to begin of the consuming reactions of the
  /// compound with given Id
  virtual reaction_iterator
  consumingReactionsBegin(const CompoundIdType&) const = 0;
  /// \brief Const iterator pointing to end of the consuming reactions of the
  /// compound with given Id
  virtual reaction_iterator
  consumingReactionsEnd(const CompoundIdType&) const = 0;
  /// \brief Return a shared_ptr to the container of consuming reactions of the
  /// compound with given Id
  virtual std::shared_ptr<const hglib::GraphElementContainer<Reaction_t*>>
  consumingReactions(const CompoundIdType&) const = 0;

  // producing reactions of a compound
  /// Check whether the compound with given Id is produced by a reaction
  virtual bool isProduced(const CompoundIdType&) const = 0;
  /// Get number of producing reactions
  virtual size_t nbProducingReactions(const CompoundIdType&) const = 0;
  /// \brief Const iterator pointing to begin of the producing reactions of the
  /// compound with given Id
  virtual reaction_iterator
  producingReactionsBegin(const CompoundIdType&) const = 0;
  /// \brief Const iterator pointing to end of the producing reactions of the
  /// compound with given Id
  virtual reaction_iterator
  producingReactionsEnd(const CompoundIdType&) const = 0;
  /// \brief Return a shared_ptr to the container of producing reactions of the
  /// compound with given Id
  virtual std::shared_ptr<const hglib::GraphElementContainer<Reaction_t*>>
  producingReactions(const CompoundIdType&) const = 0;

  // Compound property getters
  /// Get properties of the compound with the provided id.
  virtual const CompoundProperty*
  getCompoundProperties(const CompoundIdType&) const = 0;
  /// Get properties of the compound with the provided id.
  virtual CompoundProperty*
  getCompoundProperties_(const CompoundIdType&) const = 0;

  // related to reactions
  /**
    * \brief Add a reaction with the provided substrates, products, and
    * additional parameters specific to the reaction type to the
    * network
    */
  virtual const Reaction_t* addReaction(
      decltype(hglib::NAME),
      const SubstrateAndProductNames&,
      const typename Reaction_t::SpecificAttributes& attributes) = 0;
  /// Get pointer to reaction
  virtual const Reaction_t* reactionById(const ReactionIdType&) const = 0;
  /// Get pointer to reaction
  virtual const Reaction_t* reactionBySbmlId(const std::string&) const = 0;
  /// Get number of reactions
  virtual size_t nbReactions() const = 0;
  /// Get number of reversible reactions
  virtual size_t nbReversibleReactions() const = 0;
  /// Is the reaction reversible?
  virtual bool isReversible(const ReactionIdType&) const = 0;
  /// Get reverse reaction
  virtual const Reaction_t* reversibleReaction(const ReactionIdType&) const = 0;
  /// Const reaction iterator pointing to begin of the reactions
  virtual reaction_iterator reactionsBegin() const = 0;
  /// Const reaction iterator pointing to end of the reactions
  virtual reaction_iterator reactionsEnd() const = 0;
  /// Return a reference to the container of reactions.
  virtual const ReactionContainer& reactions() const = 0;

  /// Add a default irreversible reaction
  virtual const Reaction_t* addDefaultIrreversibleReaction(
      const std::string&, const std::vector<std::string>&,
      const std::vector<std::string>&) = 0;

  /// Remove a reaction with the given id from the network.
  virtual void removeReaction(const ReactionIdType&, bool) = 0;

  // substrates of a reaction
  /// Check whether the reaction with given Id has substrates
  virtual bool hasSubstrates(const ReactionIdType&) const = 0;
  /// Get number of substrates of the reaction with given Id
  virtual size_t nbSubstrates(const ReactionIdType&) const = 0;
  /// \brief Const compound iterator pointing to the begin of the substrates
  /// of the reaction with the given id
  virtual compound_iterator substratesBegin(const ReactionIdType&) const = 0;
  /// \briefConst compound iterator pointing to the end of the substrates
  /// of the reaction with the given id
  virtual compound_iterator substratesEnd(const ReactionIdType&) const = 0;
  /// Get shared_ptr of container of substrates of given reaction id
  virtual std::shared_ptr<const hglib::GraphElementContainer<Compound_t *>>
  substrates(const ReactionIdType&) const = 0;
  /// Check if given compound is a substrate of given reaction.
  virtual bool isSubstrateOfReaction(const CompoundIdType&,
                                     const ReactionIdType&) const = 0;
  /// Get substrate stoichiometry of given compound in given reaction
  virtual double substrateStoichiometry(const CompoundIdType&,
                                        const ReactionIdType&) const = 0;

  // products of a reaction
  /// Check whether the reaction with given Id has products
  virtual bool hasProducts(const ReactionIdType&) const = 0;
  /// Get number of products of the reaction with given Id
  virtual size_t nbProducts(const ReactionIdType&) const = 0;
  /// \brief Const compound iterator pointing to the begin of the products
  /// of the reaction with the given id
  virtual compound_iterator productsBegin(const ReactionIdType&) const = 0;
  /// \briefConst compound iterator pointing to the end of the products
  /// of the reaction with the given id
  virtual compound_iterator productsEnd(const ReactionIdType&) const = 0;
  /// Get shared_ptr of container of products of given reaction id
  virtual std::shared_ptr<const hglib::GraphElementContainer<Compound_t *>>
  products(const ReactionIdType&) const = 0;
  /// Check if given compound is a product of given reaction.
  virtual bool isProductOfReaction(const CompoundIdType&,
                                   const ReactionIdType&) const = 0;
  /// Get product stoichiometry of given compound in given reaction
  virtual double productStoichiometry(const CompoundIdType&,
                                      const ReactionIdType&) const = 0;


  // reaction property related
  /// Get properties of the reaction with the provided id.
  virtual const ReactionProperty* getReactionProperties(
      const ReactionIdType&) const = 0;
  /// Get properties of the reaction with the provided id.
  virtual ReactionProperty* getReactionProperties_(
      const ReactionIdType&) const = 0;

  // Related to the metabolic network property
  /// Get properties of the metabolic network.
  virtual const MetabolicNetworkProperty* getMetabolicNetworkProperties()
    const = 0;
  /// Get properties of the metabolic network.
  virtual MetabolicNetworkProperty* getMetabolicNetworkProperties_() const = 0;

  /// Get underlying directed (sub-) hypergraph
  virtual hglib::DirectedHypergraphInterface<CompoundTemplate_t, Reaction_type,
    CompoundProperty, ReactionProperty, MetabolicNetworkProperty>*
      getUnderlyingDirectedHypergraph() const = 0;

  /* **************************************************************************
   * **************************************************************************
   *                  Metabolic network hierarchy/Observer pattern
   * **************************************************************************
   * *************************************************************************/
 public:
  /// Return pointer to compartment container in the root metabolic network
  virtual CompartmentContainerPtr getRootCompartmentContainerPtr() const = 0;
};
}  // namespace metnetlib
#endif  // METNET_METABOLICNETWORKINTERFACE_H
