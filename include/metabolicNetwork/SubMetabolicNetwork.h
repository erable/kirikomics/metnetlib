/*Copyright (C) <2016-2017>  David Parsons, Martin Wannagat

This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU Affero General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/
//
// Created by Martin Wannagat on 16.11.17.
//

#ifndef HGLIB_SUBMETABOLICNETWORK_H
#define HGLIB_SUBMETABOLICNETWORK_H

#include "hglib/hypergraph/directed/DirectedSubHypergraph.h"

#include "compartment/Compartment.h"
#include "MetabolicNetworkInterface.h"

namespace metnetlib {

template<template<typename> class CompoundTemplate_t = Compound,
    typename Reaction_type = Reaction,
    typename CompoundProperty = hglib::emptyProperty,
    typename ReactionProperty = hglib::emptyProperty,
    typename MetabolicNetworkProperty = hglib::emptyProperty>
class SubMetabolicNetwork : public MetabolicNetworkInterface<CompoundTemplate_t,
    Reaction_type, CompoundProperty, ReactionProperty,
    MetabolicNetworkProperty>,
                            public MetabolicNetworkObserver {
  template <template <typename> class C_t, typename R_t,
      typename VProp, typename RProp,
      typename MNProp>
  friend class MetabolicNetwork;

  /* **************************************************************************
   * **************************************************************************
   *                    Alias declarations
   * **************************************************************************
   * *************************************************************************/
 public:
  /// Alias for MetabolicNetworkInterface
  using MetNetInterface = MetabolicNetworkInterface<CompoundTemplate_t,
      Reaction_type, CompoundProperty, ReactionProperty,
      MetabolicNetworkProperty>;
  /// Alias for type of underlying directed sub-hypergraph
  using DirectedSubHypergraph_t = hglib::DirectedSubHypergraph<
      CompoundTemplate_t, Reaction_type, CompoundProperty, ReactionProperty,
      MetabolicNetworkProperty>;
  /// Alias for DirectedHypergraphInterface
  using DirectedHypergraphInterface = hglib::DirectedHypergraphInterface<
      CompoundTemplate_t, Reaction_type, CompoundProperty, ReactionProperty,
      MetabolicNetworkProperty>;
  /// Alias for container to store pointers to compartments
  using CompartmentContainerPtr = typename MetabolicNetworkInterface<
      CompoundTemplate_t, Reaction_type, CompoundProperty, ReactionProperty,
      MetabolicNetworkProperty>::CompartmentContainerPtr;
  /// Alias for the full compound type
  using Compound_t = typename MetabolicNetworkInterface<
      CompoundTemplate_t, Reaction_type, CompoundProperty, ReactionProperty,
      MetabolicNetworkProperty>::Compound_t;
  /// Alias for reaction type
  using Reaction_t = typename MetabolicNetworkInterface<
      CompoundTemplate_t, Reaction_type, CompoundProperty, ReactionProperty,
      MetabolicNetworkProperty>::Reaction_t;
  /// Alias for data structire to provide substarte and product names
  using SubstrateAndProductNames = typename MetabolicNetworkInterface<
      CompoundTemplate_t, Reaction_type, CompoundProperty, ReactionProperty,
      MetabolicNetworkProperty>::SubstrateAndProductNames;
  /// Alias for conditional range of a container of Compartment ptr
  using CompartmentContainer = typename MetabolicNetworkInterface<
      CompoundTemplate_t, Reaction_type, CompoundProperty, ReactionProperty,
      MetabolicNetworkProperty>::CompartmentContainer;
  /// Alias for iterator over a container of compartments
  using compartment_iterator = typename MetabolicNetworkInterface<
      CompoundTemplate_t, Reaction_type, CompoundProperty, ReactionProperty,
      MetabolicNetworkProperty>::compartment_iterator;
  /// Alias for conditional range of a container of Compound_t ptr
  using CompoundContainer = typename MetabolicNetworkInterface<
      CompoundTemplate_t, Reaction_type, CompoundProperty, ReactionProperty,
      MetabolicNetworkProperty>::CompoundContainer;
  /// Alias for iterator over a container of compounds
  using compound_iterator = typename MetabolicNetworkInterface<
      CompoundTemplate_t, Reaction_type, CompoundProperty, ReactionProperty,
      MetabolicNetworkProperty>::compound_iterator;
  /// Alias for conditional range of a container of Reaction_t ptr
  using ReactionContainer = typename MetabolicNetworkInterface<
      CompoundTemplate_t, Reaction_type, CompoundProperty, ReactionProperty,
      MetabolicNetworkProperty>::ReactionContainer;
  /// Alias for iterator over a container of reactions
  using reaction_iterator = typename MetabolicNetworkInterface<
      CompoundTemplate_t, Reaction_type, CompoundProperty, ReactionProperty,
      MetabolicNetworkProperty>::reaction_iterator;
  /// Alias for compound property type
  using CompoundProperty_t = typename MetabolicNetworkInterface<
      CompoundTemplate_t, Reaction_type, CompoundProperty, ReactionProperty,
      MetabolicNetworkProperty>::CompoundProperty_t;
  /// Alias for reaction property type
  using ReactionProperty_t = typename MetabolicNetworkInterface<
      CompoundTemplate_t, Reaction_type, CompoundProperty, ReactionProperty,
      MetabolicNetworkProperty>::ReactionProperty_t;
  /// Alias for metabolic network property type
  using MetabolicNetworkProperty_t = typename MetabolicNetworkInterface<
      CompoundTemplate_t, Reaction_type, CompoundProperty, ReactionProperty,
      MetabolicNetworkProperty>::MetabolicNetworkProperty_t;

  /* **************************************************************************
   * **************************************************************************
   *                        Constructor/Destructor (Rule of five)
   * **************************************************************************
   ***************************************************************************/
 public:
  /// Default constructor
  SubMetabolicNetwork() = delete;
  /// Constructor
  SubMetabolicNetwork(MetabolicNetworkInterface<CompoundTemplate_t,
      Reaction_type, CompoundProperty, ReactionProperty,
      MetabolicNetworkProperty>*, const std::unordered_set<CompoundIdType>&,
                      const std::unordered_set<ReactionIdType>&);
  /// Destructor
  ~SubMetabolicNetwork();
  /// Copy constructor
  SubMetabolicNetwork(const SubMetabolicNetwork&);
  /// Move constructor
  SubMetabolicNetwork(SubMetabolicNetwork&&) = default;
  /// Copy assignment
  SubMetabolicNetwork& operator=(const SubMetabolicNetwork&);
  /// Move assignment
  SubMetabolicNetwork& operator=(SubMetabolicNetwork&&) = default;

  /* **************************************************************************
   * **************************************************************************
   *                        Member functions
   * **************************************************************************
   ***************************************************************************/
 public:
  // related to compartment
  /// Add a compartment
  const Compartment* addCompartment(const std::string&, double, bool) override;
  /// Get pointer to compartment
  const Compartment* compartmentBySbmlId(const std::string&) const override;
  /// Get pointer to compartment
  const Compartment* compartmentById(const CompartmentIdType&) const override;
  /// Get number of compartments
  size_t nbCompartments() const override;
  /// Const compartment iterator pointing to begin of the compartments
  compartment_iterator compartmentsBegin() const override;
  /// Const compartment iterator pointing to end of the compartments
  compartment_iterator compartmentsEnd() const override;
  /// Return a reference to the container of compartments.
  const CompartmentContainer& compartments() const override;

  /// Remove a compartment and all its compounds from the network
  void removeCompartment(const CompartmentIdType&,
                         bool removeImpliedReactions = true) override;
  /// Highest assigned compartment id of the network
  size_t compartmentContainerSize() const override;

  // related to compounds
  /// \brief Add a compound with a given Sbml Id and several additional
  /// arguments specific to the compound type
  const Compound_t* addCompound(
      const std::string&,
      const typename Compound_t::SpecificAttributes&) override;
  /// Get pointer to compound
  const Compound_t* compoundById(const CompoundIdType&) const override;
  /// Get pointer to compound
  const Compound_t* compoundBySbmlId(const std::string&) const override;
  /// Get number of compounds
  size_t nbCompounds() const override;
  /// Const compound iterator pointing to begin of the compounds
  compound_iterator compoundsBegin() const override;
  /// Const compound iterator pointing to end of the compounds
  compound_iterator compoundsEnd() const override;
  /// Return a reference to the container of compounds.
  const CompoundContainer& compounds() const override;

  /// Get arguments to create the compound
  hglib::ArgumentsToCreateVertex<Compound_t>
  argumentsToCreateCompound(const CompoundIdType&) const override;

  /// Remove a compound with the given Id
  void removeCompound(const CompoundIdType&,
                      bool removeImpliedReactions = true) override;

  // consuming reactions of a compound
  /// Check whether the compound with given Id is consumed by a reaction
  bool isConsumed(const CompoundIdType&) const override;
  /// Get number of consuming reactions
  size_t nbConsumingReactions(const CompoundIdType&) const override;
  /// \brief Const iterator pointing to begin of the consuming reactions of the
  /// compound with given Id
  reaction_iterator consumingReactionsBegin(
      const CompoundIdType&) const override;
  /// \brief Const iterator pointing to end of the consuming reactions of the
  /// compound with given Id
  reaction_iterator consumingReactionsEnd(const CompoundIdType&) const override;
  /// \brief Return a shared_ptr to the container of consuming reactions of the
  /// compound with given Id
  std::shared_ptr<const hglib::GraphElementContainer<Reaction_t*>>
  consumingReactions(const CompoundIdType&) const override;

  // producing reactions of a compound
  /// Check whether the compound with given Id is produced by a reaction
  bool isProduced(const CompoundIdType&) const override;
  /// Get number of producing reactions
  size_t nbProducingReactions(const CompoundIdType&) const override;
  /// \brief Const iterator pointing to begin of the producing reactions of the
  /// compound with given Id
  reaction_iterator producingReactionsBegin(
      const CompoundIdType&) const override;
  /// \brief Const iterator pointing to end of the producing reactions of the
  /// compound with given Id
  reaction_iterator producingReactionsEnd(const CompoundIdType&) const override;
  /// \brief Return a shared_ptr to the container of producing reactions of the
  /// compound with given Id
  std::shared_ptr<const hglib::GraphElementContainer<Reaction_t*>>
  producingReactions(const CompoundIdType&) const override;

  // Compound property getters
  /// Get properties of the compound with the provided id.
  const CompoundProperty* getCompoundProperties(
      const CompoundIdType&) const override;
  /// Get properties of the compound with the provided id.
  CompoundProperty* getCompoundProperties_(
      const CompoundIdType&) const override;

  // related to reactions
  /**
   * \brief Add a reaction with the provided substrates, products, and
   * additional parameters specific to the reaction type to the
   * network
   */
  const Reaction_t* addReaction(
      decltype(hglib::NAME),
      const SubstrateAndProductNames&,
      const typename Reaction_t::SpecificAttributes& attributes) override;
  /// Get pointer to reaction
  const Reaction_t* reactionById(const ReactionIdType&) const override;
  /// Get pointer to reaction
  const Reaction_t* reactionBySbmlId(const std::string&) const override;
  /// Get number of reactions
  size_t nbReactions() const override;
  /// Get number of reversible reactions
  size_t nbReversibleReactions() const override;
  /// Is the reaction reversible?
  bool isReversible(const ReactionIdType&) const override;
  /// Get reverse reaction
  const Reaction_t* reversibleReaction(const ReactionIdType&) const override;
  /// Const reaction iterator pointing to begin of the reactions
  reaction_iterator reactionsBegin() const override;
  /// Const reaction iterator pointing to end of the reactions
  reaction_iterator reactionsEnd() const override;
  /// Return a reference to the container of reactions.
  const ReactionContainer& reactions() const override;

  /// Add a default irreversible reaction
  const Reaction_t* addDefaultIrreversibleReaction(
      const std::string&, const std::vector<std::string>&,
      const std::vector<std::string>&) override;

  /// Remove a reaction with the given id from the network.
  void removeReaction(const ReactionIdType&,
                      bool removeReverseReaction = false) override;

  // substrates of a reaction
  /// Check whether the reaction with given Id has substrates
  bool hasSubstrates(const ReactionIdType&) const override;
  /// Get number of substrates of the reaction with given Id
  size_t nbSubstrates(const ReactionIdType&) const override;
  /// \brief Const compound iterator pointing to the begin of the substrates
  /// of the reaction with the given id
  compound_iterator substratesBegin(const ReactionIdType&) const override;
  /// \briefConst compound iterator pointing to the end of the substrates
  /// of the reaction with the given id
  compound_iterator substratesEnd(const ReactionIdType&) const override;
  /// Get shared_ptr of container of substrates of given reaction id
  std::shared_ptr<const hglib::GraphElementContainer<Compound_t *>>
  substrates(const ReactionIdType&) const override;
  /// Check if given compound is a substrate of given reaction.
  bool isSubstrateOfReaction(const CompoundIdType&,
                             const ReactionIdType&) const override;
  /// Get substrate stoichiometry of given compound in given reaction
  double substrateStoichiometry(const CompoundIdType&,
                                const ReactionIdType&) const override;

  // products of a reaction
  /// Check whether the reaction with given Id has products
  bool hasProducts(const ReactionIdType&) const override;
  /// Get number of products of the reaction with given Id
  size_t nbProducts(const ReactionIdType&) const override;
  /// \brief Const compound iterator pointing to the begin of the products
  /// of the reaction with the given id
  compound_iterator productsBegin(const ReactionIdType&) const override;
  /// \briefConst compound iterator pointing to the end of the products
  /// of the reaction with the given id
  compound_iterator productsEnd(const ReactionIdType&) const override;
  /// Get shared_ptr of container of products of given reaction id
  std::shared_ptr<const hglib::GraphElementContainer<Compound_t *>>
  products(const ReactionIdType&) const override;
  /// Check if given compound is a product of given reaction.
  bool isProductOfReaction(const CompoundIdType&,
                           const ReactionIdType&) const override;
  /// Get product stoichiometry of given compound in given reaction
  double productStoichiometry(const CompoundIdType&,
                              const ReactionIdType&) const override;

  // reaction property related
  /// Get properties of the reaction with the provided id.
  const ReactionProperty* getReactionProperties(
      const ReactionIdType&) const override;
  /// Get properties of the reaction with the provided id.
  ReactionProperty* getReactionProperties_(
      const ReactionIdType&) const override;

  // Related to the metabolic network property
  /// Get properties of the metabolic network.
  const MetabolicNetworkProperty* getMetabolicNetworkProperties()
  const override;
  /// Get properties of the metabolic network.
  MetabolicNetworkProperty* getMetabolicNetworkProperties_() const override;

  /* **************************************************************************
   * **************************************************************************
   *                        Protected member functions
   * **************************************************************************
   ***************************************************************************/
 protected:
  /// Get underlying directed sub-hypergraph
  DirectedHypergraphInterface* getUnderlyingDirectedHypergraph() const override;
  /** \brief Adapt the content of container whitelistedCompartmentList_ in
   * response to the call resetCompartmentIds in the root network
   */
  void updateCompartmentContainerUponResetCompartmentIds();
  /** \brief If the dtor of the root network was called we invalidate all
   * parents of its child hierarchy
   */
  void setParentOfChildsToNull();
  /**\brief Throw an exception if one tries to use a member function on a
   * sub-network whose parent was set to nullptr previously
   */
  void throwExceptionOnNullptrParent() const;

  /* **************************************************************************
   * **************************************************************************
   *                  Metabolic network hierarchy/Observer pattern
   * **************************************************************************
   * *************************************************************************/
 protected:
  /// Return pointer to compartment container in the root metabolic network
  CompartmentContainerPtr getRootCompartmentContainerPtr() const override;
  /// Observer update method
  void Update(const MetabolicNetworkObservable& o,
              MetabolicNetworkObservableEvent e, void* arg) override;

  /* **************************************************************************
   * **************************************************************************
   *                        Member variables
   * **************************************************************************
   ***************************************************************************/
 protected:
  /// Underlying directed sub-hypergraph
  DirectedSubHypergraph_t* directedSubHypergraph_;
  /// Parent metabolic network
  MetNetInterface* parentMetabolicNetwork_;
  /// Container of whitelisted compartments
  CompartmentContainerPtr whitelistedCompartmentList_;
  /// Metabolic network property of the sub-metabolic network
  MetabolicNetworkProperty* subMetabolicNetworkProperty_;
};
}  // namespace metnetlib

#include "SubMetabolicNetwork.hpp"
#endif  // HGLIB_SUBMETABOLICNETWORK_H
